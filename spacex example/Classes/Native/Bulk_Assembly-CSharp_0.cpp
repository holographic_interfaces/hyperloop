﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// Camer
struct Camer_t64873628;
// UnityEngine.Renderer
struct Renderer_t1092684080;
// System.Object
struct Il2CppObject;
// CameraRotation
struct CameraRotation_t2040860131;
// UnityEngine.Camera
struct Camera_t3533968274;
// canvas
struct canvas_t2927261016;
// liveVideo
struct liveVideo_t1000351471;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// liveVideo/<Start>c__Iterator0
struct U3CStartU3Ec__Iterator0_t4138896567;
// podMovement
struct podMovement_t4165861908;
// transparentWindow
struct transparentWindow_t2211602402;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array2840145358.h"
#include "AssemblyU2DCSharp_U3CModuleU3E86524790.h"
#include "AssemblyU2DCSharp_U3CModuleU3E86524790MethodDeclarations.h"
#include "AssemblyU2DCSharp_Camer64873628.h"
#include "AssemblyU2DCSharp_Camer64873628MethodDeclarations.h"
#include "mscorlib_System_Void2779279689.h"
#include "UnityEngine_UnityEngine_MonoBehaviour3012272455MethodDeclarations.h"
#include "UnityEngine_UnityEngine_WebCamTexture3635181805MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Component2126946602MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Renderer1092684080MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Material1886596500MethodDeclarations.h"
#include "UnityEngine_UnityEngine_WebCamTexture3635181805.h"
#include "UnityEngine_UnityEngine_Renderer1092684080.h"
#include "UnityEngine_UnityEngine_Component2126946602.h"
#include "UnityEngine_UnityEngine_Material1886596500.h"
#include "UnityEngine_UnityEngine_Texture1769722184.h"
#include "AssemblyU2DCSharp_CameraRotation2040860131.h"
#include "AssemblyU2DCSharp_CameraRotation2040860131MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Input1593691127MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Quaternion1891715979MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Transform284553113MethodDeclarations.h"
#include "mscorlib_System_Single958209021.h"
#include "UnityEngine_UnityEngine_Camera3533968274.h"
#include "mscorlib_System_String968488902.h"
#include "UnityEngine_UnityEngine_Transform284553113.h"
#include "UnityEngine_UnityEngine_Quaternion1891715979.h"
#include "AssemblyU2DCSharp_canvas2927261016.h"
#include "AssemblyU2DCSharp_canvas2927261016MethodDeclarations.h"
#include "AssemblyU2DCSharp_liveVideo1000351471.h"
#include "AssemblyU2DCSharp_liveVideo1000351471MethodDeclarations.h"
#include "AssemblyU2DCSharp_liveVideo_U3CStartU3Ec__Iterator4138896567MethodDeclarations.h"
#include "AssemblyU2DCSharp_liveVideo_U3CStartU3Ec__Iterator4138896567.h"
#include "mscorlib_System_Object837106420MethodDeclarations.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Boolean211005341.h"
#include "UnityEngine_UnityEngine_Application450040189MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_RawImage3831555132MethodDeclarations.h"
#include "mscorlib_System_UInt32985925326.h"
#include "mscorlib_System_Int322847414787.h"
#include "UnityEngine_UnityEngine_AsyncOperation3374395064.h"
#include "UnityEngine_UnityEngine_UserAuthorization1561365147.h"
#include "UnityEngine_UI_UnityEngine_UI_RawImage3831555132.h"
#include "UnityEngine_UI_UnityEngine_UI_Graphic933884113MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_Graphic933884113.h"
#include "mscorlib_System_NotSupportedException1374155497MethodDeclarations.h"
#include "mscorlib_System_NotSupportedException1374155497.h"
#include "AssemblyU2DCSharp_podMovement4165861908.h"
#include "AssemblyU2DCSharp_podMovement4165861908MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Time1525492538MethodDeclarations.h"
#include "AssemblyU2DCSharp_transparentWindow2211602402.h"
#include "AssemblyU2DCSharp_transparentWindow2211602402MethodDeclarations.h"

// !!0 UnityEngine.Component::GetComponent<System.Object>()
extern "C"  Il2CppObject * Component_GetComponent_TisIl2CppObject_m267839954_gshared (Component_t2126946602 * __this, const MethodInfo* method);
#define Component_GetComponent_TisIl2CppObject_m267839954(__this, method) ((  Il2CppObject * (*) (Component_t2126946602 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m267839954_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Renderer>()
#define Component_GetComponent_TisRenderer_t1092684080_m500377675(__this, method) ((  Renderer_t1092684080 * (*) (Component_t2126946602 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m267839954_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponentInChildren<System.Object>()
extern "C"  Il2CppObject * Component_GetComponentInChildren_TisIl2CppObject_m900797242_gshared (Component_t2126946602 * __this, const MethodInfo* method);
#define Component_GetComponentInChildren_TisIl2CppObject_m900797242(__this, method) ((  Il2CppObject * (*) (Component_t2126946602 *, const MethodInfo*))Component_GetComponentInChildren_TisIl2CppObject_m900797242_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponentInChildren<UnityEngine.Camera>()
#define Component_GetComponentInChildren_TisCamera_t3533968274_m140979021(__this, method) ((  Camera_t3533968274 * (*) (Component_t2126946602 *, const MethodInfo*))Component_GetComponentInChildren_TisIl2CppObject_m900797242_gshared)(__this, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Camer::.ctor()
extern "C"  void Camer__ctor_m1916359759 (Camer_t64873628 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Camer::Start()
extern TypeInfo* WebCamTexture_t3635181805_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisRenderer_t1092684080_m500377675_MethodInfo_var;
extern const uint32_t Camer_Start_m863497551_MetadataUsageId;
extern "C"  void Camer_Start_m863497551 (Camer_t64873628 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Camer_Start_m863497551_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		WebCamTexture_t3635181805 * L_0 = (WebCamTexture_t3635181805 *)il2cpp_codegen_object_new(WebCamTexture_t3635181805_il2cpp_TypeInfo_var);
		WebCamTexture__ctor_m755637529(L_0, /*hidden argument*/NULL);
		__this->set_back_2(L_0);
		Renderer_t1092684080 * L_1 = Component_GetComponent_TisRenderer_t1092684080_m500377675(__this, /*hidden argument*/Component_GetComponent_TisRenderer_t1092684080_m500377675_MethodInfo_var);
		NullCheck(L_1);
		Material_t1886596500 * L_2 = Renderer_get_material_m2720864603(L_1, /*hidden argument*/NULL);
		WebCamTexture_t3635181805 * L_3 = __this->get_back_2();
		NullCheck(L_2);
		Material_set_mainTexture_m3116438437(L_2, L_3, /*hidden argument*/NULL);
		WebCamTexture_t3635181805 * L_4 = __this->get_back_2();
		NullCheck(L_4);
		WebCamTexture_Play_m2252445503(L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Camer::Update()
extern "C"  void Camer_Update_m1004472478 (Camer_t64873628 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void CameraRotation::.ctor()
extern "C"  void CameraRotation__ctor_m1307487768 (CameraRotation_t2040860131 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CameraRotation::Start()
extern "C"  void CameraRotation_Start_m254625560 (CameraRotation_t2040860131 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void CameraRotation::Update()
extern TypeInfo* Input_t1593691127_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponentInChildren_TisCamera_t3533968274_m140979021_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2907718525;
extern Il2CppCodeGenString* _stringLiteral2907718526;
extern const uint32_t CameraRotation_Update_m3604277237_MetadataUsageId;
extern "C"  void CameraRotation_Update_m3604277237 (CameraRotation_t2040860131 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CameraRotation_Update_m3604277237_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	Camera_t3533968274 * V_3 = NULL;
	{
		V_0 = (5.0f);
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1593691127_il2cpp_TypeInfo_var);
		float L_0 = Input_GetAxis_m2027668530(NULL /*static, unused*/, _stringLiteral2907718525, /*hidden argument*/NULL);
		float L_1 = V_0;
		V_1 = ((float)((float)L_0*(float)L_1));
		float L_2 = Input_GetAxis_m2027668530(NULL /*static, unused*/, _stringLiteral2907718526, /*hidden argument*/NULL);
		float L_3 = V_0;
		V_2 = ((float)((float)L_2*(float)L_3));
		Transform_t284553113 * L_4 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		float L_5 = V_1;
		Quaternion_t1891715979  L_6 = Quaternion_Euler_m1204688217(NULL /*static, unused*/, (0.0f), L_5, (0.0f), /*hidden argument*/NULL);
		Transform_t284553113 * L_7 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_7);
		Quaternion_t1891715979  L_8 = Transform_get_localRotation_m3343229381(L_7, /*hidden argument*/NULL);
		Quaternion_t1891715979  L_9 = Quaternion_op_Multiply_m3077481361(NULL /*static, unused*/, L_6, L_8, /*hidden argument*/NULL);
		NullCheck(L_4);
		Transform_set_localRotation_m3719981474(L_4, L_9, /*hidden argument*/NULL);
		Camera_t3533968274 * L_10 = Component_GetComponentInChildren_TisCamera_t3533968274_m140979021(__this, /*hidden argument*/Component_GetComponentInChildren_TisCamera_t3533968274_m140979021_MethodInfo_var);
		V_3 = L_10;
		Camera_t3533968274 * L_11 = V_3;
		NullCheck(L_11);
		Transform_t284553113 * L_12 = Component_get_transform_m4257140443(L_11, /*hidden argument*/NULL);
		float L_13 = V_2;
		Quaternion_t1891715979  L_14 = Quaternion_Euler_m1204688217(NULL /*static, unused*/, ((-L_13)), (0.0f), (0.0f), /*hidden argument*/NULL);
		Camera_t3533968274 * L_15 = V_3;
		NullCheck(L_15);
		Transform_t284553113 * L_16 = Component_get_transform_m4257140443(L_15, /*hidden argument*/NULL);
		NullCheck(L_16);
		Quaternion_t1891715979  L_17 = Transform_get_localRotation_m3343229381(L_16, /*hidden argument*/NULL);
		Quaternion_t1891715979  L_18 = Quaternion_op_Multiply_m3077481361(NULL /*static, unused*/, L_14, L_17, /*hidden argument*/NULL);
		NullCheck(L_12);
		Transform_set_localRotation_m3719981474(L_12, L_18, /*hidden argument*/NULL);
		return;
	}
}
// System.Void canvas::.ctor()
extern "C"  void canvas__ctor_m3506401859 (canvas_t2927261016 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void canvas::Start()
extern "C"  void canvas_Start_m2453539651 (canvas_t2927261016 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void canvas::Update()
extern "C"  void canvas_Update_m3051137322 (canvas_t2927261016 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void liveVideo::.ctor()
extern "C"  void liveVideo__ctor_m1289112412 (liveVideo_t1000351471 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator liveVideo::Start()
extern TypeInfo* U3CStartU3Ec__Iterator0_t4138896567_il2cpp_TypeInfo_var;
extern const uint32_t liveVideo_Start_m623468708_MetadataUsageId;
extern "C"  Il2CppObject * liveVideo_Start_m623468708 (liveVideo_t1000351471 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (liveVideo_Start_m623468708_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CStartU3Ec__Iterator0_t4138896567 * V_0 = NULL;
	{
		U3CStartU3Ec__Iterator0_t4138896567 * L_0 = (U3CStartU3Ec__Iterator0_t4138896567 *)il2cpp_codegen_object_new(U3CStartU3Ec__Iterator0_t4138896567_il2cpp_TypeInfo_var);
		U3CStartU3Ec__Iterator0__ctor_m3533646260(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CStartU3Ec__Iterator0_t4138896567 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_3(__this);
		U3CStartU3Ec__Iterator0_t4138896567 * L_2 = V_0;
		return L_2;
	}
}
// System.Void liveVideo::Update()
extern "C"  void liveVideo_Update_m3034641201 (liveVideo_t1000351471 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void liveVideo/<Start>c__Iterator0::.ctor()
extern "C"  void U3CStartU3Ec__Iterator0__ctor_m3533646260 (U3CStartU3Ec__Iterator0_t4138896567 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object liveVideo/<Start>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CStartU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3806941406 (U3CStartU3Ec__Iterator0_t4138896567 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_2();
		return L_0;
	}
}
// System.Object liveVideo/<Start>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CStartU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m2481246322 (U3CStartU3Ec__Iterator0_t4138896567 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_2();
		return L_0;
	}
}
// System.Boolean liveVideo/<Start>c__Iterator0::MoveNext()
extern TypeInfo* WebCamTexture_t3635181805_il2cpp_TypeInfo_var;
extern const uint32_t U3CStartU3Ec__Iterator0_MoveNext_m786557632_MetadataUsageId;
extern "C"  bool U3CStartU3Ec__Iterator0_MoveNext_m786557632 (U3CStartU3Ec__Iterator0_t4138896567 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CStartU3Ec__Iterator0_MoveNext_m786557632_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = __this->get_U24PC_1();
		V_0 = L_0;
		__this->set_U24PC_1((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0039;
		}
	}
	{
		goto IL_0097;
	}

IL_0021:
	{
		AsyncOperation_t3374395064 * L_2 = Application_RequestUserAuthorization_m2737835758(NULL /*static, unused*/, 3, /*hidden argument*/NULL);
		__this->set_U24current_2(L_2);
		__this->set_U24PC_1(1);
		goto IL_0099;
	}

IL_0039:
	{
		bool L_3 = Application_HasUserAuthorization_m2306284210(NULL /*static, unused*/, 3, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0090;
		}
	}
	{
		WebCamTexture_t3635181805 * L_4 = (WebCamTexture_t3635181805 *)il2cpp_codegen_object_new(WebCamTexture_t3635181805_il2cpp_TypeInfo_var);
		WebCamTexture__ctor_m755637529(L_4, /*hidden argument*/NULL);
		__this->set_U3CwebcamTextureU3E__0_0(L_4);
		liveVideo_t1000351471 * L_5 = __this->get_U3CU3Ef__this_3();
		NullCheck(L_5);
		RawImage_t3831555132 * L_6 = L_5->get_rawimage_2();
		WebCamTexture_t3635181805 * L_7 = __this->get_U3CwebcamTextureU3E__0_0();
		NullCheck(L_6);
		RawImage_set_texture_m153141914(L_6, L_7, /*hidden argument*/NULL);
		liveVideo_t1000351471 * L_8 = __this->get_U3CU3Ef__this_3();
		NullCheck(L_8);
		RawImage_t3831555132 * L_9 = L_8->get_rawimage_2();
		NullCheck(L_9);
		Material_t1886596500 * L_10 = VirtFuncInvoker0< Material_t1886596500 * >::Invoke(26 /* UnityEngine.Material UnityEngine.UI.Graphic::get_material() */, L_9);
		WebCamTexture_t3635181805 * L_11 = __this->get_U3CwebcamTextureU3E__0_0();
		NullCheck(L_10);
		Material_set_mainTexture_m3116438437(L_10, L_11, /*hidden argument*/NULL);
		WebCamTexture_t3635181805 * L_12 = __this->get_U3CwebcamTextureU3E__0_0();
		NullCheck(L_12);
		WebCamTexture_Play_m2252445503(L_12, /*hidden argument*/NULL);
		goto IL_0090;
	}

IL_0090:
	{
		__this->set_U24PC_1((-1));
	}

IL_0097:
	{
		return (bool)0;
	}

IL_0099:
	{
		return (bool)1;
	}
	// Dead block : IL_009b: ldloc.1
}
// System.Void liveVideo/<Start>c__Iterator0::Dispose()
extern "C"  void U3CStartU3Ec__Iterator0_Dispose_m2708175089 (U3CStartU3Ec__Iterator0_t4138896567 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_1((-1));
		return;
	}
}
// System.Void liveVideo/<Start>c__Iterator0::Reset()
extern TypeInfo* NotSupportedException_t1374155497_il2cpp_TypeInfo_var;
extern const uint32_t U3CStartU3Ec__Iterator0_Reset_m1180079201_MetadataUsageId;
extern "C"  void U3CStartU3Ec__Iterator0_Reset_m1180079201 (U3CStartU3Ec__Iterator0_t4138896567 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CStartU3Ec__Iterator0_Reset_m1180079201_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1374155497 * L_0 = (NotSupportedException_t1374155497 *)il2cpp_codegen_object_new(NotSupportedException_t1374155497_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void podMovement::.ctor()
extern "C"  void podMovement__ctor_m2067566551 (podMovement_t4165861908 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void podMovement::Start()
extern "C"  void podMovement_Start_m1014704343 (podMovement_t4165861908 * __this, const MethodInfo* method)
{
	{
		__this->set_maxSpeed_3((80.0f));
		__this->set_acceleration_2((0.0f));
		return;
	}
}
// System.Void podMovement::Update()
extern TypeInfo* Input_t1593691127_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2375469974;
extern const uint32_t podMovement_Update_m1396915734_MetadataUsageId;
extern "C"  void podMovement_Update_m1396915734 (podMovement_t4165861908 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (podMovement_Update_m1396915734_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Transform_t284553113 * L_0 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		float L_1 = __this->get_acceleration_2();
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1593691127_il2cpp_TypeInfo_var);
		float L_2 = Input_GetAxis_m2027668530(NULL /*static, unused*/, _stringLiteral2375469974, /*hidden argument*/NULL);
		float L_3 = Time_get_deltaTime_m2741110510(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_Translate_m2900276092(L_0, (0.0f), (0.0f), ((float)((float)((float)((float)L_1*(float)L_2))*(float)L_3)), /*hidden argument*/NULL);
		float L_4 = __this->get_acceleration_2();
		float L_5 = __this->get_maxSpeed_3();
		if ((!(((float)L_4) < ((float)L_5))))
		{
			goto IL_004f;
		}
	}
	{
		float L_6 = __this->get_acceleration_2();
		__this->set_acceleration_2(((float)((float)L_6+(float)(1.0f))));
	}

IL_004f:
	{
		return;
	}
}
// System.Void transparentWindow::.ctor()
extern "C"  void transparentWindow__ctor_m705825737 (transparentWindow_t2211602402 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void transparentWindow::Start()
extern "C"  void transparentWindow_Start_m3947930825 (transparentWindow_t2211602402 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void transparentWindow::Update()
extern "C"  void transparentWindow_Update_m2132623460 (transparentWindow_t2211602402 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
