﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// engine
struct engine_t2996304450;
// landing
struct landing_t4242815511;
// UnityEngine.Collision
struct Collision_t1119538015;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array2840145358.h"
#include "AssemblyU2DUnityScript_U3CModuleU3E86524790.h"
#include "AssemblyU2DUnityScript_U3CModuleU3E86524790MethodDeclarations.h"
#include "AssemblyU2DUnityScript_engine2996304450.h"
#include "AssemblyU2DUnityScript_engine2996304450MethodDeclarations.h"
#include "mscorlib_System_Void2779279689.h"
#include "UnityEngine_UnityEngine_MonoBehaviour3012272455MethodDeclarations.h"
#include "mscorlib_System_Single958209021.h"
#include "UnityEngine_UnityEngine_Input1593691127MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Component2126946602MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector33525329789MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Time1525492538MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Transform284553113MethodDeclarations.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_Boolean211005341.h"
#include "UnityEngine_UnityEngine_Transform284553113.h"
#include "UnityEngine_UnityEngine_Vector33525329789.h"
#include "AssemblyU2DUnityScript_landing4242815511.h"
#include "AssemblyU2DUnityScript_landing4242815511MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Collision1119538015.h"
#include "UnityEngine_UnityEngine_Debug1588791936MethodDeclarations.h"
#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void engine::.ctor()
extern "C"  void engine__ctor_m3753848910 (engine_t2996304450 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		__this->set_rotateSpeed_2((((float)((float)2))));
		return;
	}
}
// System.Void engine::Update()
extern TypeInfo* Input_t1593691127_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral119;
extern Il2CppCodeGenString* _stringLiteral97;
extern Il2CppCodeGenString* _stringLiteral100;
extern const uint32_t engine_Update_m2132061311_MetadataUsageId;
extern "C"  void engine_Update_m2132061311 (engine_t2996304450 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (engine_Update_m2132061311_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1593691127_il2cpp_TypeInfo_var);
		bool L_0 = Input_GetKey_m3403617450(NULL /*static, unused*/, _stringLiteral119, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_002b;
		}
	}
	{
		Transform_t284553113 * L_1 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		Vector3_t3525329789  L_2 = Vector3_get_forward_m1039372701(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_3 = __this->get_rotateSpeed_2();
		float L_4 = Time_get_deltaTime_m2741110510(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_1);
		Transform_Rotate_m4090005356(L_1, L_2, ((float)((float)L_3*(float)L_4)), /*hidden argument*/NULL);
	}

IL_002b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1593691127_il2cpp_TypeInfo_var);
		bool L_5 = Input_GetKey_m3403617450(NULL /*static, unused*/, _stringLiteral97, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0056;
		}
	}
	{
		Transform_t284553113 * L_6 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		Vector3_t3525329789  L_7 = Vector3_get_back_m1326515313(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_8 = __this->get_rotateSpeed_2();
		float L_9 = Time_get_deltaTime_m2741110510(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_6);
		Transform_Rotate_m4090005356(L_6, L_7, ((float)((float)L_8*(float)L_9)), /*hidden argument*/NULL);
	}

IL_0056:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1593691127_il2cpp_TypeInfo_var);
		bool L_10 = Input_GetKey_m3403617450(NULL /*static, unused*/, _stringLiteral100, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0081;
		}
	}
	{
		Transform_t284553113 * L_11 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		Vector3_t3525329789  L_12 = Vector3_get_right_m4015137012(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_13 = __this->get_rotateSpeed_2();
		float L_14 = Time_get_deltaTime_m2741110510(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_11);
		Transform_Rotate_m4090005356(L_11, L_12, ((float)((float)L_13*(float)L_14)), /*hidden argument*/NULL);
	}

IL_0081:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1593691127_il2cpp_TypeInfo_var);
		bool L_15 = Input_GetKey_m3403617450(NULL /*static, unused*/, _stringLiteral97, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_00ac;
		}
	}
	{
		Transform_t284553113 * L_16 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		Vector3_t3525329789  L_17 = Vector3_get_left_m1616598929(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_18 = __this->get_rotateSpeed_2();
		float L_19 = Time_get_deltaTime_m2741110510(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_16);
		Transform_Rotate_m4090005356(L_16, L_17, ((float)((float)L_18*(float)L_19)), /*hidden argument*/NULL);
	}

IL_00ac:
	{
		return;
	}
}
// System.Void engine::Main()
extern "C"  void engine_Main_m1006417583 (engine_t2996304450 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void landing::.ctor()
extern "C"  void landing__ctor_m203651615 (landing_t4242815511 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void landing::OnCollisionEnter(UnityEngine.Collision)
extern TypeInfo* Debug_t1588791936_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2611552355;
extern const uint32_t landing_OnCollisionEnter_m2428764141_MetadataUsageId;
extern "C"  void landing_OnCollisionEnter_m2428764141 (landing_t4242815511 * __this, Collision_t1119538015 * ___col, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (landing_OnCollisionEnter_m2428764141_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_Log_m1731103628(NULL /*static, unused*/, _stringLiteral2611552355, /*hidden argument*/NULL);
		return;
	}
}
// System.Void landing::Main()
extern "C"  void landing_Main_m1307537086 (landing_t4242815511 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
