﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.WebCamTexture
struct WebCamTexture_t3635181805;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Camer
struct  Camer_t64873628  : public MonoBehaviour_t3012272455
{
public:
	// UnityEngine.WebCamTexture Camer::back
	WebCamTexture_t3635181805 * ___back_2;

public:
	inline static int32_t get_offset_of_back_2() { return static_cast<int32_t>(offsetof(Camer_t64873628, ___back_2)); }
	inline WebCamTexture_t3635181805 * get_back_2() const { return ___back_2; }
	inline WebCamTexture_t3635181805 ** get_address_of_back_2() { return &___back_2; }
	inline void set_back_2(WebCamTexture_t3635181805 * value)
	{
		___back_2 = value;
		Il2CppCodeGenWriteBarrier(&___back_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
