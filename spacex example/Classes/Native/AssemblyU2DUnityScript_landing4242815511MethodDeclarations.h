﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// landing
struct landing_t4242815511;
// UnityEngine.Collision
struct Collision_t1119538015;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Collision1119538015.h"

// System.Void landing::.ctor()
extern "C"  void landing__ctor_m203651615 (landing_t4242815511 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void landing::OnCollisionEnter(UnityEngine.Collision)
extern "C"  void landing_OnCollisionEnter_m2428764141 (landing_t4242815511 * __this, Collision_t1119538015 * ___col, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void landing::Main()
extern "C"  void landing_Main_m1307537086 (landing_t4242815511 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
