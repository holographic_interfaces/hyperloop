﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// transparentWindow
struct transparentWindow_t2211602402;

#include "codegen/il2cpp-codegen.h"

// System.Void transparentWindow::.ctor()
extern "C"  void transparentWindow__ctor_m705825737 (transparentWindow_t2211602402 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void transparentWindow::Start()
extern "C"  void transparentWindow_Start_m3947930825 (transparentWindow_t2211602402 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void transparentWindow::Update()
extern "C"  void transparentWindow_Update_m2132623460 (transparentWindow_t2211602402 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
