﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// canvas
struct canvas_t2927261016;

#include "codegen/il2cpp-codegen.h"

// System.Void canvas::.ctor()
extern "C"  void canvas__ctor_m3506401859 (canvas_t2927261016 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void canvas::Start()
extern "C"  void canvas_Start_m2453539651 (canvas_t2927261016 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void canvas::Update()
extern "C"  void canvas_Update_m3051137322 (canvas_t2927261016 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
