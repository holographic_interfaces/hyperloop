﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// liveVideo/<Start>c__Iterator0
struct U3CStartU3Ec__Iterator0_t4138896567;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void liveVideo/<Start>c__Iterator0::.ctor()
extern "C"  void U3CStartU3Ec__Iterator0__ctor_m3533646260 (U3CStartU3Ec__Iterator0_t4138896567 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object liveVideo/<Start>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CStartU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3806941406 (U3CStartU3Ec__Iterator0_t4138896567 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object liveVideo/<Start>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CStartU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m2481246322 (U3CStartU3Ec__Iterator0_t4138896567 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean liveVideo/<Start>c__Iterator0::MoveNext()
extern "C"  bool U3CStartU3Ec__Iterator0_MoveNext_m786557632 (U3CStartU3Ec__Iterator0_t4138896567 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void liveVideo/<Start>c__Iterator0::Dispose()
extern "C"  void U3CStartU3Ec__Iterator0_Dispose_m2708175089 (U3CStartU3Ec__Iterator0_t4138896567 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void liveVideo/<Start>c__Iterator0::Reset()
extern "C"  void U3CStartU3Ec__Iterator0_Reset_m1180079201 (U3CStartU3Ec__Iterator0_t4138896567 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
