﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.RawImage
struct RawImage_t3831555132;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// liveVideo
struct  liveVideo_t1000351471  : public MonoBehaviour_t3012272455
{
public:
	// UnityEngine.UI.RawImage liveVideo::rawimage
	RawImage_t3831555132 * ___rawimage_2;

public:
	inline static int32_t get_offset_of_rawimage_2() { return static_cast<int32_t>(offsetof(liveVideo_t1000351471, ___rawimage_2)); }
	inline RawImage_t3831555132 * get_rawimage_2() const { return ___rawimage_2; }
	inline RawImage_t3831555132 ** get_address_of_rawimage_2() { return &___rawimage_2; }
	inline void set_rawimage_2(RawImage_t3831555132 * value)
	{
		___rawimage_2 = value;
		Il2CppCodeGenWriteBarrier(&___rawimage_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
