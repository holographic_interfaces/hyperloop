﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// engine
struct engine_t2996304450;

#include "codegen/il2cpp-codegen.h"

// System.Void engine::.ctor()
extern "C"  void engine__ctor_m3753848910 (engine_t2996304450 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void engine::Update()
extern "C"  void engine_Update_m2132061311 (engine_t2996304450 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void engine::Main()
extern "C"  void engine_Main_m1006417583 (engine_t2996304450 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
