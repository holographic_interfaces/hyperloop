﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.WebCamTexture
struct WebCamTexture_t3635181805;
// System.Object
struct Il2CppObject;
// liveVideo
struct liveVideo_t1000351471;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// liveVideo/<Start>c__Iterator0
struct  U3CStartU3Ec__Iterator0_t4138896567  : public Il2CppObject
{
public:
	// UnityEngine.WebCamTexture liveVideo/<Start>c__Iterator0::<webcamTexture>__0
	WebCamTexture_t3635181805 * ___U3CwebcamTextureU3E__0_0;
	// System.Int32 liveVideo/<Start>c__Iterator0::$PC
	int32_t ___U24PC_1;
	// System.Object liveVideo/<Start>c__Iterator0::$current
	Il2CppObject * ___U24current_2;
	// liveVideo liveVideo/<Start>c__Iterator0::<>f__this
	liveVideo_t1000351471 * ___U3CU3Ef__this_3;

public:
	inline static int32_t get_offset_of_U3CwebcamTextureU3E__0_0() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t4138896567, ___U3CwebcamTextureU3E__0_0)); }
	inline WebCamTexture_t3635181805 * get_U3CwebcamTextureU3E__0_0() const { return ___U3CwebcamTextureU3E__0_0; }
	inline WebCamTexture_t3635181805 ** get_address_of_U3CwebcamTextureU3E__0_0() { return &___U3CwebcamTextureU3E__0_0; }
	inline void set_U3CwebcamTextureU3E__0_0(WebCamTexture_t3635181805 * value)
	{
		___U3CwebcamTextureU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CwebcamTextureU3E__0_0, value);
	}

	inline static int32_t get_offset_of_U24PC_1() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t4138896567, ___U24PC_1)); }
	inline int32_t get_U24PC_1() const { return ___U24PC_1; }
	inline int32_t* get_address_of_U24PC_1() { return &___U24PC_1; }
	inline void set_U24PC_1(int32_t value)
	{
		___U24PC_1 = value;
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t4138896567, ___U24current_2)); }
	inline Il2CppObject * get_U24current_2() const { return ___U24current_2; }
	inline Il2CppObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(Il2CppObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_2, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_3() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t4138896567, ___U3CU3Ef__this_3)); }
	inline liveVideo_t1000351471 * get_U3CU3Ef__this_3() const { return ___U3CU3Ef__this_3; }
	inline liveVideo_t1000351471 ** get_address_of_U3CU3Ef__this_3() { return &___U3CU3Ef__this_3; }
	inline void set_U3CU3Ef__this_3(liveVideo_t1000351471 * value)
	{
		___U3CU3Ef__this_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
