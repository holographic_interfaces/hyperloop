﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// podMovement
struct  podMovement_t4165861908  : public MonoBehaviour_t3012272455
{
public:
	// System.Single podMovement::acceleration
	float ___acceleration_2;
	// System.Single podMovement::maxSpeed
	float ___maxSpeed_3;

public:
	inline static int32_t get_offset_of_acceleration_2() { return static_cast<int32_t>(offsetof(podMovement_t4165861908, ___acceleration_2)); }
	inline float get_acceleration_2() const { return ___acceleration_2; }
	inline float* get_address_of_acceleration_2() { return &___acceleration_2; }
	inline void set_acceleration_2(float value)
	{
		___acceleration_2 = value;
	}

	inline static int32_t get_offset_of_maxSpeed_3() { return static_cast<int32_t>(offsetof(podMovement_t4165861908, ___maxSpeed_3)); }
	inline float get_maxSpeed_3() const { return ___maxSpeed_3; }
	inline float* get_address_of_maxSpeed_3() { return &___maxSpeed_3; }
	inline void set_maxSpeed_3(float value)
	{
		___maxSpeed_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
