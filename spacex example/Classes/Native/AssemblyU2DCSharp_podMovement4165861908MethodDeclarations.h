﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// podMovement
struct podMovement_t4165861908;

#include "codegen/il2cpp-codegen.h"

// System.Void podMovement::.ctor()
extern "C"  void podMovement__ctor_m2067566551 (podMovement_t4165861908 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void podMovement::Start()
extern "C"  void podMovement_Start_m1014704343 (podMovement_t4165861908 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void podMovement::Update()
extern "C"  void podMovement_Update_m1396915734 (podMovement_t4165861908 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
