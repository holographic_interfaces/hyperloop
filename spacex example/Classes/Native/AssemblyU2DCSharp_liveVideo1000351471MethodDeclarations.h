﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// liveVideo
struct liveVideo_t1000351471;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;

#include "codegen/il2cpp-codegen.h"

// System.Void liveVideo::.ctor()
extern "C"  void liveVideo__ctor_m1289112412 (liveVideo_t1000351471 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator liveVideo::Start()
extern "C"  Il2CppObject * liveVideo_Start_m623468708 (liveVideo_t1000351471 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void liveVideo::Update()
extern "C"  void liveVideo_Update_m3034641201 (liveVideo_t1000351471 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
