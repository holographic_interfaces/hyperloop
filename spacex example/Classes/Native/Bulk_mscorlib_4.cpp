﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// System.Reflection.Emit.ParameterBuilder
struct ParameterBuilder_t3382011775;
// System.String
struct String_t;
// System.Reflection.Emit.PropertyBuilder
struct PropertyBuilder_t356853651;
// System.Type
struct Type_t;
// System.Reflection.MethodInfo[]
struct MethodInfoU5BU5D_t1668237648;
// System.Object[]
struct ObjectU5BU5D_t11523773;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Reflection.ParameterInfo[]
struct ParameterInfoU5BU5D_t1127461800;
// System.Object
struct Il2CppObject;
// System.Reflection.Binder
struct Binder_t4180926488;
// System.Globalization.CultureInfo
struct CultureInfo_t3603717042;
// System.Reflection.Module
struct Module_t206139610;
// System.Exception
struct Exception_t1967233988;
// System.Reflection.Emit.TypeBuilder
struct TypeBuilder_t4287691406;
// System.Reflection.Emit.ModuleBuilder
struct ModuleBuilder_t1058295580;
// System.Type[]
struct TypeU5BU5D_t3431720054;
// System.Reflection.Assembly
struct Assembly_t1882292308;
// System.Reflection.ConstructorInfo
struct ConstructorInfo_t3542137334;
// System.Reflection.ParameterModifier[]
struct ParameterModifierU5BU5D_t3379147067;
// System.Reflection.Emit.ConstructorBuilder
struct ConstructorBuilder_t1859087886;
// System.Type[][]
struct TypeU5BU5DU5BU5D_t2239438067;
// System.Reflection.Emit.MethodBuilder
struct MethodBuilder_t765486855;
// System.Reflection.ConstructorInfo[]
struct ConstructorInfoU5BU5D_t3572023667;
// System.Reflection.EventInfo
struct EventInfo_t;
// System.Reflection.FieldInfo
struct FieldInfo_t;
// System.Reflection.FieldInfo[]
struct FieldInfoU5BU5D_t1144794227;
// System.Reflection.PropertyInfo[]
struct PropertyInfoU5BU5D_t1348579340;
// System.Reflection.PropertyInfo
struct PropertyInfo_t;
// System.String[]
struct StringU5BU5D_t2956870243;
// System.Reflection.Emit.UnmanagedMarshal
struct UnmanagedMarshal_t446138789;
// System.Runtime.InteropServices.MarshalAsAttribute
struct MarshalAsAttribute_t1016163854;
// System.Reflection.EventInfo/AddEventAdapter
struct AddEventAdapter_t836903542;
// System.Delegate
struct Delegate_t3660574010;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;
// System.Reflection.LocalVariableInfo
struct LocalVariableInfo_t721262211;
// System.Reflection.MemberFilter
struct MemberFilter_t1585748256;
// System.Reflection.MemberInfo
struct MemberInfo_t;
// System.Reflection.MemberInfoSerializationHolder
struct MemberInfoSerializationHolder_t2338510946;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t2995724695;
// System.Reflection.MethodBase
struct MethodBase_t3461000640;
// System.Reflection.Missing
struct Missing_t630192020;
// System.Reflection.MonoCMethod
struct MonoCMethod_t1488000239;
// System.Reflection.MonoEvent
struct MonoEvent_t;
// System.Reflection.MonoField
struct MonoField_t;
// System.Reflection.MonoGenericCMethod
struct MonoGenericCMethod_t1878793598;
// System.Reflection.MonoGenericMethod
struct MonoGenericMethod_t;
// System.Reflection.MonoMethod
struct MonoMethod_t;
// System.Runtime.InteropServices.DllImportAttribute
struct DllImportAttribute_t2977516789;
// System.Reflection.MonoProperty
struct MonoProperty_t;
// System.Reflection.MonoProperty/GetterAdapter
struct GetterAdapter_t943738788;
// System.Reflection.ParameterInfo
struct ParameterInfo_t2610273829;
// System.Reflection.Pointer
struct Pointer_t3455104107;
// System.Reflection.StrongNameKeyPair
struct StrongNameKeyPair_t2760016869;
// System.Security.Cryptography.RSA
struct RSA_t1557565273;
// Mono.Security.StrongName
struct StrongName_t2702370795;
// System.Reflection.TargetException
struct TargetException_t565659628;
// System.Reflection.TargetInvocationException
struct TargetInvocationException_t1980070524;
// System.Reflection.TargetParameterCountException
struct TargetParameterCountException_t2862237030;
// System.Reflection.TypeDelegator
struct TypeDelegator_t1324217559;
// System.Reflection.TypeFilter
struct TypeFilter_t2379296192;
// System.ResolveEventArgs
struct ResolveEventArgs_t4124692928;
// System.ResolveEventHandler
struct ResolveEventHandler_t2783314641;
// System.Resources.NeutralResourcesLanguageAttribute
struct NeutralResourcesLanguageAttribute_t3345045768;
// System.Resources.ResourceManager
struct ResourceManager_t1361280801;
// System.Resources.ResourceReader
struct ResourceReader_t4097835539;
// System.IO.Stream
struct Stream_t219029575;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// System.Resources.ResourceReader/ResourceCacheItem[]
struct ResourceCacheItemU5BU5D_t2722755166;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t1541724277;
// System.Resources.ResourceReader/ResourceEnumerator
struct ResourceEnumerator_t2097019538;
// System.Resources.ResourceSet
struct ResourceSet_t3790468310;
// System.IO.UnmanagedMemoryStream
struct UnmanagedMemoryStream_t4280280686;
// System.Resources.RuntimeResourceSet
struct RuntimeResourceSet_t2503739934;
// System.Resources.SatelliteContractVersionAttribute
struct SatelliteContractVersionAttribute_t1109503379;
// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct CompilationRelaxationsAttribute_t592669527;
// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct CompilerGeneratedAttribute_t853953138;
// System.Runtime.CompilerServices.DecimalConstantAttribute
struct DecimalConstantAttribute_t1650182573;
// System.Runtime.CompilerServices.DefaultDependencyAttribute
struct DefaultDependencyAttribute_t2265285942;
// System.Runtime.CompilerServices.InternalsVisibleToAttribute
struct InternalsVisibleToAttribute_t1035815915;
// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct RuntimeCompatibilityAttribute_t4174703210;
// System.Array
struct Il2CppArray;
// System.Runtime.CompilerServices.StringFreezingAttribute
struct StringFreezingAttribute_t398281917;
// System.Runtime.ConstrainedExecution.CriticalFinalizerObject
struct CriticalFinalizerObject_t3609670849;
// System.Runtime.ConstrainedExecution.ReliabilityContractAttribute
struct ReliabilityContractAttribute_t3872327357;
// System.Runtime.InteropServices.ClassInterfaceAttribute
struct ClassInterfaceAttribute_t261660349;
// System.Runtime.InteropServices.ComCompatibleVersionAttribute
struct ComCompatibleVersionAttribute_t3476486075;
// System.Runtime.InteropServices.ComDefaultInterfaceAttribute
struct ComDefaultInterfaceAttribute_t346532101;
// System.Runtime.InteropServices.ComImportAttribute
struct ComImportAttribute_t3229016760;
// System.Runtime.InteropServices.ComVisibleAttribute
struct ComVisibleAttribute_t1192118189;
// System.Runtime.InteropServices.DispIdAttribute
struct DispIdAttribute_t3066611329;
// System.Runtime.InteropServices.ExternalException
struct ExternalException_t1945928326;
// System.Runtime.InteropServices.FieldOffsetAttribute
struct FieldOffsetAttribute_t3640292593;
// System.Runtime.InteropServices.GuidAttribute
struct GuidAttribute_t4178726869;
// System.Runtime.InteropServices.InAttribute
struct InAttribute_t1020461241;
// System.Runtime.InteropServices.InterfaceTypeAttribute
struct InterfaceTypeAttribute_t465097163;
// System.Byte[]
struct ByteU5BU5D_t58506160;
// System.Char[]
struct CharU5BU5D_t3416858730;
// System.Runtime.InteropServices.MarshalDirectiveException
struct MarshalDirectiveException_t1317716672;
// System.Runtime.InteropServices.OptionalAttribute
struct OptionalAttribute_t4067831006;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array2840145358.h"
#include "mscorlib_System_Reflection_Emit_ParameterBuilder3382011775.h"
#include "mscorlib_System_Reflection_Emit_ParameterBuilder3382011775MethodDeclarations.h"
#include "mscorlib_System_Int322847414787.h"
#include "mscorlib_System_Reflection_ParameterAttributes4282458126.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_Reflection_Emit_PEFileKinds3928877179.h"
#include "mscorlib_System_Reflection_Emit_PEFileKinds3928877179MethodDeclarations.h"
#include "mscorlib_System_Reflection_Emit_PropertyBuilder356853651.h"
#include "mscorlib_System_Reflection_Emit_PropertyBuilder356853651MethodDeclarations.h"
#include "mscorlib_System_Reflection_PropertyAttributes3095558010.h"
#include "mscorlib_System_Boolean211005341.h"
#include "mscorlib_System_Reflection_Emit_MethodBuilder765486855.h"
#include "mscorlib_System_Type2779229935.h"
#include "mscorlib_System_Reflection_Emit_TypeBuilder4287691406.h"
#include "mscorlib_ArrayTypes.h"
#include "mscorlib_System_Reflection_MethodInfo3461221277.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Exception1967233988.h"
#include "mscorlib_System_Reflection_ParameterInfo2610273829.h"
#include "mscorlib_System_Reflection_BindingFlags2090192240.h"
#include "mscorlib_System_Reflection_Binder4180926488.h"
#include "mscorlib_System_Globalization_CultureInfo3603717042.h"
#include "mscorlib_System_Void2779279689.h"
#include "mscorlib_System_Reflection_Module206139610.h"
#include "mscorlib_System_Reflection_MemberInfo2843033814MethodDeclarations.h"
#include "mscorlib_System_Reflection_MemberInfo2843033814.h"
#include "mscorlib_System_NotSupportedException1374155497MethodDeclarations.h"
#include "mscorlib_System_NotSupportedException1374155497.h"
#include "mscorlib_System_Reflection_Emit_RefEmitPermissionS3789834874.h"
#include "mscorlib_System_Reflection_Emit_RefEmitPermissionS3789834874MethodDeclarations.h"
#include "mscorlib_System_Security_Permissions_SecurityActio3968659629.h"
#include "mscorlib_System_Reflection_Emit_StackBehaviour2852137698.h"
#include "mscorlib_System_Reflection_Emit_StackBehaviour2852137698MethodDeclarations.h"
#include "mscorlib_System_Reflection_Emit_TypeBuilder4287691406MethodDeclarations.h"
#include "mscorlib_System_Reflection_Emit_ModuleBuilder1058295580.h"
#include "mscorlib_System_Reflection_TypeAttributes104824479.h"
#include "mscorlib_System_Type2779229935MethodDeclarations.h"
#include "mscorlib_System_String968488902MethodDeclarations.h"
#include "mscorlib_System_Reflection_Emit_PackingSize2950865495.h"
#include "mscorlib_System_InvalidOperationException2420574324MethodDeclarations.h"
#include "mscorlib_System_Array2840145358MethodDeclarations.h"
#include "mscorlib_System_Reflection_Emit_ModuleBuilder1058295580MethodDeclarations.h"
#include "mscorlib_System_InvalidOperationException2420574324.h"
#include "mscorlib_System_Char2778706699.h"
#include "mscorlib_System_RuntimeTypeHandle1864875887.h"
#include "mscorlib_System_Reflection_Assembly1882292308.h"
#include "mscorlib_System_Reflection_Module206139610MethodDeclarations.h"
#include "mscorlib_System_Reflection_Assembly1882292308MethodDeclarations.h"
#include "mscorlib_System_Reflection_CallingConventions3959446060.h"
#include "mscorlib_System_Reflection_ConstructorInfo3542137334.h"
#include "mscorlib_System_Reflection_ParameterModifier500203470.h"
#include "mscorlib_System_Reflection_Emit_ConstructorBuilder1859087886MethodDeclarations.h"
#include "mscorlib_System_Reflection_AmbiguousMatchException3876538212MethodDeclarations.h"
#include "mscorlib_System_Reflection_Binder4180926488MethodDeclarations.h"
#include "mscorlib_System_Reflection_Emit_ConstructorBuilder1859087886.h"
#include "mscorlib_System_Reflection_MethodBase3461000640.h"
#include "mscorlib_System_Reflection_AmbiguousMatchException3876538212.h"
#include "mscorlib_System_Reflection_MethodBase3461000640MethodDeclarations.h"
#include "mscorlib_System_MonoCustomAttrs1707908163MethodDeclarations.h"
#include "mscorlib_System_Reflection_MethodAttributes455054214.h"
#include "mscorlib_System_Reflection_Emit_ILGenerator3869071517.h"
#include "mscorlib_System_Reflection_Emit_AssemblyBuilder3642540642.h"
#include "mscorlib_System_Reflection_Emit_OpCodes2031828562.h"
#include "mscorlib_System_Reflection_Emit_OpCodes2031828562MethodDeclarations.h"
#include "mscorlib_System_Reflection_Emit_OpCode4028977979.h"
#include "mscorlib_System_Reflection_Emit_ILGenerator3869071517MethodDeclarations.h"
#include "mscorlib_System_ArgumentException124305799MethodDeclarations.h"
#include "mscorlib_System_Reflection_Emit_MethodBuilder765486855MethodDeclarations.h"
#include "mscorlib_System_ArgumentException124305799.h"
#include "mscorlib_System_ArgumentNullException3214793280MethodDeclarations.h"
#include "mscorlib_System_ArgumentNullException3214793280.h"
#include "mscorlib_System_Reflection_ConstructorInfo3542137334MethodDeclarations.h"
#include "mscorlib_System_Reflection_Emit_FieldBuilder2184649998MethodDeclarations.h"
#include "mscorlib_System_AppDomain1551247802MethodDeclarations.h"
#include "mscorlib_System_TypeLoadException4088653924MethodDeclarations.h"
#include "mscorlib_System_Reflection_Emit_FieldBuilder2184649998.h"
#include "mscorlib_System_Reflection_FieldInfo1164929782MethodDeclarations.h"
#include "mscorlib_System_Reflection_FieldInfo1164929782.h"
#include "mscorlib_System_AppDomain1551247802.h"
#include "mscorlib_System_TypeLoadException4088653924.h"
#include "mscorlib_System_Collections_ArrayList2121638921MethodDeclarations.h"
#include "mscorlib_System_Collections_ArrayList2121638921.h"
#include "mscorlib_System_Reflection_EventInfo4226116758.h"
#include "mscorlib_System_Reflection_FieldAttributes3381152799.h"
#include "mscorlib_System_Reflection_PropertyInfo1490548369.h"
#include "mscorlib_System_Reflection_PropertyInfo1490548369MethodDeclarations.h"
#include "mscorlib_System_Reflection_Emit_ByRefType1509733795MethodDeclarations.h"
#include "mscorlib_System_Reflection_Emit_ByRefType1509733795.h"
#include "mscorlib_System_Reflection_Emit_AssemblyBuilder3642540642MethodDeclarations.h"
#include "mscorlib_System_Reflection_Emit_GenericTypeParamet3267237648.h"
#include "mscorlib_System_Reflection_Emit_UnmanagedMarshal446138789.h"
#include "mscorlib_System_Reflection_Emit_UnmanagedMarshal446138789MethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_MarshalAsA1016163854.h"
#include "mscorlib_System_Runtime_InteropServices_MarshalAsA1016163854MethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_UnmanagedT3644589314.h"
#include "mscorlib_System_Int162847414729.h"
#include "mscorlib_System_Reflection_EventAttributes1168020927.h"
#include "mscorlib_System_Reflection_EventAttributes1168020927MethodDeclarations.h"
#include "mscorlib_System_Reflection_EventInfo4226116758MethodDeclarations.h"
#include "mscorlib_System_Reflection_ParameterInfo2610273829MethodDeclarations.h"
#include "mscorlib_System_Reflection_MemberTypes938013741.h"
#include "mscorlib_System_Reflection_EventInfo_AddEventAdapte836903542.h"
#include "mscorlib_System_Reflection_EventInfo_AddEventAdapte836903542MethodDeclarations.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_Delegate3660574010.h"
#include "mscorlib_System_AsyncCallback1363551830.h"
#include "mscorlib_System_Reflection_FieldAttributes3381152799MethodDeclarations.h"
#include "mscorlib_System_RuntimeFieldHandle3184214143.h"
#include "mscorlib_System_RuntimeFieldHandle3184214143MethodDeclarations.h"
#include "mscorlib_System_IntPtr676692020MethodDeclarations.h"
#include "mscorlib_System_SystemException3155420757MethodDeclarations.h"
#include "mscorlib_System_SystemException3155420757.h"
#include "mscorlib_System_NonSerializedAttribute3281987872MethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_FieldOffse3640292593MethodDeclarations.h"
#include "mscorlib_System_NonSerializedAttribute3281987872.h"
#include "mscorlib_System_Runtime_InteropServices_FieldOffse3640292593.h"
#include "mscorlib_System_Reflection_LocalVariableInfo721262211.h"
#include "mscorlib_System_Reflection_LocalVariableInfo721262211MethodDeclarations.h"
#include "mscorlib_System_Object837106420MethodDeclarations.h"
#include "mscorlib_System_UInt16985925268.h"
#include "mscorlib_System_Reflection_MemberFilter1585748256.h"
#include "mscorlib_System_Reflection_MemberFilter1585748256MethodDeclarations.h"
#include "mscorlib_System_Reflection_MemberInfoSerialization2338510946.h"
#include "mscorlib_System_Reflection_MemberInfoSerialization2338510946MethodDeclarations.h"
#include "mscorlib_System_Runtime_Serialization_Serializatio2995724695.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCont986364934.h"
#include "mscorlib_System_Runtime_Serialization_Serializatio2995724695MethodDeclarations.h"
#include "mscorlib_System_Runtime_Serialization_Serialization731558744.h"
#include "mscorlib_System_Runtime_Serialization_Serialization731558744MethodDeclarations.h"
#include "mscorlib_System_Reflection_MethodInfo3461221277MethodDeclarations.h"
#include "mscorlib_System_Reflection_MemberTypes938013741MethodDeclarations.h"
#include "mscorlib_System_Reflection_MethodAttributes455054214MethodDeclarations.h"
#include "mscorlib_System_RuntimeMethodHandle2360005078.h"
#include "mscorlib_System_RuntimeMethodHandle2360005078MethodDeclarations.h"
#include "mscorlib_System_Exception1967233988MethodDeclarations.h"
#include "mscorlib_System_Reflection_MethodImplAttributes3301691718.h"
#include "mscorlib_System_Reflection_MethodImplAttributes3301691718MethodDeclarations.h"
#include "mscorlib_System_Reflection_Missing630192020.h"
#include "mscorlib_System_Reflection_Missing630192020MethodDeclarations.h"
#include "mscorlib_System_Reflection_TypeFilter2379296192MethodDeclarations.h"
#include "mscorlib_System_Reflection_TypeFilter2379296192.h"
#include "mscorlib_System_UnitySerializationHolder1142344636MethodDeclarations.h"
#include "mscorlib_System_Reflection_MonoCMethod1488000239.h"
#include "mscorlib_System_Reflection_MonoCMethod1488000239MethodDeclarations.h"
#include "mscorlib_System_Reflection_MonoMethodInfo106042080MethodDeclarations.h"
#include "mscorlib_System_Reflection_TargetParameterCountExc2862237030MethodDeclarations.h"
#include "mscorlib_System_Security_SecurityManager678461618MethodDeclarations.h"
#include "mscorlib_System_MemberAccessException3362876614MethodDeclarations.h"
#include "mscorlib_System_Reflection_TargetInvocationExcepti1980070524MethodDeclarations.h"
#include "mscorlib_System_Reflection_TargetParameterCountExc2862237030.h"
#include "mscorlib_System_MemberAccessException3362876614.h"
#include "mscorlib_System_Reflection_TargetInvocationExcepti1980070524.h"
#include "mscorlib_System_Reflection_MonoMethod1645984786MethodDeclarations.h"
#include "mscorlib_System_Text_StringBuilder3822575854MethodDeclarations.h"
#include "mscorlib_System_Text_StringBuilder3822575854.h"
#include "mscorlib_System_Reflection_MonoEvent2720766885.h"
#include "mscorlib_System_Reflection_MonoEvent2720766885MethodDeclarations.h"
#include "mscorlib_System_Reflection_MonoEventInfo4117885171MethodDeclarations.h"
#include "mscorlib_System_Reflection_MonoEventInfo4117885171.h"
#include "mscorlib_System_Reflection_MonoField2721303045.h"
#include "mscorlib_System_Reflection_MonoField2721303045MethodDeclarations.h"
#include "mscorlib_System_Reflection_TargetException565659628MethodDeclarations.h"
#include "mscorlib_System_Reflection_TargetException565659628.h"
#include "mscorlib_System_FieldAccessException3884822182MethodDeclarations.h"
#include "mscorlib_System_FieldAccessException3884822182.h"
#include "mscorlib_System_Reflection_MonoGenericCMethod1878793598.h"
#include "mscorlib_System_Reflection_MonoGenericCMethod1878793598MethodDeclarations.h"
#include "mscorlib_System_Reflection_MonoGenericMethod1242949027.h"
#include "mscorlib_System_Reflection_MonoGenericMethod1242949027MethodDeclarations.h"
#include "mscorlib_System_Reflection_MonoMethod1645984786.h"
#include "mscorlib_System_Threading_ThreadAbortException389307564.h"
#include "mscorlib_System_Runtime_InteropServices_DllImportA2977516789.h"
#include "mscorlib_System_Runtime_InteropServices_PreserveSi2759700281MethodDeclarations.h"
#include "mscorlib_System_Reflection_MonoMethodInfo106042080.h"
#include "mscorlib_System_Runtime_InteropServices_PreserveSi2759700281.h"
#include "mscorlib_System_Reflection_MonoProperty1813257286.h"
#include "mscorlib_System_Reflection_MonoProperty1813257286MethodDeclarations.h"
#include "mscorlib_System_Reflection_PInfo2267221868.h"
#include "mscorlib_System_Reflection_MonoPropertyInfo2683779348MethodDeclarations.h"
#include "mscorlib_System_Reflection_MonoPropertyInfo2683779348.h"
#include "mscorlib_System_Reflection_MonoProperty_GetterAdapt943738788.h"
#include "mscorlib_System_Delegate3660574010MethodDeclarations.h"
#include "mscorlib_System_Security_SecurityException128786772.h"
#include "mscorlib_System_Reflection_MonoProperty_GetterAdapt943738788MethodDeclarations.h"
#include "mscorlib_System_Reflection_ParameterAttributes4282458126MethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_InAttribut1020461241MethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_OptionalAt4067831006MethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_OutAttribu3987499248MethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_InAttribut1020461241.h"
#include "mscorlib_System_Runtime_InteropServices_OptionalAt4067831006.h"
#include "mscorlib_System_Runtime_InteropServices_OutAttribu3987499248.h"
#include "mscorlib_System_Reflection_ParameterModifier500203470MethodDeclarations.h"
#include "mscorlib_System_Reflection_PInfo2267221868MethodDeclarations.h"
#include "mscorlib_System_Reflection_Pointer3455104107.h"
#include "mscorlib_System_Reflection_Pointer3455104107MethodDeclarations.h"
#include "mscorlib_System_Reflection_ProcessorArchitecture2956520755.h"
#include "mscorlib_System_Reflection_ProcessorArchitecture2956520755MethodDeclarations.h"
#include "mscorlib_System_Reflection_PropertyAttributes3095558010MethodDeclarations.h"
#include "mscorlib_System_Reflection_ResourceAttributes1319414387.h"
#include "mscorlib_System_Reflection_ResourceAttributes1319414387MethodDeclarations.h"
#include "mscorlib_System_Reflection_StrongNameKeyPair2760016869.h"
#include "mscorlib_System_Reflection_StrongNameKeyPair2760016869MethodDeclarations.h"
#include "mscorlib_System_Byte2778693821.h"
#include "mscorlib_System_Security_Cryptography_RSA1557565273.h"
#include "mscorlib_Mono_Security_Cryptography_CryptoConvert3463653319MethodDeclarations.h"
#include "mscorlib_System_Security_Cryptography_CspParameter4096074019MethodDeclarations.h"
#include "mscorlib_System_Security_Cryptography_RSACryptoServ555495358MethodDeclarations.h"
#include "mscorlib_System_Security_Cryptography_CspParameter4096074019.h"
#include "mscorlib_System_Security_Cryptography_RSACryptoServ555495358.h"
#include "mscorlib_Mono_Security_StrongName2702370795.h"
#include "mscorlib_Mono_Security_StrongName2702370795MethodDeclarations.h"
#include "mscorlib_Locale2281372282MethodDeclarations.h"
#include "mscorlib_System_ApplicationException3809199252MethodDeclarations.h"
#include "mscorlib_System_Reflection_TypeAttributes104824479MethodDeclarations.h"
#include "mscorlib_System_Reflection_TypeDelegator1324217559.h"
#include "mscorlib_System_Reflection_TypeDelegator1324217559MethodDeclarations.h"
#include "mscorlib_System_ResolveEventArgs4124692928.h"
#include "mscorlib_System_ResolveEventArgs4124692928MethodDeclarations.h"
#include "mscorlib_System_EventArgs516466188MethodDeclarations.h"
#include "mscorlib_System_ResolveEventHandler2783314641.h"
#include "mscorlib_System_ResolveEventHandler2783314641MethodDeclarations.h"
#include "mscorlib_System_Resources_NeutralResourcesLanguage3345045768.h"
#include "mscorlib_System_Resources_NeutralResourcesLanguage3345045768MethodDeclarations.h"
#include "mscorlib_System_Attribute498693649MethodDeclarations.h"
#include "mscorlib_System_Resources_PredefinedResourceType2703762736.h"
#include "mscorlib_System_Resources_PredefinedResourceType2703762736MethodDeclarations.h"
#include "mscorlib_System_Resources_ResourceManager1361280801.h"
#include "mscorlib_System_Resources_ResourceManager1361280801MethodDeclarations.h"
#include "mscorlib_System_Collections_Hashtable3875263730MethodDeclarations.h"
#include "mscorlib_System_Collections_Hashtable3875263730.h"
#include "mscorlib_System_Resources_ResourceReader4097835539.h"
#include "mscorlib_System_Resources_ResourceReader4097835539MethodDeclarations.h"
#include "mscorlib_System_IO_Stream219029575.h"
#include "mscorlib_System_Text_Encoding180559927MethodDeclarations.h"
#include "mscorlib_System_IO_BinaryReader2158806251MethodDeclarations.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCont986364934MethodDeclarations.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_Bi341659722MethodDeclarations.h"
#include "mscorlib_System_IO_Stream219029575MethodDeclarations.h"
#include "mscorlib_System_Text_Encoding180559927.h"
#include "mscorlib_System_IO_BinaryReader2158806251.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCon2964666472.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_Bi341659722.h"
#include "mscorlib_System_IO_FileStream1527309539MethodDeclarations.h"
#include "mscorlib_System_IO_FileStream1527309539.h"
#include "mscorlib_System_IO_FileMode1356058118.h"
#include "mscorlib_System_IO_FileAccess995838663.h"
#include "mscorlib_System_IO_FileShare2703391818.h"
#include "mscorlib_System_Int322847414787MethodDeclarations.h"
#include "mscorlib_System_Int642847414882.h"
#include "mscorlib_System_IO_EndOfStreamException3421483780.h"
#include "mscorlib_System_IO_SeekOrigin3506139269.h"
#include "mscorlib_System_Resources_ResourceReader_ResourceI4074584572.h"
#include "mscorlib_System_Resources_ResourceReader_ResourceI4074584572MethodDeclarations.h"
#include "mscorlib_System_DateTime339033936MethodDeclarations.h"
#include "mscorlib_System_TimeSpan763862892MethodDeclarations.h"
#include "mscorlib_System_IO_MemoryStream2881531048MethodDeclarations.h"
#include "mscorlib_System_SByte2855346064.h"
#include "mscorlib_System_UInt32985925326.h"
#include "mscorlib_System_UInt64985925421.h"
#include "mscorlib_System_Single958209021.h"
#include "mscorlib_System_Double534516614.h"
#include "mscorlib_System_Decimal1688557254.h"
#include "mscorlib_System_DateTime339033936.h"
#include "mscorlib_System_TimeSpan763862892.h"
#include "mscorlib_System_IO_MemoryStream2881531048.h"
#include "mscorlib_System_Resources_ResourceReader_ResourceC3699857703.h"
#include "mscorlib_System_Threading_Monitor2071304733MethodDeclarations.h"
#include "mscorlib_System_Resources_ResourceReader_ResourceC3699857703MethodDeclarations.h"
#include "mscorlib_System_Resources_ResourceReader_ResourceE2097019538MethodDeclarations.h"
#include "mscorlib_System_Resources_ResourceReader_ResourceE2097019538.h"
#include "mscorlib_System_Collections_DictionaryEntry130027246.h"
#include "mscorlib_System_Collections_DictionaryEntry130027246MethodDeclarations.h"
#include "mscorlib_System_Resources_ResourceSet3790468310.h"
#include "mscorlib_System_Resources_ResourceSet3790468310MethodDeclarations.h"
#include "mscorlib_System_IO_UnmanagedMemoryStream4280280686.h"
#include "mscorlib_System_GC2776609905MethodDeclarations.h"
#include "mscorlib_System_ObjectDisposedException973246880MethodDeclarations.h"
#include "mscorlib_System_ObjectDisposedException973246880.h"
#include "mscorlib_System_Globalization_CultureInfo3603717042MethodDeclarations.h"
#include "mscorlib_System_Resources_RuntimeResourceSet2503739934.h"
#include "mscorlib_System_Resources_RuntimeResourceSet2503739934MethodDeclarations.h"
#include "mscorlib_System_Resources_SatelliteContractVersion1109503379.h"
#include "mscorlib_System_Resources_SatelliteContractVersion1109503379MethodDeclarations.h"
#include "mscorlib_System_Version497901645MethodDeclarations.h"
#include "mscorlib_System_Version497901645.h"
#include "mscorlib_System_Runtime_CompilerServices_Compilati1331426961.h"
#include "mscorlib_System_Runtime_CompilerServices_Compilati1331426961MethodDeclarations.h"
#include "mscorlib_System_Runtime_CompilerServices_Compilatio592669527.h"
#include "mscorlib_System_Runtime_CompilerServices_Compilatio592669527MethodDeclarations.h"
#include "mscorlib_System_Runtime_CompilerServices_CompilerGe853953138.h"
#include "mscorlib_System_Runtime_CompilerServices_CompilerGe853953138MethodDeclarations.h"
#include "mscorlib_System_Runtime_CompilerServices_DecimalCo1650182573.h"
#include "mscorlib_System_Runtime_CompilerServices_DecimalCo1650182573MethodDeclarations.h"
#include "mscorlib_System_Convert1097883944MethodDeclarations.h"
#include "mscorlib_System_Runtime_CompilerServices_DefaultDe2265285942.h"
#include "mscorlib_System_Runtime_CompilerServices_DefaultDe2265285942MethodDeclarations.h"
#include "mscorlib_System_Runtime_CompilerServices_LoadHint3551422003.h"
#include "mscorlib_System_Runtime_CompilerServices_Internals1035815915.h"
#include "mscorlib_System_Runtime_CompilerServices_Internals1035815915MethodDeclarations.h"
#include "mscorlib_System_Runtime_CompilerServices_IsVolatile953699980.h"
#include "mscorlib_System_Runtime_CompilerServices_IsVolatile953699980MethodDeclarations.h"
#include "mscorlib_System_Runtime_CompilerServices_LoadHint3551422003MethodDeclarations.h"
#include "mscorlib_System_Runtime_CompilerServices_RuntimeCo4174703210.h"
#include "mscorlib_System_Runtime_CompilerServices_RuntimeCo4174703210MethodDeclarations.h"
#include "mscorlib_System_Runtime_CompilerServices_RuntimeHe1695827251.h"
#include "mscorlib_System_Runtime_CompilerServices_RuntimeHe1695827251MethodDeclarations.h"
#include "mscorlib_System_Runtime_CompilerServices_StringFree398281917.h"
#include "mscorlib_System_Runtime_CompilerServices_StringFree398281917MethodDeclarations.h"
#include "mscorlib_System_Runtime_ConstrainedExecution_Cer3972881981.h"
#include "mscorlib_System_Runtime_ConstrainedExecution_Cer3972881981MethodDeclarations.h"
#include "mscorlib_System_Runtime_ConstrainedExecution_Consi2907429893.h"
#include "mscorlib_System_Runtime_ConstrainedExecution_Consi2907429893MethodDeclarations.h"
#include "mscorlib_System_Runtime_ConstrainedExecution_Criti3609670849.h"
#include "mscorlib_System_Runtime_ConstrainedExecution_Criti3609670849MethodDeclarations.h"
#include "mscorlib_System_Runtime_ConstrainedExecution_Relia3872327357.h"
#include "mscorlib_System_Runtime_ConstrainedExecution_Relia3872327357MethodDeclarations.h"
#include "mscorlib_System_Runtime_Hosting_ActivationArgument1102146247.h"
#include "mscorlib_System_Runtime_Hosting_ActivationArgument1102146247MethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_CallingConv422153495.h"
#include "mscorlib_System_Runtime_InteropServices_CallingConv422153495MethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_CharSet3877184238.h"
#include "mscorlib_System_Runtime_InteropServices_CharSet3877184238MethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_ClassInterf261660349.h"
#include "mscorlib_System_Runtime_InteropServices_ClassInterf261660349MethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_ClassInter1820603261.h"
#include "mscorlib_System_Runtime_InteropServices_ClassInter1820603261MethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_ComCompati3476486075.h"
#include "mscorlib_System_Runtime_InteropServices_ComCompati3476486075MethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_ComDefaultI346532101.h"
#include "mscorlib_System_Runtime_InteropServices_ComDefaultI346532101MethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_ComImportA3229016760.h"
#include "mscorlib_System_Runtime_InteropServices_ComImportA3229016760MethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_ComInterfa3505650196.h"
#include "mscorlib_System_Runtime_InteropServices_ComInterfa3505650196MethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_ComVisible1192118189.h"
#include "mscorlib_System_Runtime_InteropServices_ComVisible1192118189MethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_DispIdAttr3066611329.h"
#include "mscorlib_System_Runtime_InteropServices_DispIdAttr3066611329MethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_DllImportA2977516789MethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_ExternalEx1945928326.h"
#include "mscorlib_System_Runtime_InteropServices_ExternalEx1945928326MethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_GCHandle2146430982.h"
#include "mscorlib_System_Runtime_InteropServices_GCHandle2146430982MethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_GCHandleTy1597784416.h"
#include "mscorlib_System_Runtime_InteropServices_GCHandleTy1597784416MethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_GuidAttrib4178726869.h"
#include "mscorlib_System_Runtime_InteropServices_GuidAttrib4178726869MethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_InterfaceTy465097163.h"
#include "mscorlib_System_Runtime_InteropServices_InterfaceTy465097163MethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_Marshal3977632096.h"
#include "mscorlib_System_Runtime_InteropServices_Marshal3977632096MethodDeclarations.h"
#include "mscorlib_System_Environment63604104MethodDeclarations.h"
#include "mscorlib_System_OperatingSystem602923589MethodDeclarations.h"
#include "mscorlib_System_OperatingSystem602923589.h"
#include "mscorlib_System_PlatformID2455152771.h"
#include "mscorlib_System_Runtime_InteropServices_MarshalDir1317716672.h"
#include "mscorlib_System_Runtime_InteropServices_MarshalDir1317716672MethodDeclarations.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Int32 System.Reflection.Emit.ParameterBuilder::get_Attributes()
extern "C"  int32_t ParameterBuilder_get_Attributes_m2640139997 (ParameterBuilder_t3382011775 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_attrs_1();
		return L_0;
	}
}
// System.String System.Reflection.Emit.ParameterBuilder::get_Name()
extern "C"  String_t* ParameterBuilder_get_Name_m1429816234 (ParameterBuilder_t3382011775 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_name_0();
		return L_0;
	}
}
// System.Int32 System.Reflection.Emit.ParameterBuilder::get_Position()
extern "C"  int32_t ParameterBuilder_get_Position_m388196207 (ParameterBuilder_t3382011775 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_position_2();
		return L_0;
	}
}
// System.Reflection.PropertyAttributes System.Reflection.Emit.PropertyBuilder::get_Attributes()
extern "C"  int32_t PropertyBuilder_get_Attributes_m548385532 (PropertyBuilder_t356853651 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_attrs_0();
		return L_0;
	}
}
// System.Boolean System.Reflection.Emit.PropertyBuilder::get_CanRead()
extern "C"  bool PropertyBuilder_get_CanRead_m1928541820 (PropertyBuilder_t356853651 * __this, const MethodInfo* method)
{
	{
		MethodBuilder_t765486855 * L_0 = __this->get_get_method_4();
		return (bool)((((int32_t)((((Il2CppObject*)(MethodBuilder_t765486855 *)L_0) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Boolean System.Reflection.Emit.PropertyBuilder::get_CanWrite()
extern "C"  bool PropertyBuilder_get_CanWrite_m177907803 (PropertyBuilder_t356853651 * __this, const MethodInfo* method)
{
	{
		MethodBuilder_t765486855 * L_0 = __this->get_set_method_3();
		return (bool)((((int32_t)((((Il2CppObject*)(MethodBuilder_t765486855 *)L_0) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Type System.Reflection.Emit.PropertyBuilder::get_DeclaringType()
extern "C"  Type_t * PropertyBuilder_get_DeclaringType_m1573425887 (PropertyBuilder_t356853651 * __this, const MethodInfo* method)
{
	{
		TypeBuilder_t4287691406 * L_0 = __this->get_typeb_5();
		return L_0;
	}
}
// System.String System.Reflection.Emit.PropertyBuilder::get_Name()
extern "C"  String_t* PropertyBuilder_get_Name_m1476342296 (PropertyBuilder_t356853651 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_name_1();
		return L_0;
	}
}
// System.Type System.Reflection.Emit.PropertyBuilder::get_PropertyType()
extern "C"  Type_t * PropertyBuilder_get_PropertyType_m946002355 (PropertyBuilder_t356853651 * __this, const MethodInfo* method)
{
	{
		Type_t * L_0 = __this->get_type_2();
		return L_0;
	}
}
// System.Type System.Reflection.Emit.PropertyBuilder::get_ReflectedType()
extern "C"  Type_t * PropertyBuilder_get_ReflectedType_m803872660 (PropertyBuilder_t356853651 * __this, const MethodInfo* method)
{
	{
		TypeBuilder_t4287691406 * L_0 = __this->get_typeb_5();
		return L_0;
	}
}
// System.Reflection.MethodInfo[] System.Reflection.Emit.PropertyBuilder::GetAccessors(System.Boolean)
extern "C"  MethodInfoU5BU5D_t1668237648* PropertyBuilder_GetAccessors_m3435130372 (PropertyBuilder_t356853651 * __this, bool ___nonPublic, const MethodInfo* method)
{
	{
		return (MethodInfoU5BU5D_t1668237648*)NULL;
	}
}
// System.Object[] System.Reflection.Emit.PropertyBuilder::GetCustomAttributes(System.Boolean)
extern "C"  ObjectU5BU5D_t11523773* PropertyBuilder_GetCustomAttributes_m3021459715 (PropertyBuilder_t356853651 * __this, bool ___inherit, const MethodInfo* method)
{
	{
		Exception_t1967233988 * L_0 = PropertyBuilder_not_supported_m3524698216(__this, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Object[] System.Reflection.Emit.PropertyBuilder::GetCustomAttributes(System.Type,System.Boolean)
extern "C"  ObjectU5BU5D_t11523773* PropertyBuilder_GetCustomAttributes_m3490282608 (PropertyBuilder_t356853651 * __this, Type_t * ___attributeType, bool ___inherit, const MethodInfo* method)
{
	{
		Exception_t1967233988 * L_0 = PropertyBuilder_not_supported_m3524698216(__this, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Reflection.MethodInfo System.Reflection.Emit.PropertyBuilder::GetGetMethod(System.Boolean)
extern "C"  MethodInfo_t * PropertyBuilder_GetGetMethod_m1084139825 (PropertyBuilder_t356853651 * __this, bool ___nonPublic, const MethodInfo* method)
{
	{
		MethodBuilder_t765486855 * L_0 = __this->get_get_method_4();
		return L_0;
	}
}
// System.Reflection.ParameterInfo[] System.Reflection.Emit.PropertyBuilder::GetIndexParameters()
extern "C"  ParameterInfoU5BU5D_t1127461800* PropertyBuilder_GetIndexParameters_m3909940773 (PropertyBuilder_t356853651 * __this, const MethodInfo* method)
{
	{
		Exception_t1967233988 * L_0 = PropertyBuilder_not_supported_m3524698216(__this, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Reflection.MethodInfo System.Reflection.Emit.PropertyBuilder::GetSetMethod(System.Boolean)
extern "C"  MethodInfo_t * PropertyBuilder_GetSetMethod_m1927431485 (PropertyBuilder_t356853651 * __this, bool ___nonPublic, const MethodInfo* method)
{
	{
		MethodBuilder_t765486855 * L_0 = __this->get_set_method_3();
		return L_0;
	}
}
// System.Object System.Reflection.Emit.PropertyBuilder::GetValue(System.Object,System.Object[])
extern "C"  Il2CppObject * PropertyBuilder_GetValue_m1709912399 (PropertyBuilder_t356853651 * __this, Il2CppObject * ___obj, ObjectU5BU5D_t11523773* ___index, const MethodInfo* method)
{
	{
		return NULL;
	}
}
// System.Object System.Reflection.Emit.PropertyBuilder::GetValue(System.Object,System.Reflection.BindingFlags,System.Reflection.Binder,System.Object[],System.Globalization.CultureInfo)
extern "C"  Il2CppObject * PropertyBuilder_GetValue_m2770378927 (PropertyBuilder_t356853651 * __this, Il2CppObject * ___obj, int32_t ___invokeAttr, Binder_t4180926488 * ___binder, ObjectU5BU5D_t11523773* ___index, CultureInfo_t3603717042 * ___culture, const MethodInfo* method)
{
	{
		Exception_t1967233988 * L_0 = PropertyBuilder_not_supported_m3524698216(__this, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean System.Reflection.Emit.PropertyBuilder::IsDefined(System.Type,System.Boolean)
extern "C"  bool PropertyBuilder_IsDefined_m3818094274 (PropertyBuilder_t356853651 * __this, Type_t * ___attributeType, bool ___inherit, const MethodInfo* method)
{
	{
		Exception_t1967233988 * L_0 = PropertyBuilder_not_supported_m3524698216(__this, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Reflection.Emit.PropertyBuilder::SetValue(System.Object,System.Object,System.Object[])
extern "C"  void PropertyBuilder_SetValue_m2811621182 (PropertyBuilder_t356853651 * __this, Il2CppObject * ___obj, Il2CppObject * ___value, ObjectU5BU5D_t11523773* ___index, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void System.Reflection.Emit.PropertyBuilder::SetValue(System.Object,System.Object,System.Reflection.BindingFlags,System.Reflection.Binder,System.Object[],System.Globalization.CultureInfo)
extern "C"  void PropertyBuilder_SetValue_m307828128 (PropertyBuilder_t356853651 * __this, Il2CppObject * ___obj, Il2CppObject * ___value, int32_t ___invokeAttr, Binder_t4180926488 * ___binder, ObjectU5BU5D_t11523773* ___index, CultureInfo_t3603717042 * ___culture, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Reflection.Module System.Reflection.Emit.PropertyBuilder::get_Module()
extern "C"  Module_t206139610 * PropertyBuilder_get_Module_m3451643761 (PropertyBuilder_t356853651 * __this, const MethodInfo* method)
{
	{
		Module_t206139610 * L_0 = MemberInfo_get_Module_m3912547150(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Exception System.Reflection.Emit.PropertyBuilder::not_supported()
extern TypeInfo* NotSupportedException_t1374155497_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1016179289;
extern const uint32_t PropertyBuilder_not_supported_m3524698216_MetadataUsageId;
extern "C"  Exception_t1967233988 * PropertyBuilder_not_supported_m3524698216 (PropertyBuilder_t356853651 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PropertyBuilder_not_supported_m3524698216_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1374155497 * L_0 = (NotSupportedException_t1374155497 *)il2cpp_codegen_object_new(NotSupportedException_t1374155497_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m133757637(L_0, _stringLiteral1016179289, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void System.Reflection.Emit.RefEmitPermissionSet::.ctor(System.Security.Permissions.SecurityAction,System.String)
extern "C"  void RefEmitPermissionSet__ctor_m3807212189 (RefEmitPermissionSet_t3789834874 * __this, int32_t ___action, String_t* ___pset, const MethodInfo* method)
{
	{
		int32_t L_0 = ___action;
		__this->set_action_0(L_0);
		String_t* L_1 = ___pset;
		__this->set_pset_1(L_1);
		return;
	}
}
// Conversion methods for marshalling of: System.Reflection.Emit.RefEmitPermissionSet
extern "C" void RefEmitPermissionSet_t3789834874_marshal_pinvoke(const RefEmitPermissionSet_t3789834874& unmarshaled, RefEmitPermissionSet_t3789834874_marshaled_pinvoke& marshaled)
{
	marshaled.___action_0 = unmarshaled.get_action_0();
	marshaled.___pset_1 = il2cpp_codegen_marshal_string(unmarshaled.get_pset_1());
}
extern "C" void RefEmitPermissionSet_t3789834874_marshal_pinvoke_back(const RefEmitPermissionSet_t3789834874_marshaled_pinvoke& marshaled, RefEmitPermissionSet_t3789834874& unmarshaled)
{
	int32_t unmarshaled_action_temp = 0;
	unmarshaled_action_temp = marshaled.___action_0;
	unmarshaled.set_action_0(unmarshaled_action_temp);
	unmarshaled.set_pset_1(il2cpp_codegen_marshal_string_result(marshaled.___pset_1));
}
// Conversion method for clean up from marshalling of: System.Reflection.Emit.RefEmitPermissionSet
extern "C" void RefEmitPermissionSet_t3789834874_marshal_pinvoke_cleanup(RefEmitPermissionSet_t3789834874_marshaled_pinvoke& marshaled)
{
	il2cpp_codegen_marshal_free(marshaled.___pset_1);
	marshaled.___pset_1 = NULL;
}
// Conversion methods for marshalling of: System.Reflection.Emit.RefEmitPermissionSet
extern "C" void RefEmitPermissionSet_t3789834874_marshal_com(const RefEmitPermissionSet_t3789834874& unmarshaled, RefEmitPermissionSet_t3789834874_marshaled_com& marshaled)
{
	marshaled.___action_0 = unmarshaled.get_action_0();
	marshaled.___pset_1 = il2cpp_codegen_marshal_bstring(unmarshaled.get_pset_1());
}
extern "C" void RefEmitPermissionSet_t3789834874_marshal_com_back(const RefEmitPermissionSet_t3789834874_marshaled_com& marshaled, RefEmitPermissionSet_t3789834874& unmarshaled)
{
	int32_t unmarshaled_action_temp = 0;
	unmarshaled_action_temp = marshaled.___action_0;
	unmarshaled.set_action_0(unmarshaled_action_temp);
	unmarshaled.set_pset_1(il2cpp_codegen_marshal_bstring_result(marshaled.___pset_1));
}
// Conversion method for clean up from marshalling of: System.Reflection.Emit.RefEmitPermissionSet
extern "C" void RefEmitPermissionSet_t3789834874_marshal_com_cleanup(RefEmitPermissionSet_t3789834874_marshaled_com& marshaled)
{
	il2cpp_codegen_marshal_free_bstring(marshaled.___pset_1);
	marshaled.___pset_1 = NULL;
}
// System.Void System.Reflection.Emit.TypeBuilder::.ctor(System.Reflection.Emit.ModuleBuilder,System.Reflection.TypeAttributes,System.Int32)
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t2847414787_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral86524790;
extern Il2CppCodeGenString* _stringLiteral110844005;
extern const uint32_t TypeBuilder__ctor_m2701930902_MetadataUsageId;
extern "C"  void TypeBuilder__ctor_m2701930902 (TypeBuilder_t4287691406 * __this, ModuleBuilder_t1058295580 * ___mb, int32_t ___attr, int32_t ___table_idx, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TypeBuilder__ctor_m2701930902_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	TypeBuilder_t4287691406 * G_B2_0 = NULL;
	TypeBuilder_t4287691406 * G_B2_1 = NULL;
	TypeBuilder_t4287691406 * G_B1_0 = NULL;
	TypeBuilder_t4287691406 * G_B1_1 = NULL;
	String_t* G_B3_0 = NULL;
	TypeBuilder_t4287691406 * G_B3_1 = NULL;
	TypeBuilder_t4287691406 * G_B3_2 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type__ctor_m3982515451(__this, /*hidden argument*/NULL);
		__this->set_parent_10((Type_t *)NULL);
		int32_t L_0 = ___attr;
		__this->set_attrs_19(L_0);
		__this->set_class_size_22(0);
		int32_t L_1 = ___table_idx;
		__this->set_table_idx_20(L_1);
		int32_t L_2 = ___table_idx;
		G_B1_0 = __this;
		G_B1_1 = __this;
		if ((!(((uint32_t)L_2) == ((uint32_t)1))))
		{
			G_B2_0 = __this;
			G_B2_1 = __this;
			goto IL_0035;
		}
	}
	{
		G_B3_0 = _stringLiteral86524790;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		goto IL_0045;
	}

IL_0035:
	{
		int32_t L_3 = ___table_idx;
		int32_t L_4 = L_3;
		Il2CppObject * L_5 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_4);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = String_Concat_m389863537(NULL /*static, unused*/, _stringLiteral110844005, L_5, /*hidden argument*/NULL);
		G_B3_0 = L_6;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
	}

IL_0045:
	{
		String_t* L_7 = G_B3_0;
		V_0 = L_7;
		NullCheck(G_B3_1);
		G_B3_1->set_tname_8(L_7);
		String_t* L_8 = V_0;
		NullCheck(G_B3_2);
		G_B3_2->set_fullname_26(L_8);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_9 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set_nspace_9(L_9);
		ModuleBuilder_t1058295580 * L_10 = ___mb;
		__this->set_pmodule_21(L_10);
		TypeBuilder_setup_internal_class_m1841540458(__this, __this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Reflection.Emit.TypeBuilder::.ctor(System.Reflection.Emit.ModuleBuilder,System.String,System.Reflection.TypeAttributes,System.Type,System.Type[],System.Reflection.Emit.PackingSize,System.Int32,System.Type)
extern const Il2CppType* Il2CppObject_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* TypeU5BU5D_t3431720054_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1331805594;
extern Il2CppCodeGenString* _stringLiteral2633143155;
extern const uint32_t TypeBuilder__ctor_m2838836864_MetadataUsageId;
extern "C"  void TypeBuilder__ctor_m2838836864 (TypeBuilder_t4287691406 * __this, ModuleBuilder_t1058295580 * ___mb, String_t* ___name, int32_t ___attr, Type_t * ___parent, TypeU5BU5D_t3431720054* ___interfaces, int32_t ___packing_size, int32_t ___type_size, Type_t * ___nesting_type, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TypeBuilder__ctor_m2838836864_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type__ctor_m3982515451(__this, /*hidden argument*/NULL);
		Type_t * L_0 = ___parent;
		__this->set_parent_10(L_0);
		int32_t L_1 = ___attr;
		__this->set_attrs_19(L_1);
		int32_t L_2 = ___type_size;
		__this->set_class_size_22(L_2);
		int32_t L_3 = ___packing_size;
		__this->set_packing_size_23(L_3);
		Type_t * L_4 = ___nesting_type;
		__this->set_nesting_type_11(L_4);
		String_t* L_5 = ___name;
		TypeBuilder_check_name_m566175072(__this, _stringLiteral1331805594, L_5, /*hidden argument*/NULL);
		Type_t * L_6 = ___parent;
		if (L_6)
		{
			goto IL_0060;
		}
	}
	{
		int32_t L_7 = ___attr;
		if (!((int32_t)((int32_t)L_7&(int32_t)((int32_t)32))))
		{
			goto IL_0060;
		}
	}
	{
		int32_t L_8 = ___attr;
		if (((int32_t)((int32_t)L_8&(int32_t)((int32_t)128))))
		{
			goto IL_0060;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_9 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_9, _stringLiteral2633143155, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0060:
	{
		String_t* L_10 = ___name;
		NullCheck(L_10);
		int32_t L_11 = String_LastIndexOf_m3245805612(L_10, ((int32_t)46), /*hidden argument*/NULL);
		V_0 = L_11;
		int32_t L_12 = V_0;
		if ((((int32_t)L_12) == ((int32_t)(-1))))
		{
			goto IL_0092;
		}
	}
	{
		String_t* L_13 = ___name;
		int32_t L_14 = V_0;
		NullCheck(L_13);
		String_t* L_15 = String_Substring_m2809233063(L_13, ((int32_t)((int32_t)L_14+(int32_t)1)), /*hidden argument*/NULL);
		__this->set_tname_8(L_15);
		String_t* L_16 = ___name;
		int32_t L_17 = V_0;
		NullCheck(L_16);
		String_t* L_18 = String_Substring_m675079568(L_16, 0, L_17, /*hidden argument*/NULL);
		__this->set_nspace_9(L_18);
		goto IL_00a4;
	}

IL_0092:
	{
		String_t* L_19 = ___name;
		__this->set_tname_8(L_19);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_20 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set_nspace_9(L_20);
	}

IL_00a4:
	{
		TypeU5BU5D_t3431720054* L_21 = ___interfaces;
		if (!L_21)
		{
			goto IL_00cb;
		}
	}
	{
		TypeU5BU5D_t3431720054* L_22 = ___interfaces;
		NullCheck(L_22);
		__this->set_interfaces_12(((TypeU5BU5D_t3431720054*)SZArrayNew(TypeU5BU5D_t3431720054_il2cpp_TypeInfo_var, (uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_22)->max_length)))))));
		TypeU5BU5D_t3431720054* L_23 = ___interfaces;
		TypeU5BU5D_t3431720054* L_24 = __this->get_interfaces_12();
		TypeU5BU5D_t3431720054* L_25 = ___interfaces;
		NullCheck(L_25);
		Array_Copy_m3799309042(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_23, (Il2CppArray *)(Il2CppArray *)L_24, (((int32_t)((int32_t)(((Il2CppArray *)L_25)->max_length)))), /*hidden argument*/NULL);
	}

IL_00cb:
	{
		ModuleBuilder_t1058295580 * L_26 = ___mb;
		__this->set_pmodule_21(L_26);
		int32_t L_27 = ___attr;
		if (((int32_t)((int32_t)L_27&(int32_t)((int32_t)32))))
		{
			goto IL_00fd;
		}
	}
	{
		Type_t * L_28 = ___parent;
		if (L_28)
		{
			goto IL_00fd;
		}
	}
	{
		bool L_29 = TypeBuilder_get_IsCompilerContext_m432935841(__this, /*hidden argument*/NULL);
		if (L_29)
		{
			goto IL_00fd;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_30 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Il2CppObject_0_0_0_var), /*hidden argument*/NULL);
		__this->set_parent_10(L_30);
	}

IL_00fd:
	{
		ModuleBuilder_t1058295580 * L_31 = ___mb;
		NullCheck(L_31);
		int32_t L_32 = ModuleBuilder_get_next_table_index_m3600803990(L_31, __this, 2, (bool)1, /*hidden argument*/NULL);
		__this->set_table_idx_20(L_32);
		TypeBuilder_setup_internal_class_m1841540458(__this, __this, /*hidden argument*/NULL);
		String_t* L_33 = TypeBuilder_GetFullName_m2368980531(__this, /*hidden argument*/NULL);
		__this->set_fullname_26(L_33);
		return;
	}
}
// System.Reflection.TypeAttributes System.Reflection.Emit.TypeBuilder::GetAttributeFlagsImpl()
extern "C"  int32_t TypeBuilder_GetAttributeFlagsImpl_m2415994065 (TypeBuilder_t4287691406 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_attrs_19();
		return L_0;
	}
}
// System.Void System.Reflection.Emit.TypeBuilder::setup_internal_class(System.Reflection.Emit.TypeBuilder)
extern "C"  void TypeBuilder_setup_internal_class_m1841540458 (TypeBuilder_t4287691406 * __this, TypeBuilder_t4287691406 * ___tb, const MethodInfo* method)
{
	using namespace il2cpp::icalls;
	typedef void (*TypeBuilder_setup_internal_class_m1841540458_ftn) (TypeBuilder_t4287691406 *, TypeBuilder_t4287691406 *);
	 ((TypeBuilder_setup_internal_class_m1841540458_ftn)mscorlib::System::Reflection::Emit::TypeBuilder::setup_internal_class) (__this, ___tb);
}
// System.Void System.Reflection.Emit.TypeBuilder::create_generic_class()
extern "C"  void TypeBuilder_create_generic_class_m1633445577 (TypeBuilder_t4287691406 * __this, const MethodInfo* method)
{
	using namespace il2cpp::icalls;
	typedef void (*TypeBuilder_create_generic_class_m1633445577_ftn) (TypeBuilder_t4287691406 *);
	 ((TypeBuilder_create_generic_class_m1633445577_ftn)mscorlib::System::Reflection::Emit::TypeBuilder::create_generic_class) (__this);
}
// System.Reflection.Assembly System.Reflection.Emit.TypeBuilder::get_Assembly()
extern "C"  Assembly_t1882292308 * TypeBuilder_get_Assembly_m870988588 (TypeBuilder_t4287691406 * __this, const MethodInfo* method)
{
	{
		ModuleBuilder_t1058295580 * L_0 = __this->get_pmodule_21();
		NullCheck(L_0);
		Assembly_t1882292308 * L_1 = Module_get_Assembly_m4154223570(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.String System.Reflection.Emit.TypeBuilder::get_AssemblyQualifiedName()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1396;
extern const uint32_t TypeBuilder_get_AssemblyQualifiedName_m750470747_MetadataUsageId;
extern "C"  String_t* TypeBuilder_get_AssemblyQualifiedName_m750470747 (TypeBuilder_t4287691406 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TypeBuilder_get_AssemblyQualifiedName_m750470747_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = __this->get_fullname_26();
		Assembly_t1882292308 * L_1 = TypeBuilder_get_Assembly_m870988588(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		String_t* L_2 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.Assembly::get_FullName() */, L_1);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Concat_m1825781833(NULL /*static, unused*/, L_0, _stringLiteral1396, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Type System.Reflection.Emit.TypeBuilder::get_BaseType()
extern "C"  Type_t * TypeBuilder_get_BaseType_m1785398826 (TypeBuilder_t4287691406 * __this, const MethodInfo* method)
{
	{
		Type_t * L_0 = __this->get_parent_10();
		return L_0;
	}
}
// System.Type System.Reflection.Emit.TypeBuilder::get_DeclaringType()
extern "C"  Type_t * TypeBuilder_get_DeclaringType_m1033527492 (TypeBuilder_t4287691406 * __this, const MethodInfo* method)
{
	{
		Type_t * L_0 = __this->get_nesting_type_11();
		return L_0;
	}
}
// System.Type System.Reflection.Emit.TypeBuilder::get_UnderlyingSystemType()
extern TypeInfo* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral831934619;
extern const uint32_t TypeBuilder_get_UnderlyingSystemType_m301071877_MetadataUsageId;
extern "C"  Type_t * TypeBuilder_get_UnderlyingSystemType_m301071877 (TypeBuilder_t4287691406 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TypeBuilder_get_UnderlyingSystemType_m301071877_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = TypeBuilder_get_is_created_m695624602(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		Type_t * L_1 = __this->get_created_25();
		NullCheck(L_1);
		Type_t * L_2 = VirtFuncInvoker0< Type_t * >::Invoke(36 /* System.Type System.Type::get_UnderlyingSystemType() */, L_1);
		return L_2;
	}

IL_0017:
	{
		bool L_3 = VirtFuncInvoker0< bool >::Invoke(25 /* System.Boolean System.Type::get_IsEnum() */, __this);
		if (!L_3)
		{
			goto IL_004a;
		}
	}
	{
		bool L_4 = TypeBuilder_get_IsCompilerContext_m432935841(__this, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_004a;
		}
	}
	{
		Type_t * L_5 = __this->get_underlying_type_28();
		if (!L_5)
		{
			goto IL_003f;
		}
	}
	{
		Type_t * L_6 = __this->get_underlying_type_28();
		return L_6;
	}

IL_003f:
	{
		InvalidOperationException_t2420574324 * L_7 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_7, _stringLiteral831934619, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_7);
	}

IL_004a:
	{
		return __this;
	}
}
// System.String System.Reflection.Emit.TypeBuilder::GetFullName()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral43;
extern Il2CppCodeGenString* _stringLiteral46;
extern const uint32_t TypeBuilder_GetFullName_m2368980531_MetadataUsageId;
extern "C"  String_t* TypeBuilder_GetFullName_m2368980531 (TypeBuilder_t4287691406 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TypeBuilder_GetFullName_m2368980531_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Type_t * L_0 = __this->get_nesting_type_11();
		if (!L_0)
		{
			goto IL_0027;
		}
	}
	{
		Type_t * L_1 = __this->get_nesting_type_11();
		NullCheck(L_1);
		String_t* L_2 = VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String System.Type::get_FullName() */, L_1);
		String_t* L_3 = __this->get_tname_8();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = String_Concat_m1825781833(NULL /*static, unused*/, L_2, _stringLiteral43, L_3, /*hidden argument*/NULL);
		return L_4;
	}

IL_0027:
	{
		String_t* L_5 = __this->get_nspace_9();
		if (!L_5)
		{
			goto IL_005a;
		}
	}
	{
		String_t* L_6 = __this->get_nspace_9();
		NullCheck(L_6);
		int32_t L_7 = String_get_Length_m2979997331(L_6, /*hidden argument*/NULL);
		if ((((int32_t)L_7) <= ((int32_t)0)))
		{
			goto IL_005a;
		}
	}
	{
		String_t* L_8 = __this->get_nspace_9();
		String_t* L_9 = __this->get_tname_8();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_10 = String_Concat_m1825781833(NULL /*static, unused*/, L_8, _stringLiteral46, L_9, /*hidden argument*/NULL);
		return L_10;
	}

IL_005a:
	{
		String_t* L_11 = __this->get_tname_8();
		return L_11;
	}
}
// System.String System.Reflection.Emit.TypeBuilder::get_FullName()
extern "C"  String_t* TypeBuilder_get_FullName_m1311381250 (TypeBuilder_t4287691406 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_fullname_26();
		return L_0;
	}
}
// System.Reflection.Module System.Reflection.Emit.TypeBuilder::get_Module()
extern "C"  Module_t206139610 * TypeBuilder_get_Module_m1380149676 (TypeBuilder_t4287691406 * __this, const MethodInfo* method)
{
	{
		ModuleBuilder_t1058295580 * L_0 = __this->get_pmodule_21();
		return L_0;
	}
}
// System.String System.Reflection.Emit.TypeBuilder::get_Name()
extern "C"  String_t* TypeBuilder_get_Name_m225700627 (TypeBuilder_t4287691406 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_tname_8();
		return L_0;
	}
}
// System.String System.Reflection.Emit.TypeBuilder::get_Namespace()
extern "C"  String_t* TypeBuilder_get_Namespace_m2211701173 (TypeBuilder_t4287691406 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_nspace_9();
		return L_0;
	}
}
// System.Type System.Reflection.Emit.TypeBuilder::get_ReflectedType()
extern "C"  Type_t * TypeBuilder_get_ReflectedType_m263974265 (TypeBuilder_t4287691406 * __this, const MethodInfo* method)
{
	{
		Type_t * L_0 = __this->get_nesting_type_11();
		return L_0;
	}
}
// System.Reflection.ConstructorInfo System.Reflection.Emit.TypeBuilder::GetConstructorImpl(System.Reflection.BindingFlags,System.Reflection.Binder,System.Reflection.CallingConventions,System.Type[],System.Reflection.ParameterModifier[])
extern const Il2CppType* Il2CppObject_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* AmbiguousMatchException_t3876538212_il2cpp_TypeInfo_var;
extern TypeInfo* MethodBaseU5BU5D_t1767252801_il2cpp_TypeInfo_var;
extern TypeInfo* Binder_t4180926488_il2cpp_TypeInfo_var;
extern TypeInfo* ConstructorInfo_t3542137334_il2cpp_TypeInfo_var;
extern const uint32_t TypeBuilder_GetConstructorImpl_m4034724308_MetadataUsageId;
extern "C"  ConstructorInfo_t3542137334 * TypeBuilder_GetConstructorImpl_m4034724308 (TypeBuilder_t4287691406 * __this, int32_t ___bindingAttr, Binder_t4180926488 * ___binder, int32_t ___callConvention, TypeU5BU5D_t3431720054* ___types, ParameterModifierU5BU5D_t3379147067* ___modifiers, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TypeBuilder_GetConstructorImpl_m4034724308_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ConstructorBuilder_t1859087886 * V_0 = NULL;
	int32_t V_1 = 0;
	ConstructorBuilder_t1859087886 * V_2 = NULL;
	ConstructorBuilderU5BU5D_t281281019* V_3 = NULL;
	int32_t V_4 = 0;
	MethodBaseU5BU5D_t1767252801* V_5 = NULL;
	ConstructorInfo_t3542137334 * V_6 = NULL;
	ConstructorBuilderU5BU5D_t281281019* V_7 = NULL;
	int32_t V_8 = 0;
	{
		TypeBuilder_check_created_m3022374359(__this, /*hidden argument*/NULL);
		Type_t * L_0 = __this->get_created_25();
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Il2CppObject_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_0) == ((Il2CppObject*)(Type_t *)L_1))))
		{
			goto IL_0112;
		}
	}
	{
		ConstructorBuilderU5BU5D_t281281019* L_2 = __this->get_ctors_15();
		if (L_2)
		{
			goto IL_0028;
		}
	}
	{
		return (ConstructorInfo_t3542137334 *)NULL;
	}

IL_0028:
	{
		V_0 = (ConstructorBuilder_t1859087886 *)NULL;
		V_1 = 0;
		ConstructorBuilderU5BU5D_t281281019* L_3 = __this->get_ctors_15();
		V_3 = L_3;
		V_4 = 0;
		goto IL_0064;
	}

IL_003b:
	{
		ConstructorBuilderU5BU5D_t281281019* L_4 = V_3;
		int32_t L_5 = V_4;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		int32_t L_6 = L_5;
		V_2 = ((L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6)));
		int32_t L_7 = ___callConvention;
		if ((((int32_t)L_7) == ((int32_t)3)))
		{
			goto IL_0058;
		}
	}
	{
		ConstructorBuilder_t1859087886 * L_8 = V_2;
		NullCheck(L_8);
		int32_t L_9 = ConstructorBuilder_get_CallingConvention_m1759375919(L_8, /*hidden argument*/NULL);
		int32_t L_10 = ___callConvention;
		if ((((int32_t)L_9) == ((int32_t)L_10)))
		{
			goto IL_0058;
		}
	}
	{
		goto IL_005e;
	}

IL_0058:
	{
		ConstructorBuilder_t1859087886 * L_11 = V_2;
		V_0 = L_11;
		int32_t L_12 = V_1;
		V_1 = ((int32_t)((int32_t)L_12+(int32_t)1));
	}

IL_005e:
	{
		int32_t L_13 = V_4;
		V_4 = ((int32_t)((int32_t)L_13+(int32_t)1));
	}

IL_0064:
	{
		int32_t L_14 = V_4;
		ConstructorBuilderU5BU5D_t281281019* L_15 = V_3;
		NullCheck(L_15);
		if ((((int32_t)L_14) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_15)->max_length)))))))
		{
			goto IL_003b;
		}
	}
	{
		int32_t L_16 = V_1;
		if (L_16)
		{
			goto IL_0076;
		}
	}
	{
		return (ConstructorInfo_t3542137334 *)NULL;
	}

IL_0076:
	{
		TypeU5BU5D_t3431720054* L_17 = ___types;
		if (L_17)
		{
			goto IL_008c;
		}
	}
	{
		int32_t L_18 = V_1;
		if ((((int32_t)L_18) <= ((int32_t)1)))
		{
			goto IL_008a;
		}
	}
	{
		AmbiguousMatchException_t3876538212 * L_19 = (AmbiguousMatchException_t3876538212 *)il2cpp_codegen_object_new(AmbiguousMatchException_t3876538212_il2cpp_TypeInfo_var);
		AmbiguousMatchException__ctor_m3345667202(L_19, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_19);
	}

IL_008a:
	{
		ConstructorBuilder_t1859087886 * L_20 = V_0;
		return L_20;
	}

IL_008c:
	{
		int32_t L_21 = V_1;
		V_5 = ((MethodBaseU5BU5D_t1767252801*)SZArrayNew(MethodBaseU5BU5D_t1767252801_il2cpp_TypeInfo_var, (uint32_t)L_21));
		int32_t L_22 = V_1;
		if ((!(((uint32_t)L_22) == ((uint32_t)1))))
		{
			goto IL_00a5;
		}
	}
	{
		MethodBaseU5BU5D_t1767252801* L_23 = V_5;
		ConstructorBuilder_t1859087886 * L_24 = V_0;
		NullCheck(L_23);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_23, 0);
		ArrayElementTypeCheck (L_23, L_24);
		(L_23)->SetAt(static_cast<il2cpp_array_size_t>(0), (MethodBase_t3461000640 *)L_24);
		goto IL_00f2;
	}

IL_00a5:
	{
		V_1 = 0;
		ConstructorBuilderU5BU5D_t281281019* L_25 = __this->get_ctors_15();
		V_7 = L_25;
		V_8 = 0;
		goto IL_00e7;
	}

IL_00b7:
	{
		ConstructorBuilderU5BU5D_t281281019* L_26 = V_7;
		int32_t L_27 = V_8;
		NullCheck(L_26);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_26, L_27);
		int32_t L_28 = L_27;
		V_6 = ((L_26)->GetAt(static_cast<il2cpp_array_size_t>(L_28)));
		int32_t L_29 = ___callConvention;
		if ((((int32_t)L_29) == ((int32_t)3)))
		{
			goto IL_00d7;
		}
	}
	{
		ConstructorInfo_t3542137334 * L_30 = V_6;
		NullCheck(L_30);
		int32_t L_31 = VirtFuncInvoker0< int32_t >::Invoke(21 /* System.Reflection.CallingConventions System.Reflection.MethodBase::get_CallingConvention() */, L_30);
		int32_t L_32 = ___callConvention;
		if ((((int32_t)L_31) == ((int32_t)L_32)))
		{
			goto IL_00d7;
		}
	}
	{
		goto IL_00e1;
	}

IL_00d7:
	{
		MethodBaseU5BU5D_t1767252801* L_33 = V_5;
		int32_t L_34 = V_1;
		int32_t L_35 = L_34;
		V_1 = ((int32_t)((int32_t)L_35+(int32_t)1));
		ConstructorInfo_t3542137334 * L_36 = V_6;
		NullCheck(L_33);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_33, L_35);
		ArrayElementTypeCheck (L_33, L_36);
		(L_33)->SetAt(static_cast<il2cpp_array_size_t>(L_35), (MethodBase_t3461000640 *)L_36);
	}

IL_00e1:
	{
		int32_t L_37 = V_8;
		V_8 = ((int32_t)((int32_t)L_37+(int32_t)1));
	}

IL_00e7:
	{
		int32_t L_38 = V_8;
		ConstructorBuilderU5BU5D_t281281019* L_39 = V_7;
		NullCheck(L_39);
		if ((((int32_t)L_38) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_39)->max_length)))))))
		{
			goto IL_00b7;
		}
	}

IL_00f2:
	{
		Binder_t4180926488 * L_40 = ___binder;
		if (L_40)
		{
			goto IL_00ff;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Binder_t4180926488_il2cpp_TypeInfo_var);
		Binder_t4180926488 * L_41 = Binder_get_DefaultBinder_m695054407(NULL /*static, unused*/, /*hidden argument*/NULL);
		___binder = L_41;
	}

IL_00ff:
	{
		Binder_t4180926488 * L_42 = ___binder;
		int32_t L_43 = ___bindingAttr;
		MethodBaseU5BU5D_t1767252801* L_44 = V_5;
		TypeU5BU5D_t3431720054* L_45 = ___types;
		ParameterModifierU5BU5D_t3379147067* L_46 = ___modifiers;
		NullCheck(L_42);
		MethodBase_t3461000640 * L_47 = VirtFuncInvoker4< MethodBase_t3461000640 *, int32_t, MethodBaseU5BU5D_t1767252801*, TypeU5BU5D_t3431720054*, ParameterModifierU5BU5D_t3379147067* >::Invoke(7 /* System.Reflection.MethodBase System.Reflection.Binder::SelectMethod(System.Reflection.BindingFlags,System.Reflection.MethodBase[],System.Type[],System.Reflection.ParameterModifier[]) */, L_42, L_43, L_44, L_45, L_46);
		return ((ConstructorInfo_t3542137334 *)CastclassClass(L_47, ConstructorInfo_t3542137334_il2cpp_TypeInfo_var));
	}

IL_0112:
	{
		Type_t * L_48 = __this->get_created_25();
		int32_t L_49 = ___bindingAttr;
		Binder_t4180926488 * L_50 = ___binder;
		int32_t L_51 = ___callConvention;
		TypeU5BU5D_t3431720054* L_52 = ___types;
		ParameterModifierU5BU5D_t3379147067* L_53 = ___modifiers;
		NullCheck(L_48);
		ConstructorInfo_t3542137334 * L_54 = VirtFuncInvoker5< ConstructorInfo_t3542137334 *, int32_t, Binder_t4180926488 *, int32_t, TypeU5BU5D_t3431720054*, ParameterModifierU5BU5D_t3379147067* >::Invoke(75 /* System.Reflection.ConstructorInfo System.Type::GetConstructor(System.Reflection.BindingFlags,System.Reflection.Binder,System.Reflection.CallingConventions,System.Type[],System.Reflection.ParameterModifier[]) */, L_48, L_49, L_50, L_51, L_52, L_53);
		return L_54;
	}
}
// System.Boolean System.Reflection.Emit.TypeBuilder::IsDefined(System.Type,System.Boolean)
extern TypeInfo* NotSupportedException_t1374155497_il2cpp_TypeInfo_var;
extern TypeInfo* MonoCustomAttrs_t1707908163_il2cpp_TypeInfo_var;
extern const uint32_t TypeBuilder_IsDefined_m3802777959_MetadataUsageId;
extern "C"  bool TypeBuilder_IsDefined_m3802777959 (TypeBuilder_t4287691406 * __this, Type_t * ___attributeType, bool ___inherit, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TypeBuilder_IsDefined_m3802777959_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = TypeBuilder_get_is_created_m695624602(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_001c;
		}
	}
	{
		bool L_1 = TypeBuilder_get_IsCompilerContext_m432935841(__this, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_001c;
		}
	}
	{
		NotSupportedException_t1374155497 * L_2 = (NotSupportedException_t1374155497 *)il2cpp_codegen_object_new(NotSupportedException_t1374155497_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_2, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_001c:
	{
		Type_t * L_3 = ___attributeType;
		bool L_4 = ___inherit;
		IL2CPP_RUNTIME_CLASS_INIT(MonoCustomAttrs_t1707908163_il2cpp_TypeInfo_var);
		bool L_5 = MonoCustomAttrs_IsDefined_m1052740451(NULL /*static, unused*/, __this, L_3, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Object[] System.Reflection.Emit.TypeBuilder::GetCustomAttributes(System.Boolean)
extern "C"  ObjectU5BU5D_t11523773* TypeBuilder_GetCustomAttributes_m425271400 (TypeBuilder_t4287691406 * __this, bool ___inherit, const MethodInfo* method)
{
	{
		TypeBuilder_check_created_m3022374359(__this, /*hidden argument*/NULL);
		Type_t * L_0 = __this->get_created_25();
		bool L_1 = ___inherit;
		NullCheck(L_0);
		ObjectU5BU5D_t11523773* L_2 = VirtFuncInvoker1< ObjectU5BU5D_t11523773*, bool >::Invoke(12 /* System.Object[] System.Reflection.MemberInfo::GetCustomAttributes(System.Boolean) */, L_0, L_1);
		return L_2;
	}
}
// System.Object[] System.Reflection.Emit.TypeBuilder::GetCustomAttributes(System.Type,System.Boolean)
extern "C"  ObjectU5BU5D_t11523773* TypeBuilder_GetCustomAttributes_m3833453653 (TypeBuilder_t4287691406 * __this, Type_t * ___attributeType, bool ___inherit, const MethodInfo* method)
{
	{
		TypeBuilder_check_created_m3022374359(__this, /*hidden argument*/NULL);
		Type_t * L_0 = __this->get_created_25();
		Type_t * L_1 = ___attributeType;
		bool L_2 = ___inherit;
		NullCheck(L_0);
		ObjectU5BU5D_t11523773* L_3 = VirtFuncInvoker2< ObjectU5BU5D_t11523773*, Type_t *, bool >::Invoke(13 /* System.Object[] System.Reflection.MemberInfo::GetCustomAttributes(System.Type,System.Boolean) */, L_0, L_1, L_2);
		return L_3;
	}
}
// System.Reflection.Emit.ConstructorBuilder System.Reflection.Emit.TypeBuilder::DefineConstructor(System.Reflection.MethodAttributes,System.Reflection.CallingConventions,System.Type[])
extern "C"  ConstructorBuilder_t1859087886 * TypeBuilder_DefineConstructor_m2901130055 (TypeBuilder_t4287691406 * __this, int32_t ___attributes, int32_t ___callingConvention, TypeU5BU5D_t3431720054* ___parameterTypes, const MethodInfo* method)
{
	{
		int32_t L_0 = ___attributes;
		int32_t L_1 = ___callingConvention;
		TypeU5BU5D_t3431720054* L_2 = ___parameterTypes;
		ConstructorBuilder_t1859087886 * L_3 = TypeBuilder_DefineConstructor_m467278373(__this, L_0, L_1, L_2, (TypeU5BU5DU5BU5D_t2239438067*)(TypeU5BU5DU5BU5D_t2239438067*)NULL, (TypeU5BU5DU5BU5D_t2239438067*)(TypeU5BU5DU5BU5D_t2239438067*)NULL, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Reflection.Emit.ConstructorBuilder System.Reflection.Emit.TypeBuilder::DefineConstructor(System.Reflection.MethodAttributes,System.Reflection.CallingConventions,System.Type[],System.Type[][],System.Type[][])
extern TypeInfo* ConstructorBuilder_t1859087886_il2cpp_TypeInfo_var;
extern TypeInfo* ConstructorBuilderU5BU5D_t281281019_il2cpp_TypeInfo_var;
extern const uint32_t TypeBuilder_DefineConstructor_m467278373_MetadataUsageId;
extern "C"  ConstructorBuilder_t1859087886 * TypeBuilder_DefineConstructor_m467278373 (TypeBuilder_t4287691406 * __this, int32_t ___attributes, int32_t ___callingConvention, TypeU5BU5D_t3431720054* ___parameterTypes, TypeU5BU5DU5BU5D_t2239438067* ___requiredCustomModifiers, TypeU5BU5DU5BU5D_t2239438067* ___optionalCustomModifiers, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TypeBuilder_DefineConstructor_m467278373_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ConstructorBuilder_t1859087886 * V_0 = NULL;
	ConstructorBuilderU5BU5D_t281281019* V_1 = NULL;
	{
		TypeBuilder_check_not_created_m4283406443(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___attributes;
		int32_t L_1 = ___callingConvention;
		TypeU5BU5D_t3431720054* L_2 = ___parameterTypes;
		TypeU5BU5DU5BU5D_t2239438067* L_3 = ___requiredCustomModifiers;
		TypeU5BU5DU5BU5D_t2239438067* L_4 = ___optionalCustomModifiers;
		ConstructorBuilder_t1859087886 * L_5 = (ConstructorBuilder_t1859087886 *)il2cpp_codegen_object_new(ConstructorBuilder_t1859087886_il2cpp_TypeInfo_var);
		ConstructorBuilder__ctor_m3776339611(L_5, __this, L_0, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		ConstructorBuilderU5BU5D_t281281019* L_6 = __this->get_ctors_15();
		if (!L_6)
		{
			goto IL_005a;
		}
	}
	{
		ConstructorBuilderU5BU5D_t281281019* L_7 = __this->get_ctors_15();
		NullCheck(L_7);
		V_1 = ((ConstructorBuilderU5BU5D_t281281019*)SZArrayNew(ConstructorBuilderU5BU5D_t281281019_il2cpp_TypeInfo_var, (uint32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_7)->max_length))))+(int32_t)1))));
		ConstructorBuilderU5BU5D_t281281019* L_8 = __this->get_ctors_15();
		ConstructorBuilderU5BU5D_t281281019* L_9 = V_1;
		ConstructorBuilderU5BU5D_t281281019* L_10 = __this->get_ctors_15();
		NullCheck(L_10);
		Array_Copy_m3799309042(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_8, (Il2CppArray *)(Il2CppArray *)L_9, (((int32_t)((int32_t)(((Il2CppArray *)L_10)->max_length)))), /*hidden argument*/NULL);
		ConstructorBuilderU5BU5D_t281281019* L_11 = V_1;
		ConstructorBuilderU5BU5D_t281281019* L_12 = __this->get_ctors_15();
		NullCheck(L_12);
		ConstructorBuilder_t1859087886 * L_13 = V_0;
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, (((int32_t)((int32_t)(((Il2CppArray *)L_12)->max_length)))));
		ArrayElementTypeCheck (L_11, L_13);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>((((int32_t)((int32_t)(((Il2CppArray *)L_12)->max_length))))), (ConstructorBuilder_t1859087886 *)L_13);
		ConstructorBuilderU5BU5D_t281281019* L_14 = V_1;
		__this->set_ctors_15(L_14);
		goto IL_006f;
	}

IL_005a:
	{
		__this->set_ctors_15(((ConstructorBuilderU5BU5D_t281281019*)SZArrayNew(ConstructorBuilderU5BU5D_t281281019_il2cpp_TypeInfo_var, (uint32_t)1)));
		ConstructorBuilderU5BU5D_t281281019* L_15 = __this->get_ctors_15();
		ConstructorBuilder_t1859087886 * L_16 = V_0;
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, 0);
		ArrayElementTypeCheck (L_15, L_16);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(0), (ConstructorBuilder_t1859087886 *)L_16);
	}

IL_006f:
	{
		ConstructorBuilder_t1859087886 * L_17 = V_0;
		return L_17;
	}
}
// System.Reflection.Emit.ConstructorBuilder System.Reflection.Emit.TypeBuilder::DefineDefaultConstructor(System.Reflection.MethodAttributes)
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* NotSupportedException_t1374155497_il2cpp_TypeInfo_var;
extern TypeInfo* OpCodes_t2031828562_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral364273558;
extern const uint32_t TypeBuilder_DefineDefaultConstructor_m4180060431_MetadataUsageId;
extern "C"  ConstructorBuilder_t1859087886 * TypeBuilder_DefineDefaultConstructor_m4180060431 (TypeBuilder_t4287691406 * __this, int32_t ___attributes, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TypeBuilder_DefineDefaultConstructor_m4180060431_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Type_t * V_0 = NULL;
	ConstructorInfo_t3542137334 * V_1 = NULL;
	ConstructorBuilder_t1859087886 * V_2 = NULL;
	ILGenerator_t3869071517 * V_3 = NULL;
	{
		Type_t * L_0 = __this->get_parent_10();
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		Type_t * L_1 = __this->get_parent_10();
		V_0 = L_1;
		goto IL_0028;
	}

IL_0017:
	{
		ModuleBuilder_t1058295580 * L_2 = __this->get_pmodule_21();
		NullCheck(L_2);
		AssemblyBuilder_t3642540642 * L_3 = L_2->get_assemblyb_14();
		NullCheck(L_3);
		Type_t * L_4 = L_3->get_corlib_object_type_25();
		V_0 = L_4;
	}

IL_0028:
	{
		Type_t * L_5 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		TypeU5BU5D_t3431720054* L_6 = ((Type_t_StaticFields*)Type_t_il2cpp_TypeInfo_var->static_fields)->get_EmptyTypes_3();
		NullCheck(L_5);
		ConstructorInfo_t3542137334 * L_7 = VirtFuncInvoker4< ConstructorInfo_t3542137334 *, int32_t, Binder_t4180926488 *, TypeU5BU5D_t3431720054*, ParameterModifierU5BU5D_t3379147067* >::Invoke(74 /* System.Reflection.ConstructorInfo System.Type::GetConstructor(System.Reflection.BindingFlags,System.Reflection.Binder,System.Type[],System.Reflection.ParameterModifier[]) */, L_5, ((int32_t)52), (Binder_t4180926488 *)NULL, L_6, (ParameterModifierU5BU5D_t3379147067*)(ParameterModifierU5BU5D_t3379147067*)NULL);
		V_1 = L_7;
		ConstructorInfo_t3542137334 * L_8 = V_1;
		if (L_8)
		{
			goto IL_0049;
		}
	}
	{
		NotSupportedException_t1374155497 * L_9 = (NotSupportedException_t1374155497 *)il2cpp_codegen_object_new(NotSupportedException_t1374155497_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m133757637(L_9, _stringLiteral364273558, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0049:
	{
		int32_t L_10 = ___attributes;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		TypeU5BU5D_t3431720054* L_11 = ((Type_t_StaticFields*)Type_t_il2cpp_TypeInfo_var->static_fields)->get_EmptyTypes_3();
		ConstructorBuilder_t1859087886 * L_12 = TypeBuilder_DefineConstructor_m2901130055(__this, L_10, 1, L_11, /*hidden argument*/NULL);
		V_2 = L_12;
		ConstructorBuilder_t1859087886 * L_13 = V_2;
		NullCheck(L_13);
		ILGenerator_t3869071517 * L_14 = ConstructorBuilder_GetILGenerator_m1097527344(L_13, /*hidden argument*/NULL);
		V_3 = L_14;
		ILGenerator_t3869071517 * L_15 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(OpCodes_t2031828562_il2cpp_TypeInfo_var);
		OpCode_t4028977979  L_16 = ((OpCodes_t2031828562_StaticFields*)OpCodes_t2031828562_il2cpp_TypeInfo_var->static_fields)->get_Ldarg_0_2();
		NullCheck(L_15);
		VirtActionInvoker1< OpCode_t4028977979  >::Invoke(7 /* System.Void System.Reflection.Emit.ILGenerator::Emit(System.Reflection.Emit.OpCode) */, L_15, L_16);
		ILGenerator_t3869071517 * L_17 = V_3;
		OpCode_t4028977979  L_18 = ((OpCodes_t2031828562_StaticFields*)OpCodes_t2031828562_il2cpp_TypeInfo_var->static_fields)->get_Call_39();
		ConstructorInfo_t3542137334 * L_19 = V_1;
		NullCheck(L_17);
		VirtActionInvoker2< OpCode_t4028977979 , ConstructorInfo_t3542137334 * >::Invoke(9 /* System.Void System.Reflection.Emit.ILGenerator::Emit(System.Reflection.Emit.OpCode,System.Reflection.ConstructorInfo) */, L_17, L_18, L_19);
		ILGenerator_t3869071517 * L_20 = V_3;
		OpCode_t4028977979  L_21 = ((OpCodes_t2031828562_StaticFields*)OpCodes_t2031828562_il2cpp_TypeInfo_var->static_fields)->get_Ret_41();
		NullCheck(L_20);
		VirtActionInvoker1< OpCode_t4028977979  >::Invoke(7 /* System.Void System.Reflection.Emit.ILGenerator::Emit(System.Reflection.Emit.OpCode) */, L_20, L_21);
		ConstructorBuilder_t1859087886 * L_22 = V_2;
		return L_22;
	}
}
// System.Void System.Reflection.Emit.TypeBuilder::append_method(System.Reflection.Emit.MethodBuilder)
extern TypeInfo* MethodBuilderU5BU5D_t3930099966_il2cpp_TypeInfo_var;
extern const uint32_t TypeBuilder_append_method_m2404188841_MetadataUsageId;
extern "C"  void TypeBuilder_append_method_m2404188841 (TypeBuilder_t4287691406 * __this, MethodBuilder_t765486855 * ___mb, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TypeBuilder_append_method_m2404188841_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	MethodBuilderU5BU5D_t3930099966* V_0 = NULL;
	{
		MethodBuilderU5BU5D_t3930099966* L_0 = __this->get_methods_14();
		if (!L_0)
		{
			goto IL_004c;
		}
	}
	{
		MethodBuilderU5BU5D_t3930099966* L_1 = __this->get_methods_14();
		NullCheck(L_1);
		int32_t L_2 = __this->get_num_methods_13();
		if ((!(((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length))))) == ((uint32_t)L_2))))
		{
			goto IL_0047;
		}
	}
	{
		MethodBuilderU5BU5D_t3930099966* L_3 = __this->get_methods_14();
		NullCheck(L_3);
		V_0 = ((MethodBuilderU5BU5D_t3930099966*)SZArrayNew(MethodBuilderU5BU5D_t3930099966_il2cpp_TypeInfo_var, (uint32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_3)->max_length))))*(int32_t)2))));
		MethodBuilderU5BU5D_t3930099966* L_4 = __this->get_methods_14();
		MethodBuilderU5BU5D_t3930099966* L_5 = V_0;
		int32_t L_6 = __this->get_num_methods_13();
		Array_Copy_m3799309042(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_4, (Il2CppArray *)(Il2CppArray *)L_5, L_6, /*hidden argument*/NULL);
		MethodBuilderU5BU5D_t3930099966* L_7 = V_0;
		__this->set_methods_14(L_7);
	}

IL_0047:
	{
		goto IL_0058;
	}

IL_004c:
	{
		__this->set_methods_14(((MethodBuilderU5BU5D_t3930099966*)SZArrayNew(MethodBuilderU5BU5D_t3930099966_il2cpp_TypeInfo_var, (uint32_t)1)));
	}

IL_0058:
	{
		MethodBuilderU5BU5D_t3930099966* L_8 = __this->get_methods_14();
		int32_t L_9 = __this->get_num_methods_13();
		MethodBuilder_t765486855 * L_10 = ___mb;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, L_9);
		ArrayElementTypeCheck (L_8, L_10);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(L_9), (MethodBuilder_t765486855 *)L_10);
		int32_t L_11 = __this->get_num_methods_13();
		__this->set_num_methods_13(((int32_t)((int32_t)L_11+(int32_t)1)));
		return;
	}
}
// System.Reflection.Emit.MethodBuilder System.Reflection.Emit.TypeBuilder::DefineMethod(System.String,System.Reflection.MethodAttributes,System.Type,System.Type[])
extern "C"  MethodBuilder_t765486855 * TypeBuilder_DefineMethod_m1271641900 (TypeBuilder_t4287691406 * __this, String_t* ___name, int32_t ___attributes, Type_t * ___returnType, TypeU5BU5D_t3431720054* ___parameterTypes, const MethodInfo* method)
{
	{
		String_t* L_0 = ___name;
		int32_t L_1 = ___attributes;
		Type_t * L_2 = ___returnType;
		TypeU5BU5D_t3431720054* L_3 = ___parameterTypes;
		MethodBuilder_t765486855 * L_4 = TypeBuilder_DefineMethod_m1021339424(__this, L_0, L_1, 1, L_2, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Reflection.Emit.MethodBuilder System.Reflection.Emit.TypeBuilder::DefineMethod(System.String,System.Reflection.MethodAttributes,System.Reflection.CallingConventions,System.Type,System.Type[])
extern "C"  MethodBuilder_t765486855 * TypeBuilder_DefineMethod_m1021339424 (TypeBuilder_t4287691406 * __this, String_t* ___name, int32_t ___attributes, int32_t ___callingConvention, Type_t * ___returnType, TypeU5BU5D_t3431720054* ___parameterTypes, const MethodInfo* method)
{
	{
		String_t* L_0 = ___name;
		int32_t L_1 = ___attributes;
		int32_t L_2 = ___callingConvention;
		Type_t * L_3 = ___returnType;
		TypeU5BU5D_t3431720054* L_4 = ___parameterTypes;
		MethodBuilder_t765486855 * L_5 = TypeBuilder_DefineMethod_m775963488(__this, L_0, L_1, L_2, L_3, (TypeU5BU5D_t3431720054*)(TypeU5BU5D_t3431720054*)NULL, (TypeU5BU5D_t3431720054*)(TypeU5BU5D_t3431720054*)NULL, L_4, (TypeU5BU5DU5BU5D_t2239438067*)(TypeU5BU5DU5BU5D_t2239438067*)NULL, (TypeU5BU5DU5BU5D_t2239438067*)(TypeU5BU5DU5BU5D_t2239438067*)NULL, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Reflection.Emit.MethodBuilder System.Reflection.Emit.TypeBuilder::DefineMethod(System.String,System.Reflection.MethodAttributes,System.Reflection.CallingConventions,System.Type,System.Type[],System.Type[],System.Type[],System.Type[][],System.Type[][])
extern TypeInfo* ArgumentException_t124305799_il2cpp_TypeInfo_var;
extern TypeInfo* MethodBuilder_t765486855_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3373707;
extern Il2CppCodeGenString* _stringLiteral4092230572;
extern const uint32_t TypeBuilder_DefineMethod_m775963488_MetadataUsageId;
extern "C"  MethodBuilder_t765486855 * TypeBuilder_DefineMethod_m775963488 (TypeBuilder_t4287691406 * __this, String_t* ___name, int32_t ___attributes, int32_t ___callingConvention, Type_t * ___returnType, TypeU5BU5D_t3431720054* ___returnTypeRequiredCustomModifiers, TypeU5BU5D_t3431720054* ___returnTypeOptionalCustomModifiers, TypeU5BU5D_t3431720054* ___parameterTypes, TypeU5BU5DU5BU5D_t2239438067* ___parameterTypeRequiredCustomModifiers, TypeU5BU5DU5BU5D_t2239438067* ___parameterTypeOptionalCustomModifiers, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TypeBuilder_DefineMethod_m775963488_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	MethodBuilder_t765486855 * V_0 = NULL;
	{
		String_t* L_0 = ___name;
		TypeBuilder_check_name_m566175072(__this, _stringLiteral3373707, L_0, /*hidden argument*/NULL);
		TypeBuilder_check_not_created_m4283406443(__this, /*hidden argument*/NULL);
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(27 /* System.Boolean System.Type::get_IsInterface() */, __this);
		if (!L_1)
		{
			goto IL_0046;
		}
	}
	{
		int32_t L_2 = ___attributes;
		if (!((int32_t)((int32_t)L_2&(int32_t)((int32_t)1024))))
		{
			goto IL_0032;
		}
	}
	{
		int32_t L_3 = ___attributes;
		if (((int32_t)((int32_t)L_3&(int32_t)((int32_t)64))))
		{
			goto IL_0046;
		}
	}

IL_0032:
	{
		int32_t L_4 = ___attributes;
		if (((int32_t)((int32_t)L_4&(int32_t)((int32_t)16))))
		{
			goto IL_0046;
		}
	}
	{
		ArgumentException_t124305799 * L_5 = (ArgumentException_t124305799 *)il2cpp_codegen_object_new(ArgumentException_t124305799_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3544856547(L_5, _stringLiteral4092230572, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0046:
	{
		Type_t * L_6 = ___returnType;
		if (L_6)
		{
			goto IL_005f;
		}
	}
	{
		ModuleBuilder_t1058295580 * L_7 = __this->get_pmodule_21();
		NullCheck(L_7);
		AssemblyBuilder_t3642540642 * L_8 = L_7->get_assemblyb_14();
		NullCheck(L_8);
		Type_t * L_9 = L_8->get_corlib_void_type_28();
		___returnType = L_9;
	}

IL_005f:
	{
		String_t* L_10 = ___name;
		int32_t L_11 = ___attributes;
		int32_t L_12 = ___callingConvention;
		Type_t * L_13 = ___returnType;
		TypeU5BU5D_t3431720054* L_14 = ___returnTypeRequiredCustomModifiers;
		TypeU5BU5D_t3431720054* L_15 = ___returnTypeOptionalCustomModifiers;
		TypeU5BU5D_t3431720054* L_16 = ___parameterTypes;
		TypeU5BU5DU5BU5D_t2239438067* L_17 = ___parameterTypeRequiredCustomModifiers;
		TypeU5BU5DU5BU5D_t2239438067* L_18 = ___parameterTypeOptionalCustomModifiers;
		MethodBuilder_t765486855 * L_19 = (MethodBuilder_t765486855 *)il2cpp_codegen_object_new(MethodBuilder_t765486855_il2cpp_TypeInfo_var);
		MethodBuilder__ctor_m531243935(L_19, __this, L_10, L_11, L_12, L_13, L_14, L_15, L_16, L_17, L_18, /*hidden argument*/NULL);
		V_0 = L_19;
		MethodBuilder_t765486855 * L_20 = V_0;
		TypeBuilder_append_method_m2404188841(__this, L_20, /*hidden argument*/NULL);
		MethodBuilder_t765486855 * L_21 = V_0;
		return L_21;
	}
}
// System.Void System.Reflection.Emit.TypeBuilder::DefineMethodOverride(System.Reflection.MethodInfo,System.Reflection.MethodInfo)
extern TypeInfo* ArgumentNullException_t3214793280_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentException_t124305799_il2cpp_TypeInfo_var;
extern TypeInfo* MethodBuilder_t765486855_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral596168561;
extern Il2CppCodeGenString* _stringLiteral3635316939;
extern Il2CppCodeGenString* _stringLiteral1594520192;
extern const uint32_t TypeBuilder_DefineMethodOverride_m608613282_MetadataUsageId;
extern "C"  void TypeBuilder_DefineMethodOverride_m608613282 (TypeBuilder_t4287691406 * __this, MethodInfo_t * ___methodInfoBody, MethodInfo_t * ___methodInfoDeclaration, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TypeBuilder_DefineMethodOverride_m608613282_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	MethodBuilder_t765486855 * V_0 = NULL;
	{
		MethodInfo_t * L_0 = ___methodInfoBody;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t3214793280 * L_1 = (ArgumentNullException_t3214793280 *)il2cpp_codegen_object_new(ArgumentNullException_t3214793280_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_1, _stringLiteral596168561, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		MethodInfo_t * L_2 = ___methodInfoDeclaration;
		if (L_2)
		{
			goto IL_0022;
		}
	}
	{
		ArgumentNullException_t3214793280 * L_3 = (ArgumentNullException_t3214793280 *)il2cpp_codegen_object_new(ArgumentNullException_t3214793280_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_3, _stringLiteral3635316939, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0022:
	{
		TypeBuilder_check_not_created_m4283406443(__this, /*hidden argument*/NULL);
		MethodInfo_t * L_4 = ___methodInfoBody;
		NullCheck(L_4);
		Type_t * L_5 = VirtFuncInvoker0< Type_t * >::Invoke(6 /* System.Type System.Reflection.MemberInfo::get_DeclaringType() */, L_4);
		if ((((Il2CppObject*)(Type_t *)L_5) == ((Il2CppObject*)(TypeBuilder_t4287691406 *)__this)))
		{
			goto IL_003f;
		}
	}
	{
		ArgumentException_t124305799 * L_6 = (ArgumentException_t124305799 *)il2cpp_codegen_object_new(ArgumentException_t124305799_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3544856547(L_6, _stringLiteral1594520192, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_6);
	}

IL_003f:
	{
		MethodInfo_t * L_7 = ___methodInfoBody;
		if (!((MethodBuilder_t765486855 *)IsInstSealed(L_7, MethodBuilder_t765486855_il2cpp_TypeInfo_var)))
		{
			goto IL_0058;
		}
	}
	{
		MethodInfo_t * L_8 = ___methodInfoBody;
		V_0 = ((MethodBuilder_t765486855 *)CastclassSealed(L_8, MethodBuilder_t765486855_il2cpp_TypeInfo_var));
		MethodBuilder_t765486855 * L_9 = V_0;
		MethodInfo_t * L_10 = ___methodInfoDeclaration;
		NullCheck(L_9);
		MethodBuilder_set_override_m3395137323(L_9, L_10, /*hidden argument*/NULL);
	}

IL_0058:
	{
		return;
	}
}
// System.Type System.Reflection.Emit.TypeBuilder::create_runtime_class(System.Reflection.Emit.TypeBuilder)
extern "C"  Type_t * TypeBuilder_create_runtime_class_m3021676378 (TypeBuilder_t4287691406 * __this, TypeBuilder_t4287691406 * ___tb, const MethodInfo* method)
{
	using namespace il2cpp::icalls;
	typedef Type_t * (*TypeBuilder_create_runtime_class_m3021676378_ftn) (TypeBuilder_t4287691406 *, TypeBuilder_t4287691406 *);
	return  ((TypeBuilder_create_runtime_class_m3021676378_ftn)mscorlib::System::Reflection::Emit::TypeBuilder::create_runtime_class) (__this, ___tb);
}
// System.Boolean System.Reflection.Emit.TypeBuilder::is_nested_in(System.Type)
extern "C"  bool TypeBuilder_is_nested_in_m2129219235 (TypeBuilder_t4287691406 * __this, Type_t * ___t, const MethodInfo* method)
{
	{
		goto IL_0016;
	}

IL_0005:
	{
		Type_t * L_0 = ___t;
		if ((!(((Il2CppObject*)(Type_t *)L_0) == ((Il2CppObject*)(TypeBuilder_t4287691406 *)__this))))
		{
			goto IL_000e;
		}
	}
	{
		return (bool)1;
	}

IL_000e:
	{
		Type_t * L_1 = ___t;
		NullCheck(L_1);
		Type_t * L_2 = VirtFuncInvoker0< Type_t * >::Invoke(6 /* System.Type System.Type::get_DeclaringType() */, L_1);
		___t = L_2;
	}

IL_0016:
	{
		Type_t * L_3 = ___t;
		if (L_3)
		{
			goto IL_0005;
		}
	}
	{
		return (bool)0;
	}
}
// System.Boolean System.Reflection.Emit.TypeBuilder::has_ctor_method()
extern TypeInfo* ConstructorInfo_t3542137334_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern const uint32_t TypeBuilder_has_ctor_method_m2530448875_MetadataUsageId;
extern "C"  bool TypeBuilder_has_ctor_method_m2530448875 (TypeBuilder_t4287691406 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TypeBuilder_has_ctor_method_m2530448875_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	MethodBuilder_t765486855 * V_2 = NULL;
	{
		V_0 = ((int32_t)6144);
		V_1 = 0;
		goto IL_003f;
	}

IL_000d:
	{
		MethodBuilderU5BU5D_t3930099966* L_0 = __this->get_methods_14();
		int32_t L_1 = V_1;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		int32_t L_2 = L_1;
		V_2 = ((L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2)));
		MethodBuilder_t765486855 * L_3 = V_2;
		NullCheck(L_3);
		String_t* L_4 = MethodBuilder_get_Name_m3413262028(L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(ConstructorInfo_t3542137334_il2cpp_TypeInfo_var);
		String_t* L_5 = ((ConstructorInfo_t3542137334_StaticFields*)ConstructorInfo_t3542137334_il2cpp_TypeInfo_var->static_fields)->get_ConstructorName_0();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_6 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_4, L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_003b;
		}
	}
	{
		MethodBuilder_t765486855 * L_7 = V_2;
		NullCheck(L_7);
		int32_t L_8 = MethodBuilder_get_Attributes_m2121429796(L_7, /*hidden argument*/NULL);
		int32_t L_9 = V_0;
		int32_t L_10 = V_0;
		if ((!(((uint32_t)((int32_t)((int32_t)L_8&(int32_t)L_9))) == ((uint32_t)L_10))))
		{
			goto IL_003b;
		}
	}
	{
		return (bool)1;
	}

IL_003b:
	{
		int32_t L_11 = V_1;
		V_1 = ((int32_t)((int32_t)L_11+(int32_t)1));
	}

IL_003f:
	{
		int32_t L_12 = V_1;
		int32_t L_13 = __this->get_num_methods_13();
		if ((((int32_t)L_12) < ((int32_t)L_13)))
		{
			goto IL_000d;
		}
	}
	{
		return (bool)0;
	}
}
// System.Type System.Reflection.Emit.TypeBuilder::CreateType()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* TypeBuilder_t4287691406_il2cpp_TypeInfo_var;
extern TypeInfo* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern TypeInfo* TypeLoadException_t4088653924_il2cpp_TypeInfo_var;
extern TypeInfo* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral86524790;
extern Il2CppCodeGenString* _stringLiteral3641216495;
extern Il2CppCodeGenString* _stringLiteral2675744938;
extern Il2CppCodeGenString* _stringLiteral1382317582;
extern Il2CppCodeGenString* _stringLiteral3828759318;
extern Il2CppCodeGenString* _stringLiteral2839848753;
extern const uint32_t TypeBuilder_CreateType_m1359739084_MetadataUsageId;
extern "C"  Type_t * TypeBuilder_CreateType_m1359739084 (TypeBuilder_t4287691406 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TypeBuilder_CreateType_m1359739084_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	FieldBuilder_t2184649998 * V_0 = NULL;
	FieldBuilderU5BU5D_t758429435* V_1 = NULL;
	int32_t V_2 = 0;
	Type_t * V_3 = NULL;
	TypeBuilder_t4287691406 * V_4 = NULL;
	bool V_5 = false;
	int32_t V_6 = 0;
	MethodBuilder_t765486855 * V_7 = NULL;
	ConstructorBuilder_t1859087886 * V_8 = NULL;
	ConstructorBuilderU5BU5D_t281281019* V_9 = NULL;
	int32_t V_10 = 0;
	{
		bool L_0 = __this->get_createTypeCalled_27();
		if (!L_0)
		{
			goto IL_0012;
		}
	}
	{
		Type_t * L_1 = __this->get_created_25();
		return L_1;
	}

IL_0012:
	{
		bool L_2 = VirtFuncInvoker0< bool >::Invoke(27 /* System.Boolean System.Type::get_IsInterface() */, __this);
		if (L_2)
		{
			goto IL_0069;
		}
	}
	{
		Type_t * L_3 = __this->get_parent_10();
		if (L_3)
		{
			goto IL_0069;
		}
	}
	{
		ModuleBuilder_t1058295580 * L_4 = __this->get_pmodule_21();
		NullCheck(L_4);
		AssemblyBuilder_t3642540642 * L_5 = L_4->get_assemblyb_14();
		NullCheck(L_5);
		Type_t * L_6 = L_5->get_corlib_object_type_25();
		if ((((Il2CppObject*)(TypeBuilder_t4287691406 *)__this) == ((Il2CppObject*)(Type_t *)L_6)))
		{
			goto IL_0069;
		}
	}
	{
		String_t* L_7 = TypeBuilder_get_FullName_m1311381250(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_8 = String_op_Inequality_m2125462205(NULL /*static, unused*/, L_7, _stringLiteral86524790, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0069;
		}
	}
	{
		ModuleBuilder_t1058295580 * L_9 = __this->get_pmodule_21();
		NullCheck(L_9);
		AssemblyBuilder_t3642540642 * L_10 = L_9->get_assemblyb_14();
		NullCheck(L_10);
		Type_t * L_11 = L_10->get_corlib_object_type_25();
		TypeBuilder_SetParent_m2226876871(__this, L_11, /*hidden argument*/NULL);
	}

IL_0069:
	{
		TypeBuilder_create_generic_class_m1633445577(__this, /*hidden argument*/NULL);
		FieldBuilderU5BU5D_t758429435* L_12 = __this->get_fields_17();
		if (!L_12)
		{
			goto IL_010c;
		}
	}
	{
		FieldBuilderU5BU5D_t758429435* L_13 = __this->get_fields_17();
		V_1 = L_13;
		V_2 = 0;
		goto IL_0103;
	}

IL_0088:
	{
		FieldBuilderU5BU5D_t758429435* L_14 = V_1;
		int32_t L_15 = V_2;
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, L_15);
		int32_t L_16 = L_15;
		V_0 = ((L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_16)));
		FieldBuilder_t2184649998 * L_17 = V_0;
		if (L_17)
		{
			goto IL_0097;
		}
	}
	{
		goto IL_00ff;
	}

IL_0097:
	{
		FieldBuilder_t2184649998 * L_18 = V_0;
		NullCheck(L_18);
		Type_t * L_19 = FieldBuilder_get_FieldType_m1219556799(L_18, /*hidden argument*/NULL);
		V_3 = L_19;
		FieldBuilder_t2184649998 * L_20 = V_0;
		NullCheck(L_20);
		bool L_21 = VirtFuncInvoker0< bool >::Invoke(19 /* System.Boolean System.Reflection.FieldInfo::get_IsStatic() */, L_20);
		if (L_21)
		{
			goto IL_00ff;
		}
	}
	{
		Type_t * L_22 = V_3;
		if (!((TypeBuilder_t4287691406 *)IsInstSealed(L_22, TypeBuilder_t4287691406_il2cpp_TypeInfo_var)))
		{
			goto IL_00ff;
		}
	}
	{
		Type_t * L_23 = V_3;
		NullCheck(L_23);
		bool L_24 = VirtFuncInvoker0< bool >::Invoke(33 /* System.Boolean System.Type::get_IsValueType() */, L_23);
		if (!L_24)
		{
			goto IL_00ff;
		}
	}
	{
		Type_t * L_25 = V_3;
		if ((((Il2CppObject*)(Type_t *)L_25) == ((Il2CppObject*)(TypeBuilder_t4287691406 *)__this)))
		{
			goto IL_00ff;
		}
	}
	{
		Type_t * L_26 = V_3;
		bool L_27 = TypeBuilder_is_nested_in_m2129219235(__this, L_26, /*hidden argument*/NULL);
		if (!L_27)
		{
			goto IL_00ff;
		}
	}
	{
		Type_t * L_28 = V_3;
		V_4 = ((TypeBuilder_t4287691406 *)CastclassSealed(L_28, TypeBuilder_t4287691406_il2cpp_TypeInfo_var));
		TypeBuilder_t4287691406 * L_29 = V_4;
		NullCheck(L_29);
		bool L_30 = TypeBuilder_get_is_created_m695624602(L_29, /*hidden argument*/NULL);
		if (L_30)
		{
			goto IL_00ff;
		}
	}
	{
		AppDomain_t1551247802 * L_31 = AppDomain_get_CurrentDomain_m3448347417(NULL /*static, unused*/, /*hidden argument*/NULL);
		TypeBuilder_t4287691406 * L_32 = V_4;
		NullCheck(L_31);
		AppDomain_DoTypeResolve_m2539304708(L_31, L_32, /*hidden argument*/NULL);
		TypeBuilder_t4287691406 * L_33 = V_4;
		NullCheck(L_33);
		bool L_34 = TypeBuilder_get_is_created_m695624602(L_33, /*hidden argument*/NULL);
		if (L_34)
		{
			goto IL_00ff;
		}
	}

IL_00ff:
	{
		int32_t L_35 = V_2;
		V_2 = ((int32_t)((int32_t)L_35+(int32_t)1));
	}

IL_0103:
	{
		int32_t L_36 = V_2;
		FieldBuilderU5BU5D_t758429435* L_37 = V_1;
		NullCheck(L_37);
		if ((((int32_t)L_36) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_37)->max_length)))))))
		{
			goto IL_0088;
		}
	}

IL_010c:
	{
		Type_t * L_38 = __this->get_parent_10();
		if (!L_38)
		{
			goto IL_0162;
		}
	}
	{
		Type_t * L_39 = __this->get_parent_10();
		NullCheck(L_39);
		bool L_40 = VirtFuncInvoker0< bool >::Invoke(31 /* System.Boolean System.Type::get_IsSealed() */, L_39);
		if (!L_40)
		{
			goto IL_0162;
		}
	}
	{
		ObjectU5BU5D_t11523773* L_41 = ((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_41);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_41, 0);
		ArrayElementTypeCheck (L_41, _stringLiteral3641216495);
		(L_41)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral3641216495);
		ObjectU5BU5D_t11523773* L_42 = L_41;
		String_t* L_43 = TypeBuilder_get_FullName_m1311381250(__this, /*hidden argument*/NULL);
		NullCheck(L_42);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_42, 1);
		ArrayElementTypeCheck (L_42, L_43);
		(L_42)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_43);
		ObjectU5BU5D_t11523773* L_44 = L_42;
		NullCheck(L_44);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_44, 2);
		ArrayElementTypeCheck (L_44, _stringLiteral2675744938);
		(L_44)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral2675744938);
		ObjectU5BU5D_t11523773* L_45 = L_44;
		Assembly_t1882292308 * L_46 = TypeBuilder_get_Assembly_m870988588(__this, /*hidden argument*/NULL);
		NullCheck(L_45);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_45, 3);
		ArrayElementTypeCheck (L_45, L_46);
		(L_45)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_46);
		ObjectU5BU5D_t11523773* L_47 = L_45;
		NullCheck(L_47);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_47, 4);
		ArrayElementTypeCheck (L_47, _stringLiteral1382317582);
		(L_47)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral1382317582);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_48 = String_Concat_m3016520001(NULL /*static, unused*/, L_47, /*hidden argument*/NULL);
		TypeLoadException_t4088653924 * L_49 = (TypeLoadException_t4088653924 *)il2cpp_codegen_object_new(TypeLoadException_t4088653924_il2cpp_TypeInfo_var);
		TypeLoadException__ctor_m249965312(L_49, L_48, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_49);
	}

IL_0162:
	{
		Type_t * L_50 = __this->get_parent_10();
		ModuleBuilder_t1058295580 * L_51 = __this->get_pmodule_21();
		NullCheck(L_51);
		AssemblyBuilder_t3642540642 * L_52 = L_51->get_assemblyb_14();
		NullCheck(L_52);
		Type_t * L_53 = L_52->get_corlib_enum_type_27();
		if ((!(((Il2CppObject*)(Type_t *)L_50) == ((Il2CppObject*)(Type_t *)L_53))))
		{
			goto IL_01c3;
		}
	}
	{
		MethodBuilderU5BU5D_t3930099966* L_54 = __this->get_methods_14();
		if (!L_54)
		{
			goto IL_01c3;
		}
	}
	{
		ObjectU5BU5D_t11523773* L_55 = ((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_55);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_55, 0);
		ArrayElementTypeCheck (L_55, _stringLiteral3641216495);
		(L_55)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral3641216495);
		ObjectU5BU5D_t11523773* L_56 = L_55;
		String_t* L_57 = TypeBuilder_get_FullName_m1311381250(__this, /*hidden argument*/NULL);
		NullCheck(L_56);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_56, 1);
		ArrayElementTypeCheck (L_56, L_57);
		(L_56)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_57);
		ObjectU5BU5D_t11523773* L_58 = L_56;
		NullCheck(L_58);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_58, 2);
		ArrayElementTypeCheck (L_58, _stringLiteral2675744938);
		(L_58)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral2675744938);
		ObjectU5BU5D_t11523773* L_59 = L_58;
		Assembly_t1882292308 * L_60 = TypeBuilder_get_Assembly_m870988588(__this, /*hidden argument*/NULL);
		NullCheck(L_59);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_59, 3);
		ArrayElementTypeCheck (L_59, L_60);
		(L_59)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_60);
		ObjectU5BU5D_t11523773* L_61 = L_59;
		NullCheck(L_61);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_61, 4);
		ArrayElementTypeCheck (L_61, _stringLiteral3828759318);
		(L_61)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral3828759318);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_62 = String_Concat_m3016520001(NULL /*static, unused*/, L_61, /*hidden argument*/NULL);
		TypeLoadException_t4088653924 * L_63 = (TypeLoadException_t4088653924 *)il2cpp_codegen_object_new(TypeLoadException_t4088653924_il2cpp_TypeInfo_var);
		TypeLoadException__ctor_m249965312(L_63, L_62, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_63);
	}

IL_01c3:
	{
		MethodBuilderU5BU5D_t3930099966* L_64 = __this->get_methods_14();
		if (!L_64)
		{
			goto IL_0232;
		}
	}
	{
		bool L_65 = VirtFuncInvoker0< bool >::Invoke(20 /* System.Boolean System.Type::get_IsAbstract() */, __this);
		V_5 = (bool)((((int32_t)L_65) == ((int32_t)0))? 1 : 0);
		V_6 = 0;
		goto IL_0225;
	}

IL_01e1:
	{
		MethodBuilderU5BU5D_t3930099966* L_66 = __this->get_methods_14();
		int32_t L_67 = V_6;
		NullCheck(L_66);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_66, L_67);
		int32_t L_68 = L_67;
		V_7 = ((L_66)->GetAt(static_cast<il2cpp_array_size_t>(L_68)));
		bool L_69 = V_5;
		if (!L_69)
		{
			goto IL_0211;
		}
	}
	{
		MethodBuilder_t765486855 * L_70 = V_7;
		NullCheck(L_70);
		bool L_71 = VirtFuncInvoker0< bool >::Invoke(25 /* System.Boolean System.Reflection.MethodBase::get_IsAbstract() */, L_70);
		if (!L_71)
		{
			goto IL_0211;
		}
	}
	{
		MethodBuilder_t765486855 * L_72 = V_7;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_73 = String_Concat_m389863537(NULL /*static, unused*/, _stringLiteral2839848753, L_72, /*hidden argument*/NULL);
		InvalidOperationException_t2420574324 * L_74 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_74, L_73, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_74);
	}

IL_0211:
	{
		MethodBuilder_t765486855 * L_75 = V_7;
		NullCheck(L_75);
		MethodBuilder_check_override_m3740987736(L_75, /*hidden argument*/NULL);
		MethodBuilder_t765486855 * L_76 = V_7;
		NullCheck(L_76);
		MethodBuilder_fixup_m526432253(L_76, /*hidden argument*/NULL);
		int32_t L_77 = V_6;
		V_6 = ((int32_t)((int32_t)L_77+(int32_t)1));
	}

IL_0225:
	{
		int32_t L_78 = V_6;
		int32_t L_79 = __this->get_num_methods_13();
		if ((((int32_t)L_78) < ((int32_t)L_79)))
		{
			goto IL_01e1;
		}
	}

IL_0232:
	{
		bool L_80 = VirtFuncInvoker0< bool >::Invoke(27 /* System.Boolean System.Type::get_IsInterface() */, __this);
		if (L_80)
		{
			goto IL_0297;
		}
	}
	{
		bool L_81 = VirtFuncInvoker0< bool >::Invoke(33 /* System.Boolean System.Type::get_IsValueType() */, __this);
		if (L_81)
		{
			goto IL_0297;
		}
	}
	{
		ConstructorBuilderU5BU5D_t281281019* L_82 = __this->get_ctors_15();
		if (L_82)
		{
			goto IL_0297;
		}
	}
	{
		String_t* L_83 = __this->get_tname_8();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_84 = String_op_Inequality_m2125462205(NULL /*static, unused*/, L_83, _stringLiteral86524790, /*hidden argument*/NULL);
		if (!L_84)
		{
			goto IL_0297;
		}
	}
	{
		int32_t L_85 = TypeBuilder_GetAttributeFlagsImpl_m2415994065(__this, /*hidden argument*/NULL);
		if ((((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_85&(int32_t)((int32_t)128)))|(int32_t)((int32_t)256)))) == ((int32_t)((int32_t)384))))
		{
			goto IL_0297;
		}
	}
	{
		bool L_86 = TypeBuilder_has_ctor_method_m2530448875(__this, /*hidden argument*/NULL);
		if (L_86)
		{
			goto IL_0297;
		}
	}
	{
		TypeBuilder_DefineDefaultConstructor_m4180060431(__this, 6, /*hidden argument*/NULL);
	}

IL_0297:
	{
		ConstructorBuilderU5BU5D_t281281019* L_87 = __this->get_ctors_15();
		if (!L_87)
		{
			goto IL_02d1;
		}
	}
	{
		ConstructorBuilderU5BU5D_t281281019* L_88 = __this->get_ctors_15();
		V_9 = L_88;
		V_10 = 0;
		goto IL_02c6;
	}

IL_02b2:
	{
		ConstructorBuilderU5BU5D_t281281019* L_89 = V_9;
		int32_t L_90 = V_10;
		NullCheck(L_89);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_89, L_90);
		int32_t L_91 = L_90;
		V_8 = ((L_89)->GetAt(static_cast<il2cpp_array_size_t>(L_91)));
		ConstructorBuilder_t1859087886 * L_92 = V_8;
		NullCheck(L_92);
		ConstructorBuilder_fixup_m3991829194(L_92, /*hidden argument*/NULL);
		int32_t L_93 = V_10;
		V_10 = ((int32_t)((int32_t)L_93+(int32_t)1));
	}

IL_02c6:
	{
		int32_t L_94 = V_10;
		ConstructorBuilderU5BU5D_t281281019* L_95 = V_9;
		NullCheck(L_95);
		if ((((int32_t)L_94) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_95)->max_length)))))))
		{
			goto IL_02b2;
		}
	}

IL_02d1:
	{
		__this->set_createTypeCalled_27((bool)1);
		Type_t * L_96 = TypeBuilder_create_runtime_class_m3021676378(__this, __this, /*hidden argument*/NULL);
		__this->set_created_25(L_96);
		Type_t * L_97 = __this->get_created_25();
		if (!L_97)
		{
			goto IL_02f7;
		}
	}
	{
		Type_t * L_98 = __this->get_created_25();
		return L_98;
	}

IL_02f7:
	{
		return __this;
	}
}
// System.Reflection.ConstructorInfo[] System.Reflection.Emit.TypeBuilder::GetConstructors(System.Reflection.BindingFlags)
extern TypeInfo* NotSupportedException_t1374155497_il2cpp_TypeInfo_var;
extern const uint32_t TypeBuilder_GetConstructors_m3659149026_MetadataUsageId;
extern "C"  ConstructorInfoU5BU5D_t3572023667* TypeBuilder_GetConstructors_m3659149026 (TypeBuilder_t4287691406 * __this, int32_t ___bindingAttr, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TypeBuilder_GetConstructors_m3659149026_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = TypeBuilder_get_is_created_m695624602(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0018;
		}
	}
	{
		Type_t * L_1 = __this->get_created_25();
		int32_t L_2 = ___bindingAttr;
		NullCheck(L_1);
		ConstructorInfoU5BU5D_t3572023667* L_3 = VirtFuncInvoker1< ConstructorInfoU5BU5D_t3572023667*, int32_t >::Invoke(76 /* System.Reflection.ConstructorInfo[] System.Type::GetConstructors(System.Reflection.BindingFlags) */, L_1, L_2);
		return L_3;
	}

IL_0018:
	{
		bool L_4 = TypeBuilder_get_IsCompilerContext_m432935841(__this, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0029;
		}
	}
	{
		NotSupportedException_t1374155497 * L_5 = (NotSupportedException_t1374155497 *)il2cpp_codegen_object_new(NotSupportedException_t1374155497_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_5, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0029:
	{
		int32_t L_6 = ___bindingAttr;
		ConstructorInfoU5BU5D_t3572023667* L_7 = TypeBuilder_GetConstructorsInternal_m731270847(__this, L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
// System.Reflection.ConstructorInfo[] System.Reflection.Emit.TypeBuilder::GetConstructorsInternal(System.Reflection.BindingFlags)
extern TypeInfo* ConstructorInfoU5BU5D_t3572023667_il2cpp_TypeInfo_var;
extern TypeInfo* ArrayList_t2121638921_il2cpp_TypeInfo_var;
extern const uint32_t TypeBuilder_GetConstructorsInternal_m731270847_MetadataUsageId;
extern "C"  ConstructorInfoU5BU5D_t3572023667* TypeBuilder_GetConstructorsInternal_m731270847 (TypeBuilder_t4287691406 * __this, int32_t ___bindingAttr, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TypeBuilder_GetConstructorsInternal_m731270847_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ArrayList_t2121638921 * V_0 = NULL;
	bool V_1 = false;
	int32_t V_2 = 0;
	ConstructorBuilder_t1859087886 * V_3 = NULL;
	ConstructorBuilderU5BU5D_t281281019* V_4 = NULL;
	int32_t V_5 = 0;
	ConstructorInfoU5BU5D_t3572023667* V_6 = NULL;
	{
		ConstructorBuilderU5BU5D_t281281019* L_0 = __this->get_ctors_15();
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		return ((ConstructorInfoU5BU5D_t3572023667*)SZArrayNew(ConstructorInfoU5BU5D_t3572023667_il2cpp_TypeInfo_var, (uint32_t)0));
	}

IL_0012:
	{
		ArrayList_t2121638921 * L_1 = (ArrayList_t2121638921 *)il2cpp_codegen_object_new(ArrayList_t2121638921_il2cpp_TypeInfo_var);
		ArrayList__ctor_m1878432947(L_1, /*hidden argument*/NULL);
		V_0 = L_1;
		ConstructorBuilderU5BU5D_t281281019* L_2 = __this->get_ctors_15();
		V_4 = L_2;
		V_5 = 0;
		goto IL_00a3;
	}

IL_0028:
	{
		ConstructorBuilderU5BU5D_t281281019* L_3 = V_4;
		int32_t L_4 = V_5;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		V_3 = ((L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5)));
		V_1 = (bool)0;
		ConstructorBuilder_t1859087886 * L_6 = V_3;
		NullCheck(L_6);
		int32_t L_7 = ConstructorBuilder_get_Attributes_m1523127289(L_6, /*hidden argument*/NULL);
		V_2 = L_7;
		int32_t L_8 = V_2;
		if ((!(((uint32_t)((int32_t)((int32_t)L_8&(int32_t)7))) == ((uint32_t)6))))
		{
			goto IL_0050;
		}
	}
	{
		int32_t L_9 = ___bindingAttr;
		if (!((int32_t)((int32_t)L_9&(int32_t)((int32_t)16))))
		{
			goto IL_004b;
		}
	}
	{
		V_1 = (bool)1;
	}

IL_004b:
	{
		goto IL_005b;
	}

IL_0050:
	{
		int32_t L_10 = ___bindingAttr;
		if (!((int32_t)((int32_t)L_10&(int32_t)((int32_t)32))))
		{
			goto IL_005b;
		}
	}
	{
		V_1 = (bool)1;
	}

IL_005b:
	{
		bool L_11 = V_1;
		if (L_11)
		{
			goto IL_0066;
		}
	}
	{
		goto IL_009d;
	}

IL_0066:
	{
		V_1 = (bool)0;
		int32_t L_12 = V_2;
		if (!((int32_t)((int32_t)L_12&(int32_t)((int32_t)16))))
		{
			goto IL_0080;
		}
	}
	{
		int32_t L_13 = ___bindingAttr;
		if (!((int32_t)((int32_t)L_13&(int32_t)8)))
		{
			goto IL_007b;
		}
	}
	{
		V_1 = (bool)1;
	}

IL_007b:
	{
		goto IL_008a;
	}

IL_0080:
	{
		int32_t L_14 = ___bindingAttr;
		if (!((int32_t)((int32_t)L_14&(int32_t)4)))
		{
			goto IL_008a;
		}
	}
	{
		V_1 = (bool)1;
	}

IL_008a:
	{
		bool L_15 = V_1;
		if (L_15)
		{
			goto IL_0095;
		}
	}
	{
		goto IL_009d;
	}

IL_0095:
	{
		ArrayList_t2121638921 * L_16 = V_0;
		ConstructorBuilder_t1859087886 * L_17 = V_3;
		NullCheck(L_16);
		VirtFuncInvoker1< int32_t, Il2CppObject * >::Invoke(30 /* System.Int32 System.Collections.ArrayList::Add(System.Object) */, L_16, L_17);
	}

IL_009d:
	{
		int32_t L_18 = V_5;
		V_5 = ((int32_t)((int32_t)L_18+(int32_t)1));
	}

IL_00a3:
	{
		int32_t L_19 = V_5;
		ConstructorBuilderU5BU5D_t281281019* L_20 = V_4;
		NullCheck(L_20);
		if ((((int32_t)L_19) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_20)->max_length)))))))
		{
			goto IL_0028;
		}
	}
	{
		ArrayList_t2121638921 * L_21 = V_0;
		NullCheck(L_21);
		int32_t L_22 = VirtFuncInvoker0< int32_t >::Invoke(23 /* System.Int32 System.Collections.ArrayList::get_Count() */, L_21);
		V_6 = ((ConstructorInfoU5BU5D_t3572023667*)SZArrayNew(ConstructorInfoU5BU5D_t3572023667_il2cpp_TypeInfo_var, (uint32_t)L_22));
		ArrayList_t2121638921 * L_23 = V_0;
		ConstructorInfoU5BU5D_t3572023667* L_24 = V_6;
		NullCheck(L_23);
		VirtActionInvoker1< Il2CppArray * >::Invoke(40 /* System.Void System.Collections.ArrayList::CopyTo(System.Array) */, L_23, (Il2CppArray *)(Il2CppArray *)L_24);
		ConstructorInfoU5BU5D_t3572023667* L_25 = V_6;
		return L_25;
	}
}
// System.Type System.Reflection.Emit.TypeBuilder::GetElementType()
extern TypeInfo* NotSupportedException_t1374155497_il2cpp_TypeInfo_var;
extern const uint32_t TypeBuilder_GetElementType_m1124741270_MetadataUsageId;
extern "C"  Type_t * TypeBuilder_GetElementType_m1124741270 (TypeBuilder_t4287691406 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TypeBuilder_GetElementType_m1124741270_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1374155497 * L_0 = (NotSupportedException_t1374155497 *)il2cpp_codegen_object_new(NotSupportedException_t1374155497_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Reflection.EventInfo System.Reflection.Emit.TypeBuilder::GetEvent(System.String,System.Reflection.BindingFlags)
extern "C"  EventInfo_t * TypeBuilder_GetEvent_m1147005997 (TypeBuilder_t4287691406 * __this, String_t* ___name, int32_t ___bindingAttr, const MethodInfo* method)
{
	{
		TypeBuilder_check_created_m3022374359(__this, /*hidden argument*/NULL);
		Type_t * L_0 = __this->get_created_25();
		String_t* L_1 = ___name;
		int32_t L_2 = ___bindingAttr;
		NullCheck(L_0);
		EventInfo_t * L_3 = VirtFuncInvoker2< EventInfo_t *, String_t*, int32_t >::Invoke(45 /* System.Reflection.EventInfo System.Type::GetEvent(System.String,System.Reflection.BindingFlags) */, L_0, L_1, L_2);
		return L_3;
	}
}
// System.Reflection.FieldInfo System.Reflection.Emit.TypeBuilder::GetField(System.String,System.Reflection.BindingFlags)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern const uint32_t TypeBuilder_GetField_m950073389_MetadataUsageId;
extern "C"  FieldInfo_t * TypeBuilder_GetField_m950073389 (TypeBuilder_t4287691406 * __this, String_t* ___name, int32_t ___bindingAttr, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TypeBuilder_GetField_m950073389_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	int32_t V_1 = 0;
	FieldInfo_t * V_2 = NULL;
	FieldBuilderU5BU5D_t758429435* V_3 = NULL;
	int32_t V_4 = 0;
	{
		Type_t * L_0 = __this->get_created_25();
		if (!L_0)
		{
			goto IL_0019;
		}
	}
	{
		Type_t * L_1 = __this->get_created_25();
		String_t* L_2 = ___name;
		int32_t L_3 = ___bindingAttr;
		NullCheck(L_1);
		FieldInfo_t * L_4 = VirtFuncInvoker2< FieldInfo_t *, String_t*, int32_t >::Invoke(47 /* System.Reflection.FieldInfo System.Type::GetField(System.String,System.Reflection.BindingFlags) */, L_1, L_2, L_3);
		return L_4;
	}

IL_0019:
	{
		FieldBuilderU5BU5D_t758429435* L_5 = __this->get_fields_17();
		if (L_5)
		{
			goto IL_0026;
		}
	}
	{
		return (FieldInfo_t *)NULL;
	}

IL_0026:
	{
		FieldBuilderU5BU5D_t758429435* L_6 = __this->get_fields_17();
		V_3 = L_6;
		V_4 = 0;
		goto IL_00ca;
	}

IL_0035:
	{
		FieldBuilderU5BU5D_t758429435* L_7 = V_3;
		int32_t L_8 = V_4;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, L_8);
		int32_t L_9 = L_8;
		V_2 = ((L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9)));
		FieldInfo_t * L_10 = V_2;
		if (L_10)
		{
			goto IL_0045;
		}
	}
	{
		goto IL_00c4;
	}

IL_0045:
	{
		FieldInfo_t * L_11 = V_2;
		NullCheck(L_11);
		String_t* L_12 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_11);
		String_t* L_13 = ___name;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_14 = String_op_Inequality_m2125462205(NULL /*static, unused*/, L_12, L_13, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_005b;
		}
	}
	{
		goto IL_00c4;
	}

IL_005b:
	{
		V_0 = (bool)0;
		FieldInfo_t * L_15 = V_2;
		NullCheck(L_15);
		int32_t L_16 = VirtFuncInvoker0< int32_t >::Invoke(14 /* System.Reflection.FieldAttributes System.Reflection.FieldInfo::get_Attributes() */, L_15);
		V_1 = L_16;
		int32_t L_17 = V_1;
		if ((!(((uint32_t)((int32_t)((int32_t)L_17&(int32_t)7))) == ((uint32_t)6))))
		{
			goto IL_007d;
		}
	}
	{
		int32_t L_18 = ___bindingAttr;
		if (!((int32_t)((int32_t)L_18&(int32_t)((int32_t)16))))
		{
			goto IL_0078;
		}
	}
	{
		V_0 = (bool)1;
	}

IL_0078:
	{
		goto IL_0088;
	}

IL_007d:
	{
		int32_t L_19 = ___bindingAttr;
		if (!((int32_t)((int32_t)L_19&(int32_t)((int32_t)32))))
		{
			goto IL_0088;
		}
	}
	{
		V_0 = (bool)1;
	}

IL_0088:
	{
		bool L_20 = V_0;
		if (L_20)
		{
			goto IL_0093;
		}
	}
	{
		goto IL_00c4;
	}

IL_0093:
	{
		V_0 = (bool)0;
		int32_t L_21 = V_1;
		if (!((int32_t)((int32_t)L_21&(int32_t)((int32_t)16))))
		{
			goto IL_00ad;
		}
	}
	{
		int32_t L_22 = ___bindingAttr;
		if (!((int32_t)((int32_t)L_22&(int32_t)8)))
		{
			goto IL_00a8;
		}
	}
	{
		V_0 = (bool)1;
	}

IL_00a8:
	{
		goto IL_00b7;
	}

IL_00ad:
	{
		int32_t L_23 = ___bindingAttr;
		if (!((int32_t)((int32_t)L_23&(int32_t)4)))
		{
			goto IL_00b7;
		}
	}
	{
		V_0 = (bool)1;
	}

IL_00b7:
	{
		bool L_24 = V_0;
		if (L_24)
		{
			goto IL_00c2;
		}
	}
	{
		goto IL_00c4;
	}

IL_00c2:
	{
		FieldInfo_t * L_25 = V_2;
		return L_25;
	}

IL_00c4:
	{
		int32_t L_26 = V_4;
		V_4 = ((int32_t)((int32_t)L_26+(int32_t)1));
	}

IL_00ca:
	{
		int32_t L_27 = V_4;
		FieldBuilderU5BU5D_t758429435* L_28 = V_3;
		NullCheck(L_28);
		if ((((int32_t)L_27) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_28)->max_length)))))))
		{
			goto IL_0035;
		}
	}
	{
		return (FieldInfo_t *)NULL;
	}
}
// System.Reflection.FieldInfo[] System.Reflection.Emit.TypeBuilder::GetFields(System.Reflection.BindingFlags)
extern TypeInfo* FieldInfoU5BU5D_t1144794227_il2cpp_TypeInfo_var;
extern TypeInfo* ArrayList_t2121638921_il2cpp_TypeInfo_var;
extern const uint32_t TypeBuilder_GetFields_m3934792546_MetadataUsageId;
extern "C"  FieldInfoU5BU5D_t1144794227* TypeBuilder_GetFields_m3934792546 (TypeBuilder_t4287691406 * __this, int32_t ___bindingAttr, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TypeBuilder_GetFields_m3934792546_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ArrayList_t2121638921 * V_0 = NULL;
	bool V_1 = false;
	int32_t V_2 = 0;
	FieldInfo_t * V_3 = NULL;
	FieldBuilderU5BU5D_t758429435* V_4 = NULL;
	int32_t V_5 = 0;
	FieldInfoU5BU5D_t1144794227* V_6 = NULL;
	{
		Type_t * L_0 = __this->get_created_25();
		if (!L_0)
		{
			goto IL_0018;
		}
	}
	{
		Type_t * L_1 = __this->get_created_25();
		int32_t L_2 = ___bindingAttr;
		NullCheck(L_1);
		FieldInfoU5BU5D_t1144794227* L_3 = VirtFuncInvoker1< FieldInfoU5BU5D_t1144794227*, int32_t >::Invoke(48 /* System.Reflection.FieldInfo[] System.Type::GetFields(System.Reflection.BindingFlags) */, L_1, L_2);
		return L_3;
	}

IL_0018:
	{
		FieldBuilderU5BU5D_t758429435* L_4 = __this->get_fields_17();
		if (L_4)
		{
			goto IL_002a;
		}
	}
	{
		return ((FieldInfoU5BU5D_t1144794227*)SZArrayNew(FieldInfoU5BU5D_t1144794227_il2cpp_TypeInfo_var, (uint32_t)0));
	}

IL_002a:
	{
		ArrayList_t2121638921 * L_5 = (ArrayList_t2121638921 *)il2cpp_codegen_object_new(ArrayList_t2121638921_il2cpp_TypeInfo_var);
		ArrayList__ctor_m1878432947(L_5, /*hidden argument*/NULL);
		V_0 = L_5;
		FieldBuilderU5BU5D_t758429435* L_6 = __this->get_fields_17();
		V_4 = L_6;
		V_5 = 0;
		goto IL_00c6;
	}

IL_0040:
	{
		FieldBuilderU5BU5D_t758429435* L_7 = V_4;
		int32_t L_8 = V_5;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, L_8);
		int32_t L_9 = L_8;
		V_3 = ((L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9)));
		FieldInfo_t * L_10 = V_3;
		if (L_10)
		{
			goto IL_0051;
		}
	}
	{
		goto IL_00c0;
	}

IL_0051:
	{
		V_1 = (bool)0;
		FieldInfo_t * L_11 = V_3;
		NullCheck(L_11);
		int32_t L_12 = VirtFuncInvoker0< int32_t >::Invoke(14 /* System.Reflection.FieldAttributes System.Reflection.FieldInfo::get_Attributes() */, L_11);
		V_2 = L_12;
		int32_t L_13 = V_2;
		if ((!(((uint32_t)((int32_t)((int32_t)L_13&(int32_t)7))) == ((uint32_t)6))))
		{
			goto IL_0073;
		}
	}
	{
		int32_t L_14 = ___bindingAttr;
		if (!((int32_t)((int32_t)L_14&(int32_t)((int32_t)16))))
		{
			goto IL_006e;
		}
	}
	{
		V_1 = (bool)1;
	}

IL_006e:
	{
		goto IL_007e;
	}

IL_0073:
	{
		int32_t L_15 = ___bindingAttr;
		if (!((int32_t)((int32_t)L_15&(int32_t)((int32_t)32))))
		{
			goto IL_007e;
		}
	}
	{
		V_1 = (bool)1;
	}

IL_007e:
	{
		bool L_16 = V_1;
		if (L_16)
		{
			goto IL_0089;
		}
	}
	{
		goto IL_00c0;
	}

IL_0089:
	{
		V_1 = (bool)0;
		int32_t L_17 = V_2;
		if (!((int32_t)((int32_t)L_17&(int32_t)((int32_t)16))))
		{
			goto IL_00a3;
		}
	}
	{
		int32_t L_18 = ___bindingAttr;
		if (!((int32_t)((int32_t)L_18&(int32_t)8)))
		{
			goto IL_009e;
		}
	}
	{
		V_1 = (bool)1;
	}

IL_009e:
	{
		goto IL_00ad;
	}

IL_00a3:
	{
		int32_t L_19 = ___bindingAttr;
		if (!((int32_t)((int32_t)L_19&(int32_t)4)))
		{
			goto IL_00ad;
		}
	}
	{
		V_1 = (bool)1;
	}

IL_00ad:
	{
		bool L_20 = V_1;
		if (L_20)
		{
			goto IL_00b8;
		}
	}
	{
		goto IL_00c0;
	}

IL_00b8:
	{
		ArrayList_t2121638921 * L_21 = V_0;
		FieldInfo_t * L_22 = V_3;
		NullCheck(L_21);
		VirtFuncInvoker1< int32_t, Il2CppObject * >::Invoke(30 /* System.Int32 System.Collections.ArrayList::Add(System.Object) */, L_21, L_22);
	}

IL_00c0:
	{
		int32_t L_23 = V_5;
		V_5 = ((int32_t)((int32_t)L_23+(int32_t)1));
	}

IL_00c6:
	{
		int32_t L_24 = V_5;
		FieldBuilderU5BU5D_t758429435* L_25 = V_4;
		NullCheck(L_25);
		if ((((int32_t)L_24) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_25)->max_length)))))))
		{
			goto IL_0040;
		}
	}
	{
		ArrayList_t2121638921 * L_26 = V_0;
		NullCheck(L_26);
		int32_t L_27 = VirtFuncInvoker0< int32_t >::Invoke(23 /* System.Int32 System.Collections.ArrayList::get_Count() */, L_26);
		V_6 = ((FieldInfoU5BU5D_t1144794227*)SZArrayNew(FieldInfoU5BU5D_t1144794227_il2cpp_TypeInfo_var, (uint32_t)L_27));
		ArrayList_t2121638921 * L_28 = V_0;
		FieldInfoU5BU5D_t1144794227* L_29 = V_6;
		NullCheck(L_28);
		VirtActionInvoker1< Il2CppArray * >::Invoke(40 /* System.Void System.Collections.ArrayList::CopyTo(System.Array) */, L_28, (Il2CppArray *)(Il2CppArray *)L_29);
		FieldInfoU5BU5D_t1144794227* L_30 = V_6;
		return L_30;
	}
}
// System.Type[] System.Reflection.Emit.TypeBuilder::GetInterfaces()
extern TypeInfo* TypeU5BU5D_t3431720054_il2cpp_TypeInfo_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t TypeBuilder_GetInterfaces_m1695120734_MetadataUsageId;
extern "C"  TypeU5BU5D_t3431720054* TypeBuilder_GetInterfaces_m1695120734 (TypeBuilder_t4287691406 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TypeBuilder_GetInterfaces_m1695120734_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	TypeU5BU5D_t3431720054* V_0 = NULL;
	{
		bool L_0 = TypeBuilder_get_is_created_m695624602(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		Type_t * L_1 = __this->get_created_25();
		NullCheck(L_1);
		TypeU5BU5D_t3431720054* L_2 = VirtFuncInvoker0< TypeU5BU5D_t3431720054* >::Invoke(40 /* System.Type[] System.Type::GetInterfaces() */, L_1);
		return L_2;
	}

IL_0017:
	{
		TypeU5BU5D_t3431720054* L_3 = __this->get_interfaces_12();
		if (!L_3)
		{
			goto IL_003f;
		}
	}
	{
		TypeU5BU5D_t3431720054* L_4 = __this->get_interfaces_12();
		NullCheck(L_4);
		V_0 = ((TypeU5BU5D_t3431720054*)SZArrayNew(TypeU5BU5D_t3431720054_il2cpp_TypeInfo_var, (uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_4)->max_length))))));
		TypeU5BU5D_t3431720054* L_5 = __this->get_interfaces_12();
		TypeU5BU5D_t3431720054* L_6 = V_0;
		NullCheck((Il2CppArray *)(Il2CppArray *)L_5);
		VirtActionInvoker2< Il2CppArray *, int32_t >::Invoke(9 /* System.Void System.Array::CopyTo(System.Array,System.Int32) */, (Il2CppArray *)(Il2CppArray *)L_5, (Il2CppArray *)(Il2CppArray *)L_6, 0);
		TypeU5BU5D_t3431720054* L_7 = V_0;
		return L_7;
	}

IL_003f:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		TypeU5BU5D_t3431720054* L_8 = ((Type_t_StaticFields*)Type_t_il2cpp_TypeInfo_var->static_fields)->get_EmptyTypes_3();
		return L_8;
	}
}
// System.Reflection.MethodInfo[] System.Reflection.Emit.TypeBuilder::GetMethodsByName(System.String,System.Reflection.BindingFlags,System.Boolean,System.Type)
extern TypeInfo* ArrayList_t2121638921_il2cpp_TypeInfo_var;
extern TypeInfo* MethodInfoU5BU5D_t1668237648_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern const uint32_t TypeBuilder_GetMethodsByName_m2328060642_MetadataUsageId;
extern "C"  MethodInfoU5BU5D_t1668237648* TypeBuilder_GetMethodsByName_m2328060642 (TypeBuilder_t4287691406 * __this, String_t* ___name, int32_t ___bindingAttr, bool ___ignoreCase, Type_t * ___reflected_type, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TypeBuilder_GetMethodsByName_m2328060642_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	MethodInfoU5BU5D_t1668237648* V_0 = NULL;
	bool V_1 = false;
	int32_t V_2 = 0;
	MethodInfoU5BU5D_t1668237648* V_3 = NULL;
	ArrayList_t2121638921 * V_4 = NULL;
	bool V_5 = false;
	int32_t V_6 = 0;
	MethodInfo_t * V_7 = NULL;
	ArrayList_t2121638921 * V_8 = NULL;
	MethodInfo_t * V_9 = NULL;
	MethodInfoU5BU5D_t1668237648* V_10 = NULL;
	int32_t V_11 = 0;
	MethodInfoU5BU5D_t1668237648* V_12 = NULL;
	int32_t V_13 = 0;
	{
		int32_t L_0 = ___bindingAttr;
		if (((int32_t)((int32_t)L_0&(int32_t)2)))
		{
			goto IL_0142;
		}
	}
	{
		Type_t * L_1 = __this->get_parent_10();
		if (!L_1)
		{
			goto IL_0142;
		}
	}
	{
		Type_t * L_2 = __this->get_parent_10();
		int32_t L_3 = ___bindingAttr;
		NullCheck(L_2);
		MethodInfoU5BU5D_t1668237648* L_4 = VirtFuncInvoker1< MethodInfoU5BU5D_t1668237648*, int32_t >::Invoke(55 /* System.Reflection.MethodInfo[] System.Type::GetMethods(System.Reflection.BindingFlags) */, L_2, L_3);
		V_3 = L_4;
		MethodInfoU5BU5D_t1668237648* L_5 = V_3;
		NullCheck(L_5);
		ArrayList_t2121638921 * L_6 = (ArrayList_t2121638921 *)il2cpp_codegen_object_new(ArrayList_t2121638921_il2cpp_TypeInfo_var);
		ArrayList__ctor_m3809992068(L_6, (((int32_t)((int32_t)(((Il2CppArray *)L_5)->max_length)))), /*hidden argument*/NULL);
		V_4 = L_6;
		int32_t L_7 = ___bindingAttr;
		V_5 = (bool)((((int32_t)((((int32_t)((int32_t)((int32_t)L_7&(int32_t)((int32_t)64)))) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		V_6 = 0;
		goto IL_00dc;
	}

IL_003e:
	{
		MethodInfoU5BU5D_t1668237648* L_8 = V_3;
		int32_t L_9 = V_6;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, L_9);
		int32_t L_10 = L_9;
		V_7 = ((L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_10)));
		MethodInfo_t * L_11 = V_7;
		NullCheck(L_11);
		int32_t L_12 = VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Reflection.MethodAttributes System.Reflection.MethodBase::get_Attributes() */, L_11);
		V_2 = L_12;
		MethodInfo_t * L_13 = V_7;
		NullCheck(L_13);
		bool L_14 = VirtFuncInvoker0< bool >::Invoke(23 /* System.Boolean System.Reflection.MethodBase::get_IsStatic() */, L_13);
		if (!L_14)
		{
			goto IL_0064;
		}
	}
	{
		bool L_15 = V_5;
		if (L_15)
		{
			goto IL_0064;
		}
	}
	{
		goto IL_00d6;
	}

IL_0064:
	{
		int32_t L_16 = V_2;
		V_13 = ((int32_t)((int32_t)L_16&(int32_t)7));
		int32_t L_17 = V_13;
		if (((int32_t)((int32_t)L_17-(int32_t)1)) == 0)
		{
			goto IL_00af;
		}
		if (((int32_t)((int32_t)L_17-(int32_t)1)) == 1)
		{
			goto IL_00b6;
		}
		if (((int32_t)((int32_t)L_17-(int32_t)1)) == 2)
		{
			goto IL_009f;
		}
		if (((int32_t)((int32_t)L_17-(int32_t)1)) == 3)
		{
			goto IL_00b6;
		}
		if (((int32_t)((int32_t)L_17-(int32_t)1)) == 4)
		{
			goto IL_00b6;
		}
		if (((int32_t)((int32_t)L_17-(int32_t)1)) == 5)
		{
			goto IL_008f;
		}
	}
	{
		goto IL_00b6;
	}

IL_008f:
	{
		int32_t L_18 = ___bindingAttr;
		V_1 = (bool)((((int32_t)((((int32_t)((int32_t)((int32_t)L_18&(int32_t)((int32_t)16)))) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_00c6;
	}

IL_009f:
	{
		int32_t L_19 = ___bindingAttr;
		V_1 = (bool)((((int32_t)((((int32_t)((int32_t)((int32_t)L_19&(int32_t)((int32_t)32)))) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_00c6;
	}

IL_00af:
	{
		V_1 = (bool)0;
		goto IL_00c6;
	}

IL_00b6:
	{
		int32_t L_20 = ___bindingAttr;
		V_1 = (bool)((((int32_t)((((int32_t)((int32_t)((int32_t)L_20&(int32_t)((int32_t)32)))) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_00c6;
	}

IL_00c6:
	{
		bool L_21 = V_1;
		if (!L_21)
		{
			goto IL_00d6;
		}
	}
	{
		ArrayList_t2121638921 * L_22 = V_4;
		MethodInfo_t * L_23 = V_7;
		NullCheck(L_22);
		VirtFuncInvoker1< int32_t, Il2CppObject * >::Invoke(30 /* System.Int32 System.Collections.ArrayList::Add(System.Object) */, L_22, L_23);
	}

IL_00d6:
	{
		int32_t L_24 = V_6;
		V_6 = ((int32_t)((int32_t)L_24+(int32_t)1));
	}

IL_00dc:
	{
		int32_t L_25 = V_6;
		MethodInfoU5BU5D_t1668237648* L_26 = V_3;
		NullCheck(L_26);
		if ((((int32_t)L_25) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_26)->max_length)))))))
		{
			goto IL_003e;
		}
	}
	{
		MethodBuilderU5BU5D_t3930099966* L_27 = __this->get_methods_14();
		if (L_27)
		{
			goto IL_010b;
		}
	}
	{
		ArrayList_t2121638921 * L_28 = V_4;
		NullCheck(L_28);
		int32_t L_29 = VirtFuncInvoker0< int32_t >::Invoke(23 /* System.Int32 System.Collections.ArrayList::get_Count() */, L_28);
		V_0 = ((MethodInfoU5BU5D_t1668237648*)SZArrayNew(MethodInfoU5BU5D_t1668237648_il2cpp_TypeInfo_var, (uint32_t)L_29));
		ArrayList_t2121638921 * L_30 = V_4;
		MethodInfoU5BU5D_t1668237648* L_31 = V_0;
		NullCheck(L_30);
		VirtActionInvoker1< Il2CppArray * >::Invoke(40 /* System.Void System.Collections.ArrayList::CopyTo(System.Array) */, L_30, (Il2CppArray *)(Il2CppArray *)L_31);
		goto IL_013d;
	}

IL_010b:
	{
		MethodBuilderU5BU5D_t3930099966* L_32 = __this->get_methods_14();
		NullCheck(L_32);
		ArrayList_t2121638921 * L_33 = V_4;
		NullCheck(L_33);
		int32_t L_34 = VirtFuncInvoker0< int32_t >::Invoke(23 /* System.Int32 System.Collections.ArrayList::get_Count() */, L_33);
		V_0 = ((MethodInfoU5BU5D_t1668237648*)SZArrayNew(MethodInfoU5BU5D_t1668237648_il2cpp_TypeInfo_var, (uint32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_32)->max_length))))+(int32_t)L_34))));
		ArrayList_t2121638921 * L_35 = V_4;
		MethodInfoU5BU5D_t1668237648* L_36 = V_0;
		NullCheck(L_35);
		VirtActionInvoker2< Il2CppArray *, int32_t >::Invoke(41 /* System.Void System.Collections.ArrayList::CopyTo(System.Array,System.Int32) */, L_35, (Il2CppArray *)(Il2CppArray *)L_36, 0);
		MethodBuilderU5BU5D_t3930099966* L_37 = __this->get_methods_14();
		MethodInfoU5BU5D_t1668237648* L_38 = V_0;
		ArrayList_t2121638921 * L_39 = V_4;
		NullCheck(L_39);
		int32_t L_40 = VirtFuncInvoker0< int32_t >::Invoke(23 /* System.Int32 System.Collections.ArrayList::get_Count() */, L_39);
		NullCheck((Il2CppArray *)(Il2CppArray *)L_37);
		VirtActionInvoker2< Il2CppArray *, int32_t >::Invoke(9 /* System.Void System.Array::CopyTo(System.Array,System.Int32) */, (Il2CppArray *)(Il2CppArray *)L_37, (Il2CppArray *)(Il2CppArray *)L_38, L_40);
	}

IL_013d:
	{
		goto IL_0149;
	}

IL_0142:
	{
		MethodBuilderU5BU5D_t3930099966* L_41 = __this->get_methods_14();
		V_0 = (MethodInfoU5BU5D_t1668237648*)L_41;
	}

IL_0149:
	{
		MethodInfoU5BU5D_t1668237648* L_42 = V_0;
		if (L_42)
		{
			goto IL_0156;
		}
	}
	{
		return ((MethodInfoU5BU5D_t1668237648*)SZArrayNew(MethodInfoU5BU5D_t1668237648_il2cpp_TypeInfo_var, (uint32_t)0));
	}

IL_0156:
	{
		ArrayList_t2121638921 * L_43 = (ArrayList_t2121638921 *)il2cpp_codegen_object_new(ArrayList_t2121638921_il2cpp_TypeInfo_var);
		ArrayList__ctor_m1878432947(L_43, /*hidden argument*/NULL);
		V_8 = L_43;
		MethodInfoU5BU5D_t1668237648* L_44 = V_0;
		V_10 = L_44;
		V_11 = 0;
		goto IL_0211;
	}

IL_0168:
	{
		MethodInfoU5BU5D_t1668237648* L_45 = V_10;
		int32_t L_46 = V_11;
		NullCheck(L_45);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_45, L_46);
		int32_t L_47 = L_46;
		V_9 = ((L_45)->GetAt(static_cast<il2cpp_array_size_t>(L_47)));
		MethodInfo_t * L_48 = V_9;
		if (L_48)
		{
			goto IL_017b;
		}
	}
	{
		goto IL_020b;
	}

IL_017b:
	{
		String_t* L_49 = ___name;
		if (!L_49)
		{
			goto IL_0199;
		}
	}
	{
		MethodInfo_t * L_50 = V_9;
		NullCheck(L_50);
		String_t* L_51 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_50);
		String_t* L_52 = ___name;
		bool L_53 = ___ignoreCase;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		int32_t L_54 = String_Compare_m1309590114(NULL /*static, unused*/, L_51, L_52, L_53, /*hidden argument*/NULL);
		if (!L_54)
		{
			goto IL_0199;
		}
	}
	{
		goto IL_020b;
	}

IL_0199:
	{
		V_1 = (bool)0;
		MethodInfo_t * L_55 = V_9;
		NullCheck(L_55);
		int32_t L_56 = VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Reflection.MethodAttributes System.Reflection.MethodBase::get_Attributes() */, L_55);
		V_2 = L_56;
		int32_t L_57 = V_2;
		if ((!(((uint32_t)((int32_t)((int32_t)L_57&(int32_t)7))) == ((uint32_t)6))))
		{
			goto IL_01bc;
		}
	}
	{
		int32_t L_58 = ___bindingAttr;
		if (!((int32_t)((int32_t)L_58&(int32_t)((int32_t)16))))
		{
			goto IL_01b7;
		}
	}
	{
		V_1 = (bool)1;
	}

IL_01b7:
	{
		goto IL_01c7;
	}

IL_01bc:
	{
		int32_t L_59 = ___bindingAttr;
		if (!((int32_t)((int32_t)L_59&(int32_t)((int32_t)32))))
		{
			goto IL_01c7;
		}
	}
	{
		V_1 = (bool)1;
	}

IL_01c7:
	{
		bool L_60 = V_1;
		if (L_60)
		{
			goto IL_01d2;
		}
	}
	{
		goto IL_020b;
	}

IL_01d2:
	{
		V_1 = (bool)0;
		int32_t L_61 = V_2;
		if (!((int32_t)((int32_t)L_61&(int32_t)((int32_t)16))))
		{
			goto IL_01ec;
		}
	}
	{
		int32_t L_62 = ___bindingAttr;
		if (!((int32_t)((int32_t)L_62&(int32_t)8)))
		{
			goto IL_01e7;
		}
	}
	{
		V_1 = (bool)1;
	}

IL_01e7:
	{
		goto IL_01f6;
	}

IL_01ec:
	{
		int32_t L_63 = ___bindingAttr;
		if (!((int32_t)((int32_t)L_63&(int32_t)4)))
		{
			goto IL_01f6;
		}
	}
	{
		V_1 = (bool)1;
	}

IL_01f6:
	{
		bool L_64 = V_1;
		if (L_64)
		{
			goto IL_0201;
		}
	}
	{
		goto IL_020b;
	}

IL_0201:
	{
		ArrayList_t2121638921 * L_65 = V_8;
		MethodInfo_t * L_66 = V_9;
		NullCheck(L_65);
		VirtFuncInvoker1< int32_t, Il2CppObject * >::Invoke(30 /* System.Int32 System.Collections.ArrayList::Add(System.Object) */, L_65, L_66);
	}

IL_020b:
	{
		int32_t L_67 = V_11;
		V_11 = ((int32_t)((int32_t)L_67+(int32_t)1));
	}

IL_0211:
	{
		int32_t L_68 = V_11;
		MethodInfoU5BU5D_t1668237648* L_69 = V_10;
		NullCheck(L_69);
		if ((((int32_t)L_68) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_69)->max_length)))))))
		{
			goto IL_0168;
		}
	}
	{
		ArrayList_t2121638921 * L_70 = V_8;
		NullCheck(L_70);
		int32_t L_71 = VirtFuncInvoker0< int32_t >::Invoke(23 /* System.Int32 System.Collections.ArrayList::get_Count() */, L_70);
		V_12 = ((MethodInfoU5BU5D_t1668237648*)SZArrayNew(MethodInfoU5BU5D_t1668237648_il2cpp_TypeInfo_var, (uint32_t)L_71));
		ArrayList_t2121638921 * L_72 = V_8;
		MethodInfoU5BU5D_t1668237648* L_73 = V_12;
		NullCheck(L_72);
		VirtActionInvoker1< Il2CppArray * >::Invoke(40 /* System.Void System.Collections.ArrayList::CopyTo(System.Array) */, L_72, (Il2CppArray *)(Il2CppArray *)L_73);
		MethodInfoU5BU5D_t1668237648* L_74 = V_12;
		return L_74;
	}
}
// System.Reflection.MethodInfo[] System.Reflection.Emit.TypeBuilder::GetMethods(System.Reflection.BindingFlags)
extern "C"  MethodInfoU5BU5D_t1668237648* TypeBuilder_GetMethods_m3761501928 (TypeBuilder_t4287691406 * __this, int32_t ___bindingAttr, const MethodInfo* method)
{
	{
		int32_t L_0 = ___bindingAttr;
		MethodInfoU5BU5D_t1668237648* L_1 = TypeBuilder_GetMethodsByName_m2328060642(__this, (String_t*)NULL, L_0, (bool)0, __this, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Reflection.MethodInfo System.Reflection.Emit.TypeBuilder::GetMethodImpl(System.String,System.Reflection.BindingFlags,System.Reflection.Binder,System.Reflection.CallingConventions,System.Type[],System.Reflection.ParameterModifier[])
extern TypeInfo* MethodBaseU5BU5D_t1767252801_il2cpp_TypeInfo_var;
extern TypeInfo* Binder_t4180926488_il2cpp_TypeInfo_var;
extern TypeInfo* MethodInfo_t_il2cpp_TypeInfo_var;
extern const uint32_t TypeBuilder_GetMethodImpl_m2675220686_MetadataUsageId;
extern "C"  MethodInfo_t * TypeBuilder_GetMethodImpl_m2675220686 (TypeBuilder_t4287691406 * __this, String_t* ___name, int32_t ___bindingAttr, Binder_t4180926488 * ___binder, int32_t ___callConvention, TypeU5BU5D_t3431720054* ___types, ParameterModifierU5BU5D_t3379147067* ___modifiers, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TypeBuilder_GetMethodImpl_m2675220686_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	MethodInfoU5BU5D_t1668237648* V_1 = NULL;
	MethodInfo_t * V_2 = NULL;
	MethodBaseU5BU5D_t1767252801* V_3 = NULL;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	MethodInfo_t * V_6 = NULL;
	MethodInfoU5BU5D_t1668237648* V_7 = NULL;
	int32_t V_8 = 0;
	MethodInfo_t * V_9 = NULL;
	MethodInfoU5BU5D_t1668237648* V_10 = NULL;
	int32_t V_11 = 0;
	int32_t G_B3_0 = 0;
	{
		TypeBuilder_check_created_m3022374359(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___bindingAttr;
		V_0 = (bool)((((int32_t)((((int32_t)((int32_t)((int32_t)L_0&(int32_t)1))) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		String_t* L_1 = ___name;
		int32_t L_2 = ___bindingAttr;
		bool L_3 = V_0;
		MethodInfoU5BU5D_t1668237648* L_4 = TypeBuilder_GetMethodsByName_m2328060642(__this, L_1, L_2, L_3, __this, /*hidden argument*/NULL);
		V_1 = L_4;
		V_2 = (MethodInfo_t *)NULL;
		TypeU5BU5D_t3431720054* L_5 = ___types;
		if (!L_5)
		{
			goto IL_002d;
		}
	}
	{
		TypeU5BU5D_t3431720054* L_6 = ___types;
		NullCheck(L_6);
		G_B3_0 = (((int32_t)((int32_t)(((Il2CppArray *)L_6)->max_length))));
		goto IL_002e;
	}

IL_002d:
	{
		G_B3_0 = 0;
	}

IL_002e:
	{
		V_4 = G_B3_0;
		V_5 = 0;
		MethodInfoU5BU5D_t1668237648* L_7 = V_1;
		V_7 = L_7;
		V_8 = 0;
		goto IL_0072;
	}

IL_003e:
	{
		MethodInfoU5BU5D_t1668237648* L_8 = V_7;
		int32_t L_9 = V_8;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, L_9);
		int32_t L_10 = L_9;
		V_6 = ((L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_10)));
		int32_t L_11 = ___callConvention;
		if ((((int32_t)L_11) == ((int32_t)3)))
		{
			goto IL_0063;
		}
	}
	{
		MethodInfo_t * L_12 = V_6;
		NullCheck(L_12);
		int32_t L_13 = VirtFuncInvoker0< int32_t >::Invoke(21 /* System.Reflection.CallingConventions System.Reflection.MethodBase::get_CallingConvention() */, L_12);
		int32_t L_14 = ___callConvention;
		int32_t L_15 = ___callConvention;
		if ((((int32_t)((int32_t)((int32_t)L_13&(int32_t)L_14))) == ((int32_t)L_15)))
		{
			goto IL_0063;
		}
	}
	{
		goto IL_006c;
	}

IL_0063:
	{
		MethodInfo_t * L_16 = V_6;
		V_2 = L_16;
		int32_t L_17 = V_5;
		V_5 = ((int32_t)((int32_t)L_17+(int32_t)1));
	}

IL_006c:
	{
		int32_t L_18 = V_8;
		V_8 = ((int32_t)((int32_t)L_18+(int32_t)1));
	}

IL_0072:
	{
		int32_t L_19 = V_8;
		MethodInfoU5BU5D_t1668237648* L_20 = V_7;
		NullCheck(L_20);
		if ((((int32_t)L_19) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_20)->max_length)))))))
		{
			goto IL_003e;
		}
	}
	{
		int32_t L_21 = V_5;
		if (L_21)
		{
			goto IL_0086;
		}
	}
	{
		return (MethodInfo_t *)NULL;
	}

IL_0086:
	{
		int32_t L_22 = V_5;
		if ((!(((uint32_t)L_22) == ((uint32_t)1))))
		{
			goto IL_0097;
		}
	}
	{
		int32_t L_23 = V_4;
		if (L_23)
		{
			goto IL_0097;
		}
	}
	{
		MethodInfo_t * L_24 = V_2;
		return L_24;
	}

IL_0097:
	{
		int32_t L_25 = V_5;
		V_3 = ((MethodBaseU5BU5D_t1767252801*)SZArrayNew(MethodBaseU5BU5D_t1767252801_il2cpp_TypeInfo_var, (uint32_t)L_25));
		int32_t L_26 = V_5;
		if ((!(((uint32_t)L_26) == ((uint32_t)1))))
		{
			goto IL_00b0;
		}
	}
	{
		MethodBaseU5BU5D_t1767252801* L_27 = V_3;
		MethodInfo_t * L_28 = V_2;
		NullCheck(L_27);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_27, 0);
		ArrayElementTypeCheck (L_27, L_28);
		(L_27)->SetAt(static_cast<il2cpp_array_size_t>(0), (MethodBase_t3461000640 *)L_28);
		goto IL_00ff;
	}

IL_00b0:
	{
		V_5 = 0;
		MethodInfoU5BU5D_t1668237648* L_29 = V_1;
		V_10 = L_29;
		V_11 = 0;
		goto IL_00f4;
	}

IL_00be:
	{
		MethodInfoU5BU5D_t1668237648* L_30 = V_10;
		int32_t L_31 = V_11;
		NullCheck(L_30);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_30, L_31);
		int32_t L_32 = L_31;
		V_9 = ((L_30)->GetAt(static_cast<il2cpp_array_size_t>(L_32)));
		int32_t L_33 = ___callConvention;
		if ((((int32_t)L_33) == ((int32_t)3)))
		{
			goto IL_00e3;
		}
	}
	{
		MethodInfo_t * L_34 = V_9;
		NullCheck(L_34);
		int32_t L_35 = VirtFuncInvoker0< int32_t >::Invoke(21 /* System.Reflection.CallingConventions System.Reflection.MethodBase::get_CallingConvention() */, L_34);
		int32_t L_36 = ___callConvention;
		int32_t L_37 = ___callConvention;
		if ((((int32_t)((int32_t)((int32_t)L_35&(int32_t)L_36))) == ((int32_t)L_37)))
		{
			goto IL_00e3;
		}
	}
	{
		goto IL_00ee;
	}

IL_00e3:
	{
		MethodBaseU5BU5D_t1767252801* L_38 = V_3;
		int32_t L_39 = V_5;
		int32_t L_40 = L_39;
		V_5 = ((int32_t)((int32_t)L_40+(int32_t)1));
		MethodInfo_t * L_41 = V_9;
		NullCheck(L_38);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_38, L_40);
		ArrayElementTypeCheck (L_38, L_41);
		(L_38)->SetAt(static_cast<il2cpp_array_size_t>(L_40), (MethodBase_t3461000640 *)L_41);
	}

IL_00ee:
	{
		int32_t L_42 = V_11;
		V_11 = ((int32_t)((int32_t)L_42+(int32_t)1));
	}

IL_00f4:
	{
		int32_t L_43 = V_11;
		MethodInfoU5BU5D_t1668237648* L_44 = V_10;
		NullCheck(L_44);
		if ((((int32_t)L_43) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_44)->max_length)))))))
		{
			goto IL_00be;
		}
	}

IL_00ff:
	{
		TypeU5BU5D_t3431720054* L_45 = ___types;
		if (L_45)
		{
			goto IL_0112;
		}
	}
	{
		MethodBaseU5BU5D_t1767252801* L_46 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(Binder_t4180926488_il2cpp_TypeInfo_var);
		MethodBase_t3461000640 * L_47 = Binder_FindMostDerivedMatch_m1519815265(NULL /*static, unused*/, L_46, /*hidden argument*/NULL);
		return ((MethodInfo_t *)CastclassClass(L_47, MethodInfo_t_il2cpp_TypeInfo_var));
	}

IL_0112:
	{
		Binder_t4180926488 * L_48 = ___binder;
		if (L_48)
		{
			goto IL_011f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Binder_t4180926488_il2cpp_TypeInfo_var);
		Binder_t4180926488 * L_49 = Binder_get_DefaultBinder_m695054407(NULL /*static, unused*/, /*hidden argument*/NULL);
		___binder = L_49;
	}

IL_011f:
	{
		Binder_t4180926488 * L_50 = ___binder;
		int32_t L_51 = ___bindingAttr;
		MethodBaseU5BU5D_t1767252801* L_52 = V_3;
		TypeU5BU5D_t3431720054* L_53 = ___types;
		ParameterModifierU5BU5D_t3379147067* L_54 = ___modifiers;
		NullCheck(L_50);
		MethodBase_t3461000640 * L_55 = VirtFuncInvoker4< MethodBase_t3461000640 *, int32_t, MethodBaseU5BU5D_t1767252801*, TypeU5BU5D_t3431720054*, ParameterModifierU5BU5D_t3379147067* >::Invoke(7 /* System.Reflection.MethodBase System.Reflection.Binder::SelectMethod(System.Reflection.BindingFlags,System.Reflection.MethodBase[],System.Type[],System.Reflection.ParameterModifier[]) */, L_50, L_51, L_52, L_53, L_54);
		return ((MethodInfo_t *)CastclassClass(L_55, MethodInfo_t_il2cpp_TypeInfo_var));
	}
}
// System.Reflection.PropertyInfo[] System.Reflection.Emit.TypeBuilder::GetProperties(System.Reflection.BindingFlags)
extern TypeInfo* PropertyInfoU5BU5D_t1348579340_il2cpp_TypeInfo_var;
extern TypeInfo* ArrayList_t2121638921_il2cpp_TypeInfo_var;
extern const uint32_t TypeBuilder_GetProperties_m1011883015_MetadataUsageId;
extern "C"  PropertyInfoU5BU5D_t1348579340* TypeBuilder_GetProperties_m1011883015 (TypeBuilder_t4287691406 * __this, int32_t ___bindingAttr, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TypeBuilder_GetProperties_m1011883015_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ArrayList_t2121638921 * V_0 = NULL;
	bool V_1 = false;
	int32_t V_2 = 0;
	MethodInfo_t * V_3 = NULL;
	PropertyInfo_t * V_4 = NULL;
	PropertyBuilderU5BU5D_t2931941570* V_5 = NULL;
	int32_t V_6 = 0;
	PropertyInfoU5BU5D_t1348579340* V_7 = NULL;
	{
		bool L_0 = TypeBuilder_get_is_created_m695624602(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0018;
		}
	}
	{
		Type_t * L_1 = __this->get_created_25();
		int32_t L_2 = ___bindingAttr;
		NullCheck(L_1);
		PropertyInfoU5BU5D_t1348579340* L_3 = VirtFuncInvoker1< PropertyInfoU5BU5D_t1348579340*, int32_t >::Invoke(56 /* System.Reflection.PropertyInfo[] System.Type::GetProperties(System.Reflection.BindingFlags) */, L_1, L_2);
		return L_3;
	}

IL_0018:
	{
		PropertyBuilderU5BU5D_t2931941570* L_4 = __this->get_properties_16();
		if (L_4)
		{
			goto IL_002a;
		}
	}
	{
		return ((PropertyInfoU5BU5D_t1348579340*)SZArrayNew(PropertyInfoU5BU5D_t1348579340_il2cpp_TypeInfo_var, (uint32_t)0));
	}

IL_002a:
	{
		ArrayList_t2121638921 * L_5 = (ArrayList_t2121638921 *)il2cpp_codegen_object_new(ArrayList_t2121638921_il2cpp_TypeInfo_var);
		ArrayList__ctor_m1878432947(L_5, /*hidden argument*/NULL);
		V_0 = L_5;
		PropertyBuilderU5BU5D_t2931941570* L_6 = __this->get_properties_16();
		V_5 = L_6;
		V_6 = 0;
		goto IL_00e0;
	}

IL_0040:
	{
		PropertyBuilderU5BU5D_t2931941570* L_7 = V_5;
		int32_t L_8 = V_6;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, L_8);
		int32_t L_9 = L_8;
		V_4 = ((L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9)));
		V_1 = (bool)0;
		PropertyInfo_t * L_10 = V_4;
		NullCheck(L_10);
		MethodInfo_t * L_11 = VirtFuncInvoker1< MethodInfo_t *, bool >::Invoke(20 /* System.Reflection.MethodInfo System.Reflection.PropertyInfo::GetGetMethod(System.Boolean) */, L_10, (bool)1);
		V_3 = L_11;
		MethodInfo_t * L_12 = V_3;
		if (L_12)
		{
			goto IL_0061;
		}
	}
	{
		PropertyInfo_t * L_13 = V_4;
		NullCheck(L_13);
		MethodInfo_t * L_14 = VirtFuncInvoker1< MethodInfo_t *, bool >::Invoke(22 /* System.Reflection.MethodInfo System.Reflection.PropertyInfo::GetSetMethod(System.Boolean) */, L_13, (bool)1);
		V_3 = L_14;
	}

IL_0061:
	{
		MethodInfo_t * L_15 = V_3;
		if (L_15)
		{
			goto IL_006c;
		}
	}
	{
		goto IL_00da;
	}

IL_006c:
	{
		MethodInfo_t * L_16 = V_3;
		NullCheck(L_16);
		int32_t L_17 = VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Reflection.MethodAttributes System.Reflection.MethodBase::get_Attributes() */, L_16);
		V_2 = L_17;
		int32_t L_18 = V_2;
		if ((!(((uint32_t)((int32_t)((int32_t)L_18&(int32_t)7))) == ((uint32_t)6))))
		{
			goto IL_008c;
		}
	}
	{
		int32_t L_19 = ___bindingAttr;
		if (!((int32_t)((int32_t)L_19&(int32_t)((int32_t)16))))
		{
			goto IL_0087;
		}
	}
	{
		V_1 = (bool)1;
	}

IL_0087:
	{
		goto IL_0097;
	}

IL_008c:
	{
		int32_t L_20 = ___bindingAttr;
		if (!((int32_t)((int32_t)L_20&(int32_t)((int32_t)32))))
		{
			goto IL_0097;
		}
	}
	{
		V_1 = (bool)1;
	}

IL_0097:
	{
		bool L_21 = V_1;
		if (L_21)
		{
			goto IL_00a2;
		}
	}
	{
		goto IL_00da;
	}

IL_00a2:
	{
		V_1 = (bool)0;
		int32_t L_22 = V_2;
		if (!((int32_t)((int32_t)L_22&(int32_t)((int32_t)16))))
		{
			goto IL_00bc;
		}
	}
	{
		int32_t L_23 = ___bindingAttr;
		if (!((int32_t)((int32_t)L_23&(int32_t)8)))
		{
			goto IL_00b7;
		}
	}
	{
		V_1 = (bool)1;
	}

IL_00b7:
	{
		goto IL_00c6;
	}

IL_00bc:
	{
		int32_t L_24 = ___bindingAttr;
		if (!((int32_t)((int32_t)L_24&(int32_t)4)))
		{
			goto IL_00c6;
		}
	}
	{
		V_1 = (bool)1;
	}

IL_00c6:
	{
		bool L_25 = V_1;
		if (L_25)
		{
			goto IL_00d1;
		}
	}
	{
		goto IL_00da;
	}

IL_00d1:
	{
		ArrayList_t2121638921 * L_26 = V_0;
		PropertyInfo_t * L_27 = V_4;
		NullCheck(L_26);
		VirtFuncInvoker1< int32_t, Il2CppObject * >::Invoke(30 /* System.Int32 System.Collections.ArrayList::Add(System.Object) */, L_26, L_27);
	}

IL_00da:
	{
		int32_t L_28 = V_6;
		V_6 = ((int32_t)((int32_t)L_28+(int32_t)1));
	}

IL_00e0:
	{
		int32_t L_29 = V_6;
		PropertyBuilderU5BU5D_t2931941570* L_30 = V_5;
		NullCheck(L_30);
		if ((((int32_t)L_29) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_30)->max_length)))))))
		{
			goto IL_0040;
		}
	}
	{
		ArrayList_t2121638921 * L_31 = V_0;
		NullCheck(L_31);
		int32_t L_32 = VirtFuncInvoker0< int32_t >::Invoke(23 /* System.Int32 System.Collections.ArrayList::get_Count() */, L_31);
		V_7 = ((PropertyInfoU5BU5D_t1348579340*)SZArrayNew(PropertyInfoU5BU5D_t1348579340_il2cpp_TypeInfo_var, (uint32_t)L_32));
		ArrayList_t2121638921 * L_33 = V_0;
		PropertyInfoU5BU5D_t1348579340* L_34 = V_7;
		NullCheck(L_33);
		VirtActionInvoker1< Il2CppArray * >::Invoke(40 /* System.Void System.Collections.ArrayList::CopyTo(System.Array) */, L_33, (Il2CppArray *)(Il2CppArray *)L_34);
		PropertyInfoU5BU5D_t1348579340* L_35 = V_7;
		return L_35;
	}
}
// System.Reflection.PropertyInfo System.Reflection.Emit.TypeBuilder::GetPropertyImpl(System.String,System.Reflection.BindingFlags,System.Reflection.Binder,System.Type,System.Type[],System.Reflection.ParameterModifier[])
extern "C"  PropertyInfo_t * TypeBuilder_GetPropertyImpl_m2672120863 (TypeBuilder_t4287691406 * __this, String_t* ___name, int32_t ___bindingAttr, Binder_t4180926488 * ___binder, Type_t * ___returnType, TypeU5BU5D_t3431720054* ___types, ParameterModifierU5BU5D_t3379147067* ___modifiers, const MethodInfo* method)
{
	{
		Exception_t1967233988 * L_0 = TypeBuilder_not_supported_m2004582477(__this, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean System.Reflection.Emit.TypeBuilder::HasElementTypeImpl()
extern "C"  bool TypeBuilder_HasElementTypeImpl_m3001033082 (TypeBuilder_t4287691406 * __this, const MethodInfo* method)
{
	{
		bool L_0 = TypeBuilder_get_is_created_m695624602(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return (bool)0;
	}

IL_000d:
	{
		Type_t * L_1 = __this->get_created_25();
		NullCheck(L_1);
		bool L_2 = VirtFuncInvoker0< bool >::Invoke(19 /* System.Boolean System.Type::get_HasElementType() */, L_1);
		return L_2;
	}
}
// System.Object System.Reflection.Emit.TypeBuilder::InvokeMember(System.String,System.Reflection.BindingFlags,System.Reflection.Binder,System.Object,System.Object[],System.Reflection.ParameterModifier[],System.Globalization.CultureInfo,System.String[])
extern "C"  Il2CppObject * TypeBuilder_InvokeMember_m1599102929 (TypeBuilder_t4287691406 * __this, String_t* ___name, int32_t ___invokeAttr, Binder_t4180926488 * ___binder, Il2CppObject * ___target, ObjectU5BU5D_t11523773* ___args, ParameterModifierU5BU5D_t3379147067* ___modifiers, CultureInfo_t3603717042 * ___culture, StringU5BU5D_t2956870243* ___namedParameters, const MethodInfo* method)
{
	{
		TypeBuilder_check_created_m3022374359(__this, /*hidden argument*/NULL);
		Type_t * L_0 = __this->get_created_25();
		String_t* L_1 = ___name;
		int32_t L_2 = ___invokeAttr;
		Binder_t4180926488 * L_3 = ___binder;
		Il2CppObject * L_4 = ___target;
		ObjectU5BU5D_t11523773* L_5 = ___args;
		ParameterModifierU5BU5D_t3379147067* L_6 = ___modifiers;
		CultureInfo_t3603717042 * L_7 = ___culture;
		StringU5BU5D_t2956870243* L_8 = ___namedParameters;
		NullCheck(L_0);
		Il2CppObject * L_9 = VirtFuncInvoker8< Il2CppObject *, String_t*, int32_t, Binder_t4180926488 *, Il2CppObject *, ObjectU5BU5D_t11523773*, ParameterModifierU5BU5D_t3379147067*, CultureInfo_t3603717042 *, StringU5BU5D_t2956870243* >::Invoke(77 /* System.Object System.Type::InvokeMember(System.String,System.Reflection.BindingFlags,System.Reflection.Binder,System.Object,System.Object[],System.Reflection.ParameterModifier[],System.Globalization.CultureInfo,System.String[]) */, L_0, L_1, L_2, L_3, L_4, L_5, L_6, L_7, L_8);
		return L_9;
	}
}
// System.Boolean System.Reflection.Emit.TypeBuilder::IsArrayImpl()
extern "C"  bool TypeBuilder_IsArrayImpl_m1759767571 (TypeBuilder_t4287691406 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Boolean System.Reflection.Emit.TypeBuilder::IsByRefImpl()
extern "C"  bool TypeBuilder_IsByRefImpl_m3584762934 (TypeBuilder_t4287691406 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Boolean System.Reflection.Emit.TypeBuilder::IsPointerImpl()
extern "C"  bool TypeBuilder_IsPointerImpl_m189715127 (TypeBuilder_t4287691406 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Boolean System.Reflection.Emit.TypeBuilder::IsPrimitiveImpl()
extern "C"  bool TypeBuilder_IsPrimitiveImpl_m501522337 (TypeBuilder_t4287691406 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Boolean System.Reflection.Emit.TypeBuilder::IsValueTypeImpl()
extern const Il2CppType* ValueType_t4014882752_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t TypeBuilder_IsValueTypeImpl_m983821317_MetadataUsageId;
extern "C"  bool TypeBuilder_IsValueTypeImpl_m983821317 (TypeBuilder_t4287691406 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TypeBuilder_IsValueTypeImpl_m983821317_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B5_0 = 0;
	{
		ModuleBuilder_t1058295580 * L_0 = __this->get_pmodule_21();
		NullCheck(L_0);
		AssemblyBuilder_t3642540642 * L_1 = L_0->get_assemblyb_14();
		NullCheck(L_1);
		Type_t * L_2 = L_1->get_corlib_value_type_26();
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		bool L_3 = Type_type_is_subtype_of_m3361675332(NULL /*static, unused*/, __this, L_2, (bool)0, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_0032;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_4 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(ValueType_t4014882752_0_0_0_var), /*hidden argument*/NULL);
		bool L_5 = Type_type_is_subtype_of_m3361675332(NULL /*static, unused*/, __this, L_4, (bool)0, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0060;
		}
	}

IL_0032:
	{
		ModuleBuilder_t1058295580 * L_6 = __this->get_pmodule_21();
		NullCheck(L_6);
		AssemblyBuilder_t3642540642 * L_7 = L_6->get_assemblyb_14();
		NullCheck(L_7);
		Type_t * L_8 = L_7->get_corlib_value_type_26();
		if ((((Il2CppObject*)(TypeBuilder_t4287691406 *)__this) == ((Il2CppObject*)(Type_t *)L_8)))
		{
			goto IL_0060;
		}
	}
	{
		ModuleBuilder_t1058295580 * L_9 = __this->get_pmodule_21();
		NullCheck(L_9);
		AssemblyBuilder_t3642540642 * L_10 = L_9->get_assemblyb_14();
		NullCheck(L_10);
		Type_t * L_11 = L_10->get_corlib_enum_type_27();
		G_B5_0 = ((((int32_t)((((Il2CppObject*)(TypeBuilder_t4287691406 *)__this) == ((Il2CppObject*)(Type_t *)L_11))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0061;
	}

IL_0060:
	{
		G_B5_0 = 0;
	}

IL_0061:
	{
		return (bool)G_B5_0;
	}
}
// System.Type System.Reflection.Emit.TypeBuilder::MakeByRefType()
extern TypeInfo* ByRefType_t1509733795_il2cpp_TypeInfo_var;
extern const uint32_t TypeBuilder_MakeByRefType_m1248220084_MetadataUsageId;
extern "C"  Type_t * TypeBuilder_MakeByRefType_m1248220084 (TypeBuilder_t4287691406 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TypeBuilder_MakeByRefType_m1248220084_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ByRefType_t1509733795 * L_0 = (ByRefType_t1509733795 *)il2cpp_codegen_object_new(ByRefType_t1509733795_il2cpp_TypeInfo_var);
		ByRefType__ctor_m2862936166(L_0, __this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Type System.Reflection.Emit.TypeBuilder::MakeGenericType(System.Type[])
extern "C"  Type_t * TypeBuilder_MakeGenericType_m740236136 (TypeBuilder_t4287691406 * __this, TypeU5BU5D_t3431720054* ___typeArguments, const MethodInfo* method)
{
	{
		TypeU5BU5D_t3431720054* L_0 = ___typeArguments;
		Type_t * L_1 = Type_MakeGenericType_m2305919041(__this, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.RuntimeTypeHandle System.Reflection.Emit.TypeBuilder::get_TypeHandle()
extern "C"  RuntimeTypeHandle_t1864875887  TypeBuilder_get_TypeHandle_m2527260375 (TypeBuilder_t4287691406 * __this, const MethodInfo* method)
{
	{
		TypeBuilder_check_created_m3022374359(__this, /*hidden argument*/NULL);
		Type_t * L_0 = __this->get_created_25();
		NullCheck(L_0);
		RuntimeTypeHandle_t1864875887  L_1 = VirtFuncInvoker0< RuntimeTypeHandle_t1864875887  >::Invoke(35 /* System.RuntimeTypeHandle System.Type::get_TypeHandle() */, L_0);
		return L_1;
	}
}
// System.Void System.Reflection.Emit.TypeBuilder::SetParent(System.Type)
extern const Il2CppType* Il2CppObject_0_0_0_var;
extern TypeInfo* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2633143155;
extern const uint32_t TypeBuilder_SetParent_m2226876871_MetadataUsageId;
extern "C"  void TypeBuilder_SetParent_m2226876871 (TypeBuilder_t4287691406 * __this, Type_t * ___parent, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TypeBuilder_SetParent_m2226876871_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		TypeBuilder_check_not_created_m4283406443(__this, /*hidden argument*/NULL);
		Type_t * L_0 = ___parent;
		if (L_0)
		{
			goto IL_0057;
		}
	}
	{
		int32_t L_1 = __this->get_attrs_19();
		if (!((int32_t)((int32_t)L_1&(int32_t)((int32_t)32))))
		{
			goto IL_0042;
		}
	}
	{
		int32_t L_2 = __this->get_attrs_19();
		if (((int32_t)((int32_t)L_2&(int32_t)((int32_t)128))))
		{
			goto IL_0036;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_3 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, _stringLiteral2633143155, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0036:
	{
		__this->set_parent_10((Type_t *)NULL);
		goto IL_0052;
	}

IL_0042:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_4 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Il2CppObject_0_0_0_var), /*hidden argument*/NULL);
		__this->set_parent_10(L_4);
	}

IL_0052:
	{
		goto IL_005e;
	}

IL_0057:
	{
		Type_t * L_5 = ___parent;
		__this->set_parent_10(L_5);
	}

IL_005e:
	{
		TypeBuilder_setup_internal_class_m1841540458(__this, __this, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 System.Reflection.Emit.TypeBuilder::get_next_table_index(System.Object,System.Int32,System.Boolean)
extern "C"  int32_t TypeBuilder_get_next_table_index_m2109425124 (TypeBuilder_t4287691406 * __this, Il2CppObject * ___obj, int32_t ___table, bool ___inc, const MethodInfo* method)
{
	{
		ModuleBuilder_t1058295580 * L_0 = __this->get_pmodule_21();
		Il2CppObject * L_1 = ___obj;
		int32_t L_2 = ___table;
		bool L_3 = ___inc;
		NullCheck(L_0);
		int32_t L_4 = ModuleBuilder_get_next_table_index_m3600803990(L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Boolean System.Reflection.Emit.TypeBuilder::get_IsCompilerContext()
extern "C"  bool TypeBuilder_get_IsCompilerContext_m432935841 (TypeBuilder_t4287691406 * __this, const MethodInfo* method)
{
	{
		ModuleBuilder_t1058295580 * L_0 = __this->get_pmodule_21();
		NullCheck(L_0);
		AssemblyBuilder_t3642540642 * L_1 = L_0->get_assemblyb_14();
		NullCheck(L_1);
		bool L_2 = AssemblyBuilder_get_IsCompilerContext_m332025933(L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Boolean System.Reflection.Emit.TypeBuilder::get_is_created()
extern "C"  bool TypeBuilder_get_is_created_m695624602 (TypeBuilder_t4287691406 * __this, const MethodInfo* method)
{
	{
		Type_t * L_0 = __this->get_created_25();
		return (bool)((((int32_t)((((Il2CppObject*)(Type_t *)L_0) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Exception System.Reflection.Emit.TypeBuilder::not_supported()
extern TypeInfo* NotSupportedException_t1374155497_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1016179289;
extern const uint32_t TypeBuilder_not_supported_m2004582477_MetadataUsageId;
extern "C"  Exception_t1967233988 * TypeBuilder_not_supported_m2004582477 (TypeBuilder_t4287691406 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TypeBuilder_not_supported_m2004582477_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1374155497 * L_0 = (NotSupportedException_t1374155497 *)il2cpp_codegen_object_new(NotSupportedException_t1374155497_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m133757637(L_0, _stringLiteral1016179289, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void System.Reflection.Emit.TypeBuilder::check_not_created()
extern TypeInfo* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3419414666;
extern const uint32_t TypeBuilder_check_not_created_m4283406443_MetadataUsageId;
extern "C"  void TypeBuilder_check_not_created_m4283406443 (TypeBuilder_t4287691406 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TypeBuilder_check_not_created_m4283406443_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = TypeBuilder_get_is_created_m695624602(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, _stringLiteral3419414666, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		return;
	}
}
// System.Void System.Reflection.Emit.TypeBuilder::check_created()
extern "C"  void TypeBuilder_check_created_m3022374359 (TypeBuilder_t4287691406 * __this, const MethodInfo* method)
{
	{
		bool L_0 = TypeBuilder_get_is_created_m695624602(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		Exception_t1967233988 * L_1 = TypeBuilder_not_supported_m2004582477(__this, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0012:
	{
		return;
	}
}
// System.Void System.Reflection.Emit.TypeBuilder::check_name(System.String,System.String)
extern TypeInfo* ArgumentNullException_t3214793280_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentException_t124305799_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1606718200;
extern Il2CppCodeGenString* _stringLiteral557212949;
extern const uint32_t TypeBuilder_check_name_m566175072_MetadataUsageId;
extern "C"  void TypeBuilder_check_name_m566175072 (TypeBuilder_t4287691406 * __this, String_t* ___argName, String_t* ___name, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TypeBuilder_check_name_m566175072_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___name;
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		String_t* L_1 = ___argName;
		ArgumentNullException_t3214793280 * L_2 = (ArgumentNullException_t3214793280 *)il2cpp_codegen_object_new(ArgumentNullException_t3214793280_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_2, L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_000d:
	{
		String_t* L_3 = ___name;
		NullCheck(L_3);
		int32_t L_4 = String_get_Length_m2979997331(L_3, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0024;
		}
	}
	{
		String_t* L_5 = ___argName;
		ArgumentException_t124305799 * L_6 = (ArgumentException_t124305799 *)il2cpp_codegen_object_new(ArgumentException_t124305799_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m732321503(L_6, _stringLiteral1606718200, L_5, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_6);
	}

IL_0024:
	{
		String_t* L_7 = ___name;
		NullCheck(L_7);
		uint16_t L_8 = String_get_Chars_m3015341861(L_7, 0, /*hidden argument*/NULL);
		if (L_8)
		{
			goto IL_003c;
		}
	}
	{
		String_t* L_9 = ___argName;
		ArgumentException_t124305799 * L_10 = (ArgumentException_t124305799 *)il2cpp_codegen_object_new(ArgumentException_t124305799_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m732321503(L_10, _stringLiteral557212949, L_9, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_10);
	}

IL_003c:
	{
		return;
	}
}
// System.String System.Reflection.Emit.TypeBuilder::ToString()
extern "C"  String_t* TypeBuilder_ToString_m3882393003 (TypeBuilder_t4287691406 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = TypeBuilder_get_FullName_m1311381250(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Boolean System.Reflection.Emit.TypeBuilder::IsAssignableFrom(System.Type)
extern "C"  bool TypeBuilder_IsAssignableFrom_m2834740190 (TypeBuilder_t4287691406 * __this, Type_t * ___c, const MethodInfo* method)
{
	{
		Type_t * L_0 = ___c;
		bool L_1 = Type_IsAssignableFrom_m1817311413(__this, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean System.Reflection.Emit.TypeBuilder::IsSubclassOf(System.Type)
extern "C"  bool TypeBuilder_IsSubclassOf_m919862466 (TypeBuilder_t4287691406 * __this, Type_t * ___c, const MethodInfo* method)
{
	{
		Type_t * L_0 = ___c;
		bool L_1 = Type_IsSubclassOf_m1095320857(__this, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean System.Reflection.Emit.TypeBuilder::IsAssignableTo(System.Type)
extern const Il2CppType* Il2CppObject_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t TypeBuilder_IsAssignableTo_m3927127213_MetadataUsageId;
extern "C"  bool TypeBuilder_IsAssignableTo_m3927127213 (TypeBuilder_t4287691406 * __this, Type_t * ___c, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TypeBuilder_IsAssignableTo_m3927127213_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Type_t * V_0 = NULL;
	TypeU5BU5D_t3431720054* V_1 = NULL;
	int32_t V_2 = 0;
	{
		Type_t * L_0 = ___c;
		if ((!(((Il2CppObject*)(Type_t *)L_0) == ((Il2CppObject*)(TypeBuilder_t4287691406 *)__this))))
		{
			goto IL_0009;
		}
	}
	{
		return (bool)1;
	}

IL_0009:
	{
		Type_t * L_1 = ___c;
		NullCheck(L_1);
		bool L_2 = VirtFuncInvoker0< bool >::Invoke(27 /* System.Boolean System.Type::get_IsInterface() */, L_1);
		if (!L_2)
		{
			goto IL_0084;
		}
	}
	{
		Type_t * L_3 = __this->get_parent_10();
		if (!L_3)
		{
			goto IL_003d;
		}
	}
	{
		bool L_4 = TypeBuilder_get_is_created_m695624602(__this, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_003d;
		}
	}
	{
		Type_t * L_5 = ___c;
		Type_t * L_6 = __this->get_parent_10();
		NullCheck(L_5);
		bool L_7 = VirtFuncInvoker1< bool, Type_t * >::Invoke(41 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, L_5, L_6);
		if (!L_7)
		{
			goto IL_003d;
		}
	}
	{
		return (bool)1;
	}

IL_003d:
	{
		TypeU5BU5D_t3431720054* L_8 = __this->get_interfaces_12();
		if (L_8)
		{
			goto IL_004a;
		}
	}
	{
		return (bool)0;
	}

IL_004a:
	{
		TypeU5BU5D_t3431720054* L_9 = __this->get_interfaces_12();
		V_1 = L_9;
		V_2 = 0;
		goto IL_006e;
	}

IL_0058:
	{
		TypeU5BU5D_t3431720054* L_10 = V_1;
		int32_t L_11 = V_2;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, L_11);
		int32_t L_12 = L_11;
		V_0 = ((L_10)->GetAt(static_cast<il2cpp_array_size_t>(L_12)));
		Type_t * L_13 = ___c;
		Type_t * L_14 = V_0;
		NullCheck(L_13);
		bool L_15 = VirtFuncInvoker1< bool, Type_t * >::Invoke(41 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, L_13, L_14);
		if (!L_15)
		{
			goto IL_006a;
		}
	}
	{
		return (bool)1;
	}

IL_006a:
	{
		int32_t L_16 = V_2;
		V_2 = ((int32_t)((int32_t)L_16+(int32_t)1));
	}

IL_006e:
	{
		int32_t L_17 = V_2;
		TypeU5BU5D_t3431720054* L_18 = V_1;
		NullCheck(L_18);
		if ((((int32_t)L_17) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_18)->max_length)))))))
		{
			goto IL_0058;
		}
	}
	{
		bool L_19 = TypeBuilder_get_is_created_m695624602(__this, /*hidden argument*/NULL);
		if (L_19)
		{
			goto IL_0084;
		}
	}
	{
		return (bool)0;
	}

IL_0084:
	{
		Type_t * L_20 = __this->get_parent_10();
		if (L_20)
		{
			goto IL_009d;
		}
	}
	{
		Type_t * L_21 = ___c;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_22 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Il2CppObject_0_0_0_var), /*hidden argument*/NULL);
		return (bool)((((Il2CppObject*)(Type_t *)L_21) == ((Il2CppObject*)(Type_t *)L_22))? 1 : 0);
	}

IL_009d:
	{
		Type_t * L_23 = ___c;
		Type_t * L_24 = __this->get_parent_10();
		NullCheck(L_23);
		bool L_25 = VirtFuncInvoker1< bool, Type_t * >::Invoke(41 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, L_23, L_24);
		return L_25;
	}
}
// System.Type[] System.Reflection.Emit.TypeBuilder::GetGenericArguments()
extern TypeInfo* TypeU5BU5D_t3431720054_il2cpp_TypeInfo_var;
extern const uint32_t TypeBuilder_GetGenericArguments_m1892425923_MetadataUsageId;
extern "C"  TypeU5BU5D_t3431720054* TypeBuilder_GetGenericArguments_m1892425923 (TypeBuilder_t4287691406 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TypeBuilder_GetGenericArguments_m1892425923_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	TypeU5BU5D_t3431720054* V_0 = NULL;
	{
		GenericTypeParameterBuilderU5BU5D_t685103793* L_0 = __this->get_generic_params_24();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return (TypeU5BU5D_t3431720054*)NULL;
	}

IL_000d:
	{
		GenericTypeParameterBuilderU5BU5D_t685103793* L_1 = __this->get_generic_params_24();
		NullCheck(L_1);
		V_0 = ((TypeU5BU5D_t3431720054*)SZArrayNew(TypeU5BU5D_t3431720054_il2cpp_TypeInfo_var, (uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length))))));
		GenericTypeParameterBuilderU5BU5D_t685103793* L_2 = __this->get_generic_params_24();
		TypeU5BU5D_t3431720054* L_3 = V_0;
		NullCheck((Il2CppArray *)(Il2CppArray *)L_2);
		VirtActionInvoker2< Il2CppArray *, int32_t >::Invoke(9 /* System.Void System.Array::CopyTo(System.Array,System.Int32) */, (Il2CppArray *)(Il2CppArray *)L_2, (Il2CppArray *)(Il2CppArray *)L_3, 0);
		TypeU5BU5D_t3431720054* L_4 = V_0;
		return L_4;
	}
}
// System.Type System.Reflection.Emit.TypeBuilder::GetGenericTypeDefinition()
extern TypeInfo* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3031235866;
extern const uint32_t TypeBuilder_GetGenericTypeDefinition_m4229681124_MetadataUsageId;
extern "C"  Type_t * TypeBuilder_GetGenericTypeDefinition_m4229681124 (TypeBuilder_t4287691406 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TypeBuilder_GetGenericTypeDefinition_m4229681124_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GenericTypeParameterBuilderU5BU5D_t685103793* L_0 = __this->get_generic_params_24();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, _stringLiteral3031235866, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		return __this;
	}
}
// System.Boolean System.Reflection.Emit.TypeBuilder::get_ContainsGenericParameters()
extern "C"  bool TypeBuilder_get_ContainsGenericParameters_m446053341 (TypeBuilder_t4287691406 * __this, const MethodInfo* method)
{
	{
		GenericTypeParameterBuilderU5BU5D_t685103793* L_0 = __this->get_generic_params_24();
		return (bool)((((int32_t)((((Il2CppObject*)(GenericTypeParameterBuilderU5BU5D_t685103793*)L_0) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Boolean System.Reflection.Emit.TypeBuilder::get_IsGenericParameter()
extern "C"  bool TypeBuilder_get_IsGenericParameter_m78451587 (TypeBuilder_t4287691406 * __this, const MethodInfo* method)
{
	using namespace il2cpp::icalls;
	typedef bool (*TypeBuilder_get_IsGenericParameter_m78451587_ftn) (TypeBuilder_t4287691406 *);
	return  ((TypeBuilder_get_IsGenericParameter_m78451587_ftn)mscorlib::System::Reflection::Emit::TypeBuilder::get_IsGenericParameter) (__this);
}
// System.Boolean System.Reflection.Emit.TypeBuilder::get_IsGenericTypeDefinition()
extern "C"  bool TypeBuilder_get_IsGenericTypeDefinition_m582825077 (TypeBuilder_t4287691406 * __this, const MethodInfo* method)
{
	{
		GenericTypeParameterBuilderU5BU5D_t685103793* L_0 = __this->get_generic_params_24();
		return (bool)((((int32_t)((((Il2CppObject*)(GenericTypeParameterBuilderU5BU5D_t685103793*)L_0) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Boolean System.Reflection.Emit.TypeBuilder::get_IsGenericType()
extern "C"  bool TypeBuilder_get_IsGenericType_m226951426 (TypeBuilder_t4287691406 * __this, const MethodInfo* method)
{
	{
		bool L_0 = TypeBuilder_get_IsGenericTypeDefinition_m582825077(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Runtime.InteropServices.MarshalAsAttribute System.Reflection.Emit.UnmanagedMarshal::ToMarshalAsAttribute()
extern TypeInfo* MarshalAsAttribute_t1016163854_il2cpp_TypeInfo_var;
extern const uint32_t UnmanagedMarshal_ToMarshalAsAttribute_m4201209287_MetadataUsageId;
extern "C"  MarshalAsAttribute_t1016163854 * UnmanagedMarshal_ToMarshalAsAttribute_m4201209287 (UnmanagedMarshal_t446138789 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnmanagedMarshal_ToMarshalAsAttribute_m4201209287_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	MarshalAsAttribute_t1016163854 * V_0 = NULL;
	{
		int32_t L_0 = __this->get_t_1();
		MarshalAsAttribute_t1016163854 * L_1 = (MarshalAsAttribute_t1016163854 *)il2cpp_codegen_object_new(MarshalAsAttribute_t1016163854_il2cpp_TypeInfo_var);
		MarshalAsAttribute__ctor_m682499176(L_1, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		MarshalAsAttribute_t1016163854 * L_2 = V_0;
		int32_t L_3 = __this->get_tbase_2();
		NullCheck(L_2);
		L_2->set_ArraySubType_1(L_3);
		MarshalAsAttribute_t1016163854 * L_4 = V_0;
		String_t* L_5 = __this->get_mcookie_4();
		NullCheck(L_4);
		L_4->set_MarshalCookie_2(L_5);
		MarshalAsAttribute_t1016163854 * L_6 = V_0;
		String_t* L_7 = __this->get_marshaltype_5();
		NullCheck(L_6);
		L_6->set_MarshalType_3(L_7);
		MarshalAsAttribute_t1016163854 * L_8 = V_0;
		Type_t * L_9 = __this->get_marshaltyperef_6();
		NullCheck(L_8);
		L_8->set_MarshalTypeRef_4(L_9);
		int32_t L_10 = __this->get_count_0();
		if ((!(((uint32_t)L_10) == ((uint32_t)(-1)))))
		{
			goto IL_0054;
		}
	}
	{
		MarshalAsAttribute_t1016163854 * L_11 = V_0;
		NullCheck(L_11);
		L_11->set_SizeConst_5(0);
		goto IL_0060;
	}

IL_0054:
	{
		MarshalAsAttribute_t1016163854 * L_12 = V_0;
		int32_t L_13 = __this->get_count_0();
		NullCheck(L_12);
		L_12->set_SizeConst_5(L_13);
	}

IL_0060:
	{
		int32_t L_14 = __this->get_param_num_7();
		if ((!(((uint32_t)L_14) == ((uint32_t)(-1)))))
		{
			goto IL_0078;
		}
	}
	{
		MarshalAsAttribute_t1016163854 * L_15 = V_0;
		NullCheck(L_15);
		L_15->set_SizeParamIndex_6(0);
		goto IL_0085;
	}

IL_0078:
	{
		MarshalAsAttribute_t1016163854 * L_16 = V_0;
		int32_t L_17 = __this->get_param_num_7();
		NullCheck(L_16);
		L_16->set_SizeParamIndex_6((((int16_t)((int16_t)L_17))));
	}

IL_0085:
	{
		MarshalAsAttribute_t1016163854 * L_18 = V_0;
		return L_18;
	}
}
// System.Void System.Reflection.EventInfo::.ctor()
extern "C"  void EventInfo__ctor_m2461757520 (EventInfo_t * __this, const MethodInfo* method)
{
	{
		MemberInfo__ctor_m3915857030(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Type System.Reflection.EventInfo::get_EventHandlerType()
extern "C"  Type_t * EventInfo_get_EventHandlerType_m2354774861 (EventInfo_t * __this, const MethodInfo* method)
{
	ParameterInfoU5BU5D_t1127461800* V_0 = NULL;
	MethodInfo_t * V_1 = NULL;
	Type_t * V_2 = NULL;
	{
		MethodInfo_t * L_0 = VirtFuncInvoker1< MethodInfo_t *, bool >::Invoke(16 /* System.Reflection.MethodInfo System.Reflection.EventInfo::GetAddMethod(System.Boolean) */, __this, (bool)1);
		V_1 = L_0;
		MethodInfo_t * L_1 = V_1;
		NullCheck(L_1);
		ParameterInfoU5BU5D_t1127461800* L_2 = VirtFuncInvoker0< ParameterInfoU5BU5D_t1127461800* >::Invoke(15 /* System.Reflection.ParameterInfo[] System.Reflection.MethodBase::GetParameters() */, L_1);
		V_0 = L_2;
		ParameterInfoU5BU5D_t1127461800* L_3 = V_0;
		NullCheck(L_3);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_3)->max_length))))) <= ((int32_t)0)))
		{
			goto IL_0023;
		}
	}
	{
		ParameterInfoU5BU5D_t1127461800* L_4 = V_0;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 0);
		int32_t L_5 = 0;
		NullCheck(((L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_5))));
		Type_t * L_6 = VirtFuncInvoker0< Type_t * >::Invoke(6 /* System.Type System.Reflection.ParameterInfo::get_ParameterType() */, ((L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_5))));
		V_2 = L_6;
		Type_t * L_7 = V_2;
		return L_7;
	}

IL_0023:
	{
		return (Type_t *)NULL;
	}
}
// System.Reflection.MemberTypes System.Reflection.EventInfo::get_MemberType()
extern "C"  int32_t EventInfo_get_MemberType_m3965977017 (EventInfo_t * __this, const MethodInfo* method)
{
	{
		return (int32_t)(2);
	}
}
// System.Void System.Reflection.EventInfo/AddEventAdapter::.ctor(System.Object,System.IntPtr)
extern "C"  void AddEventAdapter__ctor_m2445699769 (AddEventAdapter_t836903542 * __this, Il2CppObject * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->set_method_ptr_0((methodPointerType)((MethodInfo*)___method.get_m_value_0())->method);
	__this->set_method_3(___method);
	__this->set_m_target_2(___object);
}
// System.Void System.Reflection.EventInfo/AddEventAdapter::Invoke(System.Object,System.Delegate)
extern "C"  void AddEventAdapter_Invoke_m4107836009 (AddEventAdapter_t836903542 * __this, Il2CppObject * ____this, Delegate_t3660574010 * ___dele, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		AddEventAdapter_Invoke_m4107836009((AddEventAdapter_t836903542 *)__this->get_prev_9(),____this, ___dele, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ____this, Delegate_t3660574010 * ___dele, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),____this, ___dele,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ____this, Delegate_t3660574010 * ___dele, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),____this, ___dele,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, Delegate_t3660574010 * ___dele, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(____this, ___dele,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C" void pinvoke_delegate_wrapper_AddEventAdapter_t836903542(Il2CppObject* delegate, Il2CppObject * ____this, Delegate_t3660574010 * ___dele)
{
	// Marshaling of parameter '____this' to native representation
	IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Cannot marshal type 'System.Object'."));
}
// System.IAsyncResult System.Reflection.EventInfo/AddEventAdapter::BeginInvoke(System.Object,System.Delegate,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * AddEventAdapter_BeginInvoke_m2800012162 (AddEventAdapter_t836903542 * __this, Il2CppObject * ____this, Delegate_t3660574010 * ___dele, AsyncCallback_t1363551830 * ___callback, Il2CppObject * ___object, const MethodInfo* method)
{
	void *__d_args[3] = {0};
	__d_args[0] = ____this;
	__d_args[1] = ___dele;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void System.Reflection.EventInfo/AddEventAdapter::EndInvoke(System.IAsyncResult)
extern "C"  void AddEventAdapter_EndInvoke_m3991281993 (AddEventAdapter_t836903542 * __this, Il2CppObject * ___result, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// System.Void System.Reflection.FieldInfo::.ctor()
extern "C"  void FieldInfo__ctor_m1830395376 (FieldInfo_t * __this, const MethodInfo* method)
{
	{
		MemberInfo__ctor_m3915857030(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Reflection.MemberTypes System.Reflection.FieldInfo::get_MemberType()
extern "C"  int32_t FieldInfo_get_MemberType_m1921984537 (FieldInfo_t * __this, const MethodInfo* method)
{
	{
		return (int32_t)(4);
	}
}
// System.Boolean System.Reflection.FieldInfo::get_IsLiteral()
extern "C"  bool FieldInfo_get_IsLiteral_m2099153292 (FieldInfo_t * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = VirtFuncInvoker0< int32_t >::Invoke(14 /* System.Reflection.FieldAttributes System.Reflection.FieldInfo::get_Attributes() */, __this);
		return (bool)((((int32_t)((((int32_t)((int32_t)((int32_t)L_0&(int32_t)((int32_t)64)))) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Boolean System.Reflection.FieldInfo::get_IsStatic()
extern "C"  bool FieldInfo_get_IsStatic_m24721619 (FieldInfo_t * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = VirtFuncInvoker0< int32_t >::Invoke(14 /* System.Reflection.FieldAttributes System.Reflection.FieldInfo::get_Attributes() */, __this);
		return (bool)((((int32_t)((((int32_t)((int32_t)((int32_t)L_0&(int32_t)((int32_t)16)))) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Boolean System.Reflection.FieldInfo::get_IsPublic()
extern "C"  bool FieldInfo_get_IsPublic_m2574 (FieldInfo_t * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = VirtFuncInvoker0< int32_t >::Invoke(14 /* System.Reflection.FieldAttributes System.Reflection.FieldInfo::get_Attributes() */, __this);
		return (bool)((((int32_t)((int32_t)((int32_t)L_0&(int32_t)7))) == ((int32_t)6))? 1 : 0);
	}
}
// System.Boolean System.Reflection.FieldInfo::get_IsNotSerialized()
extern "C"  bool FieldInfo_get_IsNotSerialized_m3868603028 (FieldInfo_t * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = VirtFuncInvoker0< int32_t >::Invoke(14 /* System.Reflection.FieldAttributes System.Reflection.FieldInfo::get_Attributes() */, __this);
		return (bool)((((int32_t)((int32_t)((int32_t)L_0&(int32_t)((int32_t)128)))) == ((int32_t)((int32_t)128)))? 1 : 0);
	}
}
// System.Void System.Reflection.FieldInfo::SetValue(System.Object,System.Object)
extern "C"  void FieldInfo_SetValue_m1669444927 (FieldInfo_t * __this, Il2CppObject * ___obj, Il2CppObject * ___value, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___obj;
		Il2CppObject * L_1 = ___value;
		VirtActionInvoker5< Il2CppObject *, Il2CppObject *, int32_t, Binder_t4180926488 *, CultureInfo_t3603717042 * >::Invoke(22 /* System.Void System.Reflection.FieldInfo::SetValue(System.Object,System.Object,System.Reflection.BindingFlags,System.Reflection.Binder,System.Globalization.CultureInfo) */, __this, L_0, L_1, 0, (Binder_t4180926488 *)NULL, (CultureInfo_t3603717042 *)NULL);
		return;
	}
}
// System.Reflection.FieldInfo System.Reflection.FieldInfo::internal_from_handle_type(System.IntPtr,System.IntPtr)
extern "C"  FieldInfo_t * FieldInfo_internal_from_handle_type_m449136039 (Il2CppObject * __this /* static, unused */, IntPtr_t ___field_handle, IntPtr_t ___type_handle, const MethodInfo* method)
{
	using namespace il2cpp::icalls;
	typedef FieldInfo_t * (*FieldInfo_internal_from_handle_type_m449136039_ftn) (IntPtr_t, IntPtr_t);
	return  ((FieldInfo_internal_from_handle_type_m449136039_ftn)mscorlib::System::Reflection::FieldInfo::internal_from_handle_type) (___field_handle, ___type_handle);
}
// System.Reflection.FieldInfo System.Reflection.FieldInfo::GetFieldFromHandle(System.RuntimeFieldHandle)
extern TypeInfo* IntPtr_t_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentException_t124305799_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2720870788;
extern const uint32_t FieldInfo_GetFieldFromHandle_m3507098254_MetadataUsageId;
extern "C"  FieldInfo_t * FieldInfo_GetFieldFromHandle_m3507098254 (Il2CppObject * __this /* static, unused */, RuntimeFieldHandle_t3184214143  ___handle, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FieldInfo_GetFieldFromHandle_m3507098254_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = RuntimeFieldHandle_get_Value_m2124751302((&___handle), /*hidden argument*/NULL);
		IntPtr_t L_1 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
		bool L_2 = IntPtr_op_Equality_m72843924(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0021;
		}
	}
	{
		ArgumentException_t124305799 * L_3 = (ArgumentException_t124305799 *)il2cpp_codegen_object_new(ArgumentException_t124305799_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3544856547(L_3, _stringLiteral2720870788, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0021:
	{
		IntPtr_t L_4 = RuntimeFieldHandle_get_Value_m2124751302((&___handle), /*hidden argument*/NULL);
		IntPtr_t L_5 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
		FieldInfo_t * L_6 = FieldInfo_internal_from_handle_type_m449136039(NULL /*static, unused*/, L_4, L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// System.Int32 System.Reflection.FieldInfo::GetFieldOffset()
extern TypeInfo* SystemException_t3155420757_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3776226493;
extern const uint32_t FieldInfo_GetFieldOffset_m3781007919_MetadataUsageId;
extern "C"  int32_t FieldInfo_GetFieldOffset_m3781007919 (FieldInfo_t * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FieldInfo_GetFieldOffset_m3781007919_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		SystemException_t3155420757 * L_0 = (SystemException_t3155420757 *)il2cpp_codegen_object_new(SystemException_t3155420757_il2cpp_TypeInfo_var);
		SystemException__ctor_m3697314481(L_0, _stringLiteral3776226493, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Reflection.Emit.UnmanagedMarshal System.Reflection.FieldInfo::GetUnmanagedMarshal()
extern "C"  UnmanagedMarshal_t446138789 * FieldInfo_GetUnmanagedMarshal_m3736180640 (FieldInfo_t * __this, const MethodInfo* method)
{
	using namespace il2cpp::icalls;
	typedef UnmanagedMarshal_t446138789 * (*FieldInfo_GetUnmanagedMarshal_m3736180640_ftn) (FieldInfo_t *);
	return  ((FieldInfo_GetUnmanagedMarshal_m3736180640_ftn)mscorlib::System::Reflection::FieldInfo::GetUnmanagedMarshal) (__this);
}
// System.Reflection.Emit.UnmanagedMarshal System.Reflection.FieldInfo::get_UMarshal()
extern "C"  UnmanagedMarshal_t446138789 * FieldInfo_get_UMarshal_m3592475906 (FieldInfo_t * __this, const MethodInfo* method)
{
	{
		UnmanagedMarshal_t446138789 * L_0 = FieldInfo_GetUnmanagedMarshal_m3736180640(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Object[] System.Reflection.FieldInfo::GetPseudoCustomAttributes()
extern TypeInfo* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern TypeInfo* NonSerializedAttribute_t3281987872_il2cpp_TypeInfo_var;
extern TypeInfo* FieldOffsetAttribute_t3640292593_il2cpp_TypeInfo_var;
extern const uint32_t FieldInfo_GetPseudoCustomAttributes_m3219838173_MetadataUsageId;
extern "C"  ObjectU5BU5D_t11523773* FieldInfo_GetPseudoCustomAttributes_m3219838173 (FieldInfo_t * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FieldInfo_GetPseudoCustomAttributes_m3219838173_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	UnmanagedMarshal_t446138789 * V_1 = NULL;
	ObjectU5BU5D_t11523773* V_2 = NULL;
	{
		V_0 = 0;
		bool L_0 = VirtFuncInvoker0< bool >::Invoke(21 /* System.Boolean System.Reflection.FieldInfo::get_IsNotSerialized() */, __this);
		if (!L_0)
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_1 = V_0;
		V_0 = ((int32_t)((int32_t)L_1+(int32_t)1));
	}

IL_0011:
	{
		Type_t * L_2 = VirtFuncInvoker0< Type_t * >::Invoke(6 /* System.Type System.Reflection.MemberInfo::get_DeclaringType() */, __this);
		NullCheck(L_2);
		bool L_3 = VirtFuncInvoker0< bool >::Invoke(26 /* System.Boolean System.Type::get_IsExplicitLayout() */, L_2);
		if (!L_3)
		{
			goto IL_0025;
		}
	}
	{
		int32_t L_4 = V_0;
		V_0 = ((int32_t)((int32_t)L_4+(int32_t)1));
	}

IL_0025:
	{
		UnmanagedMarshal_t446138789 * L_5 = VirtFuncInvoker0< UnmanagedMarshal_t446138789 * >::Invoke(25 /* System.Reflection.Emit.UnmanagedMarshal System.Reflection.FieldInfo::get_UMarshal() */, __this);
		V_1 = L_5;
		UnmanagedMarshal_t446138789 * L_6 = V_1;
		if (!L_6)
		{
			goto IL_0036;
		}
	}
	{
		int32_t L_7 = V_0;
		V_0 = ((int32_t)((int32_t)L_7+(int32_t)1));
	}

IL_0036:
	{
		int32_t L_8 = V_0;
		if (L_8)
		{
			goto IL_003e;
		}
	}
	{
		return (ObjectU5BU5D_t11523773*)NULL;
	}

IL_003e:
	{
		int32_t L_9 = V_0;
		V_2 = ((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)L_9));
		V_0 = 0;
		bool L_10 = VirtFuncInvoker0< bool >::Invoke(21 /* System.Boolean System.Reflection.FieldInfo::get_IsNotSerialized() */, __this);
		if (!L_10)
		{
			goto IL_005e;
		}
	}
	{
		ObjectU5BU5D_t11523773* L_11 = V_2;
		int32_t L_12 = V_0;
		int32_t L_13 = L_12;
		V_0 = ((int32_t)((int32_t)L_13+(int32_t)1));
		NonSerializedAttribute_t3281987872 * L_14 = (NonSerializedAttribute_t3281987872 *)il2cpp_codegen_object_new(NonSerializedAttribute_t3281987872_il2cpp_TypeInfo_var);
		NonSerializedAttribute__ctor_m3429108074(L_14, /*hidden argument*/NULL);
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, L_13);
		ArrayElementTypeCheck (L_11, L_14);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(L_13), (Il2CppObject *)L_14);
	}

IL_005e:
	{
		Type_t * L_15 = VirtFuncInvoker0< Type_t * >::Invoke(6 /* System.Type System.Reflection.MemberInfo::get_DeclaringType() */, __this);
		NullCheck(L_15);
		bool L_16 = VirtFuncInvoker0< bool >::Invoke(26 /* System.Boolean System.Type::get_IsExplicitLayout() */, L_15);
		if (!L_16)
		{
			goto IL_0080;
		}
	}
	{
		ObjectU5BU5D_t11523773* L_17 = V_2;
		int32_t L_18 = V_0;
		int32_t L_19 = L_18;
		V_0 = ((int32_t)((int32_t)L_19+(int32_t)1));
		int32_t L_20 = VirtFuncInvoker0< int32_t >::Invoke(24 /* System.Int32 System.Reflection.FieldInfo::GetFieldOffset() */, __this);
		FieldOffsetAttribute_t3640292593 * L_21 = (FieldOffsetAttribute_t3640292593 *)il2cpp_codegen_object_new(FieldOffsetAttribute_t3640292593_il2cpp_TypeInfo_var);
		FieldOffsetAttribute__ctor_m3654465902(L_21, L_20, /*hidden argument*/NULL);
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, L_19);
		ArrayElementTypeCheck (L_17, L_21);
		(L_17)->SetAt(static_cast<il2cpp_array_size_t>(L_19), (Il2CppObject *)L_21);
	}

IL_0080:
	{
		UnmanagedMarshal_t446138789 * L_22 = V_1;
		if (!L_22)
		{
			goto IL_0093;
		}
	}
	{
		ObjectU5BU5D_t11523773* L_23 = V_2;
		int32_t L_24 = V_0;
		int32_t L_25 = L_24;
		V_0 = ((int32_t)((int32_t)L_25+(int32_t)1));
		UnmanagedMarshal_t446138789 * L_26 = V_1;
		NullCheck(L_26);
		MarshalAsAttribute_t1016163854 * L_27 = UnmanagedMarshal_ToMarshalAsAttribute_m4201209287(L_26, /*hidden argument*/NULL);
		NullCheck(L_23);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_23, L_25);
		ArrayElementTypeCheck (L_23, L_27);
		(L_23)->SetAt(static_cast<il2cpp_array_size_t>(L_25), (Il2CppObject *)L_27);
	}

IL_0093:
	{
		ObjectU5BU5D_t11523773* L_28 = V_2;
		return L_28;
	}
}
// System.Void System.Reflection.LocalVariableInfo::.ctor()
extern "C"  void LocalVariableInfo__ctor_m2779200515 (LocalVariableInfo_t721262211 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String System.Reflection.LocalVariableInfo::ToString()
extern TypeInfo* UInt16_t985925268_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3280460695;
extern Il2CppCodeGenString* _stringLiteral3297517298;
extern const uint32_t LocalVariableInfo_ToString_m1214843402_MetadataUsageId;
extern "C"  String_t* LocalVariableInfo_ToString_m1214843402 (LocalVariableInfo_t721262211 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (LocalVariableInfo_ToString_m1214843402_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = __this->get_is_pinned_1();
		if (!L_0)
		{
			goto IL_0027;
		}
	}
	{
		Type_t * L_1 = __this->get_type_0();
		uint16_t L_2 = __this->get_position_2();
		uint16_t L_3 = L_2;
		Il2CppObject * L_4 = Box(UInt16_t985925268_il2cpp_TypeInfo_var, &L_3);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = String_Format_m2398979370(NULL /*static, unused*/, _stringLiteral3280460695, L_1, L_4, /*hidden argument*/NULL);
		return L_5;
	}

IL_0027:
	{
		Type_t * L_6 = __this->get_type_0();
		uint16_t L_7 = __this->get_position_2();
		uint16_t L_8 = L_7;
		Il2CppObject * L_9 = Box(UInt16_t985925268_il2cpp_TypeInfo_var, &L_8);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_10 = String_Format_m2398979370(NULL /*static, unused*/, _stringLiteral3297517298, L_6, L_9, /*hidden argument*/NULL);
		return L_10;
	}
}
// System.Void System.Reflection.MemberFilter::.ctor(System.Object,System.IntPtr)
extern "C"  void MemberFilter__ctor_m1519003110 (MemberFilter_t1585748256 * __this, Il2CppObject * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->set_method_ptr_0((methodPointerType)((MethodInfo*)___method.get_m_value_0())->method);
	__this->set_method_3(___method);
	__this->set_m_target_2(___object);
}
// System.Boolean System.Reflection.MemberFilter::Invoke(System.Reflection.MemberInfo,System.Object)
extern "C"  bool MemberFilter_Invoke_m1252503396 (MemberFilter_t1585748256 * __this, MemberInfo_t * ___m, Il2CppObject * ___filterCriteria, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		MemberFilter_Invoke_m1252503396((MemberFilter_t1585748256 *)__this->get_prev_9(),___m, ___filterCriteria, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, MemberInfo_t * ___m, Il2CppObject * ___filterCriteria, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___m, ___filterCriteria,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (void* __this, MemberInfo_t * ___m, Il2CppObject * ___filterCriteria, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___m, ___filterCriteria,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, Il2CppObject * ___filterCriteria, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___m, ___filterCriteria,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C" bool pinvoke_delegate_wrapper_MemberFilter_t1585748256(Il2CppObject* delegate, MemberInfo_t * ___m, Il2CppObject * ___filterCriteria)
{
	// Marshaling of parameter '___m' to native representation
	IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Cannot marshal type 'System.Reflection.MemberInfo'."));
}
// System.IAsyncResult System.Reflection.MemberFilter::BeginInvoke(System.Reflection.MemberInfo,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * MemberFilter_BeginInvoke_m1668252761 (MemberFilter_t1585748256 * __this, MemberInfo_t * ___m, Il2CppObject * ___filterCriteria, AsyncCallback_t1363551830 * ___callback, Il2CppObject * ___object, const MethodInfo* method)
{
	void *__d_args[3] = {0};
	__d_args[0] = ___m;
	__d_args[1] = ___filterCriteria;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Boolean System.Reflection.MemberFilter::EndInvoke(System.IAsyncResult)
extern "C"  bool MemberFilter_EndInvoke_m3696206456 (MemberFilter_t1585748256 * __this, Il2CppObject * ___result, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Reflection.MemberInfo::.ctor()
extern "C"  void MemberInfo__ctor_m3915857030 (MemberInfo_t * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Reflection.Module System.Reflection.MemberInfo::get_Module()
extern "C"  Module_t206139610 * MemberInfo_get_Module_m3912547150 (MemberInfo_t * __this, const MethodInfo* method)
{
	{
		Type_t * L_0 = VirtFuncInvoker0< Type_t * >::Invoke(6 /* System.Type System.Reflection.MemberInfo::get_DeclaringType() */, __this);
		NullCheck(L_0);
		Module_t206139610 * L_1 = VirtFuncInvoker0< Module_t206139610 * >::Invoke(10 /* System.Reflection.Module System.Type::get_Module() */, L_0);
		return L_1;
	}
}
// System.Void System.Reflection.MemberInfoSerializationHolder::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern TypeInfo* SerializationException_t731558744_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1290870193;
extern Il2CppCodeGenString* _stringLiteral1994079235;
extern Il2CppCodeGenString* _stringLiteral2420395;
extern Il2CppCodeGenString* _stringLiteral3077552280;
extern Il2CppCodeGenString* _stringLiteral652315924;
extern const uint32_t MemberInfoSerializationHolder__ctor_m939139269_MetadataUsageId;
extern "C"  void MemberInfoSerializationHolder__ctor_m939139269 (MemberInfoSerializationHolder_t2338510946 * __this, SerializationInfo_t2995724695 * ___info, StreamingContext_t986364934  ___ctx, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MemberInfoSerializationHolder__ctor_m939139269_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	String_t* V_1 = NULL;
	Assembly_t1882292308 * V_2 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		SerializationInfo_t2995724695 * L_0 = ___info;
		NullCheck(L_0);
		String_t* L_1 = SerializationInfo_GetString_m52579033(L_0, _stringLiteral1290870193, /*hidden argument*/NULL);
		V_0 = L_1;
		SerializationInfo_t2995724695 * L_2 = ___info;
		NullCheck(L_2);
		String_t* L_3 = SerializationInfo_GetString_m52579033(L_2, _stringLiteral1994079235, /*hidden argument*/NULL);
		V_1 = L_3;
		SerializationInfo_t2995724695 * L_4 = ___info;
		NullCheck(L_4);
		String_t* L_5 = SerializationInfo_GetString_m52579033(L_4, _stringLiteral2420395, /*hidden argument*/NULL);
		__this->set__memberName_0(L_5);
		SerializationInfo_t2995724695 * L_6 = ___info;
		NullCheck(L_6);
		String_t* L_7 = SerializationInfo_GetString_m52579033(L_6, _stringLiteral3077552280, /*hidden argument*/NULL);
		__this->set__memberSignature_1(L_7);
		SerializationInfo_t2995724695 * L_8 = ___info;
		NullCheck(L_8);
		int32_t L_9 = SerializationInfo_GetInt32_m4048035953(L_8, _stringLiteral652315924, /*hidden argument*/NULL);
		__this->set__memberType_2(L_9);
	}

IL_0051:
	try
	{ // begin try (depth: 1)
		__this->set__genericArguments_4((TypeU5BU5D_t3431720054*)NULL);
		goto IL_0063;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (SerializationException_t731558744_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_005d;
		throw e;
	}

CATCH_005d:
	{ // begin catch(System.Runtime.Serialization.SerializationException)
		goto IL_0063;
	} // end catch (depth: 1)

IL_0063:
	{
		String_t* L_10 = V_0;
		Assembly_t1882292308 * L_11 = Assembly_Load_m4081902495(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		V_2 = L_11;
		Assembly_t1882292308 * L_12 = V_2;
		String_t* L_13 = V_1;
		NullCheck(L_12);
		Type_t * L_14 = VirtFuncInvoker3< Type_t *, String_t*, bool, bool >::Invoke(18 /* System.Type System.Reflection.Assembly::GetType(System.String,System.Boolean,System.Boolean) */, L_12, L_13, (bool)1, (bool)1);
		__this->set__reflectedType_3(L_14);
		return;
	}
}
// System.Void System.Reflection.MemberInfoSerializationHolder::Serialize(System.Runtime.Serialization.SerializationInfo,System.String,System.Type,System.String,System.Reflection.MemberTypes)
extern "C"  void MemberInfoSerializationHolder_Serialize_m1332311411 (Il2CppObject * __this /* static, unused */, SerializationInfo_t2995724695 * ___info, String_t* ___name, Type_t * ___klass, String_t* ___signature, int32_t ___type, const MethodInfo* method)
{
	{
		SerializationInfo_t2995724695 * L_0 = ___info;
		String_t* L_1 = ___name;
		Type_t * L_2 = ___klass;
		String_t* L_3 = ___signature;
		int32_t L_4 = ___type;
		MemberInfoSerializationHolder_Serialize_m2526201284(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, (TypeU5BU5D_t3431720054*)(TypeU5BU5D_t3431720054*)NULL, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Reflection.MemberInfoSerializationHolder::Serialize(System.Runtime.Serialization.SerializationInfo,System.String,System.Type,System.String,System.Reflection.MemberTypes,System.Type[])
extern const Il2CppType* MemberInfoSerializationHolder_t2338510946_0_0_0_var;
extern const Il2CppType* String_t_0_0_0_var;
extern const Il2CppType* TypeU5BU5D_t3431720054_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1290870193;
extern Il2CppCodeGenString* _stringLiteral1994079235;
extern Il2CppCodeGenString* _stringLiteral2420395;
extern Il2CppCodeGenString* _stringLiteral3077552280;
extern Il2CppCodeGenString* _stringLiteral652315924;
extern Il2CppCodeGenString* _stringLiteral527562079;
extern const uint32_t MemberInfoSerializationHolder_Serialize_m2526201284_MetadataUsageId;
extern "C"  void MemberInfoSerializationHolder_Serialize_m2526201284 (Il2CppObject * __this /* static, unused */, SerializationInfo_t2995724695 * ___info, String_t* ___name, Type_t * ___klass, String_t* ___signature, int32_t ___type, TypeU5BU5D_t3431720054* ___genericArguments, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MemberInfoSerializationHolder_Serialize_m2526201284_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		SerializationInfo_t2995724695 * L_0 = ___info;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(MemberInfoSerializationHolder_t2338510946_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_0);
		SerializationInfo_SetType_m3215055064(L_0, L_1, /*hidden argument*/NULL);
		SerializationInfo_t2995724695 * L_2 = ___info;
		Type_t * L_3 = ___klass;
		NullCheck(L_3);
		Module_t206139610 * L_4 = VirtFuncInvoker0< Module_t206139610 * >::Invoke(10 /* System.Reflection.Module System.Type::get_Module() */, L_3);
		NullCheck(L_4);
		Assembly_t1882292308 * L_5 = Module_get_Assembly_m4154223570(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		String_t* L_6 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.Assembly::get_FullName() */, L_5);
		Type_t * L_7 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(String_t_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_2);
		SerializationInfo_AddValue_m3341936982(L_2, _stringLiteral1290870193, L_6, L_7, /*hidden argument*/NULL);
		SerializationInfo_t2995724695 * L_8 = ___info;
		Type_t * L_9 = ___klass;
		NullCheck(L_9);
		String_t* L_10 = VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String System.Type::get_FullName() */, L_9);
		Type_t * L_11 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(String_t_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_8);
		SerializationInfo_AddValue_m3341936982(L_8, _stringLiteral1994079235, L_10, L_11, /*hidden argument*/NULL);
		SerializationInfo_t2995724695 * L_12 = ___info;
		String_t* L_13 = ___name;
		Type_t * L_14 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(String_t_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_12);
		SerializationInfo_AddValue_m3341936982(L_12, _stringLiteral2420395, L_13, L_14, /*hidden argument*/NULL);
		SerializationInfo_t2995724695 * L_15 = ___info;
		String_t* L_16 = ___signature;
		Type_t * L_17 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(String_t_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_15);
		SerializationInfo_AddValue_m3341936982(L_15, _stringLiteral3077552280, L_16, L_17, /*hidden argument*/NULL);
		SerializationInfo_t2995724695 * L_18 = ___info;
		int32_t L_19 = ___type;
		NullCheck(L_18);
		SerializationInfo_AddValue_m2348540514(L_18, _stringLiteral652315924, L_19, /*hidden argument*/NULL);
		SerializationInfo_t2995724695 * L_20 = ___info;
		TypeU5BU5D_t3431720054* L_21 = ___genericArguments;
		Type_t * L_22 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(TypeU5BU5D_t3431720054_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_20);
		SerializationInfo_AddValue_m3341936982(L_20, _stringLiteral527562079, (Il2CppObject *)(Il2CppObject *)L_21, L_22, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Reflection.MemberInfoSerializationHolder::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern TypeInfo* NotSupportedException_t1374155497_il2cpp_TypeInfo_var;
extern const uint32_t MemberInfoSerializationHolder_GetObjectData_m3301233442_MetadataUsageId;
extern "C"  void MemberInfoSerializationHolder_GetObjectData_m3301233442 (MemberInfoSerializationHolder_t2338510946 * __this, SerializationInfo_t2995724695 * ___info, StreamingContext_t986364934  ___context, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MemberInfoSerializationHolder_GetObjectData_m3301233442_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1374155497 * L_0 = (NotSupportedException_t1374155497 *)il2cpp_codegen_object_new(NotSupportedException_t1374155497_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Object System.Reflection.MemberInfoSerializationHolder::GetRealObject(System.Runtime.Serialization.StreamingContext)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* SerializationException_t731558744_il2cpp_TypeInfo_var;
extern TypeInfo* MemberTypes_t938013741_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1944928353;
extern Il2CppCodeGenString* _stringLiteral2320616926;
extern Il2CppCodeGenString* _stringLiteral1739733281;
extern Il2CppCodeGenString* _stringLiteral1209230866;
extern Il2CppCodeGenString* _stringLiteral2251356865;
extern Il2CppCodeGenString* _stringLiteral2208996953;
extern const uint32_t MemberInfoSerializationHolder_GetRealObject_m2979973448_MetadataUsageId;
extern "C"  Il2CppObject * MemberInfoSerializationHolder_GetRealObject_m2979973448 (MemberInfoSerializationHolder_t2338510946 * __this, StreamingContext_t986364934  ___context, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MemberInfoSerializationHolder_GetRealObject_m2979973448_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ConstructorInfoU5BU5D_t3572023667* V_0 = NULL;
	int32_t V_1 = 0;
	MethodInfoU5BU5D_t1668237648* V_2 = NULL;
	int32_t V_3 = 0;
	MethodInfo_t * V_4 = NULL;
	FieldInfo_t * V_5 = NULL;
	PropertyInfo_t * V_6 = NULL;
	EventInfo_t * V_7 = NULL;
	int32_t V_8 = 0;
	{
		int32_t L_0 = __this->get__memberType_2();
		V_8 = L_0;
		int32_t L_1 = V_8;
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 0)
		{
			goto IL_003f;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 1)
		{
			goto IL_01c2;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 2)
		{
			goto IL_0031;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 3)
		{
			goto IL_014c;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 4)
		{
			goto IL_0031;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 5)
		{
			goto IL_0031;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 6)
		{
			goto IL_0031;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 7)
		{
			goto IL_0099;
		}
	}

IL_0031:
	{
		int32_t L_2 = V_8;
		if ((((int32_t)L_2) == ((int32_t)((int32_t)16))))
		{
			goto IL_0187;
		}
	}
	{
		goto IL_01fd;
	}

IL_003f:
	{
		Type_t * L_3 = __this->get__reflectedType_3();
		NullCheck(L_3);
		ConstructorInfoU5BU5D_t3572023667* L_4 = VirtFuncInvoker1< ConstructorInfoU5BU5D_t3572023667*, int32_t >::Invoke(76 /* System.Reflection.ConstructorInfo[] System.Type::GetConstructors(System.Reflection.BindingFlags) */, L_3, ((int32_t)60));
		V_0 = L_4;
		V_1 = 0;
		goto IL_0074;
	}

IL_0054:
	{
		ConstructorInfoU5BU5D_t3572023667* L_5 = V_0;
		int32_t L_6 = V_1;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		int32_t L_7 = L_6;
		NullCheck(((L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_7))));
		String_t* L_8 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, ((L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_7))));
		String_t* L_9 = __this->get__memberSignature_1();
		NullCheck(L_8);
		bool L_10 = String_Equals_m3541721061(L_8, L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0070;
		}
	}
	{
		ConstructorInfoU5BU5D_t3572023667* L_11 = V_0;
		int32_t L_12 = V_1;
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, L_12);
		int32_t L_13 = L_12;
		return ((L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_13)));
	}

IL_0070:
	{
		int32_t L_14 = V_1;
		V_1 = ((int32_t)((int32_t)L_14+(int32_t)1));
	}

IL_0074:
	{
		int32_t L_15 = V_1;
		ConstructorInfoU5BU5D_t3572023667* L_16 = V_0;
		NullCheck(L_16);
		if ((((int32_t)L_15) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_16)->max_length)))))))
		{
			goto IL_0054;
		}
	}
	{
		String_t* L_17 = __this->get__memberSignature_1();
		Type_t * L_18 = __this->get__reflectedType_3();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_19 = String_Format_m2398979370(NULL /*static, unused*/, _stringLiteral1944928353, L_17, L_18, /*hidden argument*/NULL);
		SerializationException_t731558744 * L_20 = (SerializationException_t731558744 *)il2cpp_codegen_object_new(SerializationException_t731558744_il2cpp_TypeInfo_var);
		SerializationException__ctor_m4216356480(L_20, L_19, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_20);
	}

IL_0099:
	{
		Type_t * L_21 = __this->get__reflectedType_3();
		NullCheck(L_21);
		MethodInfoU5BU5D_t1668237648* L_22 = VirtFuncInvoker1< MethodInfoU5BU5D_t1668237648*, int32_t >::Invoke(55 /* System.Reflection.MethodInfo[] System.Type::GetMethods(System.Reflection.BindingFlags) */, L_21, ((int32_t)60));
		V_2 = L_22;
		V_3 = 0;
		goto IL_0127;
	}

IL_00ae:
	{
		MethodInfoU5BU5D_t1668237648* L_23 = V_2;
		int32_t L_24 = V_3;
		NullCheck(L_23);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_23, L_24);
		int32_t L_25 = L_24;
		NullCheck(((L_23)->GetAt(static_cast<il2cpp_array_size_t>(L_25))));
		String_t* L_26 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, ((L_23)->GetAt(static_cast<il2cpp_array_size_t>(L_25))));
		String_t* L_27 = __this->get__memberSignature_1();
		NullCheck(L_26);
		bool L_28 = String_Equals_m3541721061(L_26, L_27, /*hidden argument*/NULL);
		if (!L_28)
		{
			goto IL_00ca;
		}
	}
	{
		MethodInfoU5BU5D_t1668237648* L_29 = V_2;
		int32_t L_30 = V_3;
		NullCheck(L_29);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_29, L_30);
		int32_t L_31 = L_30;
		return ((L_29)->GetAt(static_cast<il2cpp_array_size_t>(L_31)));
	}

IL_00ca:
	{
		TypeU5BU5D_t3431720054* L_32 = __this->get__genericArguments_4();
		if (!L_32)
		{
			goto IL_0123;
		}
	}
	{
		MethodInfoU5BU5D_t1668237648* L_33 = V_2;
		int32_t L_34 = V_3;
		NullCheck(L_33);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_33, L_34);
		int32_t L_35 = L_34;
		NullCheck(((L_33)->GetAt(static_cast<il2cpp_array_size_t>(L_35))));
		bool L_36 = VirtFuncInvoker0< bool >::Invoke(30 /* System.Boolean System.Reflection.MethodInfo::get_IsGenericMethod() */, ((L_33)->GetAt(static_cast<il2cpp_array_size_t>(L_35))));
		if (!L_36)
		{
			goto IL_0123;
		}
	}
	{
		MethodInfoU5BU5D_t1668237648* L_37 = V_2;
		int32_t L_38 = V_3;
		NullCheck(L_37);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_37, L_38);
		int32_t L_39 = L_38;
		NullCheck(((L_37)->GetAt(static_cast<il2cpp_array_size_t>(L_39))));
		TypeU5BU5D_t3431720054* L_40 = VirtFuncInvoker0< TypeU5BU5D_t3431720054* >::Invoke(27 /* System.Type[] System.Reflection.MethodInfo::GetGenericArguments() */, ((L_37)->GetAt(static_cast<il2cpp_array_size_t>(L_39))));
		NullCheck(L_40);
		TypeU5BU5D_t3431720054* L_41 = __this->get__genericArguments_4();
		NullCheck(L_41);
		if ((!(((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_40)->max_length))))) == ((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_41)->max_length))))))))
		{
			goto IL_0123;
		}
	}
	{
		MethodInfoU5BU5D_t1668237648* L_42 = V_2;
		int32_t L_43 = V_3;
		NullCheck(L_42);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_42, L_43);
		int32_t L_44 = L_43;
		TypeU5BU5D_t3431720054* L_45 = __this->get__genericArguments_4();
		NullCheck(((L_42)->GetAt(static_cast<il2cpp_array_size_t>(L_44))));
		MethodInfo_t * L_46 = VirtFuncInvoker1< MethodInfo_t *, TypeU5BU5D_t3431720054* >::Invoke(33 /* System.Reflection.MethodInfo System.Reflection.MethodInfo::MakeGenericMethod(System.Type[]) */, ((L_42)->GetAt(static_cast<il2cpp_array_size_t>(L_44))), L_45);
		V_4 = L_46;
		MethodInfo_t * L_47 = V_4;
		NullCheck(L_47);
		String_t* L_48 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_47);
		String_t* L_49 = __this->get__memberSignature_1();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_50 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_48, L_49, /*hidden argument*/NULL);
		if (!L_50)
		{
			goto IL_0123;
		}
	}
	{
		MethodInfo_t * L_51 = V_4;
		return L_51;
	}

IL_0123:
	{
		int32_t L_52 = V_3;
		V_3 = ((int32_t)((int32_t)L_52+(int32_t)1));
	}

IL_0127:
	{
		int32_t L_53 = V_3;
		MethodInfoU5BU5D_t1668237648* L_54 = V_2;
		NullCheck(L_54);
		if ((((int32_t)L_53) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_54)->max_length)))))))
		{
			goto IL_00ae;
		}
	}
	{
		String_t* L_55 = __this->get__memberSignature_1();
		Type_t * L_56 = __this->get__reflectedType_3();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_57 = String_Format_m2398979370(NULL /*static, unused*/, _stringLiteral2320616926, L_55, L_56, /*hidden argument*/NULL);
		SerializationException_t731558744 * L_58 = (SerializationException_t731558744 *)il2cpp_codegen_object_new(SerializationException_t731558744_il2cpp_TypeInfo_var);
		SerializationException__ctor_m4216356480(L_58, L_57, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_58);
	}

IL_014c:
	{
		Type_t * L_59 = __this->get__reflectedType_3();
		String_t* L_60 = __this->get__memberName_0();
		NullCheck(L_59);
		FieldInfo_t * L_61 = VirtFuncInvoker2< FieldInfo_t *, String_t*, int32_t >::Invoke(47 /* System.Reflection.FieldInfo System.Type::GetField(System.String,System.Reflection.BindingFlags) */, L_59, L_60, ((int32_t)60));
		V_5 = L_61;
		FieldInfo_t * L_62 = V_5;
		if (!L_62)
		{
			goto IL_016b;
		}
	}
	{
		FieldInfo_t * L_63 = V_5;
		return L_63;
	}

IL_016b:
	{
		String_t* L_64 = __this->get__memberName_0();
		Type_t * L_65 = __this->get__reflectedType_3();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_66 = String_Format_m2398979370(NULL /*static, unused*/, _stringLiteral1739733281, L_64, L_65, /*hidden argument*/NULL);
		SerializationException_t731558744 * L_67 = (SerializationException_t731558744 *)il2cpp_codegen_object_new(SerializationException_t731558744_il2cpp_TypeInfo_var);
		SerializationException__ctor_m4216356480(L_67, L_66, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_67);
	}

IL_0187:
	{
		Type_t * L_68 = __this->get__reflectedType_3();
		String_t* L_69 = __this->get__memberName_0();
		NullCheck(L_68);
		PropertyInfo_t * L_70 = VirtFuncInvoker2< PropertyInfo_t *, String_t*, int32_t >::Invoke(58 /* System.Reflection.PropertyInfo System.Type::GetProperty(System.String,System.Reflection.BindingFlags) */, L_68, L_69, ((int32_t)60));
		V_6 = L_70;
		PropertyInfo_t * L_71 = V_6;
		if (!L_71)
		{
			goto IL_01a6;
		}
	}
	{
		PropertyInfo_t * L_72 = V_6;
		return L_72;
	}

IL_01a6:
	{
		String_t* L_73 = __this->get__memberName_0();
		Type_t * L_74 = __this->get__reflectedType_3();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_75 = String_Format_m2398979370(NULL /*static, unused*/, _stringLiteral1209230866, L_73, L_74, /*hidden argument*/NULL);
		SerializationException_t731558744 * L_76 = (SerializationException_t731558744 *)il2cpp_codegen_object_new(SerializationException_t731558744_il2cpp_TypeInfo_var);
		SerializationException__ctor_m4216356480(L_76, L_75, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_76);
	}

IL_01c2:
	{
		Type_t * L_77 = __this->get__reflectedType_3();
		String_t* L_78 = __this->get__memberName_0();
		NullCheck(L_77);
		EventInfo_t * L_79 = VirtFuncInvoker2< EventInfo_t *, String_t*, int32_t >::Invoke(45 /* System.Reflection.EventInfo System.Type::GetEvent(System.String,System.Reflection.BindingFlags) */, L_77, L_78, ((int32_t)60));
		V_7 = L_79;
		EventInfo_t * L_80 = V_7;
		if (!L_80)
		{
			goto IL_01e1;
		}
	}
	{
		EventInfo_t * L_81 = V_7;
		return L_81;
	}

IL_01e1:
	{
		String_t* L_82 = __this->get__memberName_0();
		Type_t * L_83 = __this->get__reflectedType_3();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_84 = String_Format_m2398979370(NULL /*static, unused*/, _stringLiteral2251356865, L_82, L_83, /*hidden argument*/NULL);
		SerializationException_t731558744 * L_85 = (SerializationException_t731558744 *)il2cpp_codegen_object_new(SerializationException_t731558744_il2cpp_TypeInfo_var);
		SerializationException__ctor_m4216356480(L_85, L_84, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_85);
	}

IL_01fd:
	{
		int32_t L_86 = __this->get__memberType_2();
		int32_t L_87 = L_86;
		Il2CppObject * L_88 = Box(MemberTypes_t938013741_il2cpp_TypeInfo_var, &L_87);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_89 = String_Format_m2471250780(NULL /*static, unused*/, _stringLiteral2208996953, L_88, /*hidden argument*/NULL);
		SerializationException_t731558744 * L_90 = (SerializationException_t731558744 *)il2cpp_codegen_object_new(SerializationException_t731558744_il2cpp_TypeInfo_var);
		SerializationException__ctor_m4216356480(L_90, L_89, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_90);
	}
}
// System.Void System.Reflection.MethodBase::.ctor()
extern "C"  void MethodBase__ctor_m2869737180 (MethodBase_t3461000640 * __this, const MethodInfo* method)
{
	{
		MemberInfo__ctor_m3915857030(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Reflection.MethodBase System.Reflection.MethodBase::GetMethodFromHandleNoGenericCheck(System.RuntimeMethodHandle)
extern TypeInfo* IntPtr_t_il2cpp_TypeInfo_var;
extern const uint32_t MethodBase_GetMethodFromHandleNoGenericCheck_m3857932574_MetadataUsageId;
extern "C"  MethodBase_t3461000640 * MethodBase_GetMethodFromHandleNoGenericCheck_m3857932574 (Il2CppObject * __this /* static, unused */, RuntimeMethodHandle_t2360005078  ___handle, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MethodBase_GetMethodFromHandleNoGenericCheck_m3857932574_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = RuntimeMethodHandle_get_Value_m2270388129((&___handle), /*hidden argument*/NULL);
		IntPtr_t L_1 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
		MethodBase_t3461000640 * L_2 = MethodBase_GetMethodFromIntPtr_m1804781085(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Reflection.MethodBase System.Reflection.MethodBase::GetMethodFromIntPtr(System.IntPtr,System.IntPtr)
extern TypeInfo* IntPtr_t_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentException_t124305799_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2720870788;
extern const uint32_t MethodBase_GetMethodFromIntPtr_m1804781085_MetadataUsageId;
extern "C"  MethodBase_t3461000640 * MethodBase_GetMethodFromIntPtr_m1804781085 (Il2CppObject * __this /* static, unused */, IntPtr_t ___handle, IntPtr_t ___declaringType, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MethodBase_GetMethodFromIntPtr_m1804781085_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	MethodBase_t3461000640 * V_0 = NULL;
	{
		IntPtr_t L_0 = ___handle;
		IntPtr_t L_1 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
		bool L_2 = IntPtr_op_Equality_m72843924(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_001b;
		}
	}
	{
		ArgumentException_t124305799 * L_3 = (ArgumentException_t124305799 *)il2cpp_codegen_object_new(ArgumentException_t124305799_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3544856547(L_3, _stringLiteral2720870788, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_001b:
	{
		IntPtr_t L_4 = ___handle;
		IntPtr_t L_5 = ___declaringType;
		MethodBase_t3461000640 * L_6 = MethodBase_GetMethodFromHandleInternalType_m1518673981(NULL /*static, unused*/, L_4, L_5, /*hidden argument*/NULL);
		V_0 = L_6;
		MethodBase_t3461000640 * L_7 = V_0;
		if (L_7)
		{
			goto IL_0034;
		}
	}
	{
		ArgumentException_t124305799 * L_8 = (ArgumentException_t124305799 *)il2cpp_codegen_object_new(ArgumentException_t124305799_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3544856547(L_8, _stringLiteral2720870788, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_8);
	}

IL_0034:
	{
		MethodBase_t3461000640 * L_9 = V_0;
		return L_9;
	}
}
// System.Reflection.MethodBase System.Reflection.MethodBase::GetMethodFromHandle(System.RuntimeMethodHandle)
extern TypeInfo* IntPtr_t_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentException_t124305799_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1453320788;
extern const uint32_t MethodBase_GetMethodFromHandle_m3548347628_MetadataUsageId;
extern "C"  MethodBase_t3461000640 * MethodBase_GetMethodFromHandle_m3548347628 (Il2CppObject * __this /* static, unused */, RuntimeMethodHandle_t2360005078  ___handle, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MethodBase_GetMethodFromHandle_m3548347628_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	MethodBase_t3461000640 * V_0 = NULL;
	Type_t * V_1 = NULL;
	{
		IntPtr_t L_0 = RuntimeMethodHandle_get_Value_m2270388129((&___handle), /*hidden argument*/NULL);
		IntPtr_t L_1 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
		MethodBase_t3461000640 * L_2 = MethodBase_GetMethodFromIntPtr_m1804781085(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		MethodBase_t3461000640 * L_3 = V_0;
		NullCheck(L_3);
		Type_t * L_4 = VirtFuncInvoker0< Type_t * >::Invoke(6 /* System.Type System.Reflection.MemberInfo::get_DeclaringType() */, L_3);
		V_1 = L_4;
		Type_t * L_5 = V_1;
		NullCheck(L_5);
		bool L_6 = VirtFuncInvoker0< bool >::Invoke(82 /* System.Boolean System.Type::get_IsGenericType() */, L_5);
		if (L_6)
		{
			goto IL_002f;
		}
	}
	{
		Type_t * L_7 = V_1;
		NullCheck(L_7);
		bool L_8 = VirtFuncInvoker0< bool >::Invoke(80 /* System.Boolean System.Type::get_IsGenericTypeDefinition() */, L_7);
		if (!L_8)
		{
			goto IL_003a;
		}
	}

IL_002f:
	{
		ArgumentException_t124305799 * L_9 = (ArgumentException_t124305799 *)il2cpp_codegen_object_new(ArgumentException_t124305799_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3544856547(L_9, _stringLiteral1453320788, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_003a:
	{
		MethodBase_t3461000640 * L_10 = V_0;
		return L_10;
	}
}
// System.Reflection.MethodBase System.Reflection.MethodBase::GetMethodFromHandleInternalType(System.IntPtr,System.IntPtr)
extern "C"  MethodBase_t3461000640 * MethodBase_GetMethodFromHandleInternalType_m1518673981 (Il2CppObject * __this /* static, unused */, IntPtr_t ___method_handle, IntPtr_t ___type_handle, const MethodInfo* method)
{
	using namespace il2cpp::icalls;
	typedef MethodBase_t3461000640 * (*MethodBase_GetMethodFromHandleInternalType_m1518673981_ftn) (IntPtr_t, IntPtr_t);
	return  ((MethodBase_GetMethodFromHandleInternalType_m1518673981_ftn)mscorlib::System::Reflection::MethodBase::GetMethodFromHandleInternalType) (___method_handle, ___type_handle);
}
// System.Int32 System.Reflection.MethodBase::GetParameterCount()
extern "C"  int32_t MethodBase_GetParameterCount_m3370013370 (MethodBase_t3461000640 * __this, const MethodInfo* method)
{
	ParameterInfoU5BU5D_t1127461800* V_0 = NULL;
	{
		ParameterInfoU5BU5D_t1127461800* L_0 = VirtFuncInvoker0< ParameterInfoU5BU5D_t1127461800* >::Invoke(15 /* System.Reflection.ParameterInfo[] System.Reflection.MethodBase::GetParameters() */, __this);
		V_0 = L_0;
		ParameterInfoU5BU5D_t1127461800* L_1 = V_0;
		if (L_1)
		{
			goto IL_000f;
		}
	}
	{
		return 0;
	}

IL_000f:
	{
		ParameterInfoU5BU5D_t1127461800* L_2 = V_0;
		NullCheck(L_2);
		return (((int32_t)((int32_t)(((Il2CppArray *)L_2)->max_length))));
	}
}
// System.Object System.Reflection.MethodBase::Invoke(System.Object,System.Object[])
extern "C"  Il2CppObject * MethodBase_Invoke_m3435166155 (MethodBase_t3461000640 * __this, Il2CppObject * ___obj, ObjectU5BU5D_t11523773* ___parameters, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___obj;
		ObjectU5BU5D_t11523773* L_1 = ___parameters;
		Il2CppObject * L_2 = VirtFuncInvoker5< Il2CppObject *, Il2CppObject *, int32_t, Binder_t4180926488 *, ObjectU5BU5D_t11523773*, CultureInfo_t3603717042 * >::Invoke(18 /* System.Object System.Reflection.MethodBase::Invoke(System.Object,System.Reflection.BindingFlags,System.Reflection.Binder,System.Object[],System.Globalization.CultureInfo) */, __this, L_0, 0, (Binder_t4180926488 *)NULL, L_1, (CultureInfo_t3603717042 *)NULL);
		return L_2;
	}
}
// System.Reflection.CallingConventions System.Reflection.MethodBase::get_CallingConvention()
extern "C"  int32_t MethodBase_get_CallingConvention_m1001031613 (MethodBase_t3461000640 * __this, const MethodInfo* method)
{
	{
		return (int32_t)(1);
	}
}
// System.Boolean System.Reflection.MethodBase::get_IsPublic()
extern "C"  bool MethodBase_get_IsPublic_m4089159654 (MethodBase_t3461000640 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Reflection.MethodAttributes System.Reflection.MethodBase::get_Attributes() */, __this);
		return (bool)((((int32_t)((int32_t)((int32_t)L_0&(int32_t)7))) == ((int32_t)6))? 1 : 0);
	}
}
// System.Boolean System.Reflection.MethodBase::get_IsStatic()
extern "C"  bool MethodBase_get_IsStatic_m4113878699 (MethodBase_t3461000640 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Reflection.MethodAttributes System.Reflection.MethodBase::get_Attributes() */, __this);
		return (bool)((((int32_t)((((int32_t)((int32_t)((int32_t)L_0&(int32_t)((int32_t)16)))) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Boolean System.Reflection.MethodBase::get_IsVirtual()
extern "C"  bool MethodBase_get_IsVirtual_m2071496208 (MethodBase_t3461000640 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Reflection.MethodAttributes System.Reflection.MethodBase::get_Attributes() */, __this);
		return (bool)((((int32_t)((((int32_t)((int32_t)((int32_t)L_0&(int32_t)((int32_t)64)))) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Boolean System.Reflection.MethodBase::get_IsAbstract()
extern "C"  bool MethodBase_get_IsAbstract_m1047696479 (MethodBase_t3461000640 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Reflection.MethodAttributes System.Reflection.MethodBase::get_Attributes() */, __this);
		return (bool)((((int32_t)((((int32_t)((int32_t)((int32_t)L_0&(int32_t)((int32_t)1024)))) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Int32 System.Reflection.MethodBase::get_next_table_index(System.Object,System.Int32,System.Boolean)
extern TypeInfo* MethodBuilder_t765486855_il2cpp_TypeInfo_var;
extern TypeInfo* ConstructorBuilder_t1859087886_il2cpp_TypeInfo_var;
extern TypeInfo* Exception_t1967233988_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3540446057;
extern const uint32_t MethodBase_get_next_table_index_m1161386136_MetadataUsageId;
extern "C"  int32_t MethodBase_get_next_table_index_m1161386136 (MethodBase_t3461000640 * __this, Il2CppObject * ___obj, int32_t ___table, bool ___inc, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MethodBase_get_next_table_index_m1161386136_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	MethodBuilder_t765486855 * V_0 = NULL;
	ConstructorBuilder_t1859087886 * V_1 = NULL;
	{
		if (!((MethodBuilder_t765486855 *)IsInstSealed(__this, MethodBuilder_t765486855_il2cpp_TypeInfo_var)))
		{
			goto IL_001c;
		}
	}
	{
		V_0 = ((MethodBuilder_t765486855 *)CastclassSealed(__this, MethodBuilder_t765486855_il2cpp_TypeInfo_var));
		MethodBuilder_t765486855 * L_0 = V_0;
		Il2CppObject * L_1 = ___obj;
		int32_t L_2 = ___table;
		bool L_3 = ___inc;
		NullCheck(L_0);
		int32_t L_4 = MethodBuilder_get_next_table_index_m3631261195(L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return L_4;
	}

IL_001c:
	{
		if (!((ConstructorBuilder_t1859087886 *)IsInstSealed(__this, ConstructorBuilder_t1859087886_il2cpp_TypeInfo_var)))
		{
			goto IL_0038;
		}
	}
	{
		V_1 = ((ConstructorBuilder_t1859087886 *)CastclassSealed(__this, ConstructorBuilder_t1859087886_il2cpp_TypeInfo_var));
		ConstructorBuilder_t1859087886 * L_5 = V_1;
		Il2CppObject * L_6 = ___obj;
		int32_t L_7 = ___table;
		bool L_8 = ___inc;
		NullCheck(L_5);
		int32_t L_9 = ConstructorBuilder_get_next_table_index_m4060451216(L_5, L_6, L_7, L_8, /*hidden argument*/NULL);
		return L_9;
	}

IL_0038:
	{
		Exception_t1967233988 * L_10 = (Exception_t1967233988 *)il2cpp_codegen_object_new(Exception_t1967233988_il2cpp_TypeInfo_var);
		Exception__ctor_m3870771296(L_10, _stringLiteral3540446057, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_10);
	}
}
// System.Type[] System.Reflection.MethodBase::GetGenericArguments()
extern TypeInfo* NotSupportedException_t1374155497_il2cpp_TypeInfo_var;
extern const uint32_t MethodBase_GetGenericArguments_m3136597879_MetadataUsageId;
extern "C"  TypeU5BU5D_t3431720054* MethodBase_GetGenericArguments_m3136597879 (MethodBase_t3461000640 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MethodBase_GetGenericArguments_m3136597879_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1374155497 * L_0 = (NotSupportedException_t1374155497 *)il2cpp_codegen_object_new(NotSupportedException_t1374155497_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean System.Reflection.MethodBase::get_ContainsGenericParameters()
extern "C"  bool MethodBase_get_ContainsGenericParameters_m30026257 (MethodBase_t3461000640 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Boolean System.Reflection.MethodBase::get_IsGenericMethodDefinition()
extern "C"  bool MethodBase_get_IsGenericMethodDefinition_m3580926992 (MethodBase_t3461000640 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Boolean System.Reflection.MethodBase::get_IsGenericMethod()
extern "C"  bool MethodBase_get_IsGenericMethod_m3165593181 (MethodBase_t3461000640 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Void System.Reflection.MethodInfo::.ctor()
extern "C"  void MethodInfo__ctor_m2414387615 (MethodInfo_t * __this, const MethodInfo* method)
{
	{
		MethodBase__ctor_m2869737180(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Reflection.MemberTypes System.Reflection.MethodInfo::get_MemberType()
extern "C"  int32_t MethodInfo_get_MemberType_m1399337402 (MethodInfo_t * __this, const MethodInfo* method)
{
	{
		return (int32_t)(8);
	}
}
// System.Type System.Reflection.MethodInfo::get_ReturnType()
extern "C"  Type_t * MethodInfo_get_ReturnType_m1349485746 (MethodInfo_t * __this, const MethodInfo* method)
{
	{
		return (Type_t *)NULL;
	}
}
// System.Reflection.MethodInfo System.Reflection.MethodInfo::MakeGenericMethod(System.Type[])
extern TypeInfo* NotSupportedException_t1374155497_il2cpp_TypeInfo_var;
extern const uint32_t MethodInfo_MakeGenericMethod_m548581224_MetadataUsageId;
extern "C"  MethodInfo_t * MethodInfo_MakeGenericMethod_m548581224 (MethodInfo_t * __this, TypeU5BU5D_t3431720054* ___typeArguments, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MethodInfo_MakeGenericMethod_m548581224_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Type_t * L_0 = Object_GetType_m2022236990(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		String_t* L_1 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Type::ToString() */, L_0);
		NotSupportedException_t1374155497 * L_2 = (NotSupportedException_t1374155497 *)il2cpp_codegen_object_new(NotSupportedException_t1374155497_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m133757637(L_2, L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}
}
// System.Type[] System.Reflection.MethodInfo::GetGenericArguments()
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t MethodInfo_GetGenericArguments_m2245069050_MetadataUsageId;
extern "C"  TypeU5BU5D_t3431720054* MethodInfo_GetGenericArguments_m2245069050 (MethodInfo_t * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MethodInfo_GetGenericArguments_m2245069050_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		TypeU5BU5D_t3431720054* L_0 = ((Type_t_StaticFields*)Type_t_il2cpp_TypeInfo_var->static_fields)->get_EmptyTypes_3();
		return L_0;
	}
}
// System.Boolean System.Reflection.MethodInfo::get_IsGenericMethod()
extern "C"  bool MethodInfo_get_IsGenericMethod_m2274064352 (MethodInfo_t * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Boolean System.Reflection.MethodInfo::get_IsGenericMethodDefinition()
extern "C"  bool MethodInfo_get_IsGenericMethodDefinition_m1886989779 (MethodInfo_t * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Boolean System.Reflection.MethodInfo::get_ContainsGenericParameters()
extern "C"  bool MethodInfo_get_ContainsGenericParameters_m2631056340 (MethodInfo_t * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Void System.Reflection.Missing::.ctor()
extern "C"  void Missing__ctor_m823829586 (Missing_t630192020 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Reflection.Missing::.cctor()
extern TypeInfo* Missing_t630192020_il2cpp_TypeInfo_var;
extern const uint32_t Missing__cctor_m3581784475_MetadataUsageId;
extern "C"  void Missing__cctor_m3581784475 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Missing__cctor_m3581784475_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Missing_t630192020 * L_0 = (Missing_t630192020 *)il2cpp_codegen_object_new(Missing_t630192020_il2cpp_TypeInfo_var);
		Missing__ctor_m823829586(L_0, /*hidden argument*/NULL);
		((Missing_t630192020_StaticFields*)Missing_t630192020_il2cpp_TypeInfo_var->static_fields)->set_Value_0(L_0);
		return;
	}
}
// System.Void System.Reflection.Missing::System.Runtime.Serialization.ISerializable.GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void Missing_System_Runtime_Serialization_ISerializable_GetObjectData_m3445091833 (Missing_t630192020 * __this, SerializationInfo_t2995724695 * ___info, StreamingContext_t986364934  ___context, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void System.Reflection.Module::.ctor()
extern "C"  void Module__ctor_m1944669826 (Module_t206139610 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Reflection.Module::.cctor()
extern TypeInfo* TypeFilter_t2379296192_il2cpp_TypeInfo_var;
extern TypeInfo* Module_t206139610_il2cpp_TypeInfo_var;
extern const MethodInfo* Module_filter_by_type_name_m1428086970_MethodInfo_var;
extern const MethodInfo* Module_filter_by_type_name_ignore_case_m825562172_MethodInfo_var;
extern const uint32_t Module__cctor_m3968093547_MetadataUsageId;
extern "C"  void Module__cctor_m3968093547 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Module__cctor_m3968093547_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0;
		L_0.set_m_value_0((void*)Module_filter_by_type_name_m1428086970_MethodInfo_var);
		TypeFilter_t2379296192 * L_1 = (TypeFilter_t2379296192 *)il2cpp_codegen_object_new(TypeFilter_t2379296192_il2cpp_TypeInfo_var);
		TypeFilter__ctor_m3304815558(L_1, NULL, L_0, /*hidden argument*/NULL);
		((Module_t206139610_StaticFields*)Module_t206139610_il2cpp_TypeInfo_var->static_fields)->set_FilterTypeName_1(L_1);
		IntPtr_t L_2;
		L_2.set_m_value_0((void*)Module_filter_by_type_name_ignore_case_m825562172_MethodInfo_var);
		TypeFilter_t2379296192 * L_3 = (TypeFilter_t2379296192 *)il2cpp_codegen_object_new(TypeFilter_t2379296192_il2cpp_TypeInfo_var);
		TypeFilter__ctor_m3304815558(L_3, NULL, L_2, /*hidden argument*/NULL);
		((Module_t206139610_StaticFields*)Module_t206139610_il2cpp_TypeInfo_var->static_fields)->set_FilterTypeNameIgnoreCase_2(L_3);
		return;
	}
}
// System.Reflection.Assembly System.Reflection.Module::get_Assembly()
extern "C"  Assembly_t1882292308 * Module_get_Assembly_m4154223570 (Module_t206139610 * __this, const MethodInfo* method)
{
	{
		Assembly_t1882292308 * L_0 = __this->get_assembly_4();
		return L_0;
	}
}
// System.String System.Reflection.Module::get_Name()
extern "C"  String_t* Module_get_Name_m2964298041 (Module_t206139610 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_name_6();
		return L_0;
	}
}
// System.String System.Reflection.Module::get_ScopeName()
extern "C"  String_t* Module_get_ScopeName_m724946931 (Module_t206139610 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_scopename_7();
		return L_0;
	}
}
// System.Object[] System.Reflection.Module::GetCustomAttributes(System.Type,System.Boolean)
extern TypeInfo* MonoCustomAttrs_t1707908163_il2cpp_TypeInfo_var;
extern const uint32_t Module_GetCustomAttributes_m2375287279_MetadataUsageId;
extern "C"  ObjectU5BU5D_t11523773* Module_GetCustomAttributes_m2375287279 (Module_t206139610 * __this, Type_t * ___attributeType, bool ___inherit, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Module_GetCustomAttributes_m2375287279_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Type_t * L_0 = ___attributeType;
		bool L_1 = ___inherit;
		IL2CPP_RUNTIME_CLASS_INIT(MonoCustomAttrs_t1707908163_il2cpp_TypeInfo_var);
		ObjectU5BU5D_t11523773* L_2 = MonoCustomAttrs_GetCustomAttributes_m3020328693(NULL /*static, unused*/, __this, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void System.Reflection.Module::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern TypeInfo* ArgumentNullException_t3214793280_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3237038;
extern const uint32_t Module_GetObjectData_m8314144_MetadataUsageId;
extern "C"  void Module_GetObjectData_m8314144 (Module_t206139610 * __this, SerializationInfo_t2995724695 * ___info, StreamingContext_t986364934  ___context, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Module_GetObjectData_m8314144_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		SerializationInfo_t2995724695 * L_0 = ___info;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t3214793280 * L_1 = (ArgumentNullException_t3214793280 *)il2cpp_codegen_object_new(ArgumentNullException_t3214793280_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_1, _stringLiteral3237038, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		SerializationInfo_t2995724695 * L_2 = ___info;
		StreamingContext_t986364934  L_3 = ___context;
		UnitySerializationHolder_GetModuleData_m3074840415(NULL /*static, unused*/, __this, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Type System.Reflection.Module::GetType(System.String)
extern "C"  Type_t * Module_GetType_m461877388 (Module_t206139610 * __this, String_t* ___className, const MethodInfo* method)
{
	{
		String_t* L_0 = ___className;
		Type_t * L_1 = VirtFuncInvoker3< Type_t *, String_t*, bool, bool >::Invoke(10 /* System.Type System.Reflection.Module::GetType(System.String,System.Boolean,System.Boolean) */, __this, L_0, (bool)0, (bool)0);
		return L_1;
	}
}
// System.Type System.Reflection.Module::GetType(System.String,System.Boolean,System.Boolean)
extern TypeInfo* ArgumentNullException_t3214793280_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentException_t124305799_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4285078563;
extern Il2CppCodeGenString* _stringLiteral3299190626;
extern const uint32_t Module_GetType_m1969891820_MetadataUsageId;
extern "C"  Type_t * Module_GetType_m1969891820 (Module_t206139610 * __this, String_t* ___className, bool ___throwOnError, bool ___ignoreCase, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Module_GetType_m1969891820_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___className;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t3214793280 * L_1 = (ArgumentNullException_t3214793280 *)il2cpp_codegen_object_new(ArgumentNullException_t3214793280_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_1, _stringLiteral4285078563, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		String_t* L_2 = ___className;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		bool L_4 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_002c;
		}
	}
	{
		ArgumentException_t124305799 * L_5 = (ArgumentException_t124305799 *)il2cpp_codegen_object_new(ArgumentException_t124305799_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3544856547(L_5, _stringLiteral3299190626, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_002c:
	{
		Assembly_t1882292308 * L_6 = __this->get_assembly_4();
		String_t* L_7 = ___className;
		bool L_8 = ___throwOnError;
		bool L_9 = ___ignoreCase;
		NullCheck(L_6);
		Type_t * L_10 = Assembly_InternalGetType_m2843289707(L_6, __this, L_7, L_8, L_9, /*hidden argument*/NULL);
		return L_10;
	}
}
// System.Type[] System.Reflection.Module::InternalGetTypes()
extern "C"  TypeU5BU5D_t3431720054* Module_InternalGetTypes_m2940532442 (Module_t206139610 * __this, const MethodInfo* method)
{
	using namespace il2cpp::icalls;
	typedef TypeU5BU5D_t3431720054* (*Module_InternalGetTypes_m2940532442_ftn) (Module_t206139610 *);
	return  ((Module_InternalGetTypes_m2940532442_ftn)mscorlib::System::Reflection::Module::InternalGetTypes) (__this);
}
// System.Type[] System.Reflection.Module::GetTypes()
extern "C"  TypeU5BU5D_t3431720054* Module_GetTypes_m134489405 (Module_t206139610 * __this, const MethodInfo* method)
{
	{
		TypeU5BU5D_t3431720054* L_0 = Module_InternalGetTypes_m2940532442(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Boolean System.Reflection.Module::IsDefined(System.Type,System.Boolean)
extern TypeInfo* MonoCustomAttrs_t1707908163_il2cpp_TypeInfo_var;
extern const uint32_t Module_IsDefined_m3759276225_MetadataUsageId;
extern "C"  bool Module_IsDefined_m3759276225 (Module_t206139610 * __this, Type_t * ___attributeType, bool ___inherit, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Module_IsDefined_m3759276225_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Type_t * L_0 = ___attributeType;
		bool L_1 = ___inherit;
		IL2CPP_RUNTIME_CLASS_INIT(MonoCustomAttrs_t1707908163_il2cpp_TypeInfo_var);
		bool L_2 = MonoCustomAttrs_IsDefined_m1052740451(NULL /*static, unused*/, __this, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Boolean System.Reflection.Module::IsResource()
extern "C"  bool Module_IsResource_m3085935772 (Module_t206139610 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_is_resource_8();
		return L_0;
	}
}
// System.String System.Reflection.Module::ToString()
extern "C"  String_t* Module_ToString_m2326023121 (Module_t206139610 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_name_6();
		return L_0;
	}
}
// System.Boolean System.Reflection.Module::filter_by_type_name(System.Type,System.Object)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral42;
extern const uint32_t Module_filter_by_type_name_m1428086970_MetadataUsageId;
extern "C"  bool Module_filter_by_type_name_m1428086970 (Il2CppObject * __this /* static, unused */, Type_t * ___m, Il2CppObject * ___filterCriteria, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Module_filter_by_type_name_m1428086970_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	{
		Il2CppObject * L_0 = ___filterCriteria;
		V_0 = ((String_t*)CastclassSealed(L_0, String_t_il2cpp_TypeInfo_var));
		String_t* L_1 = V_0;
		NullCheck(L_1);
		bool L_2 = String_EndsWith_m2265568550(L_1, _stringLiteral42, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0032;
		}
	}
	{
		Type_t * L_3 = ___m;
		NullCheck(L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_3);
		String_t* L_5 = V_0;
		String_t* L_6 = V_0;
		NullCheck(L_6);
		int32_t L_7 = String_get_Length_m2979997331(L_6, /*hidden argument*/NULL);
		NullCheck(L_5);
		String_t* L_8 = String_Substring_m675079568(L_5, 0, ((int32_t)((int32_t)L_7-(int32_t)1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		bool L_9 = String_StartsWith_m1500793453(L_4, L_8, /*hidden argument*/NULL);
		return L_9;
	}

IL_0032:
	{
		Type_t * L_10 = ___m;
		NullCheck(L_10);
		String_t* L_11 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_10);
		String_t* L_12 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_13 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_11, L_12, /*hidden argument*/NULL);
		return L_13;
	}
}
// System.Boolean System.Reflection.Module::filter_by_type_name_ignore_case(System.Type,System.Object)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral42;
extern const uint32_t Module_filter_by_type_name_ignore_case_m825562172_MetadataUsageId;
extern "C"  bool Module_filter_by_type_name_ignore_case_m825562172 (Il2CppObject * __this /* static, unused */, Type_t * ___m, Il2CppObject * ___filterCriteria, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Module_filter_by_type_name_ignore_case_m825562172_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	{
		Il2CppObject * L_0 = ___filterCriteria;
		V_0 = ((String_t*)CastclassSealed(L_0, String_t_il2cpp_TypeInfo_var));
		String_t* L_1 = V_0;
		NullCheck(L_1);
		bool L_2 = String_EndsWith_m2265568550(L_1, _stringLiteral42, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_003c;
		}
	}
	{
		Type_t * L_3 = ___m;
		NullCheck(L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_3);
		NullCheck(L_4);
		String_t* L_5 = String_ToLower_m2421900555(L_4, /*hidden argument*/NULL);
		String_t* L_6 = V_0;
		String_t* L_7 = V_0;
		NullCheck(L_7);
		int32_t L_8 = String_get_Length_m2979997331(L_7, /*hidden argument*/NULL);
		NullCheck(L_6);
		String_t* L_9 = String_Substring_m675079568(L_6, 0, ((int32_t)((int32_t)L_8-(int32_t)1)), /*hidden argument*/NULL);
		NullCheck(L_9);
		String_t* L_10 = String_ToLower_m2421900555(L_9, /*hidden argument*/NULL);
		NullCheck(L_5);
		bool L_11 = String_StartsWith_m1500793453(L_5, L_10, /*hidden argument*/NULL);
		return L_11;
	}

IL_003c:
	{
		Type_t * L_12 = ___m;
		NullCheck(L_12);
		String_t* L_13 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_12);
		String_t* L_14 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		int32_t L_15 = String_Compare_m1309590114(NULL /*static, unused*/, L_13, L_14, (bool)1, /*hidden argument*/NULL);
		return (bool)((((int32_t)L_15) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void System.Reflection.MonoCMethod::.ctor()
extern TypeInfo* ConstructorInfo_t3542137334_il2cpp_TypeInfo_var;
extern const uint32_t MonoCMethod__ctor_m1949313751_MetadataUsageId;
extern "C"  void MonoCMethod__ctor_m1949313751 (MonoCMethod_t1488000239 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MonoCMethod__ctor_m1949313751_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ConstructorInfo_t3542137334_il2cpp_TypeInfo_var);
		ConstructorInfo__ctor_m1318341424(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Reflection.MethodImplAttributes System.Reflection.MonoCMethod::GetMethodImplementationFlags()
extern "C"  int32_t MonoCMethod_GetMethodImplementationFlags_m3447640924 (MonoCMethod_t1488000239 * __this, const MethodInfo* method)
{
	{
		IntPtr_t L_0 = __this->get_mhandle_2();
		int32_t L_1 = MonoMethodInfo_GetMethodImplementationFlags_m3858313503(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Reflection.ParameterInfo[] System.Reflection.MonoCMethod::GetParameters()
extern "C"  ParameterInfoU5BU5D_t1127461800* MonoCMethod_GetParameters_m993439667 (MonoCMethod_t1488000239 * __this, const MethodInfo* method)
{
	{
		IntPtr_t L_0 = __this->get_mhandle_2();
		ParameterInfoU5BU5D_t1127461800* L_1 = MonoMethodInfo_GetParametersInfo_m1882458608(NULL /*static, unused*/, L_0, __this, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Object System.Reflection.MonoCMethod::InternalInvoke(System.Object,System.Object[],System.Exception&)
extern "C"  Il2CppObject * MonoCMethod_InternalInvoke_m1703330793 (MonoCMethod_t1488000239 * __this, Il2CppObject * ___obj, ObjectU5BU5D_t11523773* ___parameters, Exception_t1967233988 ** ___exc, const MethodInfo* method)
{
	using namespace il2cpp::icalls;
	typedef Il2CppObject * (*MonoCMethod_InternalInvoke_m1703330793_ftn) (MonoCMethod_t1488000239 *, Il2CppObject *, ObjectU5BU5D_t11523773*, Exception_t1967233988 **);
	return  ((MonoCMethod_InternalInvoke_m1703330793_ftn)mscorlib::System::Reflection::MonoMethod::InternalInvoke) (__this, ___obj, ___parameters, ___exc);
}
// System.Object System.Reflection.MonoCMethod::Invoke(System.Object,System.Reflection.BindingFlags,System.Reflection.Binder,System.Object[],System.Globalization.CultureInfo)
extern TypeInfo* Binder_t4180926488_il2cpp_TypeInfo_var;
extern TypeInfo* TargetParameterCountException_t2862237030_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentException_t124305799_il2cpp_TypeInfo_var;
extern TypeInfo* SecurityManager_t678461618_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* MemberAccessException_t3362876614_il2cpp_TypeInfo_var;
extern TypeInfo* Exception_t1967233988_il2cpp_TypeInfo_var;
extern TypeInfo* TargetInvocationException_t1980070524_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1513113777;
extern Il2CppCodeGenString* _stringLiteral556291225;
extern Il2CppCodeGenString* _stringLiteral176545962;
extern Il2CppCodeGenString* _stringLiteral203100290;
extern Il2CppCodeGenString* _stringLiteral3556540816;
extern const uint32_t MonoCMethod_Invoke_m3483919886_MetadataUsageId;
extern "C"  Il2CppObject * MonoCMethod_Invoke_m3483919886 (MonoCMethod_t1488000239 * __this, Il2CppObject * ___obj, int32_t ___invokeAttr, Binder_t4180926488 * ___binder, ObjectU5BU5D_t11523773* ___parameters, CultureInfo_t3603717042 * ___culture, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MonoCMethod_Invoke_m3483919886_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ParameterInfoU5BU5D_t1127461800* V_0 = NULL;
	int32_t V_1 = 0;
	Exception_t1967233988 * V_2 = NULL;
	Il2CppObject * V_3 = NULL;
	Exception_t1967233988 * V_4 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	Il2CppObject * G_B33_0 = NULL;
	{
		Binder_t4180926488 * L_0 = ___binder;
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Binder_t4180926488_il2cpp_TypeInfo_var);
		Binder_t4180926488 * L_1 = Binder_get_DefaultBinder_m695054407(NULL /*static, unused*/, /*hidden argument*/NULL);
		___binder = L_1;
	}

IL_000d:
	{
		ParameterInfoU5BU5D_t1127461800* L_2 = VirtFuncInvoker0< ParameterInfoU5BU5D_t1127461800* >::Invoke(15 /* System.Reflection.ParameterInfo[] System.Reflection.MonoCMethod::GetParameters() */, __this);
		V_0 = L_2;
		ObjectU5BU5D_t11523773* L_3 = ___parameters;
		if (L_3)
		{
			goto IL_0023;
		}
	}
	{
		ParameterInfoU5BU5D_t1127461800* L_4 = V_0;
		NullCheck(L_4);
		if ((((int32_t)((int32_t)(((Il2CppArray *)L_4)->max_length)))))
		{
			goto IL_0036;
		}
	}

IL_0023:
	{
		ObjectU5BU5D_t11523773* L_5 = ___parameters;
		if (!L_5)
		{
			goto IL_0041;
		}
	}
	{
		ObjectU5BU5D_t11523773* L_6 = ___parameters;
		NullCheck(L_6);
		ParameterInfoU5BU5D_t1127461800* L_7 = V_0;
		NullCheck(L_7);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_6)->max_length))))) == ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_7)->max_length)))))))
		{
			goto IL_0041;
		}
	}

IL_0036:
	{
		TargetParameterCountException_t2862237030 * L_8 = (TargetParameterCountException_t2862237030 *)il2cpp_codegen_object_new(TargetParameterCountException_t2862237030_il2cpp_TypeInfo_var);
		TargetParameterCountException__ctor_m2555943426(L_8, _stringLiteral1513113777, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_8);
	}

IL_0041:
	{
		int32_t L_9 = ___invokeAttr;
		if (((int32_t)((int32_t)L_9&(int32_t)((int32_t)65536))))
		{
			goto IL_006d;
		}
	}
	{
		Binder_t4180926488 * L_10 = ___binder;
		ObjectU5BU5D_t11523773* L_11 = ___parameters;
		ParameterInfoU5BU5D_t1127461800* L_12 = V_0;
		CultureInfo_t3603717042 * L_13 = ___culture;
		IL2CPP_RUNTIME_CLASS_INIT(Binder_t4180926488_il2cpp_TypeInfo_var);
		bool L_14 = Binder_ConvertArgs_m2442440337(NULL /*static, unused*/, L_10, L_11, L_12, L_13, /*hidden argument*/NULL);
		if (L_14)
		{
			goto IL_0068;
		}
	}
	{
		ArgumentException_t124305799 * L_15 = (ArgumentException_t124305799 *)il2cpp_codegen_object_new(ArgumentException_t124305799_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3544856547(L_15, _stringLiteral556291225, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_15);
	}

IL_0068:
	{
		goto IL_00a2;
	}

IL_006d:
	{
		V_1 = 0;
		goto IL_0099;
	}

IL_0074:
	{
		ObjectU5BU5D_t11523773* L_16 = ___parameters;
		int32_t L_17 = V_1;
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, L_17);
		int32_t L_18 = L_17;
		NullCheck(((L_16)->GetAt(static_cast<il2cpp_array_size_t>(L_18))));
		Type_t * L_19 = Object_GetType_m2022236990(((L_16)->GetAt(static_cast<il2cpp_array_size_t>(L_18))), /*hidden argument*/NULL);
		ParameterInfoU5BU5D_t1127461800* L_20 = V_0;
		int32_t L_21 = V_1;
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, L_21);
		int32_t L_22 = L_21;
		NullCheck(((L_20)->GetAt(static_cast<il2cpp_array_size_t>(L_22))));
		Type_t * L_23 = VirtFuncInvoker0< Type_t * >::Invoke(6 /* System.Type System.Reflection.ParameterInfo::get_ParameterType() */, ((L_20)->GetAt(static_cast<il2cpp_array_size_t>(L_22))));
		if ((((Il2CppObject*)(Type_t *)L_19) == ((Il2CppObject*)(Type_t *)L_23)))
		{
			goto IL_0095;
		}
	}
	{
		ArgumentException_t124305799 * L_24 = (ArgumentException_t124305799 *)il2cpp_codegen_object_new(ArgumentException_t124305799_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3544856547(L_24, _stringLiteral1513113777, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_24);
	}

IL_0095:
	{
		int32_t L_25 = V_1;
		V_1 = ((int32_t)((int32_t)L_25+(int32_t)1));
	}

IL_0099:
	{
		int32_t L_26 = V_1;
		ParameterInfoU5BU5D_t1127461800* L_27 = V_0;
		NullCheck(L_27);
		if ((((int32_t)L_26) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_27)->max_length)))))))
		{
			goto IL_0074;
		}
	}

IL_00a2:
	{
		IL2CPP_RUNTIME_CLASS_INIT(SecurityManager_t678461618_il2cpp_TypeInfo_var);
		bool L_28 = SecurityManager_get_SecurityEnabled_m2857115566(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_28)
		{
			goto IL_00b2;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SecurityManager_t678461618_il2cpp_TypeInfo_var);
		SecurityManager_ReflectedLinkDemandInvoke_m361815255(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
	}

IL_00b2:
	{
		Il2CppObject * L_29 = ___obj;
		if (L_29)
		{
			goto IL_00e3;
		}
	}
	{
		Type_t * L_30 = VirtFuncInvoker0< Type_t * >::Invoke(6 /* System.Type System.Reflection.MonoCMethod::get_DeclaringType() */, __this);
		NullCheck(L_30);
		bool L_31 = VirtFuncInvoker0< bool >::Invoke(79 /* System.Boolean System.Type::get_ContainsGenericParameters() */, L_30);
		if (!L_31)
		{
			goto IL_00e3;
		}
	}
	{
		Type_t * L_32 = VirtFuncInvoker0< Type_t * >::Invoke(6 /* System.Type System.Reflection.MonoCMethod::get_DeclaringType() */, __this);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_33 = String_Concat_m2809334143(NULL /*static, unused*/, _stringLiteral176545962, L_32, _stringLiteral203100290, /*hidden argument*/NULL);
		MemberAccessException_t3362876614 * L_34 = (MemberAccessException_t3362876614 *)il2cpp_codegen_object_new(MemberAccessException_t3362876614_il2cpp_TypeInfo_var);
		MemberAccessException__ctor_m2147680482(L_34, L_33, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_34);
	}

IL_00e3:
	{
		int32_t L_35 = ___invokeAttr;
		if (!((int32_t)((int32_t)L_35&(int32_t)((int32_t)512))))
		{
			goto IL_0115;
		}
	}
	{
		Type_t * L_36 = VirtFuncInvoker0< Type_t * >::Invoke(6 /* System.Type System.Reflection.MonoCMethod::get_DeclaringType() */, __this);
		NullCheck(L_36);
		bool L_37 = VirtFuncInvoker0< bool >::Invoke(20 /* System.Boolean System.Type::get_IsAbstract() */, L_36);
		if (!L_37)
		{
			goto IL_0115;
		}
	}
	{
		Type_t * L_38 = VirtFuncInvoker0< Type_t * >::Invoke(6 /* System.Type System.Reflection.MonoCMethod::get_DeclaringType() */, __this);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_39 = String_Format_m2471250780(NULL /*static, unused*/, _stringLiteral3556540816, L_38, /*hidden argument*/NULL);
		MemberAccessException_t3362876614 * L_40 = (MemberAccessException_t3362876614 *)il2cpp_codegen_object_new(MemberAccessException_t3362876614_il2cpp_TypeInfo_var);
		MemberAccessException__ctor_m2147680482(L_40, L_39, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_40);
	}

IL_0115:
	{
		V_2 = (Exception_t1967233988 *)NULL;
		V_3 = NULL;
	}

IL_0119:
	try
	{ // begin try (depth: 1)
		Il2CppObject * L_41 = ___obj;
		ObjectU5BU5D_t11523773* L_42 = ___parameters;
		Il2CppObject * L_43 = MonoCMethod_InternalInvoke_m1703330793(__this, L_41, L_42, (&V_2), /*hidden argument*/NULL);
		V_3 = L_43;
		goto IL_0139;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1967233988_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_012a;
		throw e;
	}

CATCH_012a:
	{ // begin catch(System.Exception)
		{
			V_4 = ((Exception_t1967233988 *)__exception_local);
			Exception_t1967233988 * L_44 = V_4;
			TargetInvocationException_t1980070524 * L_45 = (TargetInvocationException_t1980070524 *)il2cpp_codegen_object_new(TargetInvocationException_t1980070524_il2cpp_TypeInfo_var);
			TargetInvocationException__ctor_m2876037082(L_45, L_44, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_45);
		}

IL_0134:
		{
			goto IL_0139;
		}
	} // end catch (depth: 1)

IL_0139:
	{
		Exception_t1967233988 * L_46 = V_2;
		if (!L_46)
		{
			goto IL_0141;
		}
	}
	{
		Exception_t1967233988 * L_47 = V_2;
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_47);
	}

IL_0141:
	{
		Il2CppObject * L_48 = ___obj;
		if (L_48)
		{
			goto IL_014d;
		}
	}
	{
		Il2CppObject * L_49 = V_3;
		G_B33_0 = L_49;
		goto IL_014e;
	}

IL_014d:
	{
		G_B33_0 = NULL;
	}

IL_014e:
	{
		return G_B33_0;
	}
}
// System.Object System.Reflection.MonoCMethod::Invoke(System.Reflection.BindingFlags,System.Reflection.Binder,System.Object[],System.Globalization.CultureInfo)
extern "C"  Il2CppObject * MonoCMethod_Invoke_m2473345692 (MonoCMethod_t1488000239 * __this, int32_t ___invokeAttr, Binder_t4180926488 * ___binder, ObjectU5BU5D_t11523773* ___parameters, CultureInfo_t3603717042 * ___culture, const MethodInfo* method)
{
	{
		int32_t L_0 = ___invokeAttr;
		Binder_t4180926488 * L_1 = ___binder;
		ObjectU5BU5D_t11523773* L_2 = ___parameters;
		CultureInfo_t3603717042 * L_3 = ___culture;
		Il2CppObject * L_4 = VirtFuncInvoker5< Il2CppObject *, Il2CppObject *, int32_t, Binder_t4180926488 *, ObjectU5BU5D_t11523773*, CultureInfo_t3603717042 * >::Invoke(18 /* System.Object System.Reflection.MonoCMethod::Invoke(System.Object,System.Reflection.BindingFlags,System.Reflection.Binder,System.Object[],System.Globalization.CultureInfo) */, __this, NULL, L_0, L_1, L_2, L_3);
		return L_4;
	}
}
// System.RuntimeMethodHandle System.Reflection.MonoCMethod::get_MethodHandle()
extern "C"  RuntimeMethodHandle_t2360005078  MonoCMethod_get_MethodHandle_m1736599926 (MonoCMethod_t1488000239 * __this, const MethodInfo* method)
{
	{
		IntPtr_t L_0 = __this->get_mhandle_2();
		RuntimeMethodHandle_t2360005078  L_1;
		memset(&L_1, 0, sizeof(L_1));
		RuntimeMethodHandle__ctor_m794057668(&L_1, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Reflection.MethodAttributes System.Reflection.MonoCMethod::get_Attributes()
extern "C"  int32_t MonoCMethod_get_Attributes_m2492640542 (MonoCMethod_t1488000239 * __this, const MethodInfo* method)
{
	{
		IntPtr_t L_0 = __this->get_mhandle_2();
		int32_t L_1 = MonoMethodInfo_GetAttributes_m3368954492(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Reflection.CallingConventions System.Reflection.MonoCMethod::get_CallingConvention()
extern "C"  int32_t MonoCMethod_get_CallingConvention_m3412315242 (MonoCMethod_t1488000239 * __this, const MethodInfo* method)
{
	{
		IntPtr_t L_0 = __this->get_mhandle_2();
		int32_t L_1 = MonoMethodInfo_GetCallingConvention_m4186754404(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Type System.Reflection.MonoCMethod::get_ReflectedType()
extern "C"  Type_t * MonoCMethod_get_ReflectedType_m3209895836 (MonoCMethod_t1488000239 * __this, const MethodInfo* method)
{
	{
		Type_t * L_0 = __this->get_reftype_4();
		return L_0;
	}
}
// System.Type System.Reflection.MonoCMethod::get_DeclaringType()
extern "C"  Type_t * MonoCMethod_get_DeclaringType_m3979449063 (MonoCMethod_t1488000239 * __this, const MethodInfo* method)
{
	{
		IntPtr_t L_0 = __this->get_mhandle_2();
		Type_t * L_1 = MonoMethodInfo_GetDeclaringType_m1399000903(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.String System.Reflection.MonoCMethod::get_Name()
extern "C"  String_t* MonoCMethod_get_Name_m2330810846 (MonoCMethod_t1488000239 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_name_3();
		if (!L_0)
		{
			goto IL_0012;
		}
	}
	{
		String_t* L_1 = __this->get_name_3();
		return L_1;
	}

IL_0012:
	{
		String_t* L_2 = MonoMethod_get_name_m439428219(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Boolean System.Reflection.MonoCMethod::IsDefined(System.Type,System.Boolean)
extern TypeInfo* MonoCustomAttrs_t1707908163_il2cpp_TypeInfo_var;
extern const uint32_t MonoCMethod_IsDefined_m2679410586_MetadataUsageId;
extern "C"  bool MonoCMethod_IsDefined_m2679410586 (MonoCMethod_t1488000239 * __this, Type_t * ___attributeType, bool ___inherit, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MonoCMethod_IsDefined_m2679410586_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Type_t * L_0 = ___attributeType;
		bool L_1 = ___inherit;
		IL2CPP_RUNTIME_CLASS_INIT(MonoCustomAttrs_t1707908163_il2cpp_TypeInfo_var);
		bool L_2 = MonoCustomAttrs_IsDefined_m1052740451(NULL /*static, unused*/, __this, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Object[] System.Reflection.MonoCMethod::GetCustomAttributes(System.Boolean)
extern TypeInfo* MonoCustomAttrs_t1707908163_il2cpp_TypeInfo_var;
extern const uint32_t MonoCMethod_GetCustomAttributes_m3440607709_MetadataUsageId;
extern "C"  ObjectU5BU5D_t11523773* MonoCMethod_GetCustomAttributes_m3440607709 (MonoCMethod_t1488000239 * __this, bool ___inherit, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MonoCMethod_GetCustomAttributes_m3440607709_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = ___inherit;
		IL2CPP_RUNTIME_CLASS_INIT(MonoCustomAttrs_t1707908163_il2cpp_TypeInfo_var);
		ObjectU5BU5D_t11523773* L_1 = MonoCustomAttrs_GetCustomAttributes_m3210789640(NULL /*static, unused*/, __this, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Object[] System.Reflection.MonoCMethod::GetCustomAttributes(System.Type,System.Boolean)
extern TypeInfo* MonoCustomAttrs_t1707908163_il2cpp_TypeInfo_var;
extern const uint32_t MonoCMethod_GetCustomAttributes_m2022749770_MetadataUsageId;
extern "C"  ObjectU5BU5D_t11523773* MonoCMethod_GetCustomAttributes_m2022749770 (MonoCMethod_t1488000239 * __this, Type_t * ___attributeType, bool ___inherit, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MonoCMethod_GetCustomAttributes_m2022749770_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Type_t * L_0 = ___attributeType;
		bool L_1 = ___inherit;
		IL2CPP_RUNTIME_CLASS_INIT(MonoCustomAttrs_t1707908163_il2cpp_TypeInfo_var);
		ObjectU5BU5D_t11523773* L_2 = MonoCustomAttrs_GetCustomAttributes_m3020328693(NULL /*static, unused*/, __this, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.String System.Reflection.MonoCMethod::ToString()
extern TypeInfo* StringBuilder_t3822575854_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral82833644;
extern Il2CppCodeGenString* _stringLiteral40;
extern Il2CppCodeGenString* _stringLiteral1396;
extern Il2CppCodeGenString* _stringLiteral41633914;
extern Il2CppCodeGenString* _stringLiteral41;
extern const uint32_t MonoCMethod_ToString_m1692535926_MetadataUsageId;
extern "C"  String_t* MonoCMethod_ToString_m1692535926 (MonoCMethod_t1488000239 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MonoCMethod_ToString_m1692535926_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	StringBuilder_t3822575854 * V_0 = NULL;
	ParameterInfoU5BU5D_t1127461800* V_1 = NULL;
	int32_t V_2 = 0;
	{
		StringBuilder_t3822575854 * L_0 = (StringBuilder_t3822575854 *)il2cpp_codegen_object_new(StringBuilder_t3822575854_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m135953004(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		StringBuilder_t3822575854 * L_1 = V_0;
		NullCheck(L_1);
		StringBuilder_Append_m3898090075(L_1, _stringLiteral82833644, /*hidden argument*/NULL);
		StringBuilder_t3822575854 * L_2 = V_0;
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MonoCMethod::get_Name() */, __this);
		NullCheck(L_2);
		StringBuilder_Append_m3898090075(L_2, L_3, /*hidden argument*/NULL);
		StringBuilder_t3822575854 * L_4 = V_0;
		NullCheck(L_4);
		StringBuilder_Append_m3898090075(L_4, _stringLiteral40, /*hidden argument*/NULL);
		ParameterInfoU5BU5D_t1127461800* L_5 = VirtFuncInvoker0< ParameterInfoU5BU5D_t1127461800* >::Invoke(15 /* System.Reflection.ParameterInfo[] System.Reflection.MonoCMethod::GetParameters() */, __this);
		V_1 = L_5;
		V_2 = 0;
		goto IL_0064;
	}

IL_0039:
	{
		int32_t L_6 = V_2;
		if ((((int32_t)L_6) <= ((int32_t)0)))
		{
			goto IL_004c;
		}
	}
	{
		StringBuilder_t3822575854 * L_7 = V_0;
		NullCheck(L_7);
		StringBuilder_Append_m3898090075(L_7, _stringLiteral1396, /*hidden argument*/NULL);
	}

IL_004c:
	{
		StringBuilder_t3822575854 * L_8 = V_0;
		ParameterInfoU5BU5D_t1127461800* L_9 = V_1;
		int32_t L_10 = V_2;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, L_10);
		int32_t L_11 = L_10;
		NullCheck(((L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11))));
		Type_t * L_12 = VirtFuncInvoker0< Type_t * >::Invoke(6 /* System.Type System.Reflection.ParameterInfo::get_ParameterType() */, ((L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11))));
		NullCheck(L_12);
		String_t* L_13 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_12);
		NullCheck(L_8);
		StringBuilder_Append_m3898090075(L_8, L_13, /*hidden argument*/NULL);
		int32_t L_14 = V_2;
		V_2 = ((int32_t)((int32_t)L_14+(int32_t)1));
	}

IL_0064:
	{
		int32_t L_15 = V_2;
		ParameterInfoU5BU5D_t1127461800* L_16 = V_1;
		NullCheck(L_16);
		if ((((int32_t)L_15) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_16)->max_length)))))))
		{
			goto IL_0039;
		}
	}
	{
		int32_t L_17 = VirtFuncInvoker0< int32_t >::Invoke(21 /* System.Reflection.CallingConventions System.Reflection.MonoCMethod::get_CallingConvention() */, __this);
		if ((!(((uint32_t)L_17) == ((uint32_t)3))))
		{
			goto IL_0085;
		}
	}
	{
		StringBuilder_t3822575854 * L_18 = V_0;
		NullCheck(L_18);
		StringBuilder_Append_m3898090075(L_18, _stringLiteral41633914, /*hidden argument*/NULL);
	}

IL_0085:
	{
		StringBuilder_t3822575854 * L_19 = V_0;
		NullCheck(L_19);
		StringBuilder_Append_m3898090075(L_19, _stringLiteral41, /*hidden argument*/NULL);
		StringBuilder_t3822575854 * L_20 = V_0;
		NullCheck(L_20);
		String_t* L_21 = StringBuilder_ToString_m350379841(L_20, /*hidden argument*/NULL);
		return L_21;
	}
}
// System.Void System.Reflection.MonoCMethod::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void MonoCMethod_GetObjectData_m3949447925 (MonoCMethod_t1488000239 * __this, SerializationInfo_t2995724695 * ___info, StreamingContext_t986364934  ___context, const MethodInfo* method)
{
	{
		SerializationInfo_t2995724695 * L_0 = ___info;
		String_t* L_1 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MonoCMethod::get_Name() */, __this);
		Type_t * L_2 = VirtFuncInvoker0< Type_t * >::Invoke(9 /* System.Type System.Reflection.MonoCMethod::get_ReflectedType() */, __this);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Reflection.MonoCMethod::ToString() */, __this);
		MemberInfoSerializationHolder_Serialize_m1332311411(NULL /*static, unused*/, L_0, L_1, L_2, L_3, 1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Reflection.MonoEvent::.ctor()
extern "C"  void MonoEvent__ctor_m1732756001 (MonoEvent_t * __this, const MethodInfo* method)
{
	{
		EventInfo__ctor_m2461757520(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Reflection.EventAttributes System.Reflection.MonoEvent::get_Attributes()
extern "C"  int32_t MonoEvent_get_Attributes_m3196030141 (MonoEvent_t * __this, const MethodInfo* method)
{
	MonoEventInfo_t4117885171  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		MonoEventInfo_t4117885171  L_0 = MonoEventInfo_GetEventInfo_m3342016238(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = (&V_0)->get_attrs_6();
		return L_1;
	}
}
// System.Reflection.MethodInfo System.Reflection.MonoEvent::GetAddMethod(System.Boolean)
extern "C"  MethodInfo_t * MonoEvent_GetAddMethod_m3118173230 (MonoEvent_t * __this, bool ___nonPublic, const MethodInfo* method)
{
	MonoEventInfo_t4117885171  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		MonoEventInfo_t4117885171  L_0 = MonoEventInfo_GetEventInfo_m3342016238(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		V_0 = L_0;
		bool L_1 = ___nonPublic;
		if (L_1)
		{
			goto IL_002a;
		}
	}
	{
		MethodInfo_t * L_2 = (&V_0)->get_add_method_3();
		if (!L_2)
		{
			goto IL_0032;
		}
	}
	{
		MethodInfo_t * L_3 = (&V_0)->get_add_method_3();
		NullCheck(L_3);
		bool L_4 = VirtFuncInvoker0< bool >::Invoke(22 /* System.Boolean System.Reflection.MethodBase::get_IsPublic() */, L_3);
		if (!L_4)
		{
			goto IL_0032;
		}
	}

IL_002a:
	{
		MethodInfo_t * L_5 = (&V_0)->get_add_method_3();
		return L_5;
	}

IL_0032:
	{
		return (MethodInfo_t *)NULL;
	}
}
// System.Type System.Reflection.MonoEvent::get_DeclaringType()
extern "C"  Type_t * MonoEvent_get_DeclaringType_m3994610097 (MonoEvent_t * __this, const MethodInfo* method)
{
	MonoEventInfo_t4117885171  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		MonoEventInfo_t4117885171  L_0 = MonoEventInfo_GetEventInfo_m3342016238(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		V_0 = L_0;
		Type_t * L_1 = (&V_0)->get_declaring_type_0();
		return L_1;
	}
}
// System.Type System.Reflection.MonoEvent::get_ReflectedType()
extern "C"  Type_t * MonoEvent_get_ReflectedType_m3225056870 (MonoEvent_t * __this, const MethodInfo* method)
{
	MonoEventInfo_t4117885171  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		MonoEventInfo_t4117885171  L_0 = MonoEventInfo_GetEventInfo_m3342016238(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		V_0 = L_0;
		Type_t * L_1 = (&V_0)->get_reflected_type_1();
		return L_1;
	}
}
// System.String System.Reflection.MonoEvent::get_Name()
extern "C"  String_t* MonoEvent_get_Name_m4045390228 (MonoEvent_t * __this, const MethodInfo* method)
{
	MonoEventInfo_t4117885171  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		MonoEventInfo_t4117885171  L_0 = MonoEventInfo_GetEventInfo_m3342016238(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		V_0 = L_0;
		String_t* L_1 = (&V_0)->get_name_2();
		return L_1;
	}
}
// System.String System.Reflection.MonoEvent::ToString()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral32;
extern const uint32_t MonoEvent_ToString_m3407115308_MetadataUsageId;
extern "C"  String_t* MonoEvent_ToString_m3407115308 (MonoEvent_t * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MonoEvent_ToString_m3407115308_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Type_t * L_0 = VirtFuncInvoker0< Type_t * >::Invoke(15 /* System.Type System.Reflection.EventInfo::get_EventHandlerType() */, __this);
		String_t* L_1 = MonoEvent_get_Name_m4045390228(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = String_Concat_m2809334143(NULL /*static, unused*/, L_0, _stringLiteral32, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Boolean System.Reflection.MonoEvent::IsDefined(System.Type,System.Boolean)
extern TypeInfo* MonoCustomAttrs_t1707908163_il2cpp_TypeInfo_var;
extern const uint32_t MonoEvent_IsDefined_m3229573604_MetadataUsageId;
extern "C"  bool MonoEvent_IsDefined_m3229573604 (MonoEvent_t * __this, Type_t * ___attributeType, bool ___inherit, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MonoEvent_IsDefined_m3229573604_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Type_t * L_0 = ___attributeType;
		bool L_1 = ___inherit;
		IL2CPP_RUNTIME_CLASS_INIT(MonoCustomAttrs_t1707908163_il2cpp_TypeInfo_var);
		bool L_2 = MonoCustomAttrs_IsDefined_m1052740451(NULL /*static, unused*/, __this, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Object[] System.Reflection.MonoEvent::GetCustomAttributes(System.Boolean)
extern TypeInfo* MonoCustomAttrs_t1707908163_il2cpp_TypeInfo_var;
extern const uint32_t MonoEvent_GetCustomAttributes_m2984693479_MetadataUsageId;
extern "C"  ObjectU5BU5D_t11523773* MonoEvent_GetCustomAttributes_m2984693479 (MonoEvent_t * __this, bool ___inherit, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MonoEvent_GetCustomAttributes_m2984693479_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = ___inherit;
		IL2CPP_RUNTIME_CLASS_INIT(MonoCustomAttrs_t1707908163_il2cpp_TypeInfo_var);
		ObjectU5BU5D_t11523773* L_1 = MonoCustomAttrs_GetCustomAttributes_m3210789640(NULL /*static, unused*/, __this, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Object[] System.Reflection.MonoEvent::GetCustomAttributes(System.Type,System.Boolean)
extern TypeInfo* MonoCustomAttrs_t1707908163_il2cpp_TypeInfo_var;
extern const uint32_t MonoEvent_GetCustomAttributes_m1928774740_MetadataUsageId;
extern "C"  ObjectU5BU5D_t11523773* MonoEvent_GetCustomAttributes_m1928774740 (MonoEvent_t * __this, Type_t * ___attributeType, bool ___inherit, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MonoEvent_GetCustomAttributes_m1928774740_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Type_t * L_0 = ___attributeType;
		bool L_1 = ___inherit;
		IL2CPP_RUNTIME_CLASS_INIT(MonoCustomAttrs_t1707908163_il2cpp_TypeInfo_var);
		ObjectU5BU5D_t11523773* L_2 = MonoCustomAttrs_GetCustomAttributes_m3020328693(NULL /*static, unused*/, __this, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void System.Reflection.MonoEvent::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void MonoEvent_GetObjectData_m2378937663 (MonoEvent_t * __this, SerializationInfo_t2995724695 * ___info, StreamingContext_t986364934  ___context, const MethodInfo* method)
{
	{
		SerializationInfo_t2995724695 * L_0 = ___info;
		String_t* L_1 = MonoEvent_get_Name_m4045390228(__this, /*hidden argument*/NULL);
		Type_t * L_2 = MonoEvent_get_ReflectedType_m3225056870(__this, /*hidden argument*/NULL);
		String_t* L_3 = MonoEvent_ToString_m3407115308(__this, /*hidden argument*/NULL);
		MemberInfoSerializationHolder_Serialize_m1332311411(NULL /*static, unused*/, L_0, L_1, L_2, L_3, 2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Reflection.MonoEventInfo::get_event_info(System.Reflection.MonoEvent,System.Reflection.MonoEventInfo&)
extern "C"  void MonoEventInfo_get_event_info_m4253936399 (Il2CppObject * __this /* static, unused */, MonoEvent_t * ___ev, MonoEventInfo_t4117885171 * ___info, const MethodInfo* method)
{
	using namespace il2cpp::icalls;
	typedef void (*MonoEventInfo_get_event_info_m4253936399_ftn) (MonoEvent_t *, MonoEventInfo_t4117885171 *);
	 ((MonoEventInfo_get_event_info_m4253936399_ftn)mscorlib::System::Reflection::MonoEventInfo::get_event_info) (___ev, ___info);
}
// System.Reflection.MonoEventInfo System.Reflection.MonoEventInfo::GetEventInfo(System.Reflection.MonoEvent)
extern "C"  MonoEventInfo_t4117885171  MonoEventInfo_GetEventInfo_m3342016238 (Il2CppObject * __this /* static, unused */, MonoEvent_t * ___ev, const MethodInfo* method)
{
	MonoEventInfo_t4117885171  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		MonoEvent_t * L_0 = ___ev;
		MonoEventInfo_get_event_info_m4253936399(NULL /*static, unused*/, L_0, (&V_0), /*hidden argument*/NULL);
		MonoEventInfo_t4117885171  L_1 = V_0;
		return L_1;
	}
}
// System.Void System.Reflection.MonoField::.ctor()
extern "C"  void MonoField__ctor_m3189620673 (MonoField_t * __this, const MethodInfo* method)
{
	{
		FieldInfo__ctor_m1830395376(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Reflection.FieldAttributes System.Reflection.MonoField::get_Attributes()
extern "C"  int32_t MonoField_get_Attributes_m2200334717 (MonoField_t * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_attrs_4();
		return L_0;
	}
}
// System.RuntimeFieldHandle System.Reflection.MonoField::get_FieldHandle()
extern "C"  RuntimeFieldHandle_t3184214143  MonoField_get_FieldHandle_m3810546370 (MonoField_t * __this, const MethodInfo* method)
{
	{
		RuntimeFieldHandle_t3184214143  L_0 = __this->get_fhandle_1();
		return L_0;
	}
}
// System.Type System.Reflection.MonoField::get_FieldType()
extern "C"  Type_t * MonoField_get_FieldType_m3878403556 (MonoField_t * __this, const MethodInfo* method)
{
	{
		Type_t * L_0 = __this->get_type_3();
		return L_0;
	}
}
// System.Type System.Reflection.MonoField::GetParentType(System.Boolean)
extern "C"  Type_t * MonoField_GetParentType_m3845576618 (MonoField_t * __this, bool ___declaring, const MethodInfo* method)
{
	using namespace il2cpp::icalls;
	typedef Type_t * (*MonoField_GetParentType_m3845576618_ftn) (MonoField_t *, bool);
	return  ((MonoField_GetParentType_m3845576618_ftn)mscorlib::System::Reflection::MonoField::GetParentType) (__this, ___declaring);
}
// System.Type System.Reflection.MonoField::get_ReflectedType()
extern "C"  Type_t * MonoField_get_ReflectedType_m249627654 (MonoField_t * __this, const MethodInfo* method)
{
	{
		Type_t * L_0 = MonoField_GetParentType_m3845576618(__this, (bool)0, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Type System.Reflection.MonoField::get_DeclaringType()
extern "C"  Type_t * MonoField_get_DeclaringType_m1019180881 (MonoField_t * __this, const MethodInfo* method)
{
	{
		Type_t * L_0 = MonoField_GetParentType_m3845576618(__this, (bool)1, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.String System.Reflection.MonoField::get_Name()
extern "C"  String_t* MonoField_get_Name_m561340404 (MonoField_t * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_name_2();
		return L_0;
	}
}
// System.Boolean System.Reflection.MonoField::IsDefined(System.Type,System.Boolean)
extern TypeInfo* MonoCustomAttrs_t1707908163_il2cpp_TypeInfo_var;
extern const uint32_t MonoField_IsDefined_m1408878468_MetadataUsageId;
extern "C"  bool MonoField_IsDefined_m1408878468 (MonoField_t * __this, Type_t * ___attributeType, bool ___inherit, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MonoField_IsDefined_m1408878468_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Type_t * L_0 = ___attributeType;
		bool L_1 = ___inherit;
		IL2CPP_RUNTIME_CLASS_INIT(MonoCustomAttrs_t1707908163_il2cpp_TypeInfo_var);
		bool L_2 = MonoCustomAttrs_IsDefined_m1052740451(NULL /*static, unused*/, __this, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Object[] System.Reflection.MonoField::GetCustomAttributes(System.Boolean)
extern TypeInfo* MonoCustomAttrs_t1707908163_il2cpp_TypeInfo_var;
extern const uint32_t MonoField_GetCustomAttributes_m3992853639_MetadataUsageId;
extern "C"  ObjectU5BU5D_t11523773* MonoField_GetCustomAttributes_m3992853639 (MonoField_t * __this, bool ___inherit, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MonoField_GetCustomAttributes_m3992853639_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = ___inherit;
		IL2CPP_RUNTIME_CLASS_INIT(MonoCustomAttrs_t1707908163_il2cpp_TypeInfo_var);
		ObjectU5BU5D_t11523773* L_1 = MonoCustomAttrs_GetCustomAttributes_m3210789640(NULL /*static, unused*/, __this, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Object[] System.Reflection.MonoField::GetCustomAttributes(System.Type,System.Boolean)
extern TypeInfo* MonoCustomAttrs_t1707908163_il2cpp_TypeInfo_var;
extern const uint32_t MonoField_GetCustomAttributes_m3211174388_MetadataUsageId;
extern "C"  ObjectU5BU5D_t11523773* MonoField_GetCustomAttributes_m3211174388 (MonoField_t * __this, Type_t * ___attributeType, bool ___inherit, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MonoField_GetCustomAttributes_m3211174388_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Type_t * L_0 = ___attributeType;
		bool L_1 = ___inherit;
		IL2CPP_RUNTIME_CLASS_INIT(MonoCustomAttrs_t1707908163_il2cpp_TypeInfo_var);
		ObjectU5BU5D_t11523773* L_2 = MonoCustomAttrs_GetCustomAttributes_m3020328693(NULL /*static, unused*/, __this, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Int32 System.Reflection.MonoField::GetFieldOffset()
extern "C"  int32_t MonoField_GetFieldOffset_m1550054270 (MonoField_t * __this, const MethodInfo* method)
{
	using namespace il2cpp::icalls;
	typedef int32_t (*MonoField_GetFieldOffset_m1550054270_ftn) (MonoField_t *);
	return  ((MonoField_GetFieldOffset_m1550054270_ftn)mscorlib::System::Reflection::MonoField::GetFieldOffset) (__this);
}
// System.Object System.Reflection.MonoField::GetValueInternal(System.Object)
extern "C"  Il2CppObject * MonoField_GetValueInternal_m2439735918 (MonoField_t * __this, Il2CppObject * ___obj, const MethodInfo* method)
{
	using namespace il2cpp::icalls;
	typedef Il2CppObject * (*MonoField_GetValueInternal_m2439735918_ftn) (MonoField_t *, Il2CppObject *);
	return  ((MonoField_GetValueInternal_m2439735918_ftn)mscorlib::System::Reflection::MonoField::GetValueInternal) (__this, ___obj);
}
// System.Object System.Reflection.MonoField::GetValue(System.Object)
extern TypeInfo* TargetException_t565659628_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral775673898;
extern const uint32_t MonoField_GetValue_m496927467_MetadataUsageId;
extern "C"  Il2CppObject * MonoField_GetValue_m496927467 (MonoField_t * __this, Il2CppObject * ___obj, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MonoField_GetValue_m496927467_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = VirtFuncInvoker0< bool >::Invoke(19 /* System.Boolean System.Reflection.FieldInfo::get_IsStatic() */, __this);
		if (L_0)
		{
			goto IL_001c;
		}
	}
	{
		Il2CppObject * L_1 = ___obj;
		if (L_1)
		{
			goto IL_001c;
		}
	}
	{
		TargetException_t565659628 * L_2 = (TargetException_t565659628 *)il2cpp_codegen_object_new(TargetException_t565659628_il2cpp_TypeInfo_var);
		TargetException__ctor_m1662849864(L_2, _stringLiteral775673898, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_001c:
	{
		bool L_3 = VirtFuncInvoker0< bool >::Invoke(18 /* System.Boolean System.Reflection.FieldInfo::get_IsLiteral() */, __this);
		if (L_3)
		{
			goto IL_002d;
		}
	}
	{
		MonoField_CheckGeneric_m3068937042(__this, /*hidden argument*/NULL);
	}

IL_002d:
	{
		Il2CppObject * L_4 = ___obj;
		Il2CppObject * L_5 = MonoField_GetValueInternal_m2439735918(__this, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.String System.Reflection.MonoField::ToString()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3279482895;
extern const uint32_t MonoField_ToString_m4218032780_MetadataUsageId;
extern "C"  String_t* MonoField_ToString_m4218032780 (MonoField_t * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MonoField_ToString_m4218032780_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Type_t * L_0 = __this->get_type_3();
		String_t* L_1 = __this->get_name_2();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = String_Format_m2398979370(NULL /*static, unused*/, _stringLiteral3279482895, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void System.Reflection.MonoField::SetValueInternal(System.Reflection.FieldInfo,System.Object,System.Object)
extern "C"  void MonoField_SetValueInternal_m1841098555 (Il2CppObject * __this /* static, unused */, FieldInfo_t * ___fi, Il2CppObject * ___obj, Il2CppObject * ___value, const MethodInfo* method)
{
	using namespace il2cpp::icalls;
	typedef void (*MonoField_SetValueInternal_m1841098555_ftn) (FieldInfo_t *, Il2CppObject *, Il2CppObject *);
	 ((MonoField_SetValueInternal_m1841098555_ftn)mscorlib::System::Reflection::MonoField::SetValueInternal) (___fi, ___obj, ___value);
}
// System.Void System.Reflection.MonoField::SetValue(System.Object,System.Object,System.Reflection.BindingFlags,System.Reflection.Binder,System.Globalization.CultureInfo)
extern TypeInfo* TargetException_t565659628_il2cpp_TypeInfo_var;
extern TypeInfo* FieldAccessException_t3884822182_il2cpp_TypeInfo_var;
extern TypeInfo* Binder_t4180926488_il2cpp_TypeInfo_var;
extern TypeInfo* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentException_t124305799_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral775673898;
extern Il2CppCodeGenString* _stringLiteral880986616;
extern Il2CppCodeGenString* _stringLiteral1717876773;
extern Il2CppCodeGenString* _stringLiteral1927503096;
extern Il2CppCodeGenString* _stringLiteral116513;
extern const uint32_t MonoField_SetValue_m1189993934_MetadataUsageId;
extern "C"  void MonoField_SetValue_m1189993934 (MonoField_t * __this, Il2CppObject * ___obj, Il2CppObject * ___val, int32_t ___invokeAttr, Binder_t4180926488 * ___binder, CultureInfo_t3603717042 * ___culture, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MonoField_SetValue_m1189993934_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		bool L_0 = VirtFuncInvoker0< bool >::Invoke(19 /* System.Boolean System.Reflection.FieldInfo::get_IsStatic() */, __this);
		if (L_0)
		{
			goto IL_001c;
		}
	}
	{
		Il2CppObject * L_1 = ___obj;
		if (L_1)
		{
			goto IL_001c;
		}
	}
	{
		TargetException_t565659628 * L_2 = (TargetException_t565659628 *)il2cpp_codegen_object_new(TargetException_t565659628_il2cpp_TypeInfo_var);
		TargetException__ctor_m1662849864(L_2, _stringLiteral775673898, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_001c:
	{
		bool L_3 = VirtFuncInvoker0< bool >::Invoke(18 /* System.Boolean System.Reflection.FieldInfo::get_IsLiteral() */, __this);
		if (!L_3)
		{
			goto IL_0032;
		}
	}
	{
		FieldAccessException_t3884822182 * L_4 = (FieldAccessException_t3884822182 *)il2cpp_codegen_object_new(FieldAccessException_t3884822182_il2cpp_TypeInfo_var);
		FieldAccessException__ctor_m843948638(L_4, _stringLiteral880986616, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4);
	}

IL_0032:
	{
		Binder_t4180926488 * L_5 = ___binder;
		if (L_5)
		{
			goto IL_0040;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Binder_t4180926488_il2cpp_TypeInfo_var);
		Binder_t4180926488 * L_6 = Binder_get_DefaultBinder_m695054407(NULL /*static, unused*/, /*hidden argument*/NULL);
		___binder = L_6;
	}

IL_0040:
	{
		MonoField_CheckGeneric_m3068937042(__this, /*hidden argument*/NULL);
		Il2CppObject * L_7 = ___val;
		if (!L_7)
		{
			goto IL_009e;
		}
	}
	{
		Binder_t4180926488 * L_8 = ___binder;
		Il2CppObject * L_9 = ___val;
		Type_t * L_10 = __this->get_type_3();
		CultureInfo_t3603717042 * L_11 = ___culture;
		NullCheck(L_8);
		Il2CppObject * L_12 = VirtFuncInvoker3< Il2CppObject *, Il2CppObject *, Type_t *, CultureInfo_t3603717042 * >::Invoke(5 /* System.Object System.Reflection.Binder::ChangeType(System.Object,System.Type,System.Globalization.CultureInfo) */, L_8, L_9, L_10, L_11);
		V_0 = L_12;
		Il2CppObject * L_13 = V_0;
		if (L_13)
		{
			goto IL_009b;
		}
	}
	{
		ObjectU5BU5D_t11523773* L_14 = ((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)4));
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, 0);
		ArrayElementTypeCheck (L_14, _stringLiteral1717876773);
		(L_14)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral1717876773);
		ObjectU5BU5D_t11523773* L_15 = L_14;
		Il2CppObject * L_16 = ___val;
		NullCheck(L_16);
		Type_t * L_17 = Object_GetType_m2022236990(L_16, /*hidden argument*/NULL);
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, 1);
		ArrayElementTypeCheck (L_15, L_17);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_17);
		ObjectU5BU5D_t11523773* L_18 = L_15;
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, 2);
		ArrayElementTypeCheck (L_18, _stringLiteral1927503096);
		(L_18)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral1927503096);
		ObjectU5BU5D_t11523773* L_19 = L_18;
		Type_t * L_20 = __this->get_type_3();
		NullCheck(L_19);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_19, 3);
		ArrayElementTypeCheck (L_19, L_20);
		(L_19)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_20);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_21 = String_Concat_m3016520001(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		ArgumentException_t124305799 * L_22 = (ArgumentException_t124305799 *)il2cpp_codegen_object_new(ArgumentException_t124305799_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m732321503(L_22, L_21, _stringLiteral116513, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_22);
	}

IL_009b:
	{
		Il2CppObject * L_23 = V_0;
		___val = L_23;
	}

IL_009e:
	{
		Il2CppObject * L_24 = ___obj;
		Il2CppObject * L_25 = ___val;
		MonoField_SetValueInternal_m1841098555(NULL /*static, unused*/, __this, L_24, L_25, /*hidden argument*/NULL);
		return;
	}
}
// System.Reflection.MonoField System.Reflection.MonoField::Clone(System.String)
extern TypeInfo* MonoField_t_il2cpp_TypeInfo_var;
extern const uint32_t MonoField_Clone_m2100853670_MetadataUsageId;
extern "C"  MonoField_t * MonoField_Clone_m2100853670 (MonoField_t * __this, String_t* ___newName, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MonoField_Clone_m2100853670_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	MonoField_t * V_0 = NULL;
	{
		MonoField_t * L_0 = (MonoField_t *)il2cpp_codegen_object_new(MonoField_t_il2cpp_TypeInfo_var);
		MonoField__ctor_m3189620673(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		MonoField_t * L_1 = V_0;
		String_t* L_2 = ___newName;
		NullCheck(L_1);
		L_1->set_name_2(L_2);
		MonoField_t * L_3 = V_0;
		Type_t * L_4 = __this->get_type_3();
		NullCheck(L_3);
		L_3->set_type_3(L_4);
		MonoField_t * L_5 = V_0;
		int32_t L_6 = __this->get_attrs_4();
		NullCheck(L_5);
		L_5->set_attrs_4(L_6);
		MonoField_t * L_7 = V_0;
		IntPtr_t L_8 = __this->get_klass_0();
		NullCheck(L_7);
		L_7->set_klass_0(L_8);
		MonoField_t * L_9 = V_0;
		RuntimeFieldHandle_t3184214143  L_10 = __this->get_fhandle_1();
		NullCheck(L_9);
		L_9->set_fhandle_1(L_10);
		MonoField_t * L_11 = V_0;
		return L_11;
	}
}
// System.Void System.Reflection.MonoField::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void MonoField_GetObjectData_m2261672671 (MonoField_t * __this, SerializationInfo_t2995724695 * ___info, StreamingContext_t986364934  ___context, const MethodInfo* method)
{
	{
		SerializationInfo_t2995724695 * L_0 = ___info;
		String_t* L_1 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MonoField::get_Name() */, __this);
		Type_t * L_2 = VirtFuncInvoker0< Type_t * >::Invoke(9 /* System.Type System.Reflection.MonoField::get_ReflectedType() */, __this);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Reflection.MonoField::ToString() */, __this);
		MemberInfoSerializationHolder_Serialize_m1332311411(NULL /*static, unused*/, L_0, L_1, L_2, L_3, 4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Reflection.MonoField::CheckGeneric()
extern TypeInfo* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4212372703;
extern const uint32_t MonoField_CheckGeneric_m3068937042_MetadataUsageId;
extern "C"  void MonoField_CheckGeneric_m3068937042 (MonoField_t * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MonoField_CheckGeneric_m3068937042_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Type_t * L_0 = VirtFuncInvoker0< Type_t * >::Invoke(6 /* System.Type System.Reflection.MonoField::get_DeclaringType() */, __this);
		NullCheck(L_0);
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(79 /* System.Boolean System.Type::get_ContainsGenericParameters() */, L_0);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_2 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_2, _stringLiteral4212372703, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_001b:
	{
		return;
	}
}
// System.Void System.Reflection.MonoGenericCMethod::.ctor()
extern TypeInfo* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern const uint32_t MonoGenericCMethod__ctor_m673538526_MetadataUsageId;
extern "C"  void MonoGenericCMethod__ctor_m673538526 (MonoGenericCMethod_t1878793598 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MonoGenericCMethod__ctor_m673538526_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		MonoCMethod__ctor_m1949313751(__this, /*hidden argument*/NULL);
		InvalidOperationException_t2420574324 * L_0 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m355676978(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Type System.Reflection.MonoGenericCMethod::get_ReflectedType()
extern "C"  Type_t * MonoGenericCMethod_get_ReflectedType_m1976633775 (MonoGenericCMethod_t1878793598 * __this, const MethodInfo* method)
{
	using namespace il2cpp::icalls;
	typedef Type_t * (*MonoGenericCMethod_get_ReflectedType_m1976633775_ftn) (MonoGenericCMethod_t1878793598 *);
	return  ((MonoGenericCMethod_get_ReflectedType_m1976633775_ftn)mscorlib::System::Reflection::MonoGenericCMethod::get_ReflectedType) (__this);
}
// System.Void System.Reflection.MonoGenericMethod::.ctor()
extern TypeInfo* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern const uint32_t MonoGenericMethod__ctor_m883290339_MetadataUsageId;
extern "C"  void MonoGenericMethod__ctor_m883290339 (MonoGenericMethod_t * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MonoGenericMethod__ctor_m883290339_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		MonoMethod__ctor_m370255050(__this, /*hidden argument*/NULL);
		InvalidOperationException_t2420574324 * L_0 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m355676978(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Type System.Reflection.MonoGenericMethod::get_ReflectedType()
extern "C"  Type_t * MonoGenericMethod_get_ReflectedType_m162977832 (MonoGenericMethod_t * __this, const MethodInfo* method)
{
	using namespace il2cpp::icalls;
	typedef Type_t * (*MonoGenericMethod_get_ReflectedType_m162977832_ftn) (MonoGenericMethod_t *);
	return  ((MonoGenericMethod_get_ReflectedType_m162977832_ftn)mscorlib::System::Reflection::MonoGenericMethod::get_ReflectedType) (__this);
}
// System.Void System.Reflection.MonoMethod::.ctor()
extern "C"  void MonoMethod__ctor_m370255050 (MonoMethod_t * __this, const MethodInfo* method)
{
	{
		MethodInfo__ctor_m2414387615(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Reflection.MonoMethod::.ctor(System.RuntimeMethodHandle)
extern "C"  void MonoMethod__ctor_m2200429992 (MonoMethod_t * __this, RuntimeMethodHandle_t2360005078  ___mhandle, const MethodInfo* method)
{
	{
		MethodInfo__ctor_m2414387615(__this, /*hidden argument*/NULL);
		IntPtr_t L_0 = RuntimeMethodHandle_get_Value_m2270388129((&___mhandle), /*hidden argument*/NULL);
		__this->set_mhandle_0(L_0);
		return;
	}
}
// System.String System.Reflection.MonoMethod::get_name(System.Reflection.MethodBase)
extern "C"  String_t* MonoMethod_get_name_m439428219 (Il2CppObject * __this /* static, unused */, MethodBase_t3461000640 * ___method, const MethodInfo* method)
{
	using namespace il2cpp::icalls;
	typedef String_t* (*MonoMethod_get_name_m439428219_ftn) (MethodBase_t3461000640 *);
	return  ((MonoMethod_get_name_m439428219_ftn)mscorlib::System::Reflection::MonoMethod::get_name) (___method);
}
// System.Reflection.MonoMethod System.Reflection.MonoMethod::get_base_definition(System.Reflection.MonoMethod)
extern "C"  MonoMethod_t * MonoMethod_get_base_definition_m1389391157 (Il2CppObject * __this /* static, unused */, MonoMethod_t * ___method, const MethodInfo* method)
{
	using namespace il2cpp::icalls;
	typedef MonoMethod_t * (*MonoMethod_get_base_definition_m1389391157_ftn) (MonoMethod_t *);
	return  ((MonoMethod_get_base_definition_m1389391157_ftn)mscorlib::System::Reflection::MonoMethod::get_base_definition) (___method);
}
// System.Reflection.MethodInfo System.Reflection.MonoMethod::GetBaseDefinition()
extern "C"  MethodInfo_t * MonoMethod_GetBaseDefinition_m3877450154 (MonoMethod_t * __this, const MethodInfo* method)
{
	{
		MonoMethod_t * L_0 = MonoMethod_get_base_definition_m1389391157(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Type System.Reflection.MonoMethod::get_ReturnType()
extern "C"  Type_t * MonoMethod_get_ReturnType_m2366628327 (MonoMethod_t * __this, const MethodInfo* method)
{
	{
		IntPtr_t L_0 = __this->get_mhandle_0();
		Type_t * L_1 = MonoMethodInfo_GetReturnType_m2167465812(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Reflection.MethodImplAttributes System.Reflection.MonoMethod::GetMethodImplementationFlags()
extern "C"  int32_t MonoMethod_GetMethodImplementationFlags_m510437511 (MonoMethod_t * __this, const MethodInfo* method)
{
	{
		IntPtr_t L_0 = __this->get_mhandle_0();
		int32_t L_1 = MonoMethodInfo_GetMethodImplementationFlags_m3858313503(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Reflection.ParameterInfo[] System.Reflection.MonoMethod::GetParameters()
extern TypeInfo* ParameterInfoU5BU5D_t1127461800_il2cpp_TypeInfo_var;
extern const uint32_t MonoMethod_GetParameters_m1957703466_MetadataUsageId;
extern "C"  ParameterInfoU5BU5D_t1127461800* MonoMethod_GetParameters_m1957703466 (MonoMethod_t * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MonoMethod_GetParameters_m1957703466_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ParameterInfoU5BU5D_t1127461800* V_0 = NULL;
	ParameterInfoU5BU5D_t1127461800* V_1 = NULL;
	{
		IntPtr_t L_0 = __this->get_mhandle_0();
		ParameterInfoU5BU5D_t1127461800* L_1 = MonoMethodInfo_GetParametersInfo_m1882458608(NULL /*static, unused*/, L_0, __this, /*hidden argument*/NULL);
		V_0 = L_1;
		ParameterInfoU5BU5D_t1127461800* L_2 = V_0;
		NullCheck(L_2);
		V_1 = ((ParameterInfoU5BU5D_t1127461800*)SZArrayNew(ParameterInfoU5BU5D_t1127461800_il2cpp_TypeInfo_var, (uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_2)->max_length))))));
		ParameterInfoU5BU5D_t1127461800* L_3 = V_0;
		ParameterInfoU5BU5D_t1127461800* L_4 = V_1;
		NullCheck((Il2CppArray *)(Il2CppArray *)L_3);
		VirtActionInvoker2< Il2CppArray *, int32_t >::Invoke(9 /* System.Void System.Array::CopyTo(System.Array,System.Int32) */, (Il2CppArray *)(Il2CppArray *)L_3, (Il2CppArray *)(Il2CppArray *)L_4, 0);
		ParameterInfoU5BU5D_t1127461800* L_5 = V_1;
		return L_5;
	}
}
// System.Object System.Reflection.MonoMethod::InternalInvoke(System.Object,System.Object[],System.Exception&)
extern "C"  Il2CppObject * MonoMethod_InternalInvoke_m4222289618 (MonoMethod_t * __this, Il2CppObject * ___obj, ObjectU5BU5D_t11523773* ___parameters, Exception_t1967233988 ** ___exc, const MethodInfo* method)
{
	using namespace il2cpp::icalls;
	typedef Il2CppObject * (*MonoMethod_InternalInvoke_m4222289618_ftn) (MonoMethod_t *, Il2CppObject *, ObjectU5BU5D_t11523773*, Exception_t1967233988 **);
	return  ((MonoMethod_InternalInvoke_m4222289618_ftn)mscorlib::System::Reflection::MonoMethod::InternalInvoke) (__this, ___obj, ___parameters, ___exc);
}
// System.Object System.Reflection.MonoMethod::Invoke(System.Object,System.Reflection.BindingFlags,System.Reflection.Binder,System.Object[],System.Globalization.CultureInfo)
extern TypeInfo* Binder_t4180926488_il2cpp_TypeInfo_var;
extern TypeInfo* TargetParameterCountException_t2862237030_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentException_t124305799_il2cpp_TypeInfo_var;
extern TypeInfo* SecurityManager_t678461618_il2cpp_TypeInfo_var;
extern TypeInfo* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern TypeInfo* ThreadAbortException_t389307564_il2cpp_TypeInfo_var;
extern TypeInfo* Exception_t1967233988_il2cpp_TypeInfo_var;
extern TypeInfo* TargetInvocationException_t1980070524_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1513113777;
extern Il2CppCodeGenString* _stringLiteral556291225;
extern Il2CppCodeGenString* _stringLiteral351991261;
extern const uint32_t MonoMethod_Invoke_m240151045_MetadataUsageId;
extern "C"  Il2CppObject * MonoMethod_Invoke_m240151045 (MonoMethod_t * __this, Il2CppObject * ___obj, int32_t ___invokeAttr, Binder_t4180926488 * ___binder, ObjectU5BU5D_t11523773* ___parameters, CultureInfo_t3603717042 * ___culture, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MonoMethod_Invoke_m240151045_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ParameterInfoU5BU5D_t1127461800* V_0 = NULL;
	int32_t V_1 = 0;
	Exception_t1967233988 * V_2 = NULL;
	Il2CppObject * V_3 = NULL;
	Exception_t1967233988 * V_4 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Binder_t4180926488 * L_0 = ___binder;
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Binder_t4180926488_il2cpp_TypeInfo_var);
		Binder_t4180926488 * L_1 = Binder_get_DefaultBinder_m695054407(NULL /*static, unused*/, /*hidden argument*/NULL);
		___binder = L_1;
	}

IL_000d:
	{
		IntPtr_t L_2 = __this->get_mhandle_0();
		ParameterInfoU5BU5D_t1127461800* L_3 = MonoMethodInfo_GetParametersInfo_m1882458608(NULL /*static, unused*/, L_2, __this, /*hidden argument*/NULL);
		V_0 = L_3;
		ObjectU5BU5D_t11523773* L_4 = ___parameters;
		if (L_4)
		{
			goto IL_0029;
		}
	}
	{
		ParameterInfoU5BU5D_t1127461800* L_5 = V_0;
		NullCheck(L_5);
		if ((((int32_t)((int32_t)(((Il2CppArray *)L_5)->max_length)))))
		{
			goto IL_003c;
		}
	}

IL_0029:
	{
		ObjectU5BU5D_t11523773* L_6 = ___parameters;
		if (!L_6)
		{
			goto IL_0047;
		}
	}
	{
		ObjectU5BU5D_t11523773* L_7 = ___parameters;
		NullCheck(L_7);
		ParameterInfoU5BU5D_t1127461800* L_8 = V_0;
		NullCheck(L_8);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_7)->max_length))))) == ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_8)->max_length)))))))
		{
			goto IL_0047;
		}
	}

IL_003c:
	{
		TargetParameterCountException_t2862237030 * L_9 = (TargetParameterCountException_t2862237030 *)il2cpp_codegen_object_new(TargetParameterCountException_t2862237030_il2cpp_TypeInfo_var);
		TargetParameterCountException__ctor_m2555943426(L_9, _stringLiteral1513113777, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0047:
	{
		int32_t L_10 = ___invokeAttr;
		if (((int32_t)((int32_t)L_10&(int32_t)((int32_t)65536))))
		{
			goto IL_0073;
		}
	}
	{
		Binder_t4180926488 * L_11 = ___binder;
		ObjectU5BU5D_t11523773* L_12 = ___parameters;
		ParameterInfoU5BU5D_t1127461800* L_13 = V_0;
		CultureInfo_t3603717042 * L_14 = ___culture;
		IL2CPP_RUNTIME_CLASS_INIT(Binder_t4180926488_il2cpp_TypeInfo_var);
		bool L_15 = Binder_ConvertArgs_m2442440337(NULL /*static, unused*/, L_11, L_12, L_13, L_14, /*hidden argument*/NULL);
		if (L_15)
		{
			goto IL_006e;
		}
	}
	{
		ArgumentException_t124305799 * L_16 = (ArgumentException_t124305799 *)il2cpp_codegen_object_new(ArgumentException_t124305799_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3544856547(L_16, _stringLiteral556291225, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_16);
	}

IL_006e:
	{
		goto IL_00a8;
	}

IL_0073:
	{
		V_1 = 0;
		goto IL_009f;
	}

IL_007a:
	{
		ObjectU5BU5D_t11523773* L_17 = ___parameters;
		int32_t L_18 = V_1;
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, L_18);
		int32_t L_19 = L_18;
		NullCheck(((L_17)->GetAt(static_cast<il2cpp_array_size_t>(L_19))));
		Type_t * L_20 = Object_GetType_m2022236990(((L_17)->GetAt(static_cast<il2cpp_array_size_t>(L_19))), /*hidden argument*/NULL);
		ParameterInfoU5BU5D_t1127461800* L_21 = V_0;
		int32_t L_22 = V_1;
		NullCheck(L_21);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_21, L_22);
		int32_t L_23 = L_22;
		NullCheck(((L_21)->GetAt(static_cast<il2cpp_array_size_t>(L_23))));
		Type_t * L_24 = VirtFuncInvoker0< Type_t * >::Invoke(6 /* System.Type System.Reflection.ParameterInfo::get_ParameterType() */, ((L_21)->GetAt(static_cast<il2cpp_array_size_t>(L_23))));
		if ((((Il2CppObject*)(Type_t *)L_20) == ((Il2CppObject*)(Type_t *)L_24)))
		{
			goto IL_009b;
		}
	}
	{
		ArgumentException_t124305799 * L_25 = (ArgumentException_t124305799 *)il2cpp_codegen_object_new(ArgumentException_t124305799_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3544856547(L_25, _stringLiteral1513113777, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_25);
	}

IL_009b:
	{
		int32_t L_26 = V_1;
		V_1 = ((int32_t)((int32_t)L_26+(int32_t)1));
	}

IL_009f:
	{
		int32_t L_27 = V_1;
		ParameterInfoU5BU5D_t1127461800* L_28 = V_0;
		NullCheck(L_28);
		if ((((int32_t)L_27) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_28)->max_length)))))))
		{
			goto IL_007a;
		}
	}

IL_00a8:
	{
		IL2CPP_RUNTIME_CLASS_INIT(SecurityManager_t678461618_il2cpp_TypeInfo_var);
		bool L_29 = SecurityManager_get_SecurityEnabled_m2857115566(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_29)
		{
			goto IL_00b8;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SecurityManager_t678461618_il2cpp_TypeInfo_var);
		SecurityManager_ReflectedLinkDemandInvoke_m361815255(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
	}

IL_00b8:
	{
		bool L_30 = VirtFuncInvoker0< bool >::Invoke(28 /* System.Boolean System.Reflection.MonoMethod::get_ContainsGenericParameters() */, __this);
		if (!L_30)
		{
			goto IL_00ce;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_31 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_31, _stringLiteral351991261, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_31);
	}

IL_00ce:
	{
		V_3 = NULL;
	}

IL_00d0:
	try
	{ // begin try (depth: 1)
		Il2CppObject * L_32 = ___obj;
		ObjectU5BU5D_t11523773* L_33 = ___parameters;
		Il2CppObject * L_34 = MonoMethod_InternalInvoke_m4222289618(__this, L_32, L_33, (&V_2), /*hidden argument*/NULL);
		V_3 = L_34;
		goto IL_00f8;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (ThreadAbortException_t389307564_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_00e1;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1967233988_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_00e9;
		throw e;
	}

CATCH_00e1:
	{ // begin catch(System.Threading.ThreadAbortException)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(__exception_local);
		}

IL_00e4:
		{
			goto IL_00f8;
		}
	} // end catch (depth: 1)

CATCH_00e9:
	{ // begin catch(System.Exception)
		{
			V_4 = ((Exception_t1967233988 *)__exception_local);
			Exception_t1967233988 * L_35 = V_4;
			TargetInvocationException_t1980070524 * L_36 = (TargetInvocationException_t1980070524 *)il2cpp_codegen_object_new(TargetInvocationException_t1980070524_il2cpp_TypeInfo_var);
			TargetInvocationException__ctor_m2876037082(L_36, L_35, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_36);
		}

IL_00f3:
		{
			goto IL_00f8;
		}
	} // end catch (depth: 1)

IL_00f8:
	{
		Exception_t1967233988 * L_37 = V_2;
		if (!L_37)
		{
			goto IL_0100;
		}
	}
	{
		Exception_t1967233988 * L_38 = V_2;
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_38);
	}

IL_0100:
	{
		Il2CppObject * L_39 = V_3;
		return L_39;
	}
}
// System.RuntimeMethodHandle System.Reflection.MonoMethod::get_MethodHandle()
extern "C"  RuntimeMethodHandle_t2360005078  MonoMethod_get_MethodHandle_m3375732565 (MonoMethod_t * __this, const MethodInfo* method)
{
	{
		IntPtr_t L_0 = __this->get_mhandle_0();
		RuntimeMethodHandle_t2360005078  L_1;
		memset(&L_1, 0, sizeof(L_1));
		RuntimeMethodHandle__ctor_m794057668(&L_1, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Reflection.MethodAttributes System.Reflection.MonoMethod::get_Attributes()
extern "C"  int32_t MonoMethod_get_Attributes_m3567797513 (MonoMethod_t * __this, const MethodInfo* method)
{
	{
		IntPtr_t L_0 = __this->get_mhandle_0();
		int32_t L_1 = MonoMethodInfo_GetAttributes_m3368954492(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Reflection.CallingConventions System.Reflection.MonoMethod::get_CallingConvention()
extern "C"  int32_t MonoMethod_get_CallingConvention_m1596053931 (MonoMethod_t * __this, const MethodInfo* method)
{
	{
		IntPtr_t L_0 = __this->get_mhandle_0();
		int32_t L_1 = MonoMethodInfo_GetCallingConvention_m4186754404(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Type System.Reflection.MonoMethod::get_ReflectedType()
extern "C"  Type_t * MonoMethod_get_ReflectedType_m64213147 (MonoMethod_t * __this, const MethodInfo* method)
{
	{
		Type_t * L_0 = __this->get_reftype_2();
		return L_0;
	}
}
// System.Type System.Reflection.MonoMethod::get_DeclaringType()
extern "C"  Type_t * MonoMethod_get_DeclaringType_m833766374 (MonoMethod_t * __this, const MethodInfo* method)
{
	{
		IntPtr_t L_0 = __this->get_mhandle_0();
		Type_t * L_1 = MonoMethodInfo_GetDeclaringType_m1399000903(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.String System.Reflection.MonoMethod::get_Name()
extern "C"  String_t* MonoMethod_get_Name_m3300938865 (MonoMethod_t * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_name_1();
		if (!L_0)
		{
			goto IL_0012;
		}
	}
	{
		String_t* L_1 = __this->get_name_1();
		return L_1;
	}

IL_0012:
	{
		String_t* L_2 = MonoMethod_get_name_m439428219(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Boolean System.Reflection.MonoMethod::IsDefined(System.Type,System.Boolean)
extern TypeInfo* MonoCustomAttrs_t1707908163_il2cpp_TypeInfo_var;
extern const uint32_t MonoMethod_IsDefined_m1598659081_MetadataUsageId;
extern "C"  bool MonoMethod_IsDefined_m1598659081 (MonoMethod_t * __this, Type_t * ___attributeType, bool ___inherit, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MonoMethod_IsDefined_m1598659081_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Type_t * L_0 = ___attributeType;
		bool L_1 = ___inherit;
		IL2CPP_RUNTIME_CLASS_INIT(MonoCustomAttrs_t1707908163_il2cpp_TypeInfo_var);
		bool L_2 = MonoCustomAttrs_IsDefined_m1052740451(NULL /*static, unused*/, __this, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Object[] System.Reflection.MonoMethod::GetCustomAttributes(System.Boolean)
extern TypeInfo* MonoCustomAttrs_t1707908163_il2cpp_TypeInfo_var;
extern const uint32_t MonoMethod_GetCustomAttributes_m534554826_MetadataUsageId;
extern "C"  ObjectU5BU5D_t11523773* MonoMethod_GetCustomAttributes_m534554826 (MonoMethod_t * __this, bool ___inherit, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MonoMethod_GetCustomAttributes_m534554826_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = ___inherit;
		IL2CPP_RUNTIME_CLASS_INIT(MonoCustomAttrs_t1707908163_il2cpp_TypeInfo_var);
		ObjectU5BU5D_t11523773* L_1 = MonoCustomAttrs_GetCustomAttributes_m3210789640(NULL /*static, unused*/, __this, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Object[] System.Reflection.MonoMethod::GetCustomAttributes(System.Type,System.Boolean)
extern TypeInfo* MonoCustomAttrs_t1707908163_il2cpp_TypeInfo_var;
extern const uint32_t MonoMethod_GetCustomAttributes_m673046967_MetadataUsageId;
extern "C"  ObjectU5BU5D_t11523773* MonoMethod_GetCustomAttributes_m673046967 (MonoMethod_t * __this, Type_t * ___attributeType, bool ___inherit, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MonoMethod_GetCustomAttributes_m673046967_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Type_t * L_0 = ___attributeType;
		bool L_1 = ___inherit;
		IL2CPP_RUNTIME_CLASS_INIT(MonoCustomAttrs_t1707908163_il2cpp_TypeInfo_var);
		ObjectU5BU5D_t11523773* L_2 = MonoCustomAttrs_GetCustomAttributes_m3020328693(NULL /*static, unused*/, __this, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Runtime.InteropServices.DllImportAttribute System.Reflection.MonoMethod::GetDllImportAttribute(System.IntPtr)
extern "C"  DllImportAttribute_t2977516789 * MonoMethod_GetDllImportAttribute_m3397930907 (Il2CppObject * __this /* static, unused */, IntPtr_t ___mhandle, const MethodInfo* method)
{
	using namespace il2cpp::icalls;
	typedef DllImportAttribute_t2977516789 * (*MonoMethod_GetDllImportAttribute_m3397930907_ftn) (IntPtr_t);
	return  ((MonoMethod_GetDllImportAttribute_m3397930907_ftn)mscorlib::System::Reflection::MonoMethod::GetDllImportAttribute) (___mhandle);
}
// System.Object[] System.Reflection.MonoMethod::GetPseudoCustomAttributes()
extern TypeInfo* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern TypeInfo* PreserveSigAttribute_t2759700281_il2cpp_TypeInfo_var;
extern const uint32_t MonoMethod_GetPseudoCustomAttributes_m4233716593_MetadataUsageId;
extern "C"  ObjectU5BU5D_t11523773* MonoMethod_GetPseudoCustomAttributes_m4233716593 (MonoMethod_t * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MonoMethod_GetPseudoCustomAttributes_m4233716593_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	MonoMethodInfo_t106042080  V_1;
	memset(&V_1, 0, sizeof(V_1));
	ObjectU5BU5D_t11523773* V_2 = NULL;
	DllImportAttribute_t2977516789 * V_3 = NULL;
	{
		V_0 = 0;
		IntPtr_t L_0 = __this->get_mhandle_0();
		MonoMethodInfo_t106042080  L_1 = MonoMethodInfo_GetMethodInfo_m3883823882(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_1 = L_1;
		int32_t L_2 = (&V_1)->get_iattrs_3();
		if (!((int32_t)((int32_t)L_2&(int32_t)((int32_t)128))))
		{
			goto IL_0024;
		}
	}
	{
		int32_t L_3 = V_0;
		V_0 = ((int32_t)((int32_t)L_3+(int32_t)1));
	}

IL_0024:
	{
		int32_t L_4 = (&V_1)->get_attrs_2();
		if (!((int32_t)((int32_t)L_4&(int32_t)((int32_t)8192))))
		{
			goto IL_003a;
		}
	}
	{
		int32_t L_5 = V_0;
		V_0 = ((int32_t)((int32_t)L_5+(int32_t)1));
	}

IL_003a:
	{
		int32_t L_6 = V_0;
		if (L_6)
		{
			goto IL_0042;
		}
	}
	{
		return (ObjectU5BU5D_t11523773*)NULL;
	}

IL_0042:
	{
		int32_t L_7 = V_0;
		V_2 = ((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)L_7));
		V_0 = 0;
		int32_t L_8 = (&V_1)->get_iattrs_3();
		if (!((int32_t)((int32_t)L_8&(int32_t)((int32_t)128))))
		{
			goto IL_0069;
		}
	}
	{
		ObjectU5BU5D_t11523773* L_9 = V_2;
		int32_t L_10 = V_0;
		int32_t L_11 = L_10;
		V_0 = ((int32_t)((int32_t)L_11+(int32_t)1));
		PreserveSigAttribute_t2759700281 * L_12 = (PreserveSigAttribute_t2759700281 *)il2cpp_codegen_object_new(PreserveSigAttribute_t2759700281_il2cpp_TypeInfo_var);
		PreserveSigAttribute__ctor_m245522005(L_12, /*hidden argument*/NULL);
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, L_11);
		ArrayElementTypeCheck (L_9, L_12);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(L_11), (Il2CppObject *)L_12);
	}

IL_0069:
	{
		int32_t L_13 = (&V_1)->get_attrs_2();
		if (!((int32_t)((int32_t)L_13&(int32_t)((int32_t)8192))))
		{
			goto IL_00a8;
		}
	}
	{
		IntPtr_t L_14 = __this->get_mhandle_0();
		DllImportAttribute_t2977516789 * L_15 = MonoMethod_GetDllImportAttribute_m3397930907(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		V_3 = L_15;
		int32_t L_16 = (&V_1)->get_iattrs_3();
		if (!((int32_t)((int32_t)L_16&(int32_t)((int32_t)128))))
		{
			goto IL_00a0;
		}
	}
	{
		DllImportAttribute_t2977516789 * L_17 = V_3;
		NullCheck(L_17);
		L_17->set_PreserveSig_5((bool)1);
	}

IL_00a0:
	{
		ObjectU5BU5D_t11523773* L_18 = V_2;
		int32_t L_19 = V_0;
		int32_t L_20 = L_19;
		V_0 = ((int32_t)((int32_t)L_20+(int32_t)1));
		DllImportAttribute_t2977516789 * L_21 = V_3;
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, L_20);
		ArrayElementTypeCheck (L_18, L_21);
		(L_18)->SetAt(static_cast<il2cpp_array_size_t>(L_20), (Il2CppObject *)L_21);
	}

IL_00a8:
	{
		ObjectU5BU5D_t11523773* L_22 = V_2;
		return L_22;
	}
}
// System.Boolean System.Reflection.MonoMethod::ShouldPrintFullName(System.Type)
extern "C"  bool MonoMethod_ShouldPrintFullName_m401880127 (Il2CppObject * __this /* static, unused */, Type_t * ___type, const MethodInfo* method)
{
	int32_t G_B5_0 = 0;
	int32_t G_B7_0 = 0;
	int32_t G_B9_0 = 0;
	{
		Type_t * L_0 = ___type;
		NullCheck(L_0);
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(23 /* System.Boolean System.Type::get_IsClass() */, L_0);
		if (!L_1)
		{
			goto IL_003c;
		}
	}
	{
		Type_t * L_2 = ___type;
		NullCheck(L_2);
		bool L_3 = VirtFuncInvoker0< bool >::Invoke(29 /* System.Boolean System.Type::get_IsPointer() */, L_2);
		if (!L_3)
		{
			goto IL_0039;
		}
	}
	{
		Type_t * L_4 = ___type;
		NullCheck(L_4);
		Type_t * L_5 = VirtFuncInvoker0< Type_t * >::Invoke(44 /* System.Type System.Type::GetElementType() */, L_4);
		NullCheck(L_5);
		bool L_6 = VirtFuncInvoker0< bool >::Invoke(30 /* System.Boolean System.Type::get_IsPrimitive() */, L_5);
		if (L_6)
		{
			goto IL_0036;
		}
	}
	{
		Type_t * L_7 = ___type;
		NullCheck(L_7);
		Type_t * L_8 = VirtFuncInvoker0< Type_t * >::Invoke(44 /* System.Type System.Type::GetElementType() */, L_7);
		NullCheck(L_8);
		bool L_9 = Type_get_IsNested_m2782386097(L_8, /*hidden argument*/NULL);
		G_B5_0 = ((((int32_t)L_9) == ((int32_t)0))? 1 : 0);
		goto IL_0037;
	}

IL_0036:
	{
		G_B5_0 = 0;
	}

IL_0037:
	{
		G_B7_0 = G_B5_0;
		goto IL_003a;
	}

IL_0039:
	{
		G_B7_0 = 1;
	}

IL_003a:
	{
		G_B9_0 = G_B7_0;
		goto IL_003d;
	}

IL_003c:
	{
		G_B9_0 = 0;
	}

IL_003d:
	{
		return (bool)G_B9_0;
	}
}
// System.String System.Reflection.MonoMethod::ToString()
extern TypeInfo* StringBuilder_t3822575854_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral32;
extern Il2CppCodeGenString* _stringLiteral91;
extern Il2CppCodeGenString* _stringLiteral44;
extern Il2CppCodeGenString* _stringLiteral93;
extern Il2CppCodeGenString* _stringLiteral40;
extern Il2CppCodeGenString* _stringLiteral1396;
extern Il2CppCodeGenString* _stringLiteral980771964;
extern Il2CppCodeGenString* _stringLiteral45678;
extern Il2CppCodeGenString* _stringLiteral41;
extern const uint32_t MonoMethod_ToString_m2662663945_MetadataUsageId;
extern "C"  String_t* MonoMethod_ToString_m2662663945 (MonoMethod_t * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MonoMethod_ToString_m2662663945_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	StringBuilder_t3822575854 * V_0 = NULL;
	Type_t * V_1 = NULL;
	TypeU5BU5D_t3431720054* V_2 = NULL;
	int32_t V_3 = 0;
	ParameterInfoU5BU5D_t1127461800* V_4 = NULL;
	int32_t V_5 = 0;
	Type_t * V_6 = NULL;
	bool V_7 = false;
	{
		StringBuilder_t3822575854 * L_0 = (StringBuilder_t3822575854 *)il2cpp_codegen_object_new(StringBuilder_t3822575854_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m135953004(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		Type_t * L_1 = VirtFuncInvoker0< Type_t * >::Invoke(32 /* System.Type System.Reflection.MonoMethod::get_ReturnType() */, __this);
		V_1 = L_1;
		Type_t * L_2 = V_1;
		bool L_3 = MonoMethod_ShouldPrintFullName_m401880127(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_002a;
		}
	}
	{
		StringBuilder_t3822575854 * L_4 = V_0;
		Type_t * L_5 = V_1;
		NullCheck(L_5);
		String_t* L_6 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Type::ToString() */, L_5);
		NullCheck(L_4);
		StringBuilder_Append_m3898090075(L_4, L_6, /*hidden argument*/NULL);
		goto IL_0037;
	}

IL_002a:
	{
		StringBuilder_t3822575854 * L_7 = V_0;
		Type_t * L_8 = V_1;
		NullCheck(L_8);
		String_t* L_9 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_8);
		NullCheck(L_7);
		StringBuilder_Append_m3898090075(L_7, L_9, /*hidden argument*/NULL);
	}

IL_0037:
	{
		StringBuilder_t3822575854 * L_10 = V_0;
		NullCheck(L_10);
		StringBuilder_Append_m3898090075(L_10, _stringLiteral32, /*hidden argument*/NULL);
		StringBuilder_t3822575854 * L_11 = V_0;
		String_t* L_12 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MonoMethod::get_Name() */, __this);
		NullCheck(L_11);
		StringBuilder_Append_m3898090075(L_11, L_12, /*hidden argument*/NULL);
		bool L_13 = VirtFuncInvoker0< bool >::Invoke(30 /* System.Boolean System.Reflection.MonoMethod::get_IsGenericMethod() */, __this);
		if (!L_13)
		{
			goto IL_00b0;
		}
	}
	{
		TypeU5BU5D_t3431720054* L_14 = VirtFuncInvoker0< TypeU5BU5D_t3431720054* >::Invoke(27 /* System.Type[] System.Reflection.MonoMethod::GetGenericArguments() */, __this);
		V_2 = L_14;
		StringBuilder_t3822575854 * L_15 = V_0;
		NullCheck(L_15);
		StringBuilder_Append_m3898090075(L_15, _stringLiteral91, /*hidden argument*/NULL);
		V_3 = 0;
		goto IL_009b;
	}

IL_0075:
	{
		int32_t L_16 = V_3;
		if ((((int32_t)L_16) <= ((int32_t)0)))
		{
			goto IL_0088;
		}
	}
	{
		StringBuilder_t3822575854 * L_17 = V_0;
		NullCheck(L_17);
		StringBuilder_Append_m3898090075(L_17, _stringLiteral44, /*hidden argument*/NULL);
	}

IL_0088:
	{
		StringBuilder_t3822575854 * L_18 = V_0;
		TypeU5BU5D_t3431720054* L_19 = V_2;
		int32_t L_20 = V_3;
		NullCheck(L_19);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_19, L_20);
		int32_t L_21 = L_20;
		NullCheck(((L_19)->GetAt(static_cast<il2cpp_array_size_t>(L_21))));
		String_t* L_22 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, ((L_19)->GetAt(static_cast<il2cpp_array_size_t>(L_21))));
		NullCheck(L_18);
		StringBuilder_Append_m3898090075(L_18, L_22, /*hidden argument*/NULL);
		int32_t L_23 = V_3;
		V_3 = ((int32_t)((int32_t)L_23+(int32_t)1));
	}

IL_009b:
	{
		int32_t L_24 = V_3;
		TypeU5BU5D_t3431720054* L_25 = V_2;
		NullCheck(L_25);
		if ((((int32_t)L_24) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_25)->max_length)))))))
		{
			goto IL_0075;
		}
	}
	{
		StringBuilder_t3822575854 * L_26 = V_0;
		NullCheck(L_26);
		StringBuilder_Append_m3898090075(L_26, _stringLiteral93, /*hidden argument*/NULL);
	}

IL_00b0:
	{
		StringBuilder_t3822575854 * L_27 = V_0;
		NullCheck(L_27);
		StringBuilder_Append_m3898090075(L_27, _stringLiteral40, /*hidden argument*/NULL);
		ParameterInfoU5BU5D_t1127461800* L_28 = VirtFuncInvoker0< ParameterInfoU5BU5D_t1127461800* >::Invoke(15 /* System.Reflection.ParameterInfo[] System.Reflection.MonoMethod::GetParameters() */, __this);
		V_4 = L_28;
		V_5 = 0;
		goto IL_014b;
	}

IL_00cc:
	{
		int32_t L_29 = V_5;
		if ((((int32_t)L_29) <= ((int32_t)0)))
		{
			goto IL_00e0;
		}
	}
	{
		StringBuilder_t3822575854 * L_30 = V_0;
		NullCheck(L_30);
		StringBuilder_Append_m3898090075(L_30, _stringLiteral1396, /*hidden argument*/NULL);
	}

IL_00e0:
	{
		ParameterInfoU5BU5D_t1127461800* L_31 = V_4;
		int32_t L_32 = V_5;
		NullCheck(L_31);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_31, L_32);
		int32_t L_33 = L_32;
		NullCheck(((L_31)->GetAt(static_cast<il2cpp_array_size_t>(L_33))));
		Type_t * L_34 = VirtFuncInvoker0< Type_t * >::Invoke(6 /* System.Type System.Reflection.ParameterInfo::get_ParameterType() */, ((L_31)->GetAt(static_cast<il2cpp_array_size_t>(L_33))));
		V_6 = L_34;
		Type_t * L_35 = V_6;
		NullCheck(L_35);
		bool L_36 = VirtFuncInvoker0< bool >::Invoke(22 /* System.Boolean System.Type::get_IsByRef() */, L_35);
		V_7 = L_36;
		bool L_37 = V_7;
		if (!L_37)
		{
			goto IL_0105;
		}
	}
	{
		Type_t * L_38 = V_6;
		NullCheck(L_38);
		Type_t * L_39 = VirtFuncInvoker0< Type_t * >::Invoke(44 /* System.Type System.Type::GetElementType() */, L_38);
		V_6 = L_39;
	}

IL_0105:
	{
		Type_t * L_40 = V_6;
		bool L_41 = MonoMethod_ShouldPrintFullName_m401880127(NULL /*static, unused*/, L_40, /*hidden argument*/NULL);
		if (!L_41)
		{
			goto IL_0124;
		}
	}
	{
		StringBuilder_t3822575854 * L_42 = V_0;
		Type_t * L_43 = V_6;
		NullCheck(L_43);
		String_t* L_44 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Type::ToString() */, L_43);
		NullCheck(L_42);
		StringBuilder_Append_m3898090075(L_42, L_44, /*hidden argument*/NULL);
		goto IL_0132;
	}

IL_0124:
	{
		StringBuilder_t3822575854 * L_45 = V_0;
		Type_t * L_46 = V_6;
		NullCheck(L_46);
		String_t* L_47 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_46);
		NullCheck(L_45);
		StringBuilder_Append_m3898090075(L_45, L_47, /*hidden argument*/NULL);
	}

IL_0132:
	{
		bool L_48 = V_7;
		if (!L_48)
		{
			goto IL_0145;
		}
	}
	{
		StringBuilder_t3822575854 * L_49 = V_0;
		NullCheck(L_49);
		StringBuilder_Append_m3898090075(L_49, _stringLiteral980771964, /*hidden argument*/NULL);
	}

IL_0145:
	{
		int32_t L_50 = V_5;
		V_5 = ((int32_t)((int32_t)L_50+(int32_t)1));
	}

IL_014b:
	{
		int32_t L_51 = V_5;
		ParameterInfoU5BU5D_t1127461800* L_52 = V_4;
		NullCheck(L_52);
		if ((((int32_t)L_51) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_52)->max_length)))))))
		{
			goto IL_00cc;
		}
	}
	{
		int32_t L_53 = VirtFuncInvoker0< int32_t >::Invoke(21 /* System.Reflection.CallingConventions System.Reflection.MonoMethod::get_CallingConvention() */, __this);
		if (!((int32_t)((int32_t)L_53&(int32_t)2)))
		{
			goto IL_0185;
		}
	}
	{
		ParameterInfoU5BU5D_t1127461800* L_54 = V_4;
		NullCheck(L_54);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_54)->max_length))))) <= ((int32_t)0)))
		{
			goto IL_0179;
		}
	}
	{
		StringBuilder_t3822575854 * L_55 = V_0;
		NullCheck(L_55);
		StringBuilder_Append_m3898090075(L_55, _stringLiteral1396, /*hidden argument*/NULL);
	}

IL_0179:
	{
		StringBuilder_t3822575854 * L_56 = V_0;
		NullCheck(L_56);
		StringBuilder_Append_m3898090075(L_56, _stringLiteral45678, /*hidden argument*/NULL);
	}

IL_0185:
	{
		StringBuilder_t3822575854 * L_57 = V_0;
		NullCheck(L_57);
		StringBuilder_Append_m3898090075(L_57, _stringLiteral41, /*hidden argument*/NULL);
		StringBuilder_t3822575854 * L_58 = V_0;
		NullCheck(L_58);
		String_t* L_59 = StringBuilder_ToString_m350379841(L_58, /*hidden argument*/NULL);
		return L_59;
	}
}
// System.Void System.Reflection.MonoMethod::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void MonoMethod_GetObjectData_m1160867688 (MonoMethod_t * __this, SerializationInfo_t2995724695 * ___info, StreamingContext_t986364934  ___context, const MethodInfo* method)
{
	TypeU5BU5D_t3431720054* V_0 = NULL;
	TypeU5BU5D_t3431720054* G_B4_0 = NULL;
	{
		bool L_0 = VirtFuncInvoker0< bool >::Invoke(30 /* System.Boolean System.Reflection.MonoMethod::get_IsGenericMethod() */, __this);
		if (!L_0)
		{
			goto IL_0021;
		}
	}
	{
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(29 /* System.Boolean System.Reflection.MonoMethod::get_IsGenericMethodDefinition() */, __this);
		if (L_1)
		{
			goto IL_0021;
		}
	}
	{
		TypeU5BU5D_t3431720054* L_2 = VirtFuncInvoker0< TypeU5BU5D_t3431720054* >::Invoke(27 /* System.Type[] System.Reflection.MonoMethod::GetGenericArguments() */, __this);
		G_B4_0 = L_2;
		goto IL_0022;
	}

IL_0021:
	{
		G_B4_0 = ((TypeU5BU5D_t3431720054*)(NULL));
	}

IL_0022:
	{
		V_0 = G_B4_0;
		SerializationInfo_t2995724695 * L_3 = ___info;
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MonoMethod::get_Name() */, __this);
		Type_t * L_5 = VirtFuncInvoker0< Type_t * >::Invoke(9 /* System.Type System.Reflection.MonoMethod::get_ReflectedType() */, __this);
		String_t* L_6 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Reflection.MonoMethod::ToString() */, __this);
		TypeU5BU5D_t3431720054* L_7 = V_0;
		MemberInfoSerializationHolder_Serialize_m2526201284(NULL /*static, unused*/, L_3, L_4, L_5, L_6, 8, L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Reflection.MethodInfo System.Reflection.MonoMethod::MakeGenericMethod(System.Type[])
extern TypeInfo* ArgumentNullException_t3214793280_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t2847414787_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentException_t124305799_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3884720620;
extern Il2CppCodeGenString* _stringLiteral449196032;
extern const uint32_t MonoMethod_MakeGenericMethod_m2250506909_MetadataUsageId;
extern "C"  MethodInfo_t * MonoMethod_MakeGenericMethod_m2250506909 (MonoMethod_t * __this, TypeU5BU5D_t3431720054* ___methodInstantiation, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MonoMethod_MakeGenericMethod_m2250506909_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Type_t * V_0 = NULL;
	TypeU5BU5D_t3431720054* V_1 = NULL;
	int32_t V_2 = 0;
	MethodInfo_t * V_3 = NULL;
	{
		TypeU5BU5D_t3431720054* L_0 = ___methodInstantiation;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t3214793280 * L_1 = (ArgumentNullException_t3214793280 *)il2cpp_codegen_object_new(ArgumentNullException_t3214793280_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_1, _stringLiteral3884720620, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		TypeU5BU5D_t3431720054* L_2 = ___methodInstantiation;
		V_1 = L_2;
		V_2 = 0;
		goto IL_002e;
	}

IL_001a:
	{
		TypeU5BU5D_t3431720054* L_3 = V_1;
		int32_t L_4 = V_2;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		V_0 = ((L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5)));
		Type_t * L_6 = V_0;
		if (L_6)
		{
			goto IL_002a;
		}
	}
	{
		ArgumentNullException_t3214793280 * L_7 = (ArgumentNullException_t3214793280 *)il2cpp_codegen_object_new(ArgumentNullException_t3214793280_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m2162372070(L_7, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_7);
	}

IL_002a:
	{
		int32_t L_8 = V_2;
		V_2 = ((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_002e:
	{
		int32_t L_9 = V_2;
		TypeU5BU5D_t3431720054* L_10 = V_1;
		NullCheck(L_10);
		if ((((int32_t)L_9) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_10)->max_length)))))))
		{
			goto IL_001a;
		}
	}
	{
		TypeU5BU5D_t3431720054* L_11 = ___methodInstantiation;
		MethodInfo_t * L_12 = MonoMethod_MakeGenericMethod_impl_m2160588496(__this, L_11, /*hidden argument*/NULL);
		V_3 = L_12;
		MethodInfo_t * L_13 = V_3;
		if (L_13)
		{
			goto IL_006a;
		}
	}
	{
		TypeU5BU5D_t3431720054* L_14 = VirtFuncInvoker0< TypeU5BU5D_t3431720054* >::Invoke(27 /* System.Type[] System.Reflection.MonoMethod::GetGenericArguments() */, __this);
		NullCheck(L_14);
		int32_t L_15 = (((int32_t)((int32_t)(((Il2CppArray *)L_14)->max_length))));
		Il2CppObject * L_16 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_15);
		TypeU5BU5D_t3431720054* L_17 = ___methodInstantiation;
		NullCheck(L_17);
		int32_t L_18 = (((int32_t)((int32_t)(((Il2CppArray *)L_17)->max_length))));
		Il2CppObject * L_19 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_18);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_20 = String_Format_m2398979370(NULL /*static, unused*/, _stringLiteral449196032, L_16, L_19, /*hidden argument*/NULL);
		ArgumentException_t124305799 * L_21 = (ArgumentException_t124305799 *)il2cpp_codegen_object_new(ArgumentException_t124305799_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3544856547(L_21, L_20, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_21);
	}

IL_006a:
	{
		MethodInfo_t * L_22 = V_3;
		return L_22;
	}
}
// System.Reflection.MethodInfo System.Reflection.MonoMethod::MakeGenericMethod_impl(System.Type[])
extern "C"  MethodInfo_t * MonoMethod_MakeGenericMethod_impl_m2160588496 (MonoMethod_t * __this, TypeU5BU5D_t3431720054* ___types, const MethodInfo* method)
{
	using namespace il2cpp::icalls;
	typedef MethodInfo_t * (*MonoMethod_MakeGenericMethod_impl_m2160588496_ftn) (MonoMethod_t *, TypeU5BU5D_t3431720054*);
	return  ((MonoMethod_MakeGenericMethod_impl_m2160588496_ftn)mscorlib::System::Reflection::MonoMethod::MakeGenericMethod_impl) (__this, ___types);
}
// System.Type[] System.Reflection.MonoMethod::GetGenericArguments()
extern "C"  TypeU5BU5D_t3431720054* MonoMethod_GetGenericArguments_m978560229 (MonoMethod_t * __this, const MethodInfo* method)
{
	using namespace il2cpp::icalls;
	typedef TypeU5BU5D_t3431720054* (*MonoMethod_GetGenericArguments_m978560229_ftn) (MonoMethod_t *);
	return  ((MonoMethod_GetGenericArguments_m978560229_ftn)mscorlib::System::Reflection::MonoMethod::GetGenericArguments) (__this);
}
// System.Boolean System.Reflection.MonoMethod::get_IsGenericMethodDefinition()
extern "C"  bool MonoMethod_get_IsGenericMethodDefinition_m279322622 (MonoMethod_t * __this, const MethodInfo* method)
{
	using namespace il2cpp::icalls;
	typedef bool (*MonoMethod_get_IsGenericMethodDefinition_m279322622_ftn) (MonoMethod_t *);
	return  ((MonoMethod_get_IsGenericMethodDefinition_m279322622_ftn)mscorlib::System::Reflection::MonoMethod::get_IsGenericMethodDefinition) (__this);
}
// System.Boolean System.Reflection.MonoMethod::get_IsGenericMethod()
extern "C"  bool MonoMethod_get_IsGenericMethod_m1007555531 (MonoMethod_t * __this, const MethodInfo* method)
{
	using namespace il2cpp::icalls;
	typedef bool (*MonoMethod_get_IsGenericMethod_m1007555531_ftn) (MonoMethod_t *);
	return  ((MonoMethod_get_IsGenericMethod_m1007555531_ftn)mscorlib::System::Reflection::MonoMethod::get_IsGenericMethod) (__this);
}
// System.Boolean System.Reflection.MonoMethod::get_ContainsGenericParameters()
extern "C"  bool MonoMethod_get_ContainsGenericParameters_m1023389183 (MonoMethod_t * __this, const MethodInfo* method)
{
	Type_t * V_0 = NULL;
	TypeU5BU5D_t3431720054* V_1 = NULL;
	int32_t V_2 = 0;
	{
		bool L_0 = VirtFuncInvoker0< bool >::Invoke(30 /* System.Boolean System.Reflection.MonoMethod::get_IsGenericMethod() */, __this);
		if (!L_0)
		{
			goto IL_0037;
		}
	}
	{
		TypeU5BU5D_t3431720054* L_1 = VirtFuncInvoker0< TypeU5BU5D_t3431720054* >::Invoke(27 /* System.Type[] System.Reflection.MonoMethod::GetGenericArguments() */, __this);
		V_1 = L_1;
		V_2 = 0;
		goto IL_002e;
	}

IL_0019:
	{
		TypeU5BU5D_t3431720054* L_2 = V_1;
		int32_t L_3 = V_2;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		V_0 = ((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4)));
		Type_t * L_5 = V_0;
		NullCheck(L_5);
		bool L_6 = VirtFuncInvoker0< bool >::Invoke(79 /* System.Boolean System.Type::get_ContainsGenericParameters() */, L_5);
		if (!L_6)
		{
			goto IL_002a;
		}
	}
	{
		return (bool)1;
	}

IL_002a:
	{
		int32_t L_7 = V_2;
		V_2 = ((int32_t)((int32_t)L_7+(int32_t)1));
	}

IL_002e:
	{
		int32_t L_8 = V_2;
		TypeU5BU5D_t3431720054* L_9 = V_1;
		NullCheck(L_9);
		if ((((int32_t)L_8) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_9)->max_length)))))))
		{
			goto IL_0019;
		}
	}

IL_0037:
	{
		Type_t * L_10 = VirtFuncInvoker0< Type_t * >::Invoke(6 /* System.Type System.Reflection.MonoMethod::get_DeclaringType() */, __this);
		NullCheck(L_10);
		bool L_11 = VirtFuncInvoker0< bool >::Invoke(79 /* System.Boolean System.Type::get_ContainsGenericParameters() */, L_10);
		return L_11;
	}
}
// System.Void System.Reflection.MonoMethodInfo::get_method_info(System.IntPtr,System.Reflection.MonoMethodInfo&)
extern "C"  void MonoMethodInfo_get_method_info_m2207164051 (Il2CppObject * __this /* static, unused */, IntPtr_t ___handle, MonoMethodInfo_t106042080 * ___info, const MethodInfo* method)
{
	using namespace il2cpp::icalls;
	typedef void (*MonoMethodInfo_get_method_info_m2207164051_ftn) (IntPtr_t, MonoMethodInfo_t106042080 *);
	 ((MonoMethodInfo_get_method_info_m2207164051_ftn)mscorlib::System::Reflection::MonoMethodInfo::get_method_info) (___handle, ___info);
}
// System.Reflection.MonoMethodInfo System.Reflection.MonoMethodInfo::GetMethodInfo(System.IntPtr)
extern "C"  MonoMethodInfo_t106042080  MonoMethodInfo_GetMethodInfo_m3883823882 (Il2CppObject * __this /* static, unused */, IntPtr_t ___handle, const MethodInfo* method)
{
	MonoMethodInfo_t106042080  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		IntPtr_t L_0 = ___handle;
		MonoMethodInfo_get_method_info_m2207164051(NULL /*static, unused*/, L_0, (&V_0), /*hidden argument*/NULL);
		MonoMethodInfo_t106042080  L_1 = V_0;
		return L_1;
	}
}
// System.Type System.Reflection.MonoMethodInfo::GetDeclaringType(System.IntPtr)
extern "C"  Type_t * MonoMethodInfo_GetDeclaringType_m1399000903 (Il2CppObject * __this /* static, unused */, IntPtr_t ___handle, const MethodInfo* method)
{
	MonoMethodInfo_t106042080  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		IntPtr_t L_0 = ___handle;
		MonoMethodInfo_t106042080  L_1 = MonoMethodInfo_GetMethodInfo_m3883823882(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		Type_t * L_2 = (&V_0)->get_parent_0();
		return L_2;
	}
}
// System.Type System.Reflection.MonoMethodInfo::GetReturnType(System.IntPtr)
extern "C"  Type_t * MonoMethodInfo_GetReturnType_m2167465812 (Il2CppObject * __this /* static, unused */, IntPtr_t ___handle, const MethodInfo* method)
{
	MonoMethodInfo_t106042080  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		IntPtr_t L_0 = ___handle;
		MonoMethodInfo_t106042080  L_1 = MonoMethodInfo_GetMethodInfo_m3883823882(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		Type_t * L_2 = (&V_0)->get_ret_1();
		return L_2;
	}
}
// System.Reflection.MethodAttributes System.Reflection.MonoMethodInfo::GetAttributes(System.IntPtr)
extern "C"  int32_t MonoMethodInfo_GetAttributes_m3368954492 (Il2CppObject * __this /* static, unused */, IntPtr_t ___handle, const MethodInfo* method)
{
	MonoMethodInfo_t106042080  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		IntPtr_t L_0 = ___handle;
		MonoMethodInfo_t106042080  L_1 = MonoMethodInfo_GetMethodInfo_m3883823882(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = (&V_0)->get_attrs_2();
		return L_2;
	}
}
// System.Reflection.CallingConventions System.Reflection.MonoMethodInfo::GetCallingConvention(System.IntPtr)
extern "C"  int32_t MonoMethodInfo_GetCallingConvention_m4186754404 (Il2CppObject * __this /* static, unused */, IntPtr_t ___handle, const MethodInfo* method)
{
	MonoMethodInfo_t106042080  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		IntPtr_t L_0 = ___handle;
		MonoMethodInfo_t106042080  L_1 = MonoMethodInfo_GetMethodInfo_m3883823882(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = (&V_0)->get_callconv_4();
		return L_2;
	}
}
// System.Reflection.MethodImplAttributes System.Reflection.MonoMethodInfo::GetMethodImplementationFlags(System.IntPtr)
extern "C"  int32_t MonoMethodInfo_GetMethodImplementationFlags_m3858313503 (Il2CppObject * __this /* static, unused */, IntPtr_t ___handle, const MethodInfo* method)
{
	MonoMethodInfo_t106042080  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		IntPtr_t L_0 = ___handle;
		MonoMethodInfo_t106042080  L_1 = MonoMethodInfo_GetMethodInfo_m3883823882(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = (&V_0)->get_iattrs_3();
		return L_2;
	}
}
// System.Reflection.ParameterInfo[] System.Reflection.MonoMethodInfo::get_parameter_info(System.IntPtr,System.Reflection.MemberInfo)
extern "C"  ParameterInfoU5BU5D_t1127461800* MonoMethodInfo_get_parameter_info_m584685721 (Il2CppObject * __this /* static, unused */, IntPtr_t ___handle, MemberInfo_t * ___member, const MethodInfo* method)
{
	using namespace il2cpp::icalls;
	typedef ParameterInfoU5BU5D_t1127461800* (*MonoMethodInfo_get_parameter_info_m584685721_ftn) (IntPtr_t, MemberInfo_t *);
	return  ((MonoMethodInfo_get_parameter_info_m584685721_ftn)mscorlib::System::Reflection::MonoMethodInfo::get_parameter_info) (___handle, ___member);
}
// System.Reflection.ParameterInfo[] System.Reflection.MonoMethodInfo::GetParametersInfo(System.IntPtr,System.Reflection.MemberInfo)
extern "C"  ParameterInfoU5BU5D_t1127461800* MonoMethodInfo_GetParametersInfo_m1882458608 (Il2CppObject * __this /* static, unused */, IntPtr_t ___handle, MemberInfo_t * ___member, const MethodInfo* method)
{
	{
		IntPtr_t L_0 = ___handle;
		MemberInfo_t * L_1 = ___member;
		ParameterInfoU5BU5D_t1127461800* L_2 = MonoMethodInfo_get_parameter_info_m584685721(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void System.Reflection.MonoProperty::.ctor()
extern "C"  void MonoProperty__ctor_m1169623126 (MonoProperty_t * __this, const MethodInfo* method)
{
	{
		PropertyInfo__ctor_m539791979(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Reflection.MonoProperty::CachePropertyInfo(System.Reflection.PInfo)
extern "C"  void MonoProperty_CachePropertyInfo_m1003517343 (MonoProperty_t * __this, int32_t ___flags, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_cached_3();
		int32_t L_1 = ___flags;
		int32_t L_2 = ___flags;
		if ((((int32_t)((int32_t)((int32_t)L_0&(int32_t)L_1))) == ((int32_t)L_2)))
		{
			goto IL_0029;
		}
	}
	{
		MonoPropertyInfo_t2683779348 * L_3 = __this->get_address_of_info_2();
		int32_t L_4 = ___flags;
		MonoPropertyInfo_get_property_info_m1244288251(NULL /*static, unused*/, __this, L_3, L_4, /*hidden argument*/NULL);
		int32_t L_5 = __this->get_cached_3();
		int32_t L_6 = ___flags;
		__this->set_cached_3(((int32_t)((int32_t)L_5|(int32_t)L_6)));
	}

IL_0029:
	{
		return;
	}
}
// System.Reflection.PropertyAttributes System.Reflection.MonoProperty::get_Attributes()
extern "C"  int32_t MonoProperty_get_Attributes_m1446659017 (MonoProperty_t * __this, const MethodInfo* method)
{
	{
		MonoProperty_CachePropertyInfo_m1003517343(__this, 1, /*hidden argument*/NULL);
		MonoPropertyInfo_t2683779348 * L_0 = __this->get_address_of_info_2();
		int32_t L_1 = L_0->get_attrs_4();
		return L_1;
	}
}
// System.Boolean System.Reflection.MonoProperty::get_CanRead()
extern "C"  bool MonoProperty_get_CanRead_m4207249871 (MonoProperty_t * __this, const MethodInfo* method)
{
	{
		MonoProperty_CachePropertyInfo_m1003517343(__this, 2, /*hidden argument*/NULL);
		MonoPropertyInfo_t2683779348 * L_0 = __this->get_address_of_info_2();
		MethodInfo_t * L_1 = L_0->get_get_method_2();
		return (bool)((((int32_t)((((Il2CppObject*)(MethodInfo_t *)L_1) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Boolean System.Reflection.MonoProperty::get_CanWrite()
extern "C"  bool MonoProperty_get_CanWrite_m2098380648 (MonoProperty_t * __this, const MethodInfo* method)
{
	{
		MonoProperty_CachePropertyInfo_m1003517343(__this, 4, /*hidden argument*/NULL);
		MonoPropertyInfo_t2683779348 * L_0 = __this->get_address_of_info_2();
		MethodInfo_t * L_1 = L_0->get_set_method_3();
		return (bool)((((int32_t)((((Il2CppObject*)(MethodInfo_t *)L_1) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Type System.Reflection.MonoProperty::get_PropertyType()
extern "C"  Type_t * MonoProperty_get_PropertyType_m3141941824 (MonoProperty_t * __this, const MethodInfo* method)
{
	ParameterInfoU5BU5D_t1127461800* V_0 = NULL;
	{
		MonoProperty_CachePropertyInfo_m1003517343(__this, 6, /*hidden argument*/NULL);
		MonoPropertyInfo_t2683779348 * L_0 = __this->get_address_of_info_2();
		MethodInfo_t * L_1 = L_0->get_get_method_2();
		if (!L_1)
		{
			goto IL_0028;
		}
	}
	{
		MonoPropertyInfo_t2683779348 * L_2 = __this->get_address_of_info_2();
		MethodInfo_t * L_3 = L_2->get_get_method_2();
		NullCheck(L_3);
		Type_t * L_4 = VirtFuncInvoker0< Type_t * >::Invoke(32 /* System.Type System.Reflection.MethodInfo::get_ReturnType() */, L_3);
		return L_4;
	}

IL_0028:
	{
		MonoPropertyInfo_t2683779348 * L_5 = __this->get_address_of_info_2();
		MethodInfo_t * L_6 = L_5->get_set_method_3();
		NullCheck(L_6);
		ParameterInfoU5BU5D_t1127461800* L_7 = VirtFuncInvoker0< ParameterInfoU5BU5D_t1127461800* >::Invoke(15 /* System.Reflection.ParameterInfo[] System.Reflection.MethodBase::GetParameters() */, L_6);
		V_0 = L_7;
		ParameterInfoU5BU5D_t1127461800* L_8 = V_0;
		ParameterInfoU5BU5D_t1127461800* L_9 = V_0;
		NullCheck(L_9);
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, ((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_9)->max_length))))-(int32_t)1)));
		int32_t L_10 = ((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_9)->max_length))))-(int32_t)1));
		NullCheck(((L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_10))));
		Type_t * L_11 = VirtFuncInvoker0< Type_t * >::Invoke(6 /* System.Type System.Reflection.ParameterInfo::get_ParameterType() */, ((L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_10))));
		return L_11;
	}
}
// System.Type System.Reflection.MonoProperty::get_ReflectedType()
extern "C"  Type_t * MonoProperty_get_ReflectedType_m158519463 (MonoProperty_t * __this, const MethodInfo* method)
{
	{
		MonoProperty_CachePropertyInfo_m1003517343(__this, 8, /*hidden argument*/NULL);
		MonoPropertyInfo_t2683779348 * L_0 = __this->get_address_of_info_2();
		Type_t * L_1 = L_0->get_parent_0();
		return L_1;
	}
}
// System.Type System.Reflection.MonoProperty::get_DeclaringType()
extern "C"  Type_t * MonoProperty_get_DeclaringType_m928072690 (MonoProperty_t * __this, const MethodInfo* method)
{
	{
		MonoProperty_CachePropertyInfo_m1003517343(__this, ((int32_t)16), /*hidden argument*/NULL);
		MonoPropertyInfo_t2683779348 * L_0 = __this->get_address_of_info_2();
		Type_t * L_1 = L_0->get_parent_0();
		return L_1;
	}
}
// System.String System.Reflection.MonoProperty::get_Name()
extern "C"  String_t* MonoProperty_get_Name_m3886549157 (MonoProperty_t * __this, const MethodInfo* method)
{
	{
		MonoProperty_CachePropertyInfo_m1003517343(__this, ((int32_t)32), /*hidden argument*/NULL);
		MonoPropertyInfo_t2683779348 * L_0 = __this->get_address_of_info_2();
		String_t* L_1 = L_0->get_name_1();
		return L_1;
	}
}
// System.Reflection.MethodInfo[] System.Reflection.MonoProperty::GetAccessors(System.Boolean)
extern TypeInfo* MethodInfoU5BU5D_t1668237648_il2cpp_TypeInfo_var;
extern const uint32_t MonoProperty_GetAccessors_m455294545_MetadataUsageId;
extern "C"  MethodInfoU5BU5D_t1668237648* MonoProperty_GetAccessors_m455294545 (MonoProperty_t * __this, bool ___nonPublic, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MonoProperty_GetAccessors_m455294545_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	MethodInfoU5BU5D_t1668237648* V_2 = NULL;
	int32_t V_3 = 0;
	{
		V_0 = 0;
		V_1 = 0;
		MonoProperty_CachePropertyInfo_m1003517343(__this, 6, /*hidden argument*/NULL);
		MonoPropertyInfo_t2683779348 * L_0 = __this->get_address_of_info_2();
		MethodInfo_t * L_1 = L_0->get_set_method_3();
		if (!L_1)
		{
			goto IL_0038;
		}
	}
	{
		bool L_2 = ___nonPublic;
		if (L_2)
		{
			goto IL_0036;
		}
	}
	{
		MonoPropertyInfo_t2683779348 * L_3 = __this->get_address_of_info_2();
		MethodInfo_t * L_4 = L_3->get_set_method_3();
		NullCheck(L_4);
		bool L_5 = VirtFuncInvoker0< bool >::Invoke(22 /* System.Boolean System.Reflection.MethodBase::get_IsPublic() */, L_4);
		if (!L_5)
		{
			goto IL_0038;
		}
	}

IL_0036:
	{
		V_1 = 1;
	}

IL_0038:
	{
		MonoPropertyInfo_t2683779348 * L_6 = __this->get_address_of_info_2();
		MethodInfo_t * L_7 = L_6->get_get_method_2();
		if (!L_7)
		{
			goto IL_0065;
		}
	}
	{
		bool L_8 = ___nonPublic;
		if (L_8)
		{
			goto IL_0063;
		}
	}
	{
		MonoPropertyInfo_t2683779348 * L_9 = __this->get_address_of_info_2();
		MethodInfo_t * L_10 = L_9->get_get_method_2();
		NullCheck(L_10);
		bool L_11 = VirtFuncInvoker0< bool >::Invoke(22 /* System.Boolean System.Reflection.MethodBase::get_IsPublic() */, L_10);
		if (!L_11)
		{
			goto IL_0065;
		}
	}

IL_0063:
	{
		V_0 = 1;
	}

IL_0065:
	{
		int32_t L_12 = V_0;
		int32_t L_13 = V_1;
		V_2 = ((MethodInfoU5BU5D_t1668237648*)SZArrayNew(MethodInfoU5BU5D_t1668237648_il2cpp_TypeInfo_var, (uint32_t)((int32_t)((int32_t)L_12+(int32_t)L_13))));
		V_3 = 0;
		int32_t L_14 = V_1;
		if (!L_14)
		{
			goto IL_0088;
		}
	}
	{
		MethodInfoU5BU5D_t1668237648* L_15 = V_2;
		int32_t L_16 = V_3;
		int32_t L_17 = L_16;
		V_3 = ((int32_t)((int32_t)L_17+(int32_t)1));
		MonoPropertyInfo_t2683779348 * L_18 = __this->get_address_of_info_2();
		MethodInfo_t * L_19 = L_18->get_set_method_3();
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, L_17);
		ArrayElementTypeCheck (L_15, L_19);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(L_17), (MethodInfo_t *)L_19);
	}

IL_0088:
	{
		int32_t L_20 = V_0;
		if (!L_20)
		{
			goto IL_00a0;
		}
	}
	{
		MethodInfoU5BU5D_t1668237648* L_21 = V_2;
		int32_t L_22 = V_3;
		int32_t L_23 = L_22;
		V_3 = ((int32_t)((int32_t)L_23+(int32_t)1));
		MonoPropertyInfo_t2683779348 * L_24 = __this->get_address_of_info_2();
		MethodInfo_t * L_25 = L_24->get_get_method_2();
		NullCheck(L_21);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_21, L_23);
		ArrayElementTypeCheck (L_21, L_25);
		(L_21)->SetAt(static_cast<il2cpp_array_size_t>(L_23), (MethodInfo_t *)L_25);
	}

IL_00a0:
	{
		MethodInfoU5BU5D_t1668237648* L_26 = V_2;
		return L_26;
	}
}
// System.Reflection.MethodInfo System.Reflection.MonoProperty::GetGetMethod(System.Boolean)
extern "C"  MethodInfo_t * MonoProperty_GetGetMethod_m2822601598 (MonoProperty_t * __this, bool ___nonPublic, const MethodInfo* method)
{
	{
		MonoProperty_CachePropertyInfo_m1003517343(__this, 2, /*hidden argument*/NULL);
		MonoPropertyInfo_t2683779348 * L_0 = __this->get_address_of_info_2();
		MethodInfo_t * L_1 = L_0->get_get_method_2();
		if (!L_1)
		{
			goto IL_003e;
		}
	}
	{
		bool L_2 = ___nonPublic;
		if (L_2)
		{
			goto IL_0032;
		}
	}
	{
		MonoPropertyInfo_t2683779348 * L_3 = __this->get_address_of_info_2();
		MethodInfo_t * L_4 = L_3->get_get_method_2();
		NullCheck(L_4);
		bool L_5 = VirtFuncInvoker0< bool >::Invoke(22 /* System.Boolean System.Reflection.MethodBase::get_IsPublic() */, L_4);
		if (!L_5)
		{
			goto IL_003e;
		}
	}

IL_0032:
	{
		MonoPropertyInfo_t2683779348 * L_6 = __this->get_address_of_info_2();
		MethodInfo_t * L_7 = L_6->get_get_method_2();
		return L_7;
	}

IL_003e:
	{
		return (MethodInfo_t *)NULL;
	}
}
// System.Reflection.ParameterInfo[] System.Reflection.MonoProperty::GetIndexParameters()
extern TypeInfo* ParameterInfoU5BU5D_t1127461800_il2cpp_TypeInfo_var;
extern TypeInfo* ParameterInfo_t2610273829_il2cpp_TypeInfo_var;
extern const uint32_t MonoProperty_GetIndexParameters_m2930895730_MetadataUsageId;
extern "C"  ParameterInfoU5BU5D_t1127461800* MonoProperty_GetIndexParameters_m2930895730 (MonoProperty_t * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MonoProperty_GetIndexParameters_m2930895730_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ParameterInfoU5BU5D_t1127461800* V_0 = NULL;
	ParameterInfoU5BU5D_t1127461800* V_1 = NULL;
	int32_t V_2 = 0;
	ParameterInfo_t2610273829 * V_3 = NULL;
	{
		MonoProperty_CachePropertyInfo_m1003517343(__this, 6, /*hidden argument*/NULL);
		MonoPropertyInfo_t2683779348 * L_0 = __this->get_address_of_info_2();
		MethodInfo_t * L_1 = L_0->get_get_method_2();
		if (!L_1)
		{
			goto IL_002d;
		}
	}
	{
		MonoPropertyInfo_t2683779348 * L_2 = __this->get_address_of_info_2();
		MethodInfo_t * L_3 = L_2->get_get_method_2();
		NullCheck(L_3);
		ParameterInfoU5BU5D_t1127461800* L_4 = VirtFuncInvoker0< ParameterInfoU5BU5D_t1127461800* >::Invoke(15 /* System.Reflection.ParameterInfo[] System.Reflection.MethodBase::GetParameters() */, L_3);
		V_0 = L_4;
		goto IL_006f;
	}

IL_002d:
	{
		MonoPropertyInfo_t2683779348 * L_5 = __this->get_address_of_info_2();
		MethodInfo_t * L_6 = L_5->get_set_method_3();
		if (!L_6)
		{
			goto IL_0068;
		}
	}
	{
		MonoPropertyInfo_t2683779348 * L_7 = __this->get_address_of_info_2();
		MethodInfo_t * L_8 = L_7->get_set_method_3();
		NullCheck(L_8);
		ParameterInfoU5BU5D_t1127461800* L_9 = VirtFuncInvoker0< ParameterInfoU5BU5D_t1127461800* >::Invoke(15 /* System.Reflection.ParameterInfo[] System.Reflection.MethodBase::GetParameters() */, L_8);
		V_1 = L_9;
		ParameterInfoU5BU5D_t1127461800* L_10 = V_1;
		NullCheck(L_10);
		V_0 = ((ParameterInfoU5BU5D_t1127461800*)SZArrayNew(ParameterInfoU5BU5D_t1127461800_il2cpp_TypeInfo_var, (uint32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_10)->max_length))))-(int32_t)1))));
		ParameterInfoU5BU5D_t1127461800* L_11 = V_1;
		ParameterInfoU5BU5D_t1127461800* L_12 = V_0;
		ParameterInfoU5BU5D_t1127461800* L_13 = V_0;
		NullCheck(L_13);
		Array_Copy_m3799309042(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_11, (Il2CppArray *)(Il2CppArray *)L_12, (((int32_t)((int32_t)(((Il2CppArray *)L_13)->max_length)))), /*hidden argument*/NULL);
		goto IL_006f;
	}

IL_0068:
	{
		return ((ParameterInfoU5BU5D_t1127461800*)SZArrayNew(ParameterInfoU5BU5D_t1127461800_il2cpp_TypeInfo_var, (uint32_t)0));
	}

IL_006f:
	{
		V_2 = 0;
		goto IL_0088;
	}

IL_0076:
	{
		ParameterInfoU5BU5D_t1127461800* L_14 = V_0;
		int32_t L_15 = V_2;
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, L_15);
		int32_t L_16 = L_15;
		V_3 = ((L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_16)));
		ParameterInfoU5BU5D_t1127461800* L_17 = V_0;
		int32_t L_18 = V_2;
		ParameterInfo_t2610273829 * L_19 = V_3;
		ParameterInfo_t2610273829 * L_20 = (ParameterInfo_t2610273829 *)il2cpp_codegen_object_new(ParameterInfo_t2610273829_il2cpp_TypeInfo_var);
		ParameterInfo__ctor_m3736663868(L_20, L_19, __this, /*hidden argument*/NULL);
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, L_18);
		ArrayElementTypeCheck (L_17, L_20);
		(L_17)->SetAt(static_cast<il2cpp_array_size_t>(L_18), (ParameterInfo_t2610273829 *)L_20);
		int32_t L_21 = V_2;
		V_2 = ((int32_t)((int32_t)L_21+(int32_t)1));
	}

IL_0088:
	{
		int32_t L_22 = V_2;
		ParameterInfoU5BU5D_t1127461800* L_23 = V_0;
		NullCheck(L_23);
		if ((((int32_t)L_22) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_23)->max_length)))))))
		{
			goto IL_0076;
		}
	}
	{
		ParameterInfoU5BU5D_t1127461800* L_24 = V_0;
		return L_24;
	}
}
// System.Reflection.MethodInfo System.Reflection.MonoProperty::GetSetMethod(System.Boolean)
extern "C"  MethodInfo_t * MonoProperty_GetSetMethod_m3665893258 (MonoProperty_t * __this, bool ___nonPublic, const MethodInfo* method)
{
	{
		MonoProperty_CachePropertyInfo_m1003517343(__this, 4, /*hidden argument*/NULL);
		MonoPropertyInfo_t2683779348 * L_0 = __this->get_address_of_info_2();
		MethodInfo_t * L_1 = L_0->get_set_method_3();
		if (!L_1)
		{
			goto IL_003e;
		}
	}
	{
		bool L_2 = ___nonPublic;
		if (L_2)
		{
			goto IL_0032;
		}
	}
	{
		MonoPropertyInfo_t2683779348 * L_3 = __this->get_address_of_info_2();
		MethodInfo_t * L_4 = L_3->get_set_method_3();
		NullCheck(L_4);
		bool L_5 = VirtFuncInvoker0< bool >::Invoke(22 /* System.Boolean System.Reflection.MethodBase::get_IsPublic() */, L_4);
		if (!L_5)
		{
			goto IL_003e;
		}
	}

IL_0032:
	{
		MonoPropertyInfo_t2683779348 * L_6 = __this->get_address_of_info_2();
		MethodInfo_t * L_7 = L_6->get_set_method_3();
		return L_7;
	}

IL_003e:
	{
		return (MethodInfo_t *)NULL;
	}
}
// System.Boolean System.Reflection.MonoProperty::IsDefined(System.Type,System.Boolean)
extern TypeInfo* MonoCustomAttrs_t1707908163_il2cpp_TypeInfo_var;
extern const uint32_t MonoProperty_IsDefined_m436586773_MetadataUsageId;
extern "C"  bool MonoProperty_IsDefined_m436586773 (MonoProperty_t * __this, Type_t * ___attributeType, bool ___inherit, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MonoProperty_IsDefined_m436586773_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Type_t * L_0 = ___attributeType;
		IL2CPP_RUNTIME_CLASS_INIT(MonoCustomAttrs_t1707908163_il2cpp_TypeInfo_var);
		bool L_1 = MonoCustomAttrs_IsDefined_m1052740451(NULL /*static, unused*/, __this, L_0, (bool)0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Object[] System.Reflection.MonoProperty::GetCustomAttributes(System.Boolean)
extern TypeInfo* MonoCustomAttrs_t1707908163_il2cpp_TypeInfo_var;
extern const uint32_t MonoProperty_GetCustomAttributes_m1088918294_MetadataUsageId;
extern "C"  ObjectU5BU5D_t11523773* MonoProperty_GetCustomAttributes_m1088918294 (MonoProperty_t * __this, bool ___inherit, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MonoProperty_GetCustomAttributes_m1088918294_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoCustomAttrs_t1707908163_il2cpp_TypeInfo_var);
		ObjectU5BU5D_t11523773* L_0 = MonoCustomAttrs_GetCustomAttributes_m3210789640(NULL /*static, unused*/, __this, (bool)0, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Object[] System.Reflection.MonoProperty::GetCustomAttributes(System.Type,System.Boolean)
extern TypeInfo* MonoCustomAttrs_t1707908163_il2cpp_TypeInfo_var;
extern const uint32_t MonoProperty_GetCustomAttributes_m2634004995_MetadataUsageId;
extern "C"  ObjectU5BU5D_t11523773* MonoProperty_GetCustomAttributes_m2634004995 (MonoProperty_t * __this, Type_t * ___attributeType, bool ___inherit, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MonoProperty_GetCustomAttributes_m2634004995_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Type_t * L_0 = ___attributeType;
		IL2CPP_RUNTIME_CLASS_INIT(MonoCustomAttrs_t1707908163_il2cpp_TypeInfo_var);
		ObjectU5BU5D_t11523773* L_1 = MonoCustomAttrs_GetCustomAttributes_m3020328693(NULL /*static, unused*/, __this, L_0, (bool)0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Reflection.MonoProperty/GetterAdapter System.Reflection.MonoProperty::CreateGetterDelegate(System.Reflection.MethodInfo)
extern const Il2CppType* StaticGetter_1_t3013316298_0_0_0_var;
extern const Il2CppType* Getter_2_t2060272189_0_0_0_var;
extern const Il2CppType* MonoProperty_t_0_0_0_var;
extern const Il2CppType* GetterAdapter_t943738788_0_0_0_var;
extern TypeInfo* TypeU5BU5D_t3431720054_il2cpp_TypeInfo_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* GetterAdapter_t943738788_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1706951479;
extern Il2CppCodeGenString* _stringLiteral3666055209;
extern const uint32_t MonoProperty_CreateGetterDelegate_m1512753249_MetadataUsageId;
extern "C"  GetterAdapter_t943738788 * MonoProperty_CreateGetterDelegate_m1512753249 (Il2CppObject * __this /* static, unused */, MethodInfo_t * ___method, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MonoProperty_CreateGetterDelegate_m1512753249_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	TypeU5BU5D_t3431720054* V_0 = NULL;
	Type_t * V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	MethodInfo_t * V_3 = NULL;
	Type_t * V_4 = NULL;
	String_t* V_5 = NULL;
	{
		MethodInfo_t * L_0 = ___method;
		NullCheck(L_0);
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(23 /* System.Boolean System.Reflection.MethodBase::get_IsStatic() */, L_0);
		if (!L_1)
		{
			goto IL_0033;
		}
	}
	{
		TypeU5BU5D_t3431720054* L_2 = ((TypeU5BU5D_t3431720054*)SZArrayNew(TypeU5BU5D_t3431720054_il2cpp_TypeInfo_var, (uint32_t)1));
		MethodInfo_t * L_3 = ___method;
		NullCheck(L_3);
		Type_t * L_4 = VirtFuncInvoker0< Type_t * >::Invoke(32 /* System.Type System.Reflection.MethodInfo::get_ReturnType() */, L_3);
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 0);
		ArrayElementTypeCheck (L_2, L_4);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_4);
		V_0 = L_2;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_5 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(StaticGetter_1_t3013316298_0_0_0_var), /*hidden argument*/NULL);
		V_4 = L_5;
		V_5 = _stringLiteral1706951479;
		goto IL_005f;
	}

IL_0033:
	{
		TypeU5BU5D_t3431720054* L_6 = ((TypeU5BU5D_t3431720054*)SZArrayNew(TypeU5BU5D_t3431720054_il2cpp_TypeInfo_var, (uint32_t)2));
		MethodInfo_t * L_7 = ___method;
		NullCheck(L_7);
		Type_t * L_8 = VirtFuncInvoker0< Type_t * >::Invoke(6 /* System.Type System.Reflection.MemberInfo::get_DeclaringType() */, L_7);
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 0);
		ArrayElementTypeCheck (L_6, L_8);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_8);
		TypeU5BU5D_t3431720054* L_9 = L_6;
		MethodInfo_t * L_10 = ___method;
		NullCheck(L_10);
		Type_t * L_11 = VirtFuncInvoker0< Type_t * >::Invoke(32 /* System.Type System.Reflection.MethodInfo::get_ReturnType() */, L_10);
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, 1);
		ArrayElementTypeCheck (L_9, L_11);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(1), (Type_t *)L_11);
		V_0 = L_9;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_12 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Getter_2_t2060272189_0_0_0_var), /*hidden argument*/NULL);
		V_4 = L_12;
		V_5 = _stringLiteral3666055209;
	}

IL_005f:
	{
		Type_t * L_13 = V_4;
		TypeU5BU5D_t3431720054* L_14 = V_0;
		NullCheck(L_13);
		Type_t * L_15 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t3431720054* >::Invoke(83 /* System.Type System.Type::MakeGenericType(System.Type[]) */, L_13, L_14);
		V_1 = L_15;
		Type_t * L_16 = V_1;
		MethodInfo_t * L_17 = ___method;
		Delegate_t3660574010 * L_18 = Delegate_CreateDelegate_m2816771104(NULL /*static, unused*/, L_16, L_17, /*hidden argument*/NULL);
		V_2 = L_18;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_19 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(MonoProperty_t_0_0_0_var), /*hidden argument*/NULL);
		String_t* L_20 = V_5;
		NullCheck(L_19);
		MethodInfo_t * L_21 = VirtFuncInvoker2< MethodInfo_t *, String_t*, int32_t >::Invoke(50 /* System.Reflection.MethodInfo System.Type::GetMethod(System.String,System.Reflection.BindingFlags) */, L_19, L_20, ((int32_t)40));
		V_3 = L_21;
		MethodInfo_t * L_22 = V_3;
		TypeU5BU5D_t3431720054* L_23 = V_0;
		NullCheck(L_22);
		MethodInfo_t * L_24 = VirtFuncInvoker1< MethodInfo_t *, TypeU5BU5D_t3431720054* >::Invoke(33 /* System.Reflection.MethodInfo System.Reflection.MethodInfo::MakeGenericMethod(System.Type[]) */, L_22, L_23);
		V_3 = L_24;
		Type_t * L_25 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(GetterAdapter_t943738788_0_0_0_var), /*hidden argument*/NULL);
		Il2CppObject * L_26 = V_2;
		MethodInfo_t * L_27 = V_3;
		Delegate_t3660574010 * L_28 = Delegate_CreateDelegate_m1630815211(NULL /*static, unused*/, L_25, L_26, L_27, (bool)1, /*hidden argument*/NULL);
		return ((GetterAdapter_t943738788 *)CastclassSealed(L_28, GetterAdapter_t943738788_il2cpp_TypeInfo_var));
	}
}
// System.Object System.Reflection.MonoProperty::GetValue(System.Object,System.Object[])
extern "C"  Il2CppObject * MonoProperty_GetValue_m3005067234 (MonoProperty_t * __this, Il2CppObject * ___obj, ObjectU5BU5D_t11523773* ___index, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___obj;
		ObjectU5BU5D_t11523773* L_1 = ___index;
		Il2CppObject * L_2 = VirtFuncInvoker5< Il2CppObject *, Il2CppObject *, int32_t, Binder_t4180926488 *, ObjectU5BU5D_t11523773*, CultureInfo_t3603717042 * >::Invoke(24 /* System.Object System.Reflection.MonoProperty::GetValue(System.Object,System.Reflection.BindingFlags,System.Reflection.Binder,System.Object[],System.Globalization.CultureInfo) */, __this, L_0, 0, (Binder_t4180926488 *)NULL, L_1, (CultureInfo_t3603717042 *)NULL);
		return L_2;
	}
}
// System.Object System.Reflection.MonoProperty::GetValue(System.Object,System.Reflection.BindingFlags,System.Reflection.Binder,System.Object[],System.Globalization.CultureInfo)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentException_t124305799_il2cpp_TypeInfo_var;
extern TypeInfo* SecurityException_t128786772_il2cpp_TypeInfo_var;
extern TypeInfo* TargetInvocationException_t1980070524_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral580084144;
extern Il2CppCodeGenString* _stringLiteral39;
extern const uint32_t MonoProperty_GetValue_m1867887996_MetadataUsageId;
extern "C"  Il2CppObject * MonoProperty_GetValue_m1867887996 (MonoProperty_t * __this, Il2CppObject * ___obj, int32_t ___invokeAttr, Binder_t4180926488 * ___binder, ObjectU5BU5D_t11523773* ___index, CultureInfo_t3603717042 * ___culture, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MonoProperty_GetValue_m1867887996_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	MethodInfo_t * V_1 = NULL;
	SecurityException_t128786772 * V_2 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		V_0 = NULL;
		MethodInfo_t * L_0 = VirtFuncInvoker1< MethodInfo_t *, bool >::Invoke(20 /* System.Reflection.MethodInfo System.Reflection.MonoProperty::GetGetMethod(System.Boolean) */, __this, (bool)1);
		V_1 = L_0;
		MethodInfo_t * L_1 = V_1;
		if (L_1)
		{
			goto IL_002b;
		}
	}
	{
		String_t* L_2 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MonoProperty::get_Name() */, __this);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Concat_m1825781833(NULL /*static, unused*/, _stringLiteral580084144, L_2, _stringLiteral39, /*hidden argument*/NULL);
		ArgumentException_t124305799 * L_4 = (ArgumentException_t124305799 *)il2cpp_codegen_object_new(ArgumentException_t124305799_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3544856547(L_4, L_3, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4);
	}

IL_002b:
	try
	{ // begin try (depth: 1)
		{
			ObjectU5BU5D_t11523773* L_5 = ___index;
			if (!L_5)
			{
				goto IL_003b;
			}
		}

IL_0032:
		{
			ObjectU5BU5D_t11523773* L_6 = ___index;
			NullCheck(L_6);
			if ((((int32_t)((int32_t)(((Il2CppArray *)L_6)->max_length)))))
			{
				goto IL_004d;
			}
		}

IL_003b:
		{
			MethodInfo_t * L_7 = V_1;
			Il2CppObject * L_8 = ___obj;
			int32_t L_9 = ___invokeAttr;
			Binder_t4180926488 * L_10 = ___binder;
			CultureInfo_t3603717042 * L_11 = ___culture;
			NullCheck(L_7);
			Il2CppObject * L_12 = VirtFuncInvoker5< Il2CppObject *, Il2CppObject *, int32_t, Binder_t4180926488 *, ObjectU5BU5D_t11523773*, CultureInfo_t3603717042 * >::Invoke(18 /* System.Object System.Reflection.MethodBase::Invoke(System.Object,System.Reflection.BindingFlags,System.Reflection.Binder,System.Object[],System.Globalization.CultureInfo) */, L_7, L_8, L_9, L_10, (ObjectU5BU5D_t11523773*)(ObjectU5BU5D_t11523773*)NULL, L_11);
			V_0 = L_12;
			goto IL_005b;
		}

IL_004d:
		{
			MethodInfo_t * L_13 = V_1;
			Il2CppObject * L_14 = ___obj;
			int32_t L_15 = ___invokeAttr;
			Binder_t4180926488 * L_16 = ___binder;
			ObjectU5BU5D_t11523773* L_17 = ___index;
			CultureInfo_t3603717042 * L_18 = ___culture;
			NullCheck(L_13);
			Il2CppObject * L_19 = VirtFuncInvoker5< Il2CppObject *, Il2CppObject *, int32_t, Binder_t4180926488 *, ObjectU5BU5D_t11523773*, CultureInfo_t3603717042 * >::Invoke(18 /* System.Object System.Reflection.MethodBase::Invoke(System.Object,System.Reflection.BindingFlags,System.Reflection.Binder,System.Object[],System.Globalization.CultureInfo) */, L_13, L_14, L_15, L_16, L_17, L_18);
			V_0 = L_19;
		}

IL_005b:
		{
			goto IL_006d;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (SecurityException_t128786772_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0060;
		throw e;
	}

CATCH_0060:
	{ // begin catch(System.Security.SecurityException)
		{
			V_2 = ((SecurityException_t128786772 *)__exception_local);
			SecurityException_t128786772 * L_20 = V_2;
			TargetInvocationException_t1980070524 * L_21 = (TargetInvocationException_t1980070524 *)il2cpp_codegen_object_new(TargetInvocationException_t1980070524_il2cpp_TypeInfo_var);
			TargetInvocationException__ctor_m2876037082(L_21, L_20, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_21);
		}

IL_0068:
		{
			goto IL_006d;
		}
	} // end catch (depth: 1)

IL_006d:
	{
		Il2CppObject * L_22 = V_0;
		return L_22;
	}
}
// System.Void System.Reflection.MonoProperty::SetValue(System.Object,System.Object,System.Reflection.BindingFlags,System.Reflection.Binder,System.Object[],System.Globalization.CultureInfo)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentException_t124305799_il2cpp_TypeInfo_var;
extern TypeInfo* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral952321828;
extern Il2CppCodeGenString* _stringLiteral39;
extern const uint32_t MonoProperty_SetValue_m2220407469_MetadataUsageId;
extern "C"  void MonoProperty_SetValue_m2220407469 (MonoProperty_t * __this, Il2CppObject * ___obj, Il2CppObject * ___value, int32_t ___invokeAttr, Binder_t4180926488 * ___binder, ObjectU5BU5D_t11523773* ___index, CultureInfo_t3603717042 * ___culture, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MonoProperty_SetValue_m2220407469_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	MethodInfo_t * V_0 = NULL;
	ObjectU5BU5D_t11523773* V_1 = NULL;
	int32_t V_2 = 0;
	{
		MethodInfo_t * L_0 = VirtFuncInvoker1< MethodInfo_t *, bool >::Invoke(22 /* System.Reflection.MethodInfo System.Reflection.MonoProperty::GetSetMethod(System.Boolean) */, __this, (bool)1);
		V_0 = L_0;
		MethodInfo_t * L_1 = V_0;
		if (L_1)
		{
			goto IL_0029;
		}
	}
	{
		String_t* L_2 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MonoProperty::get_Name() */, __this);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Concat_m1825781833(NULL /*static, unused*/, _stringLiteral952321828, L_2, _stringLiteral39, /*hidden argument*/NULL);
		ArgumentException_t124305799 * L_4 = (ArgumentException_t124305799 *)il2cpp_codegen_object_new(ArgumentException_t124305799_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3544856547(L_4, L_3, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4);
	}

IL_0029:
	{
		ObjectU5BU5D_t11523773* L_5 = ___index;
		if (!L_5)
		{
			goto IL_0039;
		}
	}
	{
		ObjectU5BU5D_t11523773* L_6 = ___index;
		NullCheck(L_6);
		if ((((int32_t)((int32_t)(((Il2CppArray *)L_6)->max_length)))))
		{
			goto IL_0049;
		}
	}

IL_0039:
	{
		ObjectU5BU5D_t11523773* L_7 = ((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)1));
		Il2CppObject * L_8 = ___value;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 0);
		ArrayElementTypeCheck (L_7, L_8);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_8);
		V_1 = L_7;
		goto IL_0064;
	}

IL_0049:
	{
		ObjectU5BU5D_t11523773* L_9 = ___index;
		NullCheck(L_9);
		V_2 = (((int32_t)((int32_t)(((Il2CppArray *)L_9)->max_length))));
		int32_t L_10 = V_2;
		V_1 = ((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)((int32_t)((int32_t)L_10+(int32_t)1))));
		ObjectU5BU5D_t11523773* L_11 = ___index;
		ObjectU5BU5D_t11523773* L_12 = V_1;
		NullCheck((Il2CppArray *)(Il2CppArray *)L_11);
		VirtActionInvoker2< Il2CppArray *, int32_t >::Invoke(9 /* System.Void System.Array::CopyTo(System.Array,System.Int32) */, (Il2CppArray *)(Il2CppArray *)L_11, (Il2CppArray *)(Il2CppArray *)L_12, 0);
		ObjectU5BU5D_t11523773* L_13 = V_1;
		int32_t L_14 = V_2;
		Il2CppObject * L_15 = ___value;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, L_14);
		ArrayElementTypeCheck (L_13, L_15);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(L_14), (Il2CppObject *)L_15);
	}

IL_0064:
	{
		MethodInfo_t * L_16 = V_0;
		Il2CppObject * L_17 = ___obj;
		int32_t L_18 = ___invokeAttr;
		Binder_t4180926488 * L_19 = ___binder;
		ObjectU5BU5D_t11523773* L_20 = V_1;
		CultureInfo_t3603717042 * L_21 = ___culture;
		NullCheck(L_16);
		VirtFuncInvoker5< Il2CppObject *, Il2CppObject *, int32_t, Binder_t4180926488 *, ObjectU5BU5D_t11523773*, CultureInfo_t3603717042 * >::Invoke(18 /* System.Object System.Reflection.MethodBase::Invoke(System.Object,System.Reflection.BindingFlags,System.Reflection.Binder,System.Object[],System.Globalization.CultureInfo) */, L_16, L_17, L_18, L_19, L_20, L_21);
		return;
	}
}
// System.String System.Reflection.MonoProperty::ToString()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral32;
extern const uint32_t MonoProperty_ToString_m3248274237_MetadataUsageId;
extern "C"  String_t* MonoProperty_ToString_m3248274237 (MonoProperty_t * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MonoProperty_ToString_m3248274237_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Type_t * L_0 = VirtFuncInvoker0< Type_t * >::Invoke(17 /* System.Type System.Reflection.MonoProperty::get_PropertyType() */, __this);
		NullCheck(L_0);
		String_t* L_1 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Type::ToString() */, L_0);
		String_t* L_2 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MonoProperty::get_Name() */, __this);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Concat_m1825781833(NULL /*static, unused*/, L_1, _stringLiteral32, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Type[] System.Reflection.MonoProperty::GetOptionalCustomModifiers()
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t MonoProperty_GetOptionalCustomModifiers_m2413189819_MetadataUsageId;
extern "C"  TypeU5BU5D_t3431720054* MonoProperty_GetOptionalCustomModifiers_m2413189819 (MonoProperty_t * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MonoProperty_GetOptionalCustomModifiers_m2413189819_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	TypeU5BU5D_t3431720054* V_0 = NULL;
	{
		TypeU5BU5D_t3431720054* L_0 = MonoPropertyInfo_GetTypeModifiers_m3740551417(NULL /*static, unused*/, __this, (bool)1, /*hidden argument*/NULL);
		V_0 = L_0;
		TypeU5BU5D_t3431720054* L_1 = V_0;
		if (L_1)
		{
			goto IL_0014;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		TypeU5BU5D_t3431720054* L_2 = ((Type_t_StaticFields*)Type_t_il2cpp_TypeInfo_var->static_fields)->get_EmptyTypes_3();
		return L_2;
	}

IL_0014:
	{
		TypeU5BU5D_t3431720054* L_3 = V_0;
		return L_3;
	}
}
// System.Type[] System.Reflection.MonoProperty::GetRequiredCustomModifiers()
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t MonoProperty_GetRequiredCustomModifiers_m487727196_MetadataUsageId;
extern "C"  TypeU5BU5D_t3431720054* MonoProperty_GetRequiredCustomModifiers_m487727196 (MonoProperty_t * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MonoProperty_GetRequiredCustomModifiers_m487727196_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	TypeU5BU5D_t3431720054* V_0 = NULL;
	{
		TypeU5BU5D_t3431720054* L_0 = MonoPropertyInfo_GetTypeModifiers_m3740551417(NULL /*static, unused*/, __this, (bool)0, /*hidden argument*/NULL);
		V_0 = L_0;
		TypeU5BU5D_t3431720054* L_1 = V_0;
		if (L_1)
		{
			goto IL_0014;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		TypeU5BU5D_t3431720054* L_2 = ((Type_t_StaticFields*)Type_t_il2cpp_TypeInfo_var->static_fields)->get_EmptyTypes_3();
		return L_2;
	}

IL_0014:
	{
		TypeU5BU5D_t3431720054* L_3 = V_0;
		return L_3;
	}
}
// System.Void System.Reflection.MonoProperty::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void MonoProperty_GetObjectData_m2931203316 (MonoProperty_t * __this, SerializationInfo_t2995724695 * ___info, StreamingContext_t986364934  ___context, const MethodInfo* method)
{
	{
		SerializationInfo_t2995724695 * L_0 = ___info;
		String_t* L_1 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MonoProperty::get_Name() */, __this);
		Type_t * L_2 = VirtFuncInvoker0< Type_t * >::Invoke(9 /* System.Type System.Reflection.MonoProperty::get_ReflectedType() */, __this);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Reflection.MonoProperty::ToString() */, __this);
		MemberInfoSerializationHolder_Serialize_m1332311411(NULL /*static, unused*/, L_0, L_1, L_2, L_3, ((int32_t)16), /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Reflection.MonoProperty/GetterAdapter::.ctor(System.Object,System.IntPtr)
extern "C"  void GetterAdapter__ctor_m1690259617 (GetterAdapter_t943738788 * __this, Il2CppObject * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->set_method_ptr_0((methodPointerType)((MethodInfo*)___method.get_m_value_0())->method);
	__this->set_method_3(___method);
	__this->set_m_target_2(___object);
}
// System.Object System.Reflection.MonoProperty/GetterAdapter::Invoke(System.Object)
extern "C"  Il2CppObject * GetterAdapter_Invoke_m1039216292 (GetterAdapter_t943738788 * __this, Il2CppObject * ____this, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		GetterAdapter_Invoke_m1039216292((GetterAdapter_t943738788 *)__this->get_prev_9(),____this, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ____this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),____this,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, Il2CppObject * ____this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),____this,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(____this,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C" Il2CppObject * pinvoke_delegate_wrapper_GetterAdapter_t943738788(Il2CppObject* delegate, Il2CppObject * ____this)
{
	// Marshaling of parameter '____this' to native representation
	IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Cannot marshal type 'System.Object'."));
}
// System.IAsyncResult System.Reflection.MonoProperty/GetterAdapter::BeginInvoke(System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * GetterAdapter_BeginInvoke_m183730010 (GetterAdapter_t943738788 * __this, Il2CppObject * ____this, AsyncCallback_t1363551830 * ___callback, Il2CppObject * ___object, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ____this;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Object System.Reflection.MonoProperty/GetterAdapter::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * GetterAdapter_EndInvoke_m3963824678 (GetterAdapter_t943738788 * __this, Il2CppObject * ___result, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
	return (Il2CppObject *)__result;
}
// System.Void System.Reflection.MonoPropertyInfo::get_property_info(System.Reflection.MonoProperty,System.Reflection.MonoPropertyInfo&,System.Reflection.PInfo)
extern "C"  void MonoPropertyInfo_get_property_info_m1244288251 (Il2CppObject * __this /* static, unused */, MonoProperty_t * ___prop, MonoPropertyInfo_t2683779348 * ___info, int32_t ___req_info, const MethodInfo* method)
{
	using namespace il2cpp::icalls;
	typedef void (*MonoPropertyInfo_get_property_info_m1244288251_ftn) (MonoProperty_t *, MonoPropertyInfo_t2683779348 *, int32_t);
	 ((MonoPropertyInfo_get_property_info_m1244288251_ftn)mscorlib::System::Reflection::MonoPropertyInfo::get_property_info) (___prop, ___info, ___req_info);
}
// System.Type[] System.Reflection.MonoPropertyInfo::GetTypeModifiers(System.Reflection.MonoProperty,System.Boolean)
extern "C"  TypeU5BU5D_t3431720054* MonoPropertyInfo_GetTypeModifiers_m3740551417 (Il2CppObject * __this /* static, unused */, MonoProperty_t * ___prop, bool ___optional, const MethodInfo* method)
{
	using namespace il2cpp::icalls;
	typedef TypeU5BU5D_t3431720054* (*MonoPropertyInfo_GetTypeModifiers_m3740551417_ftn) (MonoProperty_t *, bool);
	return  ((MonoPropertyInfo_GetTypeModifiers_m3740551417_ftn)mscorlib::System::Reflection::MonoPropertyInfo::GetTypeModifiers) (___prop, ___optional);
}
// System.Void System.Reflection.ParameterInfo::.ctor()
extern "C"  void ParameterInfo__ctor_m3597008161 (ParameterInfo_t2610273829 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Reflection.ParameterInfo::.ctor(System.Reflection.Emit.ParameterBuilder,System.Type,System.Reflection.MemberInfo,System.Int32)
extern "C"  void ParameterInfo__ctor_m703590456 (ParameterInfo_t2610273829 * __this, ParameterBuilder_t3382011775 * ___pb, Type_t * ___type, MemberInfo_t * ___member, int32_t ___position, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		Type_t * L_0 = ___type;
		__this->set_ClassImpl_0(L_0);
		MemberInfo_t * L_1 = ___member;
		__this->set_MemberImpl_2(L_1);
		ParameterBuilder_t3382011775 * L_2 = ___pb;
		if (!L_2)
		{
			goto IL_0045;
		}
	}
	{
		ParameterBuilder_t3382011775 * L_3 = ___pb;
		NullCheck(L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(5 /* System.String System.Reflection.Emit.ParameterBuilder::get_Name() */, L_3);
		__this->set_NameImpl_3(L_4);
		ParameterBuilder_t3382011775 * L_5 = ___pb;
		NullCheck(L_5);
		int32_t L_6 = VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 System.Reflection.Emit.ParameterBuilder::get_Position() */, L_5);
		__this->set_PositionImpl_4(((int32_t)((int32_t)L_6-(int32_t)1)));
		ParameterBuilder_t3382011775 * L_7 = ___pb;
		NullCheck(L_7);
		int32_t L_8 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 System.Reflection.Emit.ParameterBuilder::get_Attributes() */, L_7);
		__this->set_AttrsImpl_5(L_8);
		goto IL_005d;
	}

IL_0045:
	{
		__this->set_NameImpl_3((String_t*)NULL);
		int32_t L_9 = ___position;
		__this->set_PositionImpl_4(((int32_t)((int32_t)L_9-(int32_t)1)));
		__this->set_AttrsImpl_5(0);
	}

IL_005d:
	{
		return;
	}
}
// System.Void System.Reflection.ParameterInfo::.ctor(System.Reflection.ParameterInfo,System.Reflection.MemberInfo)
extern "C"  void ParameterInfo__ctor_m3736663868 (ParameterInfo_t2610273829 * __this, ParameterInfo_t2610273829 * ___pinfo, MemberInfo_t * ___member, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		ParameterInfo_t2610273829 * L_0 = ___pinfo;
		NullCheck(L_0);
		Type_t * L_1 = VirtFuncInvoker0< Type_t * >::Invoke(6 /* System.Type System.Reflection.ParameterInfo::get_ParameterType() */, L_0);
		__this->set_ClassImpl_0(L_1);
		MemberInfo_t * L_2 = ___member;
		__this->set_MemberImpl_2(L_2);
		ParameterInfo_t2610273829 * L_3 = ___pinfo;
		NullCheck(L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(9 /* System.String System.Reflection.ParameterInfo::get_Name() */, L_3);
		__this->set_NameImpl_3(L_4);
		ParameterInfo_t2610273829 * L_5 = ___pinfo;
		NullCheck(L_5);
		int32_t L_6 = VirtFuncInvoker0< int32_t >::Invoke(10 /* System.Int32 System.Reflection.ParameterInfo::get_Position() */, L_5);
		__this->set_PositionImpl_4(L_6);
		ParameterInfo_t2610273829 * L_7 = ___pinfo;
		NullCheck(L_7);
		int32_t L_8 = VirtFuncInvoker0< int32_t >::Invoke(7 /* System.Reflection.ParameterAttributes System.Reflection.ParameterInfo::get_Attributes() */, L_7);
		__this->set_AttrsImpl_5(L_8);
		return;
	}
}
// System.String System.Reflection.ParameterInfo::ToString()
extern const Il2CppType* Void_t2779279689_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Char_t2778706699_il2cpp_TypeInfo_var;
extern const uint32_t ParameterInfo_ToString_m608350892_MetadataUsageId;
extern "C"  String_t* ParameterInfo_ToString_m608350892 (ParameterInfo_t2610273829 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ParameterInfo_ToString_m608350892_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Type_t * V_0 = NULL;
	bool V_1 = false;
	String_t* V_2 = NULL;
	int32_t G_B7_0 = 0;
	String_t* G_B10_0 = NULL;
	{
		Type_t * L_0 = __this->get_ClassImpl_0();
		V_0 = L_0;
		goto IL_0013;
	}

IL_000c:
	{
		Type_t * L_1 = V_0;
		NullCheck(L_1);
		Type_t * L_2 = VirtFuncInvoker0< Type_t * >::Invoke(44 /* System.Type System.Type::GetElementType() */, L_1);
		V_0 = L_2;
	}

IL_0013:
	{
		Type_t * L_3 = V_0;
		NullCheck(L_3);
		bool L_4 = VirtFuncInvoker0< bool >::Invoke(19 /* System.Boolean System.Type::get_HasElementType() */, L_3);
		if (L_4)
		{
			goto IL_000c;
		}
	}
	{
		Type_t * L_5 = V_0;
		NullCheck(L_5);
		bool L_6 = VirtFuncInvoker0< bool >::Invoke(30 /* System.Boolean System.Type::get_IsPrimitive() */, L_5);
		if (L_6)
		{
			goto IL_0060;
		}
	}
	{
		Type_t * L_7 = __this->get_ClassImpl_0();
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_8 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Void_t2779279689_0_0_0_var), /*hidden argument*/NULL);
		if ((((Il2CppObject*)(Type_t *)L_7) == ((Il2CppObject*)(Type_t *)L_8)))
		{
			goto IL_0060;
		}
	}
	{
		Type_t * L_9 = __this->get_ClassImpl_0();
		NullCheck(L_9);
		String_t* L_10 = VirtFuncInvoker0< String_t* >::Invoke(34 /* System.String System.Type::get_Namespace() */, L_9);
		MemberInfo_t * L_11 = __this->get_MemberImpl_2();
		NullCheck(L_11);
		Type_t * L_12 = VirtFuncInvoker0< Type_t * >::Invoke(6 /* System.Type System.Reflection.MemberInfo::get_DeclaringType() */, L_11);
		NullCheck(L_12);
		String_t* L_13 = VirtFuncInvoker0< String_t* >::Invoke(34 /* System.String System.Type::get_Namespace() */, L_12);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_14 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_10, L_13, /*hidden argument*/NULL);
		G_B7_0 = ((int32_t)(L_14));
		goto IL_0061;
	}

IL_0060:
	{
		G_B7_0 = 1;
	}

IL_0061:
	{
		V_1 = (bool)G_B7_0;
		bool L_15 = V_1;
		if (!L_15)
		{
			goto IL_0078;
		}
	}
	{
		Type_t * L_16 = __this->get_ClassImpl_0();
		NullCheck(L_16);
		String_t* L_17 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_16);
		G_B10_0 = L_17;
		goto IL_0083;
	}

IL_0078:
	{
		Type_t * L_18 = __this->get_ClassImpl_0();
		NullCheck(L_18);
		String_t* L_19 = VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String System.Type::get_FullName() */, L_18);
		G_B10_0 = L_19;
	}

IL_0083:
	{
		V_2 = G_B10_0;
		bool L_20 = ParameterInfo_get_IsRetval_m843554164(__this, /*hidden argument*/NULL);
		if (L_20)
		{
			goto IL_00aa;
		}
	}
	{
		String_t* L_21 = V_2;
		uint16_t L_22 = ((uint16_t)((int32_t)32));
		Il2CppObject * L_23 = Box(Char_t2778706699_il2cpp_TypeInfo_var, &L_22);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_24 = String_Concat_m389863537(NULL /*static, unused*/, L_21, L_23, /*hidden argument*/NULL);
		V_2 = L_24;
		String_t* L_25 = V_2;
		String_t* L_26 = __this->get_NameImpl_3();
		String_t* L_27 = String_Concat_m138640077(NULL /*static, unused*/, L_25, L_26, /*hidden argument*/NULL);
		V_2 = L_27;
	}

IL_00aa:
	{
		String_t* L_28 = V_2;
		return L_28;
	}
}
// System.Type System.Reflection.ParameterInfo::get_ParameterType()
extern "C"  Type_t * ParameterInfo_get_ParameterType_m962921011 (ParameterInfo_t2610273829 * __this, const MethodInfo* method)
{
	{
		Type_t * L_0 = __this->get_ClassImpl_0();
		return L_0;
	}
}
// System.Reflection.ParameterAttributes System.Reflection.ParameterInfo::get_Attributes()
extern "C"  int32_t ParameterInfo_get_Attributes_m3667882316 (ParameterInfo_t2610273829 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_AttrsImpl_5();
		return L_0;
	}
}
// System.Boolean System.Reflection.ParameterInfo::get_IsIn()
extern "C"  bool ParameterInfo_get_IsIn_m462729209 (ParameterInfo_t2610273829 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = VirtFuncInvoker0< int32_t >::Invoke(7 /* System.Reflection.ParameterAttributes System.Reflection.ParameterInfo::get_Attributes() */, __this);
		return (bool)((((int32_t)((((int32_t)((int32_t)((int32_t)L_0&(int32_t)1))) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Boolean System.Reflection.ParameterInfo::get_IsOptional()
extern "C"  bool ParameterInfo_get_IsOptional_m2840883508 (ParameterInfo_t2610273829 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = VirtFuncInvoker0< int32_t >::Invoke(7 /* System.Reflection.ParameterAttributes System.Reflection.ParameterInfo::get_Attributes() */, __this);
		return (bool)((((int32_t)((((int32_t)((int32_t)((int32_t)L_0&(int32_t)((int32_t)16)))) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Boolean System.Reflection.ParameterInfo::get_IsOut()
extern "C"  bool ParameterInfo_get_IsOut_m1465526300 (ParameterInfo_t2610273829 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = VirtFuncInvoker0< int32_t >::Invoke(7 /* System.Reflection.ParameterAttributes System.Reflection.ParameterInfo::get_Attributes() */, __this);
		return (bool)((((int32_t)((((int32_t)((int32_t)((int32_t)L_0&(int32_t)2))) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Boolean System.Reflection.ParameterInfo::get_IsRetval()
extern "C"  bool ParameterInfo_get_IsRetval_m843554164 (ParameterInfo_t2610273829 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = VirtFuncInvoker0< int32_t >::Invoke(7 /* System.Reflection.ParameterAttributes System.Reflection.ParameterInfo::get_Attributes() */, __this);
		return (bool)((((int32_t)((((int32_t)((int32_t)((int32_t)L_0&(int32_t)8))) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Reflection.MemberInfo System.Reflection.ParameterInfo::get_Member()
extern "C"  MemberInfo_t * ParameterInfo_get_Member_m215664551 (ParameterInfo_t2610273829 * __this, const MethodInfo* method)
{
	{
		MemberInfo_t * L_0 = __this->get_MemberImpl_2();
		return L_0;
	}
}
// System.String System.Reflection.ParameterInfo::get_Name()
extern "C"  String_t* ParameterInfo_get_Name_m1246625812 (ParameterInfo_t2610273829 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_NameImpl_3();
		return L_0;
	}
}
// System.Int32 System.Reflection.ParameterInfo::get_Position()
extern "C"  int32_t ParameterInfo_get_Position_m2226484697 (ParameterInfo_t2610273829 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_PositionImpl_4();
		return L_0;
	}
}
// System.Object[] System.Reflection.ParameterInfo::GetCustomAttributes(System.Type,System.Boolean)
extern TypeInfo* MonoCustomAttrs_t1707908163_il2cpp_TypeInfo_var;
extern const uint32_t ParameterInfo_GetCustomAttributes_m3587382740_MetadataUsageId;
extern "C"  ObjectU5BU5D_t11523773* ParameterInfo_GetCustomAttributes_m3587382740 (ParameterInfo_t2610273829 * __this, Type_t * ___attributeType, bool ___inherit, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ParameterInfo_GetCustomAttributes_m3587382740_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Type_t * L_0 = ___attributeType;
		bool L_1 = ___inherit;
		IL2CPP_RUNTIME_CLASS_INIT(MonoCustomAttrs_t1707908163_il2cpp_TypeInfo_var);
		ObjectU5BU5D_t11523773* L_2 = MonoCustomAttrs_GetCustomAttributes_m3020328693(NULL /*static, unused*/, __this, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Boolean System.Reflection.ParameterInfo::IsDefined(System.Type,System.Boolean)
extern TypeInfo* MonoCustomAttrs_t1707908163_il2cpp_TypeInfo_var;
extern const uint32_t ParameterInfo_IsDefined_m1420905444_MetadataUsageId;
extern "C"  bool ParameterInfo_IsDefined_m1420905444 (ParameterInfo_t2610273829 * __this, Type_t * ___attributeType, bool ___inherit, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ParameterInfo_IsDefined_m1420905444_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Type_t * L_0 = ___attributeType;
		bool L_1 = ___inherit;
		IL2CPP_RUNTIME_CLASS_INIT(MonoCustomAttrs_t1707908163_il2cpp_TypeInfo_var);
		bool L_2 = MonoCustomAttrs_IsDefined_m1052740451(NULL /*static, unused*/, __this, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Object[] System.Reflection.ParameterInfo::GetPseudoCustomAttributes()
extern TypeInfo* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern TypeInfo* InAttribute_t1020461241_il2cpp_TypeInfo_var;
extern TypeInfo* OptionalAttribute_t4067831006_il2cpp_TypeInfo_var;
extern TypeInfo* OutAttribute_t3987499248_il2cpp_TypeInfo_var;
extern const uint32_t ParameterInfo_GetPseudoCustomAttributes_m760090638_MetadataUsageId;
extern "C"  ObjectU5BU5D_t11523773* ParameterInfo_GetPseudoCustomAttributes_m760090638 (ParameterInfo_t2610273829 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ParameterInfo_GetPseudoCustomAttributes_m760090638_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	ObjectU5BU5D_t11523773* V_1 = NULL;
	{
		V_0 = 0;
		bool L_0 = ParameterInfo_get_IsIn_m462729209(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_1 = V_0;
		V_0 = ((int32_t)((int32_t)L_1+(int32_t)1));
	}

IL_0011:
	{
		bool L_2 = ParameterInfo_get_IsOut_m1465526300(__this, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0020;
		}
	}
	{
		int32_t L_3 = V_0;
		V_0 = ((int32_t)((int32_t)L_3+(int32_t)1));
	}

IL_0020:
	{
		bool L_4 = ParameterInfo_get_IsOptional_m2840883508(__this, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_002f;
		}
	}
	{
		int32_t L_5 = V_0;
		V_0 = ((int32_t)((int32_t)L_5+(int32_t)1));
	}

IL_002f:
	{
		UnmanagedMarshal_t446138789 * L_6 = __this->get_marshalAs_6();
		if (!L_6)
		{
			goto IL_003e;
		}
	}
	{
		int32_t L_7 = V_0;
		V_0 = ((int32_t)((int32_t)L_7+(int32_t)1));
	}

IL_003e:
	{
		int32_t L_8 = V_0;
		if (L_8)
		{
			goto IL_0046;
		}
	}
	{
		return (ObjectU5BU5D_t11523773*)NULL;
	}

IL_0046:
	{
		int32_t L_9 = V_0;
		V_1 = ((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)L_9));
		V_0 = 0;
		bool L_10 = ParameterInfo_get_IsIn_m462729209(__this, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0066;
		}
	}
	{
		ObjectU5BU5D_t11523773* L_11 = V_1;
		int32_t L_12 = V_0;
		int32_t L_13 = L_12;
		V_0 = ((int32_t)((int32_t)L_13+(int32_t)1));
		InAttribute_t1020461241 * L_14 = (InAttribute_t1020461241 *)il2cpp_codegen_object_new(InAttribute_t1020461241_il2cpp_TypeInfo_var);
		InAttribute__ctor_m476828707(L_14, /*hidden argument*/NULL);
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, L_13);
		ArrayElementTypeCheck (L_11, L_14);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(L_13), (Il2CppObject *)L_14);
	}

IL_0066:
	{
		bool L_15 = ParameterInfo_get_IsOptional_m2840883508(__this, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_007d;
		}
	}
	{
		ObjectU5BU5D_t11523773* L_16 = V_1;
		int32_t L_17 = V_0;
		int32_t L_18 = L_17;
		V_0 = ((int32_t)((int32_t)L_18+(int32_t)1));
		OptionalAttribute_t4067831006 * L_19 = (OptionalAttribute_t4067831006 *)il2cpp_codegen_object_new(OptionalAttribute_t4067831006_il2cpp_TypeInfo_var);
		OptionalAttribute__ctor_m1203139678(L_19, /*hidden argument*/NULL);
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, L_18);
		ArrayElementTypeCheck (L_16, L_19);
		(L_16)->SetAt(static_cast<il2cpp_array_size_t>(L_18), (Il2CppObject *)L_19);
	}

IL_007d:
	{
		bool L_20 = ParameterInfo_get_IsOut_m1465526300(__this, /*hidden argument*/NULL);
		if (!L_20)
		{
			goto IL_0094;
		}
	}
	{
		ObjectU5BU5D_t11523773* L_21 = V_1;
		int32_t L_22 = V_0;
		int32_t L_23 = L_22;
		V_0 = ((int32_t)((int32_t)L_23+(int32_t)1));
		OutAttribute_t3987499248 * L_24 = (OutAttribute_t3987499248 *)il2cpp_codegen_object_new(OutAttribute_t3987499248_il2cpp_TypeInfo_var);
		OutAttribute__ctor_m3736730494(L_24, /*hidden argument*/NULL);
		NullCheck(L_21);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_21, L_23);
		ArrayElementTypeCheck (L_21, L_24);
		(L_21)->SetAt(static_cast<il2cpp_array_size_t>(L_23), (Il2CppObject *)L_24);
	}

IL_0094:
	{
		UnmanagedMarshal_t446138789 * L_25 = __this->get_marshalAs_6();
		if (!L_25)
		{
			goto IL_00b1;
		}
	}
	{
		ObjectU5BU5D_t11523773* L_26 = V_1;
		int32_t L_27 = V_0;
		int32_t L_28 = L_27;
		V_0 = ((int32_t)((int32_t)L_28+(int32_t)1));
		UnmanagedMarshal_t446138789 * L_29 = __this->get_marshalAs_6();
		NullCheck(L_29);
		MarshalAsAttribute_t1016163854 * L_30 = UnmanagedMarshal_ToMarshalAsAttribute_m4201209287(L_29, /*hidden argument*/NULL);
		NullCheck(L_26);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_26, L_28);
		ArrayElementTypeCheck (L_26, L_30);
		(L_26)->SetAt(static_cast<il2cpp_array_size_t>(L_28), (Il2CppObject *)L_30);
	}

IL_00b1:
	{
		ObjectU5BU5D_t11523773* L_31 = V_1;
		return L_31;
	}
}
// Conversion methods for marshalling of: System.Reflection.ParameterModifier
extern "C" void ParameterModifier_t500203470_marshal_pinvoke(const ParameterModifier_t500203470& unmarshaled, ParameterModifier_t500203470_marshaled_pinvoke& marshaled)
{
	uint32_t _unmarshaled__byref_Length = 0;
	if (unmarshaled.get__byref_0() != NULL)
	{
		_unmarshaled__byref_Length = ((Il2CppCodeGenArray*)unmarshaled.get__byref_0())->max_length;
		marshaled.____byref_0 = il2cpp_codegen_marshal_allocate_array<int32_t>(_unmarshaled__byref_Length);
	}
	for (uint32_t i = 0; i < _unmarshaled__byref_Length; i++)
	{
		(marshaled.____byref_0)[i] = (unmarshaled.get__byref_0())->GetAt(static_cast<il2cpp_array_size_t>(i));
	}
}
extern TypeInfo* BooleanU5BU5D_t3804927312_il2cpp_TypeInfo_var;
extern const uint32_t ParameterModifier_t500203470_pinvoke_FromNativeMethodDefinition_MetadataUsageId;
extern "C" void ParameterModifier_t500203470_marshal_pinvoke_back(const ParameterModifier_t500203470_marshaled_pinvoke& marshaled, ParameterModifier_t500203470& unmarshaled)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ParameterModifier_t500203470_pinvoke_FromNativeMethodDefinition_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	if (marshaled.____byref_0 != NULL)
	{
		uint32_t _loopCount;
		if (unmarshaled.get__byref_0() == NULL)
		{
			_loopCount = 1;
			unmarshaled.set__byref_0(reinterpret_cast<BooleanU5BU5D_t3804927312*>(SZArrayNew(BooleanU5BU5D_t3804927312_il2cpp_TypeInfo_var, _loopCount)));
		}
		else
		{
			_loopCount = ((Il2CppCodeGenArray*)unmarshaled.get__byref_0())->max_length;
		}
		for (uint32_t i = 0; i < _loopCount; i++)
		{
			bool _item = false;
			_item = (marshaled.____byref_0)[i];
			(unmarshaled.get__byref_0())->SetAt(static_cast<il2cpp_array_size_t>(i), _item);
		}
	}
}
// Conversion method for clean up from marshalling of: System.Reflection.ParameterModifier
extern "C" void ParameterModifier_t500203470_marshal_pinvoke_cleanup(ParameterModifier_t500203470_marshaled_pinvoke& marshaled)
{
	const uint32_t marshaled_____byref_0_CleanupLoopCount = 1;
	for (uint32_t i = 0; i < marshaled_____byref_0_CleanupLoopCount; i++)
	{
	}
	il2cpp_codegen_marshal_free(marshaled.____byref_0);
	marshaled.____byref_0 = NULL;
}
// Conversion methods for marshalling of: System.Reflection.ParameterModifier
extern "C" void ParameterModifier_t500203470_marshal_com(const ParameterModifier_t500203470& unmarshaled, ParameterModifier_t500203470_marshaled_com& marshaled)
{
	uint32_t _unmarshaled__byref_Length = 0;
	if (unmarshaled.get__byref_0() != NULL)
	{
		_unmarshaled__byref_Length = ((Il2CppCodeGenArray*)unmarshaled.get__byref_0())->max_length;
		marshaled.____byref_0 = il2cpp_codegen_marshal_allocate_array<int32_t>(_unmarshaled__byref_Length);
	}
	for (uint32_t i = 0; i < _unmarshaled__byref_Length; i++)
	{
		(marshaled.____byref_0)[i] = (unmarshaled.get__byref_0())->GetAt(static_cast<il2cpp_array_size_t>(i));
	}
}
extern TypeInfo* BooleanU5BU5D_t3804927312_il2cpp_TypeInfo_var;
extern const uint32_t ParameterModifier_t500203470_com_FromNativeMethodDefinition_MetadataUsageId;
extern "C" void ParameterModifier_t500203470_marshal_com_back(const ParameterModifier_t500203470_marshaled_com& marshaled, ParameterModifier_t500203470& unmarshaled)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ParameterModifier_t500203470_com_FromNativeMethodDefinition_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	if (marshaled.____byref_0 != NULL)
	{
		uint32_t _loopCount;
		if (unmarshaled.get__byref_0() == NULL)
		{
			_loopCount = 1;
			unmarshaled.set__byref_0(reinterpret_cast<BooleanU5BU5D_t3804927312*>(SZArrayNew(BooleanU5BU5D_t3804927312_il2cpp_TypeInfo_var, _loopCount)));
		}
		else
		{
			_loopCount = ((Il2CppCodeGenArray*)unmarshaled.get__byref_0())->max_length;
		}
		for (uint32_t i = 0; i < _loopCount; i++)
		{
			bool _item = false;
			_item = (marshaled.____byref_0)[i];
			(unmarshaled.get__byref_0())->SetAt(static_cast<il2cpp_array_size_t>(i), _item);
		}
	}
}
// Conversion method for clean up from marshalling of: System.Reflection.ParameterModifier
extern "C" void ParameterModifier_t500203470_marshal_com_cleanup(ParameterModifier_t500203470_marshaled_com& marshaled)
{
	const uint32_t marshaled_____byref_0_CleanupLoopCount = 1;
	for (uint32_t i = 0; i < marshaled_____byref_0_CleanupLoopCount; i++)
	{
	}
	il2cpp_codegen_marshal_free(marshaled.____byref_0);
	marshaled.____byref_0 = NULL;
}
// System.Void System.Reflection.Pointer::.ctor()
extern "C"  void Pointer__ctor_m226191707 (Pointer_t3455104107 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Reflection.Pointer::System.Runtime.Serialization.ISerializable.GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern TypeInfo* NotSupportedException_t1374155497_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3882881452;
extern const uint32_t Pointer_System_Runtime_Serialization_ISerializable_GetObjectData_m448849360_MetadataUsageId;
extern "C"  void Pointer_System_Runtime_Serialization_ISerializable_GetObjectData_m448849360 (Pointer_t3455104107 * __this, SerializationInfo_t2995724695 * ___info, StreamingContext_t986364934  ___context, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Pointer_System_Runtime_Serialization_ISerializable_GetObjectData_m448849360_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1374155497 * L_0 = (NotSupportedException_t1374155497 *)il2cpp_codegen_object_new(NotSupportedException_t1374155497_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m133757637(L_0, _stringLiteral3882881452, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Reflection.PropertyInfo::.ctor()
extern "C"  void PropertyInfo__ctor_m539791979 (PropertyInfo_t * __this, const MethodInfo* method)
{
	{
		MemberInfo__ctor_m3915857030(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Reflection.MemberTypes System.Reflection.PropertyInfo::get_MemberType()
extern "C"  int32_t PropertyInfo_get_MemberType_m3698268270 (PropertyInfo_t * __this, const MethodInfo* method)
{
	{
		return (int32_t)(((int32_t)16));
	}
}
// System.Reflection.MethodInfo System.Reflection.PropertyInfo::GetGetMethod()
extern "C"  MethodInfo_t * PropertyInfo_GetGetMethod_m1125167762 (PropertyInfo_t * __this, const MethodInfo* method)
{
	{
		MethodInfo_t * L_0 = VirtFuncInvoker1< MethodInfo_t *, bool >::Invoke(20 /* System.Reflection.MethodInfo System.Reflection.PropertyInfo::GetGetMethod(System.Boolean) */, __this, (bool)0);
		return L_0;
	}
}
// System.Object System.Reflection.PropertyInfo::GetValue(System.Object,System.Object[])
extern "C"  Il2CppObject * PropertyInfo_GetValue_m396207607 (PropertyInfo_t * __this, Il2CppObject * ___obj, ObjectU5BU5D_t11523773* ___index, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___obj;
		ObjectU5BU5D_t11523773* L_1 = ___index;
		Il2CppObject * L_2 = VirtFuncInvoker5< Il2CppObject *, Il2CppObject *, int32_t, Binder_t4180926488 *, ObjectU5BU5D_t11523773*, CultureInfo_t3603717042 * >::Invoke(24 /* System.Object System.Reflection.PropertyInfo::GetValue(System.Object,System.Reflection.BindingFlags,System.Reflection.Binder,System.Object[],System.Globalization.CultureInfo) */, __this, L_0, 0, (Binder_t4180926488 *)NULL, L_1, (CultureInfo_t3603717042 *)NULL);
		return L_2;
	}
}
// System.Void System.Reflection.PropertyInfo::SetValue(System.Object,System.Object,System.Object[])
extern "C"  void PropertyInfo_SetValue_m3226288870 (PropertyInfo_t * __this, Il2CppObject * ___obj, Il2CppObject * ___value, ObjectU5BU5D_t11523773* ___index, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___obj;
		Il2CppObject * L_1 = ___value;
		ObjectU5BU5D_t11523773* L_2 = ___index;
		VirtActionInvoker6< Il2CppObject *, Il2CppObject *, int32_t, Binder_t4180926488 *, ObjectU5BU5D_t11523773*, CultureInfo_t3603717042 * >::Invoke(26 /* System.Void System.Reflection.PropertyInfo::SetValue(System.Object,System.Object,System.Reflection.BindingFlags,System.Reflection.Binder,System.Object[],System.Globalization.CultureInfo) */, __this, L_0, L_1, 0, (Binder_t4180926488 *)NULL, L_2, (CultureInfo_t3603717042 *)NULL);
		return;
	}
}
// System.Type[] System.Reflection.PropertyInfo::GetOptionalCustomModifiers()
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t PropertyInfo_GetOptionalCustomModifiers_m1611882438_MetadataUsageId;
extern "C"  TypeU5BU5D_t3431720054* PropertyInfo_GetOptionalCustomModifiers_m1611882438 (PropertyInfo_t * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PropertyInfo_GetOptionalCustomModifiers_m1611882438_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		TypeU5BU5D_t3431720054* L_0 = ((Type_t_StaticFields*)Type_t_il2cpp_TypeInfo_var->static_fields)->get_EmptyTypes_3();
		return L_0;
	}
}
// System.Type[] System.Reflection.PropertyInfo::GetRequiredCustomModifiers()
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t PropertyInfo_GetRequiredCustomModifiers_m3981387111_MetadataUsageId;
extern "C"  TypeU5BU5D_t3431720054* PropertyInfo_GetRequiredCustomModifiers_m3981387111 (PropertyInfo_t * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PropertyInfo_GetRequiredCustomModifiers_m3981387111_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		TypeU5BU5D_t3431720054* L_0 = ((Type_t_StaticFields*)Type_t_il2cpp_TypeInfo_var->static_fields)->get_EmptyTypes_3();
		return L_0;
	}
}
// System.Void System.Reflection.StrongNameKeyPair::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const Il2CppType* ByteU5BU5D_t58506160_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* ByteU5BU5D_t58506160_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4252953015;
extern Il2CppCodeGenString* _stringLiteral421340423;
extern Il2CppCodeGenString* _stringLiteral3088515597;
extern Il2CppCodeGenString* _stringLiteral423984607;
extern const uint32_t StrongNameKeyPair__ctor_m583813794_MetadataUsageId;
extern "C"  void StrongNameKeyPair__ctor_m583813794 (StrongNameKeyPair_t2760016869 * __this, SerializationInfo_t2995724695 * ___info, StreamingContext_t986364934  ___context, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (StrongNameKeyPair__ctor_m583813794_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		SerializationInfo_t2995724695 * L_0 = ___info;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(ByteU5BU5D_t58506160_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_0);
		Il2CppObject * L_2 = SerializationInfo_GetValue_m4125471336(L_0, _stringLiteral4252953015, L_1, /*hidden argument*/NULL);
		__this->set__publicKey_0(((ByteU5BU5D_t58506160*)Castclass(L_2, ByteU5BU5D_t58506160_il2cpp_TypeInfo_var)));
		SerializationInfo_t2995724695 * L_3 = ___info;
		NullCheck(L_3);
		String_t* L_4 = SerializationInfo_GetString_m52579033(L_3, _stringLiteral421340423, /*hidden argument*/NULL);
		__this->set__keyPairContainer_1(L_4);
		SerializationInfo_t2995724695 * L_5 = ___info;
		NullCheck(L_5);
		bool L_6 = SerializationInfo_GetBoolean_m1462266865(L_5, _stringLiteral3088515597, /*hidden argument*/NULL);
		__this->set__keyPairExported_2(L_6);
		SerializationInfo_t2995724695 * L_7 = ___info;
		Type_t * L_8 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(ByteU5BU5D_t58506160_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_7);
		Il2CppObject * L_9 = SerializationInfo_GetValue_m4125471336(L_7, _stringLiteral423984607, L_8, /*hidden argument*/NULL);
		__this->set__keyPairArray_3(((ByteU5BU5D_t58506160*)Castclass(L_9, ByteU5BU5D_t58506160_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void System.Reflection.StrongNameKeyPair::System.Runtime.Serialization.ISerializable.GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const Il2CppType* ByteU5BU5D_t58506160_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4252953015;
extern Il2CppCodeGenString* _stringLiteral421340423;
extern Il2CppCodeGenString* _stringLiteral3088515597;
extern Il2CppCodeGenString* _stringLiteral423984607;
extern const uint32_t StrongNameKeyPair_System_Runtime_Serialization_ISerializable_GetObjectData_m285619594_MetadataUsageId;
extern "C"  void StrongNameKeyPair_System_Runtime_Serialization_ISerializable_GetObjectData_m285619594 (StrongNameKeyPair_t2760016869 * __this, SerializationInfo_t2995724695 * ___info, StreamingContext_t986364934  ___context, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (StrongNameKeyPair_System_Runtime_Serialization_ISerializable_GetObjectData_m285619594_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		SerializationInfo_t2995724695 * L_0 = ___info;
		ByteU5BU5D_t58506160* L_1 = __this->get__publicKey_0();
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_2 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(ByteU5BU5D_t58506160_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_0);
		SerializationInfo_AddValue_m3341936982(L_0, _stringLiteral4252953015, (Il2CppObject *)(Il2CppObject *)L_1, L_2, /*hidden argument*/NULL);
		SerializationInfo_t2995724695 * L_3 = ___info;
		String_t* L_4 = __this->get__keyPairContainer_1();
		NullCheck(L_3);
		SerializationInfo_AddValue_m469120675(L_3, _stringLiteral421340423, L_4, /*hidden argument*/NULL);
		SerializationInfo_t2995724695 * L_5 = ___info;
		bool L_6 = __this->get__keyPairExported_2();
		NullCheck(L_5);
		SerializationInfo_AddValue_m3573408328(L_5, _stringLiteral3088515597, L_6, /*hidden argument*/NULL);
		SerializationInfo_t2995724695 * L_7 = ___info;
		ByteU5BU5D_t58506160* L_8 = __this->get__keyPairArray_3();
		Type_t * L_9 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(ByteU5BU5D_t58506160_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_7);
		SerializationInfo_AddValue_m3341936982(L_7, _stringLiteral423984607, (Il2CppObject *)(Il2CppObject *)L_8, L_9, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Reflection.StrongNameKeyPair::System.Runtime.Serialization.IDeserializationCallback.OnDeserialization(System.Object)
extern "C"  void StrongNameKeyPair_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m3690645877 (StrongNameKeyPair_t2760016869 * __this, Il2CppObject * ___sender, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Security.Cryptography.RSA System.Reflection.StrongNameKeyPair::GetRSA()
extern TypeInfo* Il2CppObject_il2cpp_TypeInfo_var;
extern TypeInfo* CspParameters_t4096074019_il2cpp_TypeInfo_var;
extern TypeInfo* RSACryptoServiceProvider_t555495358_il2cpp_TypeInfo_var;
extern const uint32_t StrongNameKeyPair_GetRSA_m987209039_MetadataUsageId;
extern "C"  RSA_t1557565273 * StrongNameKeyPair_GetRSA_m987209039 (StrongNameKeyPair_t2760016869 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (StrongNameKeyPair_GetRSA_m987209039_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	CspParameters_t4096074019 * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		RSA_t1557565273 * L_0 = __this->get__rsa_4();
		if (!L_0)
		{
			goto IL_0012;
		}
	}
	{
		RSA_t1557565273 * L_1 = __this->get__rsa_4();
		return L_1;
	}

IL_0012:
	{
		ByteU5BU5D_t58506160* L_2 = __this->get__keyPairArray_3();
		if (!L_2)
		{
			goto IL_0045;
		}
	}

IL_001d:
	try
	{ // begin try (depth: 1)
		ByteU5BU5D_t58506160* L_3 = __this->get__keyPairArray_3();
		RSA_t1557565273 * L_4 = CryptoConvert_FromCapiKeyBlob_m3062702575(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		__this->set__rsa_4(L_4);
		goto IL_0040;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0033;
		throw e;
	}

CATCH_0033:
	{ // begin catch(System.Object)
		__this->set__keyPairArray_3((ByteU5BU5D_t58506160*)NULL);
		goto IL_0040;
	} // end catch (depth: 1)

IL_0040:
	{
		goto IL_006e;
	}

IL_0045:
	{
		String_t* L_5 = __this->get__keyPairContainer_1();
		if (!L_5)
		{
			goto IL_006e;
		}
	}
	{
		CspParameters_t4096074019 * L_6 = (CspParameters_t4096074019 *)il2cpp_codegen_object_new(CspParameters_t4096074019_il2cpp_TypeInfo_var);
		CspParameters__ctor_m3804826267(L_6, /*hidden argument*/NULL);
		V_0 = L_6;
		CspParameters_t4096074019 * L_7 = V_0;
		String_t* L_8 = __this->get__keyPairContainer_1();
		NullCheck(L_7);
		L_7->set_KeyContainerName_1(L_8);
		CspParameters_t4096074019 * L_9 = V_0;
		RSACryptoServiceProvider_t555495358 * L_10 = (RSACryptoServiceProvider_t555495358 *)il2cpp_codegen_object_new(RSACryptoServiceProvider_t555495358_il2cpp_TypeInfo_var);
		RSACryptoServiceProvider__ctor_m32080133(L_10, L_9, /*hidden argument*/NULL);
		__this->set__rsa_4(L_10);
	}

IL_006e:
	{
		RSA_t1557565273 * L_11 = __this->get__rsa_4();
		return L_11;
	}
}
// Mono.Security.StrongName System.Reflection.StrongNameKeyPair::StrongName()
extern TypeInfo* StrongName_t2702370795_il2cpp_TypeInfo_var;
extern const uint32_t StrongNameKeyPair_StrongName_m2125236127_MetadataUsageId;
extern "C"  StrongName_t2702370795 * StrongNameKeyPair_StrongName_m2125236127 (StrongNameKeyPair_t2760016869 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (StrongNameKeyPair_StrongName_m2125236127_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	RSA_t1557565273 * V_0 = NULL;
	{
		RSA_t1557565273 * L_0 = StrongNameKeyPair_GetRSA_m987209039(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		RSA_t1557565273 * L_1 = V_0;
		if (!L_1)
		{
			goto IL_0014;
		}
	}
	{
		RSA_t1557565273 * L_2 = V_0;
		StrongName_t2702370795 * L_3 = (StrongName_t2702370795 *)il2cpp_codegen_object_new(StrongName_t2702370795_il2cpp_TypeInfo_var);
		StrongName__ctor_m3580701056(L_3, L_2, /*hidden argument*/NULL);
		return L_3;
	}

IL_0014:
	{
		ByteU5BU5D_t58506160* L_4 = __this->get__publicKey_0();
		if (!L_4)
		{
			goto IL_002b;
		}
	}
	{
		ByteU5BU5D_t58506160* L_5 = __this->get__publicKey_0();
		StrongName_t2702370795 * L_6 = (StrongName_t2702370795 *)il2cpp_codegen_object_new(StrongName_t2702370795_il2cpp_TypeInfo_var);
		StrongName__ctor_m2367078620(L_6, L_5, /*hidden argument*/NULL);
		return L_6;
	}

IL_002b:
	{
		return (StrongName_t2702370795 *)NULL;
	}
}
// System.Void System.Reflection.TargetException::.ctor()
extern Il2CppCodeGenString* _stringLiteral3118499409;
extern const uint32_t TargetException__ctor_m983503354_MetadataUsageId;
extern "C"  void TargetException__ctor_m983503354 (TargetException_t565659628 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TargetException__ctor_m983503354_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = Locale_GetText_m2389348044(NULL /*static, unused*/, _stringLiteral3118499409, /*hidden argument*/NULL);
		ApplicationException__ctor_m1727689164(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Reflection.TargetException::.ctor(System.String)
extern "C"  void TargetException__ctor_m1662849864 (TargetException_t565659628 * __this, String_t* ___message, const MethodInfo* method)
{
	{
		String_t* L_0 = ___message;
		ApplicationException__ctor_m1727689164(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Reflection.TargetException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void TargetException__ctor_m1050912827 (TargetException_t565659628 * __this, SerializationInfo_t2995724695 * ___info, StreamingContext_t986364934  ___context, const MethodInfo* method)
{
	{
		SerializationInfo_t2995724695 * L_0 = ___info;
		StreamingContext_t986364934  L_1 = ___context;
		ApplicationException__ctor_m2651787575(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Reflection.TargetInvocationException::.ctor(System.Exception)
extern Il2CppCodeGenString* _stringLiteral3376423138;
extern const uint32_t TargetInvocationException__ctor_m2876037082_MetadataUsageId;
extern "C"  void TargetInvocationException__ctor_m2876037082 (TargetInvocationException_t1980070524 * __this, Exception_t1967233988 * ___inner, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TargetInvocationException__ctor_m2876037082_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Exception_t1967233988 * L_0 = ___inner;
		ApplicationException__ctor_m1208591594(__this, _stringLiteral3376423138, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Reflection.TargetInvocationException::.ctor(System.String,System.Exception)
extern "C"  void TargetInvocationException__ctor_m534921246 (TargetInvocationException_t1980070524 * __this, String_t* ___message, Exception_t1967233988 * ___inner, const MethodInfo* method)
{
	{
		String_t* L_0 = ___message;
		Exception_t1967233988 * L_1 = ___inner;
		ApplicationException__ctor_m1208591594(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Reflection.TargetInvocationException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void TargetInvocationException__ctor_m706824555 (TargetInvocationException_t1980070524 * __this, SerializationInfo_t2995724695 * ___info, StreamingContext_t986364934  ___sc, const MethodInfo* method)
{
	{
		SerializationInfo_t2995724695 * L_0 = ___info;
		StreamingContext_t986364934  L_1 = ___sc;
		ApplicationException__ctor_m2651787575(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Reflection.TargetParameterCountException::.ctor()
extern Il2CppCodeGenString* _stringLiteral1673299937;
extern const uint32_t TargetParameterCountException__ctor_m2437752064_MetadataUsageId;
extern "C"  void TargetParameterCountException__ctor_m2437752064 (TargetParameterCountException_t2862237030 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TargetParameterCountException__ctor_m2437752064_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = Locale_GetText_m2389348044(NULL /*static, unused*/, _stringLiteral1673299937, /*hidden argument*/NULL);
		ApplicationException__ctor_m1727689164(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Reflection.TargetParameterCountException::.ctor(System.String)
extern "C"  void TargetParameterCountException__ctor_m2555943426 (TargetParameterCountException_t2862237030 * __this, String_t* ___message, const MethodInfo* method)
{
	{
		String_t* L_0 = ___message;
		ApplicationException__ctor_m1727689164(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Reflection.TargetParameterCountException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void TargetParameterCountException__ctor_m2385733185 (TargetParameterCountException_t2862237030 * __this, SerializationInfo_t2995724695 * ___info, StreamingContext_t986364934  ___context, const MethodInfo* method)
{
	{
		SerializationInfo_t2995724695 * L_0 = ___info;
		StreamingContext_t986364934  L_1 = ___context;
		ApplicationException__ctor_m2651787575(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Reflection.TypeDelegator::.ctor()
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t TypeDelegator__ctor_m3978235567_MetadataUsageId;
extern "C"  void TypeDelegator__ctor_m3978235567 (TypeDelegator_t1324217559 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TypeDelegator__ctor_m3978235567_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type__ctor_m3982515451(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Reflection.Assembly System.Reflection.TypeDelegator::get_Assembly()
extern "C"  Assembly_t1882292308 * TypeDelegator_get_Assembly_m793822499 (TypeDelegator_t1324217559 * __this, const MethodInfo* method)
{
	{
		Type_t * L_0 = __this->get_typeImpl_8();
		NullCheck(L_0);
		Assembly_t1882292308 * L_1 = VirtFuncInvoker0< Assembly_t1882292308 * >::Invoke(14 /* System.Reflection.Assembly System.Type::get_Assembly() */, L_0);
		return L_1;
	}
}
// System.String System.Reflection.TypeDelegator::get_AssemblyQualifiedName()
extern "C"  String_t* TypeDelegator_get_AssemblyQualifiedName_m1620996616 (TypeDelegator_t1324217559 * __this, const MethodInfo* method)
{
	{
		Type_t * L_0 = __this->get_typeImpl_8();
		NullCheck(L_0);
		String_t* L_1 = VirtFuncInvoker0< String_t* >::Invoke(15 /* System.String System.Type::get_AssemblyQualifiedName() */, L_0);
		return L_1;
	}
}
// System.Type System.Reflection.TypeDelegator::get_BaseType()
extern "C"  Type_t * TypeDelegator_get_BaseType_m2734895567 (TypeDelegator_t1324217559 * __this, const MethodInfo* method)
{
	{
		Type_t * L_0 = __this->get_typeImpl_8();
		NullCheck(L_0);
		Type_t * L_1 = VirtFuncInvoker0< Type_t * >::Invoke(17 /* System.Type System.Type::get_BaseType() */, L_0);
		return L_1;
	}
}
// System.String System.Reflection.TypeDelegator::get_FullName()
extern "C"  String_t* TypeDelegator_get_FullName_m3758650933 (TypeDelegator_t1324217559 * __this, const MethodInfo* method)
{
	{
		Type_t * L_0 = __this->get_typeImpl_8();
		NullCheck(L_0);
		String_t* L_1 = VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String System.Type::get_FullName() */, L_0);
		return L_1;
	}
}
// System.Reflection.Module System.Reflection.TypeDelegator::get_Module()
extern "C"  Module_t206139610 * TypeDelegator_get_Module_m945912623 (TypeDelegator_t1324217559 * __this, const MethodInfo* method)
{
	{
		Type_t * L_0 = __this->get_typeImpl_8();
		NullCheck(L_0);
		Module_t206139610 * L_1 = VirtFuncInvoker0< Module_t206139610 * >::Invoke(10 /* System.Reflection.Module System.Type::get_Module() */, L_0);
		return L_1;
	}
}
// System.String System.Reflection.TypeDelegator::get_Name()
extern "C"  String_t* TypeDelegator_get_Name_m2498747334 (TypeDelegator_t1324217559 * __this, const MethodInfo* method)
{
	{
		Type_t * L_0 = __this->get_typeImpl_8();
		NullCheck(L_0);
		String_t* L_1 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_0);
		return L_1;
	}
}
// System.String System.Reflection.TypeDelegator::get_Namespace()
extern "C"  String_t* TypeDelegator_get_Namespace_m767650018 (TypeDelegator_t1324217559 * __this, const MethodInfo* method)
{
	{
		Type_t * L_0 = __this->get_typeImpl_8();
		NullCheck(L_0);
		String_t* L_1 = VirtFuncInvoker0< String_t* >::Invoke(34 /* System.String System.Type::get_Namespace() */, L_0);
		return L_1;
	}
}
// System.RuntimeTypeHandle System.Reflection.TypeDelegator::get_TypeHandle()
extern "C"  RuntimeTypeHandle_t1864875887  TypeDelegator_get_TypeHandle_m2316493264 (TypeDelegator_t1324217559 * __this, const MethodInfo* method)
{
	{
		Type_t * L_0 = __this->get_typeImpl_8();
		NullCheck(L_0);
		RuntimeTypeHandle_t1864875887  L_1 = VirtFuncInvoker0< RuntimeTypeHandle_t1864875887  >::Invoke(35 /* System.RuntimeTypeHandle System.Type::get_TypeHandle() */, L_0);
		return L_1;
	}
}
// System.Type System.Reflection.TypeDelegator::get_UnderlyingSystemType()
extern "C"  Type_t * TypeDelegator_get_UnderlyingSystemType_m3809794602 (TypeDelegator_t1324217559 * __this, const MethodInfo* method)
{
	{
		Type_t * L_0 = __this->get_typeImpl_8();
		NullCheck(L_0);
		Type_t * L_1 = VirtFuncInvoker0< Type_t * >::Invoke(36 /* System.Type System.Type::get_UnderlyingSystemType() */, L_0);
		return L_1;
	}
}
// System.Reflection.TypeAttributes System.Reflection.TypeDelegator::GetAttributeFlagsImpl()
extern "C"  int32_t TypeDelegator_GetAttributeFlagsImpl_m239890692 (TypeDelegator_t1324217559 * __this, const MethodInfo* method)
{
	{
		Type_t * L_0 = __this->get_typeImpl_8();
		NullCheck(L_0);
		int32_t L_1 = VirtFuncInvoker0< int32_t >::Invoke(16 /* System.Reflection.TypeAttributes System.Type::get_Attributes() */, L_0);
		return L_1;
	}
}
// System.Reflection.ConstructorInfo System.Reflection.TypeDelegator::GetConstructorImpl(System.Reflection.BindingFlags,System.Reflection.Binder,System.Reflection.CallingConventions,System.Type[],System.Reflection.ParameterModifier[])
extern "C"  ConstructorInfo_t3542137334 * TypeDelegator_GetConstructorImpl_m1507578863 (TypeDelegator_t1324217559 * __this, int32_t ___bindingAttr, Binder_t4180926488 * ___binder, int32_t ___callConvention, TypeU5BU5D_t3431720054* ___types, ParameterModifierU5BU5D_t3379147067* ___modifiers, const MethodInfo* method)
{
	{
		Type_t * L_0 = __this->get_typeImpl_8();
		int32_t L_1 = ___bindingAttr;
		Binder_t4180926488 * L_2 = ___binder;
		int32_t L_3 = ___callConvention;
		TypeU5BU5D_t3431720054* L_4 = ___types;
		ParameterModifierU5BU5D_t3379147067* L_5 = ___modifiers;
		NullCheck(L_0);
		ConstructorInfo_t3542137334 * L_6 = VirtFuncInvoker5< ConstructorInfo_t3542137334 *, int32_t, Binder_t4180926488 *, int32_t, TypeU5BU5D_t3431720054*, ParameterModifierU5BU5D_t3379147067* >::Invoke(75 /* System.Reflection.ConstructorInfo System.Type::GetConstructor(System.Reflection.BindingFlags,System.Reflection.Binder,System.Reflection.CallingConventions,System.Type[],System.Reflection.ParameterModifier[]) */, L_0, L_1, L_2, L_3, L_4, L_5);
		return L_6;
	}
}
// System.Reflection.ConstructorInfo[] System.Reflection.TypeDelegator::GetConstructors(System.Reflection.BindingFlags)
extern "C"  ConstructorInfoU5BU5D_t3572023667* TypeDelegator_GetConstructors_m1969957987 (TypeDelegator_t1324217559 * __this, int32_t ___bindingAttr, const MethodInfo* method)
{
	{
		Type_t * L_0 = __this->get_typeImpl_8();
		int32_t L_1 = ___bindingAttr;
		NullCheck(L_0);
		ConstructorInfoU5BU5D_t3572023667* L_2 = VirtFuncInvoker1< ConstructorInfoU5BU5D_t3572023667*, int32_t >::Invoke(76 /* System.Reflection.ConstructorInfo[] System.Type::GetConstructors(System.Reflection.BindingFlags) */, L_0, L_1);
		return L_2;
	}
}
// System.Object[] System.Reflection.TypeDelegator::GetCustomAttributes(System.Boolean)
extern "C"  ObjectU5BU5D_t11523773* TypeDelegator_GetCustomAttributes_m1964226805 (TypeDelegator_t1324217559 * __this, bool ___inherit, const MethodInfo* method)
{
	{
		Type_t * L_0 = __this->get_typeImpl_8();
		bool L_1 = ___inherit;
		NullCheck(L_0);
		ObjectU5BU5D_t11523773* L_2 = VirtFuncInvoker1< ObjectU5BU5D_t11523773*, bool >::Invoke(12 /* System.Object[] System.Reflection.MemberInfo::GetCustomAttributes(System.Boolean) */, L_0, L_1);
		return L_2;
	}
}
// System.Object[] System.Reflection.TypeDelegator::GetCustomAttributes(System.Type,System.Boolean)
extern "C"  ObjectU5BU5D_t11523773* TypeDelegator_GetCustomAttributes_m2738817378 (TypeDelegator_t1324217559 * __this, Type_t * ___attributeType, bool ___inherit, const MethodInfo* method)
{
	{
		Type_t * L_0 = __this->get_typeImpl_8();
		Type_t * L_1 = ___attributeType;
		bool L_2 = ___inherit;
		NullCheck(L_0);
		ObjectU5BU5D_t11523773* L_3 = VirtFuncInvoker2< ObjectU5BU5D_t11523773*, Type_t *, bool >::Invoke(13 /* System.Object[] System.Reflection.MemberInfo::GetCustomAttributes(System.Type,System.Boolean) */, L_0, L_1, L_2);
		return L_3;
	}
}
// System.Type System.Reflection.TypeDelegator::GetElementType()
extern "C"  Type_t * TypeDelegator_GetElementType_m3058042619 (TypeDelegator_t1324217559 * __this, const MethodInfo* method)
{
	{
		Type_t * L_0 = __this->get_typeImpl_8();
		NullCheck(L_0);
		Type_t * L_1 = VirtFuncInvoker0< Type_t * >::Invoke(44 /* System.Type System.Type::GetElementType() */, L_0);
		return L_1;
	}
}
// System.Reflection.EventInfo System.Reflection.TypeDelegator::GetEvent(System.String,System.Reflection.BindingFlags)
extern "C"  EventInfo_t * TypeDelegator_GetEvent_m939809480 (TypeDelegator_t1324217559 * __this, String_t* ___name, int32_t ___bindingAttr, const MethodInfo* method)
{
	{
		Type_t * L_0 = __this->get_typeImpl_8();
		String_t* L_1 = ___name;
		int32_t L_2 = ___bindingAttr;
		NullCheck(L_0);
		EventInfo_t * L_3 = VirtFuncInvoker2< EventInfo_t *, String_t*, int32_t >::Invoke(45 /* System.Reflection.EventInfo System.Type::GetEvent(System.String,System.Reflection.BindingFlags) */, L_0, L_1, L_2);
		return L_3;
	}
}
// System.Reflection.FieldInfo System.Reflection.TypeDelegator::GetField(System.String,System.Reflection.BindingFlags)
extern "C"  FieldInfo_t * TypeDelegator_GetField_m967065480 (TypeDelegator_t1324217559 * __this, String_t* ___name, int32_t ___bindingAttr, const MethodInfo* method)
{
	{
		Type_t * L_0 = __this->get_typeImpl_8();
		String_t* L_1 = ___name;
		int32_t L_2 = ___bindingAttr;
		NullCheck(L_0);
		FieldInfo_t * L_3 = VirtFuncInvoker2< FieldInfo_t *, String_t*, int32_t >::Invoke(47 /* System.Reflection.FieldInfo System.Type::GetField(System.String,System.Reflection.BindingFlags) */, L_0, L_1, L_2);
		return L_3;
	}
}
// System.Reflection.FieldInfo[] System.Reflection.TypeDelegator::GetFields(System.Reflection.BindingFlags)
extern "C"  FieldInfoU5BU5D_t1144794227* TypeDelegator_GetFields_m4208158627 (TypeDelegator_t1324217559 * __this, int32_t ___bindingAttr, const MethodInfo* method)
{
	{
		Type_t * L_0 = __this->get_typeImpl_8();
		int32_t L_1 = ___bindingAttr;
		NullCheck(L_0);
		FieldInfoU5BU5D_t1144794227* L_2 = VirtFuncInvoker1< FieldInfoU5BU5D_t1144794227*, int32_t >::Invoke(48 /* System.Reflection.FieldInfo[] System.Type::GetFields(System.Reflection.BindingFlags) */, L_0, L_1);
		return L_2;
	}
}
// System.Type[] System.Reflection.TypeDelegator::GetInterfaces()
extern "C"  TypeU5BU5D_t3431720054* TypeDelegator_GetInterfaces_m1212899157 (TypeDelegator_t1324217559 * __this, const MethodInfo* method)
{
	{
		Type_t * L_0 = __this->get_typeImpl_8();
		NullCheck(L_0);
		TypeU5BU5D_t3431720054* L_1 = VirtFuncInvoker0< TypeU5BU5D_t3431720054* >::Invoke(40 /* System.Type[] System.Type::GetInterfaces() */, L_0);
		return L_1;
	}
}
// System.Reflection.MethodInfo System.Reflection.TypeDelegator::GetMethodImpl(System.String,System.Reflection.BindingFlags,System.Reflection.Binder,System.Reflection.CallingConventions,System.Type[],System.Reflection.ParameterModifier[])
extern "C"  MethodInfo_t * TypeDelegator_GetMethodImpl_m1514695301 (TypeDelegator_t1324217559 * __this, String_t* ___name, int32_t ___bindingAttr, Binder_t4180926488 * ___binder, int32_t ___callConvention, TypeU5BU5D_t3431720054* ___types, ParameterModifierU5BU5D_t3379147067* ___modifiers, const MethodInfo* method)
{
	{
		Type_t * L_0 = __this->get_typeImpl_8();
		String_t* L_1 = ___name;
		int32_t L_2 = ___bindingAttr;
		Binder_t4180926488 * L_3 = ___binder;
		int32_t L_4 = ___callConvention;
		TypeU5BU5D_t3431720054* L_5 = ___types;
		ParameterModifierU5BU5D_t3379147067* L_6 = ___modifiers;
		NullCheck(L_0);
		MethodInfo_t * L_7 = Type_GetMethodImplInternal_m4242537390(L_0, L_1, L_2, L_3, L_4, L_5, L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
// System.Reflection.MethodInfo[] System.Reflection.TypeDelegator::GetMethods(System.Reflection.BindingFlags)
extern "C"  MethodInfoU5BU5D_t1668237648* TypeDelegator_GetMethods_m3579300117 (TypeDelegator_t1324217559 * __this, int32_t ___bindingAttr, const MethodInfo* method)
{
	{
		Type_t * L_0 = __this->get_typeImpl_8();
		int32_t L_1 = ___bindingAttr;
		NullCheck(L_0);
		MethodInfoU5BU5D_t1668237648* L_2 = VirtFuncInvoker1< MethodInfoU5BU5D_t1668237648*, int32_t >::Invoke(55 /* System.Reflection.MethodInfo[] System.Type::GetMethods(System.Reflection.BindingFlags) */, L_0, L_1);
		return L_2;
	}
}
// System.Reflection.PropertyInfo[] System.Reflection.TypeDelegator::GetProperties(System.Reflection.BindingFlags)
extern "C"  PropertyInfoU5BU5D_t1348579340* TypeDelegator_GetProperties_m1103738194 (TypeDelegator_t1324217559 * __this, int32_t ___bindingAttr, const MethodInfo* method)
{
	{
		Type_t * L_0 = __this->get_typeImpl_8();
		int32_t L_1 = ___bindingAttr;
		NullCheck(L_0);
		PropertyInfoU5BU5D_t1348579340* L_2 = VirtFuncInvoker1< PropertyInfoU5BU5D_t1348579340*, int32_t >::Invoke(56 /* System.Reflection.PropertyInfo[] System.Type::GetProperties(System.Reflection.BindingFlags) */, L_0, L_1);
		return L_2;
	}
}
// System.Reflection.PropertyInfo System.Reflection.TypeDelegator::GetPropertyImpl(System.String,System.Reflection.BindingFlags,System.Reflection.Binder,System.Type,System.Type[],System.Reflection.ParameterModifier[])
extern "C"  PropertyInfo_t * TypeDelegator_GetPropertyImpl_m162740912 (TypeDelegator_t1324217559 * __this, String_t* ___name, int32_t ___bindingAttr, Binder_t4180926488 * ___binder, Type_t * ___returnType, TypeU5BU5D_t3431720054* ___types, ParameterModifierU5BU5D_t3379147067* ___modifiers, const MethodInfo* method)
{
	{
		Type_t * L_0 = __this->get_typeImpl_8();
		String_t* L_1 = ___name;
		int32_t L_2 = ___bindingAttr;
		Binder_t4180926488 * L_3 = ___binder;
		Type_t * L_4 = ___returnType;
		TypeU5BU5D_t3431720054* L_5 = ___types;
		ParameterModifierU5BU5D_t3379147067* L_6 = ___modifiers;
		NullCheck(L_0);
		PropertyInfo_t * L_7 = Type_GetPropertyImplInternal_m2680281703(L_0, L_1, L_2, L_3, L_4, L_5, L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
// System.Boolean System.Reflection.TypeDelegator::HasElementTypeImpl()
extern "C"  bool TypeDelegator_HasElementTypeImpl_m454977167 (TypeDelegator_t1324217559 * __this, const MethodInfo* method)
{
	{
		Type_t * L_0 = __this->get_typeImpl_8();
		NullCheck(L_0);
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(19 /* System.Boolean System.Type::get_HasElementType() */, L_0);
		return L_1;
	}
}
// System.Object System.Reflection.TypeDelegator::InvokeMember(System.String,System.Reflection.BindingFlags,System.Reflection.Binder,System.Object,System.Object[],System.Reflection.ParameterModifier[],System.Globalization.CultureInfo,System.String[])
extern "C"  Il2CppObject * TypeDelegator_InvokeMember_m4150481760 (TypeDelegator_t1324217559 * __this, String_t* ___name, int32_t ___invokeAttr, Binder_t4180926488 * ___binder, Il2CppObject * ___target, ObjectU5BU5D_t11523773* ___args, ParameterModifierU5BU5D_t3379147067* ___modifiers, CultureInfo_t3603717042 * ___culture, StringU5BU5D_t2956870243* ___namedParameters, const MethodInfo* method)
{
	{
		Type_t * L_0 = __this->get_typeImpl_8();
		String_t* L_1 = ___name;
		int32_t L_2 = ___invokeAttr;
		Binder_t4180926488 * L_3 = ___binder;
		Il2CppObject * L_4 = ___target;
		ObjectU5BU5D_t11523773* L_5 = ___args;
		ParameterModifierU5BU5D_t3379147067* L_6 = ___modifiers;
		CultureInfo_t3603717042 * L_7 = ___culture;
		StringU5BU5D_t2956870243* L_8 = ___namedParameters;
		NullCheck(L_0);
		Il2CppObject * L_9 = VirtFuncInvoker8< Il2CppObject *, String_t*, int32_t, Binder_t4180926488 *, Il2CppObject *, ObjectU5BU5D_t11523773*, ParameterModifierU5BU5D_t3379147067*, CultureInfo_t3603717042 *, StringU5BU5D_t2956870243* >::Invoke(77 /* System.Object System.Type::InvokeMember(System.String,System.Reflection.BindingFlags,System.Reflection.Binder,System.Object,System.Object[],System.Reflection.ParameterModifier[],System.Globalization.CultureInfo,System.String[]) */, L_0, L_1, L_2, L_3, L_4, L_5, L_6, L_7, L_8);
		return L_9;
	}
}
// System.Boolean System.Reflection.TypeDelegator::IsArrayImpl()
extern "C"  bool TypeDelegator_IsArrayImpl_m3068003486 (TypeDelegator_t1324217559 * __this, const MethodInfo* method)
{
	{
		Type_t * L_0 = __this->get_typeImpl_8();
		NullCheck(L_0);
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(21 /* System.Boolean System.Type::get_IsArray() */, L_0);
		return L_1;
	}
}
// System.Boolean System.Reflection.TypeDelegator::IsByRefImpl()
extern "C"  bool TypeDelegator_IsByRefImpl_m598031553 (TypeDelegator_t1324217559 * __this, const MethodInfo* method)
{
	{
		Type_t * L_0 = __this->get_typeImpl_8();
		NullCheck(L_0);
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(22 /* System.Boolean System.Type::get_IsByRef() */, L_0);
		return L_1;
	}
}
// System.Boolean System.Reflection.TypeDelegator::IsDefined(System.Type,System.Boolean)
extern "C"  bool TypeDelegator_IsDefined_m2954211570 (TypeDelegator_t1324217559 * __this, Type_t * ___attributeType, bool ___inherit, const MethodInfo* method)
{
	{
		Type_t * L_0 = __this->get_typeImpl_8();
		Type_t * L_1 = ___attributeType;
		bool L_2 = ___inherit;
		NullCheck(L_0);
		bool L_3 = VirtFuncInvoker2< bool, Type_t *, bool >::Invoke(11 /* System.Boolean System.Reflection.MemberInfo::IsDefined(System.Type,System.Boolean) */, L_0, L_1, L_2);
		return L_3;
	}
}
// System.Boolean System.Reflection.TypeDelegator::IsPointerImpl()
extern "C"  bool TypeDelegator_IsPointerImpl_m3273979010 (TypeDelegator_t1324217559 * __this, const MethodInfo* method)
{
	{
		Type_t * L_0 = __this->get_typeImpl_8();
		NullCheck(L_0);
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(29 /* System.Boolean System.Type::get_IsPointer() */, L_0);
		return L_1;
	}
}
// System.Boolean System.Reflection.TypeDelegator::IsPrimitiveImpl()
extern "C"  bool TypeDelegator_IsPrimitiveImpl_m951679660 (TypeDelegator_t1324217559 * __this, const MethodInfo* method)
{
	{
		Type_t * L_0 = __this->get_typeImpl_8();
		NullCheck(L_0);
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(30 /* System.Boolean System.Type::get_IsPrimitive() */, L_0);
		return L_1;
	}
}
// System.Boolean System.Reflection.TypeDelegator::IsValueTypeImpl()
extern "C"  bool TypeDelegator_IsValueTypeImpl_m1433978640 (TypeDelegator_t1324217559 * __this, const MethodInfo* method)
{
	{
		Type_t * L_0 = __this->get_typeImpl_8();
		NullCheck(L_0);
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(33 /* System.Boolean System.Type::get_IsValueType() */, L_0);
		return L_1;
	}
}
// System.Void System.Reflection.TypeFilter::.ctor(System.Object,System.IntPtr)
extern "C"  void TypeFilter__ctor_m3304815558 (TypeFilter_t2379296192 * __this, Il2CppObject * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->set_method_ptr_0((methodPointerType)((MethodInfo*)___method.get_m_value_0())->method);
	__this->set_method_3(___method);
	__this->set_m_target_2(___object);
}
// System.Boolean System.Reflection.TypeFilter::Invoke(System.Type,System.Object)
extern "C"  bool TypeFilter_Invoke_m3691842469 (TypeFilter_t2379296192 * __this, Type_t * ___m, Il2CppObject * ___filterCriteria, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		TypeFilter_Invoke_m3691842469((TypeFilter_t2379296192 *)__this->get_prev_9(),___m, ___filterCriteria, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, Type_t * ___m, Il2CppObject * ___filterCriteria, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___m, ___filterCriteria,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (void* __this, Type_t * ___m, Il2CppObject * ___filterCriteria, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___m, ___filterCriteria,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, Il2CppObject * ___filterCriteria, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___m, ___filterCriteria,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C" bool pinvoke_delegate_wrapper_TypeFilter_t2379296192(Il2CppObject* delegate, Type_t * ___m, Il2CppObject * ___filterCriteria)
{
	// Marshaling of parameter '___m' to native representation
	IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Cannot marshal type 'System.Type'."));
}
// System.IAsyncResult System.Reflection.TypeFilter::BeginInvoke(System.Type,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * TypeFilter_BeginInvoke_m1184564418 (TypeFilter_t2379296192 * __this, Type_t * ___m, Il2CppObject * ___filterCriteria, AsyncCallback_t1363551830 * ___callback, Il2CppObject * ___object, const MethodInfo* method)
{
	void *__d_args[3] = {0};
	__d_args[0] = ___m;
	__d_args[1] = ___filterCriteria;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Boolean System.Reflection.TypeFilter::EndInvoke(System.IAsyncResult)
extern "C"  bool TypeFilter_EndInvoke_m3571376344 (TypeFilter_t2379296192 * __this, Il2CppObject * ___result, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.ResolveEventArgs::.ctor(System.String)
extern TypeInfo* EventArgs_t516466188_il2cpp_TypeInfo_var;
extern const uint32_t ResolveEventArgs__ctor_m2357566712_MetadataUsageId;
extern "C"  void ResolveEventArgs__ctor_m2357566712 (ResolveEventArgs_t4124692928 * __this, String_t* ___name, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ResolveEventArgs__ctor_m2357566712_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(EventArgs_t516466188_il2cpp_TypeInfo_var);
		EventArgs__ctor_m1904770202(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___name;
		__this->set_m_Name_1(L_0);
		return;
	}
}
// System.Void System.ResolveEventHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void ResolveEventHandler__ctor_m2424315661 (ResolveEventHandler_t2783314641 * __this, Il2CppObject * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->set_method_ptr_0((methodPointerType)((MethodInfo*)___method.get_m_value_0())->method);
	__this->set_method_3(___method);
	__this->set_m_target_2(___object);
}
// System.Reflection.Assembly System.ResolveEventHandler::Invoke(System.Object,System.ResolveEventArgs)
extern "C"  Assembly_t1882292308 * ResolveEventHandler_Invoke_m4207713166 (ResolveEventHandler_t2783314641 * __this, Il2CppObject * ___sender, ResolveEventArgs_t4124692928 * ___args, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		ResolveEventHandler_Invoke_m4207713166((ResolveEventHandler_t2783314641 *)__this->get_prev_9(),___sender, ___args, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef Assembly_t1882292308 * (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___sender, ResolveEventArgs_t4124692928 * ___args, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___sender, ___args,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef Assembly_t1882292308 * (*FunctionPointerType) (void* __this, Il2CppObject * ___sender, ResolveEventArgs_t4124692928 * ___args, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___sender, ___args,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Assembly_t1882292308 * (*FunctionPointerType) (void* __this, ResolveEventArgs_t4124692928 * ___args, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___sender, ___args,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C" Assembly_t1882292308 * pinvoke_delegate_wrapper_ResolveEventHandler_t2783314641(Il2CppObject* delegate, Il2CppObject * ___sender, ResolveEventArgs_t4124692928 * ___args)
{
	// Marshaling of parameter '___sender' to native representation
	IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Cannot marshal type 'System.Object'."));
}
// System.IAsyncResult System.ResolveEventHandler::BeginInvoke(System.Object,System.ResolveEventArgs,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * ResolveEventHandler_BeginInvoke_m1625165996 (ResolveEventHandler_t2783314641 * __this, Il2CppObject * ___sender, ResolveEventArgs_t4124692928 * ___args, AsyncCallback_t1363551830 * ___callback, Il2CppObject * ___object, const MethodInfo* method)
{
	void *__d_args[3] = {0};
	__d_args[0] = ___sender;
	__d_args[1] = ___args;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Reflection.Assembly System.ResolveEventHandler::EndInvoke(System.IAsyncResult)
extern "C"  Assembly_t1882292308 * ResolveEventHandler_EndInvoke_m3051650750 (ResolveEventHandler_t2783314641 * __this, Il2CppObject * ___result, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
	return (Assembly_t1882292308 *)__result;
}
// System.Void System.Resources.NeutralResourcesLanguageAttribute::.ctor(System.String)
extern TypeInfo* ArgumentNullException_t3214793280_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3741234763;
extern const uint32_t NeutralResourcesLanguageAttribute__ctor_m2317811982_MetadataUsageId;
extern "C"  void NeutralResourcesLanguageAttribute__ctor_m2317811982 (NeutralResourcesLanguageAttribute_t3345045768 * __this, String_t* ___cultureName, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (NeutralResourcesLanguageAttribute__ctor_m2317811982_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Attribute__ctor_m2985353781(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___cultureName;
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		ArgumentNullException_t3214793280 * L_1 = (ArgumentNullException_t3214793280 *)il2cpp_codegen_object_new(ArgumentNullException_t3214793280_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_1, _stringLiteral3741234763, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0017:
	{
		String_t* L_2 = ___cultureName;
		__this->set_culture_0(L_2);
		return;
	}
}
// System.Void System.Resources.ResourceManager::.ctor()
extern const Il2CppType* RuntimeResourceSet_t2503739934_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t ResourceManager__ctor_m1041657915_MetadataUsageId;
extern "C"  void ResourceManager__ctor_m1041657915 (ResourceManager_t1361280801 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ResourceManager__ctor_m1041657915_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(RuntimeResourceSet_t2503739934_0_0_0_var), /*hidden argument*/NULL);
		__this->set_resourceSetType_4(L_0);
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Resources.ResourceManager::.cctor()
extern TypeInfo* Hashtable_t3875263730_il2cpp_TypeInfo_var;
extern TypeInfo* ResourceManager_t1361280801_il2cpp_TypeInfo_var;
extern const uint32_t ResourceManager__cctor_m1744528082_MetadataUsageId;
extern "C"  void ResourceManager__cctor_m1744528082 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ResourceManager__cctor_m1744528082_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Hashtable_t3875263730 * L_0 = (Hashtable_t3875263730 *)il2cpp_codegen_object_new(Hashtable_t3875263730_il2cpp_TypeInfo_var);
		Hashtable__ctor_m1514037738(L_0, /*hidden argument*/NULL);
		((ResourceManager_t1361280801_StaticFields*)ResourceManager_t1361280801_il2cpp_TypeInfo_var->static_fields)->set_ResourceCache_0(L_0);
		Hashtable_t3875263730 * L_1 = (Hashtable_t3875263730 *)il2cpp_codegen_object_new(Hashtable_t3875263730_il2cpp_TypeInfo_var);
		Hashtable__ctor_m1514037738(L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Hashtable_t3875263730_il2cpp_TypeInfo_var);
		Hashtable_t3875263730 * L_2 = Hashtable_Synchronized_m4005846889(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		((ResourceManager_t1361280801_StaticFields*)ResourceManager_t1361280801_il2cpp_TypeInfo_var->static_fields)->set_NonExistent_1(L_2);
		((ResourceManager_t1361280801_StaticFields*)ResourceManager_t1361280801_il2cpp_TypeInfo_var->static_fields)->set_HeaderVersionNumber_2(1);
		((ResourceManager_t1361280801_StaticFields*)ResourceManager_t1361280801_il2cpp_TypeInfo_var->static_fields)->set_MagicNumber_3(((int32_t)-1091581234));
		return;
	}
}
// System.Void System.Resources.ResourceReader::.ctor(System.IO.Stream)
extern TypeInfo* Il2CppObject_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentNullException_t3214793280_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentException_t124305799_il2cpp_TypeInfo_var;
extern TypeInfo* Encoding_t180559927_il2cpp_TypeInfo_var;
extern TypeInfo* BinaryReader_t2158806251_il2cpp_TypeInfo_var;
extern TypeInfo* BinaryFormatter_t341659722_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3402977152;
extern Il2CppCodeGenString* _stringLiteral1012463546;
extern const uint32_t ResourceReader__ctor_m4021287506_MetadataUsageId;
extern "C"  void ResourceReader__ctor_m4021287506 (ResourceReader_t4097835539 * __this, Stream_t219029575 * ___stream, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ResourceReader__ctor_m4021287506_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m1772956182(L_0, /*hidden argument*/NULL);
		__this->set_readerLock_1(L_0);
		Il2CppObject * L_1 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m1772956182(L_1, /*hidden argument*/NULL);
		__this->set_cache_lock_12(L_1);
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		Stream_t219029575 * L_2 = ___stream;
		if (L_2)
		{
			goto IL_002d;
		}
	}
	{
		ArgumentNullException_t3214793280 * L_3 = (ArgumentNullException_t3214793280 *)il2cpp_codegen_object_new(ArgumentNullException_t3214793280_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_3, _stringLiteral3402977152, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002d:
	{
		Stream_t219029575 * L_4 = ___stream;
		NullCheck(L_4);
		bool L_5 = VirtFuncInvoker0< bool >::Invoke(7 /* System.Boolean System.IO.Stream::get_CanRead() */, L_4);
		if (L_5)
		{
			goto IL_0043;
		}
	}
	{
		ArgumentException_t124305799 * L_6 = (ArgumentException_t124305799 *)il2cpp_codegen_object_new(ArgumentException_t124305799_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3544856547(L_6, _stringLiteral1012463546, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_6);
	}

IL_0043:
	{
		Stream_t219029575 * L_7 = ___stream;
		IL2CPP_RUNTIME_CLASS_INIT(Encoding_t180559927_il2cpp_TypeInfo_var);
		Encoding_t180559927 * L_8 = Encoding_get_UTF8_m619558519(NULL /*static, unused*/, /*hidden argument*/NULL);
		BinaryReader_t2158806251 * L_9 = (BinaryReader_t2158806251 *)il2cpp_codegen_object_new(BinaryReader_t2158806251_il2cpp_TypeInfo_var);
		BinaryReader__ctor_m3922058197(L_9, L_7, L_8, /*hidden argument*/NULL);
		__this->set_reader_0(L_9);
		StreamingContext_t986364934  L_10;
		memset(&L_10, 0, sizeof(L_10));
		StreamingContext__ctor_m165676124(&L_10, ((int32_t)12), /*hidden argument*/NULL);
		BinaryFormatter_t341659722 * L_11 = (BinaryFormatter_t341659722 *)il2cpp_codegen_object_new(BinaryFormatter_t341659722_il2cpp_TypeInfo_var);
		BinaryFormatter__ctor_m203421689(L_11, (Il2CppObject *)NULL, L_10, /*hidden argument*/NULL);
		__this->set_formatter_2(L_11);
		ResourceReader_ReadHeaders_m1907491305(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Resources.ResourceReader::.ctor(System.String)
extern TypeInfo* Il2CppObject_il2cpp_TypeInfo_var;
extern TypeInfo* FileStream_t1527309539_il2cpp_TypeInfo_var;
extern TypeInfo* BinaryReader_t2158806251_il2cpp_TypeInfo_var;
extern TypeInfo* BinaryFormatter_t341659722_il2cpp_TypeInfo_var;
extern const uint32_t ResourceReader__ctor_m3906862375_MetadataUsageId;
extern "C"  void ResourceReader__ctor_m3906862375 (ResourceReader_t4097835539 * __this, String_t* ___fileName, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ResourceReader__ctor_m3906862375_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m1772956182(L_0, /*hidden argument*/NULL);
		__this->set_readerLock_1(L_0);
		Il2CppObject * L_1 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m1772956182(L_1, /*hidden argument*/NULL);
		__this->set_cache_lock_12(L_1);
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		String_t* L_2 = ___fileName;
		FileStream_t1527309539 * L_3 = (FileStream_t1527309539 *)il2cpp_codegen_object_new(FileStream_t1527309539_il2cpp_TypeInfo_var);
		FileStream__ctor_m3657053030(L_3, L_2, 3, 1, 1, /*hidden argument*/NULL);
		BinaryReader_t2158806251 * L_4 = (BinaryReader_t2158806251 *)il2cpp_codegen_object_new(BinaryReader_t2158806251_il2cpp_TypeInfo_var);
		BinaryReader__ctor_m449904828(L_4, L_3, /*hidden argument*/NULL);
		__this->set_reader_0(L_4);
		StreamingContext_t986364934  L_5;
		memset(&L_5, 0, sizeof(L_5));
		StreamingContext__ctor_m165676124(&L_5, ((int32_t)12), /*hidden argument*/NULL);
		BinaryFormatter_t341659722 * L_6 = (BinaryFormatter_t341659722 *)il2cpp_codegen_object_new(BinaryFormatter_t341659722_il2cpp_TypeInfo_var);
		BinaryFormatter__ctor_m203421689(L_6, (Il2CppObject *)NULL, L_5, /*hidden argument*/NULL);
		__this->set_formatter_2(L_6);
		ResourceReader_ReadHeaders_m1907491305(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator System.Resources.ResourceReader::System.Collections.IEnumerable.GetEnumerator()
extern TypeInfo* IResourceReader_t2479967900_il2cpp_TypeInfo_var;
extern const uint32_t ResourceReader_System_Collections_IEnumerable_GetEnumerator_m117947704_MetadataUsageId;
extern "C"  Il2CppObject * ResourceReader_System_Collections_IEnumerable_GetEnumerator_m117947704 (ResourceReader_t4097835539 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ResourceReader_System_Collections_IEnumerable_GetEnumerator_m117947704_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(1 /* System.Collections.IDictionaryEnumerator System.Resources.IResourceReader::GetEnumerator() */, IResourceReader_t2479967900_il2cpp_TypeInfo_var, __this);
		return L_0;
	}
}
// System.Void System.Resources.ResourceReader::System.IDisposable.Dispose()
extern "C"  void ResourceReader_System_IDisposable_Dispose_m275527876 (ResourceReader_t4097835539 * __this, const MethodInfo* method)
{
	{
		ResourceReader_Dispose_m1411491503(__this, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Resources.ResourceReader::ReadHeaders()
extern const Il2CppType* ResourceSet_t3790468310_0_0_0_var;
extern TypeInfo* ResourceManager_t1361280801_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t2847414787_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentException_t124305799_il2cpp_TypeInfo_var;
extern TypeInfo* NotSupportedException_t1374155497_il2cpp_TypeInfo_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* StringU5BU5D_t2956870243_il2cpp_TypeInfo_var;
extern TypeInfo* Int32U5BU5D_t1809983122_il2cpp_TypeInfo_var;
extern TypeInfo* Int64U5BU5D_t753178071_il2cpp_TypeInfo_var;
extern TypeInfo* ResourceInfoU5BU5D_t4060746389_il2cpp_TypeInfo_var;
extern TypeInfo* EndOfStreamException_t3421483780_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1141609311;
extern Il2CppCodeGenString* _stringLiteral1407434841;
extern Il2CppCodeGenString* _stringLiteral2580308624;
extern Il2CppCodeGenString* _stringLiteral3972244580;
extern Il2CppCodeGenString* _stringLiteral2028707867;
extern Il2CppCodeGenString* _stringLiteral3661739352;
extern Il2CppCodeGenString* _stringLiteral78963;
extern Il2CppCodeGenString* _stringLiteral1800829387;
extern Il2CppCodeGenString* _stringLiteral995775297;
extern const uint32_t ResourceReader_ReadHeaders_m1907491305_MetadataUsageId;
extern "C"  void ResourceReader_ReadHeaders_m1907491305 (ResourceReader_t4097835539 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ResourceReader_ReadHeaders_m1907491305_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	String_t* V_3 = NULL;
	String_t* V_4 = NULL;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	int32_t V_7 = 0;
	int32_t V_8 = 0;
	uint8_t V_9 = 0x0;
	int32_t V_10 = 0;
	Int64U5BU5D_t753178071* V_11 = NULL;
	int32_t V_12 = 0;
	int64_t V_13 = 0;
	int32_t V_14 = 0;
	EndOfStreamException_t3421483780 * V_15 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			BinaryReader_t2158806251 * L_0 = __this->get_reader_0();
			NullCheck(L_0);
			int32_t L_1 = VirtFuncInvoker0< int32_t >::Invoke(19 /* System.Int32 System.IO.BinaryReader::ReadInt32() */, L_0);
			V_0 = L_1;
			int32_t L_2 = V_0;
			IL2CPP_RUNTIME_CLASS_INIT(ResourceManager_t1361280801_il2cpp_TypeInfo_var);
			int32_t L_3 = ((ResourceManager_t1361280801_StaticFields*)ResourceManager_t1361280801_il2cpp_TypeInfo_var->static_fields)->get_MagicNumber_3();
			if ((((int32_t)L_2) == ((int32_t)L_3)))
			{
				goto IL_002d;
			}
		}

IL_0017:
		{
			int32_t L_4 = V_0;
			int32_t L_5 = L_4;
			Il2CppObject * L_6 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_5);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_7 = String_Format_m2471250780(NULL /*static, unused*/, _stringLiteral1141609311, L_6, /*hidden argument*/NULL);
			ArgumentException_t124305799 * L_8 = (ArgumentException_t124305799 *)il2cpp_codegen_object_new(ArgumentException_t124305799_il2cpp_TypeInfo_var);
			ArgumentException__ctor_m3544856547(L_8, L_7, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_8);
		}

IL_002d:
		{
			BinaryReader_t2158806251 * L_9 = __this->get_reader_0();
			NullCheck(L_9);
			int32_t L_10 = VirtFuncInvoker0< int32_t >::Invoke(19 /* System.Int32 System.IO.BinaryReader::ReadInt32() */, L_9);
			V_1 = L_10;
			BinaryReader_t2158806251 * L_11 = __this->get_reader_0();
			NullCheck(L_11);
			int32_t L_12 = VirtFuncInvoker0< int32_t >::Invoke(19 /* System.Int32 System.IO.BinaryReader::ReadInt32() */, L_11);
			V_2 = L_12;
			int32_t L_13 = V_1;
			IL2CPP_RUNTIME_CLASS_INIT(ResourceManager_t1361280801_il2cpp_TypeInfo_var);
			int32_t L_14 = ((ResourceManager_t1361280801_StaticFields*)ResourceManager_t1361280801_il2cpp_TypeInfo_var->static_fields)->get_HeaderVersionNumber_2();
			if ((((int32_t)L_13) <= ((int32_t)L_14)))
			{
				goto IL_0069;
			}
		}

IL_0050:
		{
			BinaryReader_t2158806251 * L_15 = __this->get_reader_0();
			NullCheck(L_15);
			Stream_t219029575 * L_16 = VirtFuncInvoker0< Stream_t219029575 * >::Invoke(5 /* System.IO.Stream System.IO.BinaryReader::get_BaseStream() */, L_15);
			int32_t L_17 = V_2;
			NullCheck(L_16);
			VirtFuncInvoker2< int64_t, int64_t, int32_t >::Invoke(20 /* System.Int64 System.IO.Stream::Seek(System.Int64,System.IO.SeekOrigin) */, L_16, (((int64_t)((int64_t)L_17))), 1);
			goto IL_00e1;
		}

IL_0069:
		{
			BinaryReader_t2158806251 * L_18 = __this->get_reader_0();
			NullCheck(L_18);
			String_t* L_19 = VirtFuncInvoker0< String_t* >::Invoke(22 /* System.String System.IO.BinaryReader::ReadString() */, L_18);
			V_3 = L_19;
			String_t* L_20 = V_3;
			NullCheck(L_20);
			bool L_21 = String_StartsWith_m1500793453(L_20, _stringLiteral1407434841, /*hidden argument*/NULL);
			if (L_21)
			{
				goto IL_0096;
			}
		}

IL_0085:
		{
			String_t* L_22 = V_3;
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_23 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral2580308624, L_22, /*hidden argument*/NULL);
			NotSupportedException_t1374155497 * L_24 = (NotSupportedException_t1374155497 *)il2cpp_codegen_object_new(NotSupportedException_t1374155497_il2cpp_TypeInfo_var);
			NotSupportedException__ctor_m133757637(L_24, L_23, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_24);
		}

IL_0096:
		{
			BinaryReader_t2158806251 * L_25 = __this->get_reader_0();
			NullCheck(L_25);
			String_t* L_26 = VirtFuncInvoker0< String_t* >::Invoke(22 /* System.String System.IO.BinaryReader::ReadString() */, L_25);
			V_4 = L_26;
			String_t* L_27 = V_4;
			IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
			Type_t * L_28 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(ResourceSet_t3790468310_0_0_0_var), /*hidden argument*/NULL);
			NullCheck(L_28);
			String_t* L_29 = VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String System.Type::get_FullName() */, L_28);
			NullCheck(L_27);
			bool L_30 = String_StartsWith_m1500793453(L_27, L_29, /*hidden argument*/NULL);
			if (L_30)
			{
				goto IL_00e1;
			}
		}

IL_00be:
		{
			String_t* L_31 = V_4;
			NullCheck(L_31);
			bool L_32 = String_StartsWith_m1500793453(L_31, _stringLiteral3972244580, /*hidden argument*/NULL);
			if (L_32)
			{
				goto IL_00e1;
			}
		}

IL_00cf:
		{
			String_t* L_33 = V_4;
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_34 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral2028707867, L_33, /*hidden argument*/NULL);
			NotSupportedException_t1374155497 * L_35 = (NotSupportedException_t1374155497 *)il2cpp_codegen_object_new(NotSupportedException_t1374155497_il2cpp_TypeInfo_var);
			NotSupportedException__ctor_m133757637(L_35, L_34, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_35);
		}

IL_00e1:
		{
			BinaryReader_t2158806251 * L_36 = __this->get_reader_0();
			NullCheck(L_36);
			int32_t L_37 = VirtFuncInvoker0< int32_t >::Invoke(19 /* System.Int32 System.IO.BinaryReader::ReadInt32() */, L_36);
			__this->set_resource_ver_10(L_37);
			int32_t L_38 = __this->get_resource_ver_10();
			if ((((int32_t)L_38) == ((int32_t)1)))
			{
				goto IL_0125;
			}
		}

IL_00fe:
		{
			int32_t L_39 = __this->get_resource_ver_10();
			if ((((int32_t)L_39) == ((int32_t)2)))
			{
				goto IL_0125;
			}
		}

IL_010a:
		{
			int32_t* L_40 = __this->get_address_of_resource_ver_10();
			String_t* L_41 = Int32_ToString_m1286526384(L_40, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_42 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral3661739352, L_41, /*hidden argument*/NULL);
			NotSupportedException_t1374155497 * L_43 = (NotSupportedException_t1374155497 *)il2cpp_codegen_object_new(NotSupportedException_t1374155497_il2cpp_TypeInfo_var);
			NotSupportedException__ctor_m133757637(L_43, L_42, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_43);
		}

IL_0125:
		{
			BinaryReader_t2158806251 * L_44 = __this->get_reader_0();
			NullCheck(L_44);
			int32_t L_45 = VirtFuncInvoker0< int32_t >::Invoke(19 /* System.Int32 System.IO.BinaryReader::ReadInt32() */, L_44);
			__this->set_resourceCount_3(L_45);
			BinaryReader_t2158806251 * L_46 = __this->get_reader_0();
			NullCheck(L_46);
			int32_t L_47 = VirtFuncInvoker0< int32_t >::Invoke(19 /* System.Int32 System.IO.BinaryReader::ReadInt32() */, L_46);
			__this->set_typeCount_4(L_47);
			int32_t L_48 = __this->get_typeCount_4();
			__this->set_typeNames_5(((StringU5BU5D_t2956870243*)SZArrayNew(StringU5BU5D_t2956870243_il2cpp_TypeInfo_var, (uint32_t)L_48)));
			V_5 = 0;
			goto IL_017a;
		}

IL_0160:
		{
			StringU5BU5D_t2956870243* L_49 = __this->get_typeNames_5();
			int32_t L_50 = V_5;
			BinaryReader_t2158806251 * L_51 = __this->get_reader_0();
			NullCheck(L_51);
			String_t* L_52 = VirtFuncInvoker0< String_t* >::Invoke(22 /* System.String System.IO.BinaryReader::ReadString() */, L_51);
			NullCheck(L_49);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_49, L_50);
			ArrayElementTypeCheck (L_49, L_52);
			(L_49)->SetAt(static_cast<il2cpp_array_size_t>(L_50), (String_t*)L_52);
			int32_t L_53 = V_5;
			V_5 = ((int32_t)((int32_t)L_53+(int32_t)1));
		}

IL_017a:
		{
			int32_t L_54 = V_5;
			int32_t L_55 = __this->get_typeCount_4();
			if ((((int32_t)L_54) < ((int32_t)L_55)))
			{
				goto IL_0160;
			}
		}

IL_0187:
		{
			BinaryReader_t2158806251 * L_56 = __this->get_reader_0();
			NullCheck(L_56);
			Stream_t219029575 * L_57 = VirtFuncInvoker0< Stream_t219029575 * >::Invoke(5 /* System.IO.Stream System.IO.BinaryReader::get_BaseStream() */, L_56);
			NullCheck(L_57);
			int64_t L_58 = VirtFuncInvoker0< int64_t >::Invoke(11 /* System.Int64 System.IO.Stream::get_Position() */, L_57);
			V_6 = (((int32_t)((int32_t)((int64_t)((int64_t)L_58&(int64_t)(((int64_t)((int64_t)7))))))));
			V_7 = 0;
			int32_t L_59 = V_6;
			if (!L_59)
			{
				goto IL_01ad;
			}
		}

IL_01a7:
		{
			int32_t L_60 = V_6;
			V_7 = ((int32_t)((int32_t)8-(int32_t)L_60));
		}

IL_01ad:
		{
			V_8 = 0;
			goto IL_01e8;
		}

IL_01b5:
		{
			BinaryReader_t2158806251 * L_61 = __this->get_reader_0();
			NullCheck(L_61);
			uint8_t L_62 = VirtFuncInvoker0< uint8_t >::Invoke(13 /* System.Byte System.IO.BinaryReader::ReadByte() */, L_61);
			V_9 = L_62;
			uint8_t L_63 = V_9;
			int32_t L_64 = V_8;
			NullCheck(_stringLiteral78963);
			uint16_t L_65 = String_get_Chars_m3015341861(_stringLiteral78963, ((int32_t)((int32_t)L_64%(int32_t)3)), /*hidden argument*/NULL);
			if ((((int32_t)L_63) == ((int32_t)L_65)))
			{
				goto IL_01e2;
			}
		}

IL_01d7:
		{
			ArgumentException_t124305799 * L_66 = (ArgumentException_t124305799 *)il2cpp_codegen_object_new(ArgumentException_t124305799_il2cpp_TypeInfo_var);
			ArgumentException__ctor_m3544856547(L_66, _stringLiteral1800829387, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_66);
		}

IL_01e2:
		{
			int32_t L_67 = V_8;
			V_8 = ((int32_t)((int32_t)L_67+(int32_t)1));
		}

IL_01e8:
		{
			int32_t L_68 = V_8;
			int32_t L_69 = V_7;
			if ((((int32_t)L_68) < ((int32_t)L_69)))
			{
				goto IL_01b5;
			}
		}

IL_01f1:
		{
			int32_t L_70 = __this->get_resourceCount_3();
			__this->set_hashes_6(((Int32U5BU5D_t1809983122*)SZArrayNew(Int32U5BU5D_t1809983122_il2cpp_TypeInfo_var, (uint32_t)L_70)));
			V_10 = 0;
			goto IL_0224;
		}

IL_020a:
		{
			Int32U5BU5D_t1809983122* L_71 = __this->get_hashes_6();
			int32_t L_72 = V_10;
			BinaryReader_t2158806251 * L_73 = __this->get_reader_0();
			NullCheck(L_73);
			int32_t L_74 = VirtFuncInvoker0< int32_t >::Invoke(19 /* System.Int32 System.IO.BinaryReader::ReadInt32() */, L_73);
			NullCheck(L_71);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_71, L_72);
			(L_71)->SetAt(static_cast<il2cpp_array_size_t>(L_72), (int32_t)L_74);
			int32_t L_75 = V_10;
			V_10 = ((int32_t)((int32_t)L_75+(int32_t)1));
		}

IL_0224:
		{
			int32_t L_76 = V_10;
			int32_t L_77 = __this->get_resourceCount_3();
			if ((((int32_t)L_76) < ((int32_t)L_77)))
			{
				goto IL_020a;
			}
		}

IL_0231:
		{
			int32_t L_78 = __this->get_resourceCount_3();
			V_11 = ((Int64U5BU5D_t753178071*)SZArrayNew(Int64U5BU5D_t753178071_il2cpp_TypeInfo_var, (uint32_t)L_78));
			V_12 = 0;
			goto IL_025d;
		}

IL_0246:
		{
			Int64U5BU5D_t753178071* L_79 = V_11;
			int32_t L_80 = V_12;
			BinaryReader_t2158806251 * L_81 = __this->get_reader_0();
			NullCheck(L_81);
			int32_t L_82 = VirtFuncInvoker0< int32_t >::Invoke(19 /* System.Int32 System.IO.BinaryReader::ReadInt32() */, L_81);
			NullCheck(L_79);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_79, L_80);
			(L_79)->SetAt(static_cast<il2cpp_array_size_t>(L_80), (int64_t)(((int64_t)((int64_t)L_82))));
			int32_t L_83 = V_12;
			V_12 = ((int32_t)((int32_t)L_83+(int32_t)1));
		}

IL_025d:
		{
			int32_t L_84 = V_12;
			int32_t L_85 = __this->get_resourceCount_3();
			if ((((int32_t)L_84) < ((int32_t)L_85)))
			{
				goto IL_0246;
			}
		}

IL_026a:
		{
			BinaryReader_t2158806251 * L_86 = __this->get_reader_0();
			NullCheck(L_86);
			int32_t L_87 = VirtFuncInvoker0< int32_t >::Invoke(19 /* System.Int32 System.IO.BinaryReader::ReadInt32() */, L_86);
			__this->set_dataSectionOffset_8(L_87);
			BinaryReader_t2158806251 * L_88 = __this->get_reader_0();
			NullCheck(L_88);
			Stream_t219029575 * L_89 = VirtFuncInvoker0< Stream_t219029575 * >::Invoke(5 /* System.IO.Stream System.IO.BinaryReader::get_BaseStream() */, L_88);
			NullCheck(L_89);
			int64_t L_90 = VirtFuncInvoker0< int64_t >::Invoke(11 /* System.Int64 System.IO.Stream::get_Position() */, L_89);
			__this->set_nameSectionOffset_9(L_90);
			BinaryReader_t2158806251 * L_91 = __this->get_reader_0();
			NullCheck(L_91);
			Stream_t219029575 * L_92 = VirtFuncInvoker0< Stream_t219029575 * >::Invoke(5 /* System.IO.Stream System.IO.BinaryReader::get_BaseStream() */, L_91);
			NullCheck(L_92);
			int64_t L_93 = VirtFuncInvoker0< int64_t >::Invoke(11 /* System.Int64 System.IO.Stream::get_Position() */, L_92);
			V_13 = L_93;
			int32_t L_94 = __this->get_resourceCount_3();
			__this->set_infos_7(((ResourceInfoU5BU5D_t4060746389*)SZArrayNew(ResourceInfoU5BU5D_t4060746389_il2cpp_TypeInfo_var, (uint32_t)L_94)));
			V_14 = 0;
			goto IL_02da;
		}

IL_02bc:
		{
			Int64U5BU5D_t753178071* L_95 = V_11;
			int32_t L_96 = V_14;
			NullCheck(L_95);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_95, L_96);
			int32_t L_97 = L_96;
			ResourceInfoU5BU5D_t4060746389* L_98 = __this->get_infos_7();
			int32_t L_99 = V_14;
			NullCheck(L_98);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_98, L_99);
			ResourceReader_CreateResourceInfo_m5769771(__this, ((L_95)->GetAt(static_cast<il2cpp_array_size_t>(L_97))), ((L_98)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_99))), /*hidden argument*/NULL);
			int32_t L_100 = V_14;
			V_14 = ((int32_t)((int32_t)L_100+(int32_t)1));
		}

IL_02da:
		{
			int32_t L_101 = V_14;
			int32_t L_102 = __this->get_resourceCount_3();
			if ((((int32_t)L_101) < ((int32_t)L_102)))
			{
				goto IL_02bc;
			}
		}

IL_02e7:
		{
			BinaryReader_t2158806251 * L_103 = __this->get_reader_0();
			NullCheck(L_103);
			Stream_t219029575 * L_104 = VirtFuncInvoker0< Stream_t219029575 * >::Invoke(5 /* System.IO.Stream System.IO.BinaryReader::get_BaseStream() */, L_103);
			int64_t L_105 = V_13;
			NullCheck(L_104);
			VirtFuncInvoker2< int64_t, int64_t, int32_t >::Invoke(20 /* System.Int64 System.IO.Stream::Seek(System.Int64,System.IO.SeekOrigin) */, L_104, L_105, 0);
			V_11 = (Int64U5BU5D_t753178071*)NULL;
			goto IL_0317;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (EndOfStreamException_t3421483780_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0303;
		throw e;
	}

CATCH_0303:
	{ // begin catch(System.IO.EndOfStreamException)
		{
			V_15 = ((EndOfStreamException_t3421483780 *)__exception_local);
			EndOfStreamException_t3421483780 * L_106 = V_15;
			ArgumentException_t124305799 * L_107 = (ArgumentException_t124305799 *)il2cpp_codegen_object_new(ArgumentException_t124305799_il2cpp_TypeInfo_var);
			ArgumentException__ctor_m2711220147(L_107, _stringLiteral995775297, L_106, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_107);
		}

IL_0312:
		{
			goto IL_0317;
		}
	} // end catch (depth: 1)

IL_0317:
	{
		return;
	}
}
// System.Void System.Resources.ResourceReader::CreateResourceInfo(System.Int64,System.Resources.ResourceReader/ResourceInfo&)
extern TypeInfo* ByteU5BU5D_t58506160_il2cpp_TypeInfo_var;
extern TypeInfo* Encoding_t180559927_il2cpp_TypeInfo_var;
extern const uint32_t ResourceReader_CreateResourceInfo_m5769771_MetadataUsageId;
extern "C"  void ResourceReader_CreateResourceInfo_m5769771 (ResourceReader_t4097835539 * __this, int64_t ___position, ResourceInfo_t4074584572 * ___info, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ResourceReader_CreateResourceInfo_m5769771_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int64_t V_0 = 0;
	int32_t V_1 = 0;
	ByteU5BU5D_t58506160* V_2 = NULL;
	String_t* V_3 = NULL;
	int64_t V_4 = 0;
	int32_t V_5 = 0;
	{
		int64_t L_0 = ___position;
		int64_t L_1 = __this->get_nameSectionOffset_9();
		V_0 = ((int64_t)((int64_t)L_0+(int64_t)L_1));
		BinaryReader_t2158806251 * L_2 = __this->get_reader_0();
		NullCheck(L_2);
		Stream_t219029575 * L_3 = VirtFuncInvoker0< Stream_t219029575 * >::Invoke(5 /* System.IO.Stream System.IO.BinaryReader::get_BaseStream() */, L_2);
		int64_t L_4 = V_0;
		NullCheck(L_3);
		VirtFuncInvoker2< int64_t, int64_t, int32_t >::Invoke(20 /* System.Int64 System.IO.Stream::Seek(System.Int64,System.IO.SeekOrigin) */, L_3, L_4, 0);
		int32_t L_5 = ResourceReader_Read7BitEncodedInt_m1807673690(__this, /*hidden argument*/NULL);
		V_1 = L_5;
		int32_t L_6 = V_1;
		V_2 = ((ByteU5BU5D_t58506160*)SZArrayNew(ByteU5BU5D_t58506160_il2cpp_TypeInfo_var, (uint32_t)L_6));
		BinaryReader_t2158806251 * L_7 = __this->get_reader_0();
		ByteU5BU5D_t58506160* L_8 = V_2;
		int32_t L_9 = V_1;
		NullCheck(L_7);
		VirtFuncInvoker3< int32_t, ByteU5BU5D_t58506160*, int32_t, int32_t >::Invoke(10 /* System.Int32 System.IO.BinaryReader::Read(System.Byte[],System.Int32,System.Int32) */, L_7, L_8, 0, L_9);
		IL2CPP_RUNTIME_CLASS_INIT(Encoding_t180559927_il2cpp_TypeInfo_var);
		Encoding_t180559927 * L_10 = Encoding_get_Unicode_m2158134329(NULL /*static, unused*/, /*hidden argument*/NULL);
		ByteU5BU5D_t58506160* L_11 = V_2;
		NullCheck(L_10);
		String_t* L_12 = VirtFuncInvoker1< String_t*, ByteU5BU5D_t58506160* >::Invoke(22 /* System.String System.Text.Encoding::GetString(System.Byte[]) */, L_10, L_11);
		V_3 = L_12;
		BinaryReader_t2158806251 * L_13 = __this->get_reader_0();
		NullCheck(L_13);
		int32_t L_14 = VirtFuncInvoker0< int32_t >::Invoke(19 /* System.Int32 System.IO.BinaryReader::ReadInt32() */, L_13);
		int32_t L_15 = __this->get_dataSectionOffset_8();
		V_4 = (((int64_t)((int64_t)((int32_t)((int32_t)L_14+(int32_t)L_15)))));
		BinaryReader_t2158806251 * L_16 = __this->get_reader_0();
		NullCheck(L_16);
		Stream_t219029575 * L_17 = VirtFuncInvoker0< Stream_t219029575 * >::Invoke(5 /* System.IO.Stream System.IO.BinaryReader::get_BaseStream() */, L_16);
		int64_t L_18 = V_4;
		NullCheck(L_17);
		VirtFuncInvoker2< int64_t, int64_t, int32_t >::Invoke(20 /* System.Int64 System.IO.Stream::Seek(System.Int64,System.IO.SeekOrigin) */, L_17, L_18, 0);
		int32_t L_19 = ResourceReader_Read7BitEncodedInt_m1807673690(__this, /*hidden argument*/NULL);
		V_5 = L_19;
		ResourceInfo_t4074584572 * L_20 = ___info;
		String_t* L_21 = V_3;
		BinaryReader_t2158806251 * L_22 = __this->get_reader_0();
		NullCheck(L_22);
		Stream_t219029575 * L_23 = VirtFuncInvoker0< Stream_t219029575 * >::Invoke(5 /* System.IO.Stream System.IO.BinaryReader::get_BaseStream() */, L_22);
		NullCheck(L_23);
		int64_t L_24 = VirtFuncInvoker0< int64_t >::Invoke(11 /* System.Int64 System.IO.Stream::get_Position() */, L_23);
		int32_t L_25 = V_5;
		ResourceInfo__ctor_m4205157633(L_20, L_21, L_24, L_25, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 System.Resources.ResourceReader::Read7BitEncodedInt()
extern "C"  int32_t ResourceReader_Read7BitEncodedInt_m1807673690 (ResourceReader_t4097835539 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	uint8_t V_2 = 0x0;
	{
		V_0 = 0;
		V_1 = 0;
	}

IL_0004:
	{
		BinaryReader_t2158806251 * L_0 = __this->get_reader_0();
		NullCheck(L_0);
		uint8_t L_1 = VirtFuncInvoker0< uint8_t >::Invoke(13 /* System.Byte System.IO.BinaryReader::ReadByte() */, L_0);
		V_2 = L_1;
		int32_t L_2 = V_0;
		uint8_t L_3 = V_2;
		int32_t L_4 = V_1;
		V_0 = ((int32_t)((int32_t)L_2|(int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_3&(int32_t)((int32_t)127)))<<(int32_t)((int32_t)((int32_t)L_4&(int32_t)((int32_t)31)))))));
		int32_t L_5 = V_1;
		V_1 = ((int32_t)((int32_t)L_5+(int32_t)7));
		uint8_t L_6 = V_2;
		if ((((int32_t)((int32_t)((int32_t)L_6&(int32_t)((int32_t)128)))) == ((int32_t)((int32_t)128))))
		{
			goto IL_0004;
		}
	}
	{
		int32_t L_7 = V_0;
		return L_7;
	}
}
// System.Object System.Resources.ResourceReader::ReadValueVer2(System.Int32)
extern TypeInfo* Boolean_t211005341_il2cpp_TypeInfo_var;
extern TypeInfo* Char_t2778706699_il2cpp_TypeInfo_var;
extern TypeInfo* Byte_t2778693821_il2cpp_TypeInfo_var;
extern TypeInfo* SByte_t2855346064_il2cpp_TypeInfo_var;
extern TypeInfo* Int16_t2847414729_il2cpp_TypeInfo_var;
extern TypeInfo* UInt16_t985925268_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t2847414787_il2cpp_TypeInfo_var;
extern TypeInfo* UInt32_t985925326_il2cpp_TypeInfo_var;
extern TypeInfo* Int64_t2847414882_il2cpp_TypeInfo_var;
extern TypeInfo* UInt64_t985925421_il2cpp_TypeInfo_var;
extern TypeInfo* Single_t958209021_il2cpp_TypeInfo_var;
extern TypeInfo* Double_t534516614_il2cpp_TypeInfo_var;
extern TypeInfo* Decimal_t1688557254_il2cpp_TypeInfo_var;
extern TypeInfo* DateTime_t339033936_il2cpp_TypeInfo_var;
extern TypeInfo* TimeSpan_t763862892_il2cpp_TypeInfo_var;
extern TypeInfo* ByteU5BU5D_t58506160_il2cpp_TypeInfo_var;
extern TypeInfo* MemoryStream_t2881531048_il2cpp_TypeInfo_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t ResourceReader_ReadValueVer2_m3068653353_MetadataUsageId;
extern "C"  Il2CppObject * ResourceReader_ReadValueVer2_m3068653353 (ResourceReader_t4097835539 * __this, int32_t ___type_index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ResourceReader_ReadValueVer2_m3068653353_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ByteU5BU5D_t58506160* V_0 = NULL;
	int32_t V_1 = 0;
	{
		int32_t L_0 = ___type_index;
		V_1 = L_0;
		int32_t L_1 = V_1;
		if (L_1 == 0)
		{
			goto IL_0095;
		}
		if (L_1 == 1)
		{
			goto IL_0097;
		}
		if (L_1 == 2)
		{
			goto IL_00a3;
		}
		if (L_1 == 3)
		{
			goto IL_00b4;
		}
		if (L_1 == 4)
		{
			goto IL_00c5;
		}
		if (L_1 == 5)
		{
			goto IL_00d6;
		}
		if (L_1 == 6)
		{
			goto IL_00e7;
		}
		if (L_1 == 7)
		{
			goto IL_00f8;
		}
		if (L_1 == 8)
		{
			goto IL_0109;
		}
		if (L_1 == 9)
		{
			goto IL_011a;
		}
		if (L_1 == 10)
		{
			goto IL_012b;
		}
		if (L_1 == 11)
		{
			goto IL_013c;
		}
		if (L_1 == 12)
		{
			goto IL_014d;
		}
		if (L_1 == 13)
		{
			goto IL_015e;
		}
		if (L_1 == 14)
		{
			goto IL_016f;
		}
		if (L_1 == 15)
		{
			goto IL_0180;
		}
		if (L_1 == 16)
		{
			goto IL_0196;
		}
		if (L_1 == 17)
		{
			goto IL_01ed;
		}
		if (L_1 == 18)
		{
			goto IL_01ed;
		}
		if (L_1 == 19)
		{
			goto IL_01ed;
		}
		if (L_1 == 20)
		{
			goto IL_01ed;
		}
		if (L_1 == 21)
		{
			goto IL_01ed;
		}
		if (L_1 == 22)
		{
			goto IL_01ed;
		}
		if (L_1 == 23)
		{
			goto IL_01ed;
		}
		if (L_1 == 24)
		{
			goto IL_01ed;
		}
		if (L_1 == 25)
		{
			goto IL_01ed;
		}
		if (L_1 == 26)
		{
			goto IL_01ed;
		}
		if (L_1 == 27)
		{
			goto IL_01ed;
		}
		if (L_1 == 28)
		{
			goto IL_01ed;
		}
		if (L_1 == 29)
		{
			goto IL_01ed;
		}
		if (L_1 == 30)
		{
			goto IL_01ed;
		}
		if (L_1 == 31)
		{
			goto IL_01ed;
		}
		if (L_1 == 32)
		{
			goto IL_01ac;
		}
		if (L_1 == 33)
		{
			goto IL_01c3;
		}
	}
	{
		goto IL_01ed;
	}

IL_0095:
	{
		return NULL;
	}

IL_0097:
	{
		BinaryReader_t2158806251 * L_2 = __this->get_reader_0();
		NullCheck(L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(22 /* System.String System.IO.BinaryReader::ReadString() */, L_2);
		return L_3;
	}

IL_00a3:
	{
		BinaryReader_t2158806251 * L_4 = __this->get_reader_0();
		NullCheck(L_4);
		bool L_5 = VirtFuncInvoker0< bool >::Invoke(12 /* System.Boolean System.IO.BinaryReader::ReadBoolean() */, L_4);
		bool L_6 = L_5;
		Il2CppObject * L_7 = Box(Boolean_t211005341_il2cpp_TypeInfo_var, &L_6);
		return L_7;
	}

IL_00b4:
	{
		BinaryReader_t2158806251 * L_8 = __this->get_reader_0();
		NullCheck(L_8);
		uint16_t L_9 = VirtFuncInvoker0< uint16_t >::Invoke(24 /* System.UInt16 System.IO.BinaryReader::ReadUInt16() */, L_8);
		uint16_t L_10 = ((uint16_t)L_9);
		Il2CppObject * L_11 = Box(Char_t2778706699_il2cpp_TypeInfo_var, &L_10);
		return L_11;
	}

IL_00c5:
	{
		BinaryReader_t2158806251 * L_12 = __this->get_reader_0();
		NullCheck(L_12);
		uint8_t L_13 = VirtFuncInvoker0< uint8_t >::Invoke(13 /* System.Byte System.IO.BinaryReader::ReadByte() */, L_12);
		uint8_t L_14 = L_13;
		Il2CppObject * L_15 = Box(Byte_t2778693821_il2cpp_TypeInfo_var, &L_14);
		return L_15;
	}

IL_00d6:
	{
		BinaryReader_t2158806251 * L_16 = __this->get_reader_0();
		NullCheck(L_16);
		int8_t L_17 = VirtFuncInvoker0< int8_t >::Invoke(21 /* System.SByte System.IO.BinaryReader::ReadSByte() */, L_16);
		int8_t L_18 = L_17;
		Il2CppObject * L_19 = Box(SByte_t2855346064_il2cpp_TypeInfo_var, &L_18);
		return L_19;
	}

IL_00e7:
	{
		BinaryReader_t2158806251 * L_20 = __this->get_reader_0();
		NullCheck(L_20);
		int16_t L_21 = VirtFuncInvoker0< int16_t >::Invoke(18 /* System.Int16 System.IO.BinaryReader::ReadInt16() */, L_20);
		int16_t L_22 = L_21;
		Il2CppObject * L_23 = Box(Int16_t2847414729_il2cpp_TypeInfo_var, &L_22);
		return L_23;
	}

IL_00f8:
	{
		BinaryReader_t2158806251 * L_24 = __this->get_reader_0();
		NullCheck(L_24);
		uint16_t L_25 = VirtFuncInvoker0< uint16_t >::Invoke(24 /* System.UInt16 System.IO.BinaryReader::ReadUInt16() */, L_24);
		uint16_t L_26 = L_25;
		Il2CppObject * L_27 = Box(UInt16_t985925268_il2cpp_TypeInfo_var, &L_26);
		return L_27;
	}

IL_0109:
	{
		BinaryReader_t2158806251 * L_28 = __this->get_reader_0();
		NullCheck(L_28);
		int32_t L_29 = VirtFuncInvoker0< int32_t >::Invoke(19 /* System.Int32 System.IO.BinaryReader::ReadInt32() */, L_28);
		int32_t L_30 = L_29;
		Il2CppObject * L_31 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_30);
		return L_31;
	}

IL_011a:
	{
		BinaryReader_t2158806251 * L_32 = __this->get_reader_0();
		NullCheck(L_32);
		uint32_t L_33 = VirtFuncInvoker0< uint32_t >::Invoke(25 /* System.UInt32 System.IO.BinaryReader::ReadUInt32() */, L_32);
		uint32_t L_34 = L_33;
		Il2CppObject * L_35 = Box(UInt32_t985925326_il2cpp_TypeInfo_var, &L_34);
		return L_35;
	}

IL_012b:
	{
		BinaryReader_t2158806251 * L_36 = __this->get_reader_0();
		NullCheck(L_36);
		int64_t L_37 = VirtFuncInvoker0< int64_t >::Invoke(20 /* System.Int64 System.IO.BinaryReader::ReadInt64() */, L_36);
		int64_t L_38 = L_37;
		Il2CppObject * L_39 = Box(Int64_t2847414882_il2cpp_TypeInfo_var, &L_38);
		return L_39;
	}

IL_013c:
	{
		BinaryReader_t2158806251 * L_40 = __this->get_reader_0();
		NullCheck(L_40);
		uint64_t L_41 = VirtFuncInvoker0< uint64_t >::Invoke(26 /* System.UInt64 System.IO.BinaryReader::ReadUInt64() */, L_40);
		uint64_t L_42 = L_41;
		Il2CppObject * L_43 = Box(UInt64_t985925421_il2cpp_TypeInfo_var, &L_42);
		return L_43;
	}

IL_014d:
	{
		BinaryReader_t2158806251 * L_44 = __this->get_reader_0();
		NullCheck(L_44);
		float L_45 = VirtFuncInvoker0< float >::Invoke(23 /* System.Single System.IO.BinaryReader::ReadSingle() */, L_44);
		float L_46 = L_45;
		Il2CppObject * L_47 = Box(Single_t958209021_il2cpp_TypeInfo_var, &L_46);
		return L_47;
	}

IL_015e:
	{
		BinaryReader_t2158806251 * L_48 = __this->get_reader_0();
		NullCheck(L_48);
		double L_49 = VirtFuncInvoker0< double >::Invoke(17 /* System.Double System.IO.BinaryReader::ReadDouble() */, L_48);
		double L_50 = L_49;
		Il2CppObject * L_51 = Box(Double_t534516614_il2cpp_TypeInfo_var, &L_50);
		return L_51;
	}

IL_016f:
	{
		BinaryReader_t2158806251 * L_52 = __this->get_reader_0();
		NullCheck(L_52);
		Decimal_t1688557254  L_53 = VirtFuncInvoker0< Decimal_t1688557254  >::Invoke(16 /* System.Decimal System.IO.BinaryReader::ReadDecimal() */, L_52);
		Decimal_t1688557254  L_54 = L_53;
		Il2CppObject * L_55 = Box(Decimal_t1688557254_il2cpp_TypeInfo_var, &L_54);
		return L_55;
	}

IL_0180:
	{
		BinaryReader_t2158806251 * L_56 = __this->get_reader_0();
		NullCheck(L_56);
		int64_t L_57 = VirtFuncInvoker0< int64_t >::Invoke(20 /* System.Int64 System.IO.BinaryReader::ReadInt64() */, L_56);
		DateTime_t339033936  L_58;
		memset(&L_58, 0, sizeof(L_58));
		DateTime__ctor_m4059138700(&L_58, L_57, /*hidden argument*/NULL);
		DateTime_t339033936  L_59 = L_58;
		Il2CppObject * L_60 = Box(DateTime_t339033936_il2cpp_TypeInfo_var, &L_59);
		return L_60;
	}

IL_0196:
	{
		BinaryReader_t2158806251 * L_61 = __this->get_reader_0();
		NullCheck(L_61);
		int64_t L_62 = VirtFuncInvoker0< int64_t >::Invoke(20 /* System.Int64 System.IO.BinaryReader::ReadInt64() */, L_61);
		TimeSpan_t763862892  L_63;
		memset(&L_63, 0, sizeof(L_63));
		TimeSpan__ctor_m477860848(&L_63, L_62, /*hidden argument*/NULL);
		TimeSpan_t763862892  L_64 = L_63;
		Il2CppObject * L_65 = Box(TimeSpan_t763862892_il2cpp_TypeInfo_var, &L_64);
		return L_65;
	}

IL_01ac:
	{
		BinaryReader_t2158806251 * L_66 = __this->get_reader_0();
		BinaryReader_t2158806251 * L_67 = __this->get_reader_0();
		NullCheck(L_67);
		int32_t L_68 = VirtFuncInvoker0< int32_t >::Invoke(19 /* System.Int32 System.IO.BinaryReader::ReadInt32() */, L_67);
		NullCheck(L_66);
		ByteU5BU5D_t58506160* L_69 = VirtFuncInvoker1< ByteU5BU5D_t58506160*, int32_t >::Invoke(14 /* System.Byte[] System.IO.BinaryReader::ReadBytes(System.Int32) */, L_66, L_68);
		return (Il2CppObject *)L_69;
	}

IL_01c3:
	{
		BinaryReader_t2158806251 * L_70 = __this->get_reader_0();
		NullCheck(L_70);
		uint32_t L_71 = VirtFuncInvoker0< uint32_t >::Invoke(25 /* System.UInt32 System.IO.BinaryReader::ReadUInt32() */, L_70);
		V_0 = ((ByteU5BU5D_t58506160*)SZArrayNew(ByteU5BU5D_t58506160_il2cpp_TypeInfo_var, (uint32_t)(((uintptr_t)L_71))));
		BinaryReader_t2158806251 * L_72 = __this->get_reader_0();
		ByteU5BU5D_t58506160* L_73 = V_0;
		ByteU5BU5D_t58506160* L_74 = V_0;
		NullCheck(L_74);
		NullCheck(L_72);
		VirtFuncInvoker3< int32_t, ByteU5BU5D_t58506160*, int32_t, int32_t >::Invoke(10 /* System.Int32 System.IO.BinaryReader::Read(System.Byte[],System.Int32,System.Int32) */, L_72, L_73, 0, (((int32_t)((int32_t)(((Il2CppArray *)L_74)->max_length)))));
		ByteU5BU5D_t58506160* L_75 = V_0;
		MemoryStream_t2881531048 * L_76 = (MemoryStream_t2881531048 *)il2cpp_codegen_object_new(MemoryStream_t2881531048_il2cpp_TypeInfo_var);
		MemoryStream__ctor_m1231145921(L_76, L_75, /*hidden argument*/NULL);
		return L_76;
	}

IL_01ed:
	{
		int32_t L_77 = ___type_index;
		___type_index = ((int32_t)((int32_t)L_77-(int32_t)((int32_t)64)));
		StringU5BU5D_t2956870243* L_78 = __this->get_typeNames_5();
		int32_t L_79 = ___type_index;
		NullCheck(L_78);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_78, L_79);
		int32_t L_80 = L_79;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_81 = il2cpp_codegen_get_type((methodPointerType)&Type_GetType_m3430407454, ((L_78)->GetAt(static_cast<il2cpp_array_size_t>(L_80))), (bool)1, "mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089");
		Il2CppObject * L_82 = ResourceReader_ReadNonPredefinedValue_m3316818289(__this, L_81, /*hidden argument*/NULL);
		return L_82;
	}
}
// System.Object System.Resources.ResourceReader::ReadValueVer1(System.Type)
extern const Il2CppType* String_t_0_0_0_var;
extern const Il2CppType* Int32_t2847414787_0_0_0_var;
extern const Il2CppType* Byte_t2778693821_0_0_0_var;
extern const Il2CppType* Double_t534516614_0_0_0_var;
extern const Il2CppType* Int16_t2847414729_0_0_0_var;
extern const Il2CppType* Int64_t2847414882_0_0_0_var;
extern const Il2CppType* SByte_t2855346064_0_0_0_var;
extern const Il2CppType* Single_t958209021_0_0_0_var;
extern const Il2CppType* TimeSpan_t763862892_0_0_0_var;
extern const Il2CppType* UInt16_t985925268_0_0_0_var;
extern const Il2CppType* UInt32_t985925326_0_0_0_var;
extern const Il2CppType* UInt64_t985925421_0_0_0_var;
extern const Il2CppType* Decimal_t1688557254_0_0_0_var;
extern const Il2CppType* DateTime_t339033936_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t2847414787_il2cpp_TypeInfo_var;
extern TypeInfo* Byte_t2778693821_il2cpp_TypeInfo_var;
extern TypeInfo* Double_t534516614_il2cpp_TypeInfo_var;
extern TypeInfo* Int16_t2847414729_il2cpp_TypeInfo_var;
extern TypeInfo* Int64_t2847414882_il2cpp_TypeInfo_var;
extern TypeInfo* SByte_t2855346064_il2cpp_TypeInfo_var;
extern TypeInfo* Single_t958209021_il2cpp_TypeInfo_var;
extern TypeInfo* TimeSpan_t763862892_il2cpp_TypeInfo_var;
extern TypeInfo* UInt16_t985925268_il2cpp_TypeInfo_var;
extern TypeInfo* UInt32_t985925326_il2cpp_TypeInfo_var;
extern TypeInfo* UInt64_t985925421_il2cpp_TypeInfo_var;
extern TypeInfo* Decimal_t1688557254_il2cpp_TypeInfo_var;
extern TypeInfo* DateTime_t339033936_il2cpp_TypeInfo_var;
extern const uint32_t ResourceReader_ReadValueVer1_m60842242_MetadataUsageId;
extern "C"  Il2CppObject * ResourceReader_ReadValueVer1_m60842242 (ResourceReader_t4097835539 * __this, Type_t * ___type, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ResourceReader_ReadValueVer1_m60842242_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Type_t * L_0 = ___type;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(String_t_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_0) == ((Il2CppObject*)(Type_t *)L_1))))
		{
			goto IL_001c;
		}
	}
	{
		BinaryReader_t2158806251 * L_2 = __this->get_reader_0();
		NullCheck(L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(22 /* System.String System.IO.BinaryReader::ReadString() */, L_2);
		return L_3;
	}

IL_001c:
	{
		Type_t * L_4 = ___type;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_5 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Int32_t2847414787_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_4) == ((Il2CppObject*)(Type_t *)L_5))))
		{
			goto IL_003d;
		}
	}
	{
		BinaryReader_t2158806251 * L_6 = __this->get_reader_0();
		NullCheck(L_6);
		int32_t L_7 = VirtFuncInvoker0< int32_t >::Invoke(19 /* System.Int32 System.IO.BinaryReader::ReadInt32() */, L_6);
		int32_t L_8 = L_7;
		Il2CppObject * L_9 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_8);
		return L_9;
	}

IL_003d:
	{
		Type_t * L_10 = ___type;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_11 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Byte_t2778693821_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_10) == ((Il2CppObject*)(Type_t *)L_11))))
		{
			goto IL_005e;
		}
	}
	{
		BinaryReader_t2158806251 * L_12 = __this->get_reader_0();
		NullCheck(L_12);
		uint8_t L_13 = VirtFuncInvoker0< uint8_t >::Invoke(13 /* System.Byte System.IO.BinaryReader::ReadByte() */, L_12);
		uint8_t L_14 = L_13;
		Il2CppObject * L_15 = Box(Byte_t2778693821_il2cpp_TypeInfo_var, &L_14);
		return L_15;
	}

IL_005e:
	{
		Type_t * L_16 = ___type;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_17 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Double_t534516614_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_16) == ((Il2CppObject*)(Type_t *)L_17))))
		{
			goto IL_007f;
		}
	}
	{
		BinaryReader_t2158806251 * L_18 = __this->get_reader_0();
		NullCheck(L_18);
		double L_19 = VirtFuncInvoker0< double >::Invoke(17 /* System.Double System.IO.BinaryReader::ReadDouble() */, L_18);
		double L_20 = L_19;
		Il2CppObject * L_21 = Box(Double_t534516614_il2cpp_TypeInfo_var, &L_20);
		return L_21;
	}

IL_007f:
	{
		Type_t * L_22 = ___type;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_23 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Int16_t2847414729_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_22) == ((Il2CppObject*)(Type_t *)L_23))))
		{
			goto IL_00a0;
		}
	}
	{
		BinaryReader_t2158806251 * L_24 = __this->get_reader_0();
		NullCheck(L_24);
		int16_t L_25 = VirtFuncInvoker0< int16_t >::Invoke(18 /* System.Int16 System.IO.BinaryReader::ReadInt16() */, L_24);
		int16_t L_26 = L_25;
		Il2CppObject * L_27 = Box(Int16_t2847414729_il2cpp_TypeInfo_var, &L_26);
		return L_27;
	}

IL_00a0:
	{
		Type_t * L_28 = ___type;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_29 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Int64_t2847414882_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_28) == ((Il2CppObject*)(Type_t *)L_29))))
		{
			goto IL_00c1;
		}
	}
	{
		BinaryReader_t2158806251 * L_30 = __this->get_reader_0();
		NullCheck(L_30);
		int64_t L_31 = VirtFuncInvoker0< int64_t >::Invoke(20 /* System.Int64 System.IO.BinaryReader::ReadInt64() */, L_30);
		int64_t L_32 = L_31;
		Il2CppObject * L_33 = Box(Int64_t2847414882_il2cpp_TypeInfo_var, &L_32);
		return L_33;
	}

IL_00c1:
	{
		Type_t * L_34 = ___type;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_35 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(SByte_t2855346064_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_34) == ((Il2CppObject*)(Type_t *)L_35))))
		{
			goto IL_00e2;
		}
	}
	{
		BinaryReader_t2158806251 * L_36 = __this->get_reader_0();
		NullCheck(L_36);
		int8_t L_37 = VirtFuncInvoker0< int8_t >::Invoke(21 /* System.SByte System.IO.BinaryReader::ReadSByte() */, L_36);
		int8_t L_38 = L_37;
		Il2CppObject * L_39 = Box(SByte_t2855346064_il2cpp_TypeInfo_var, &L_38);
		return L_39;
	}

IL_00e2:
	{
		Type_t * L_40 = ___type;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_41 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Single_t958209021_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_40) == ((Il2CppObject*)(Type_t *)L_41))))
		{
			goto IL_0103;
		}
	}
	{
		BinaryReader_t2158806251 * L_42 = __this->get_reader_0();
		NullCheck(L_42);
		float L_43 = VirtFuncInvoker0< float >::Invoke(23 /* System.Single System.IO.BinaryReader::ReadSingle() */, L_42);
		float L_44 = L_43;
		Il2CppObject * L_45 = Box(Single_t958209021_il2cpp_TypeInfo_var, &L_44);
		return L_45;
	}

IL_0103:
	{
		Type_t * L_46 = ___type;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_47 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(TimeSpan_t763862892_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_46) == ((Il2CppObject*)(Type_t *)L_47))))
		{
			goto IL_0129;
		}
	}
	{
		BinaryReader_t2158806251 * L_48 = __this->get_reader_0();
		NullCheck(L_48);
		int64_t L_49 = VirtFuncInvoker0< int64_t >::Invoke(20 /* System.Int64 System.IO.BinaryReader::ReadInt64() */, L_48);
		TimeSpan_t763862892  L_50;
		memset(&L_50, 0, sizeof(L_50));
		TimeSpan__ctor_m477860848(&L_50, L_49, /*hidden argument*/NULL);
		TimeSpan_t763862892  L_51 = L_50;
		Il2CppObject * L_52 = Box(TimeSpan_t763862892_il2cpp_TypeInfo_var, &L_51);
		return L_52;
	}

IL_0129:
	{
		Type_t * L_53 = ___type;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_54 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(UInt16_t985925268_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_53) == ((Il2CppObject*)(Type_t *)L_54))))
		{
			goto IL_014a;
		}
	}
	{
		BinaryReader_t2158806251 * L_55 = __this->get_reader_0();
		NullCheck(L_55);
		uint16_t L_56 = VirtFuncInvoker0< uint16_t >::Invoke(24 /* System.UInt16 System.IO.BinaryReader::ReadUInt16() */, L_55);
		uint16_t L_57 = L_56;
		Il2CppObject * L_58 = Box(UInt16_t985925268_il2cpp_TypeInfo_var, &L_57);
		return L_58;
	}

IL_014a:
	{
		Type_t * L_59 = ___type;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_60 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(UInt32_t985925326_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_59) == ((Il2CppObject*)(Type_t *)L_60))))
		{
			goto IL_016b;
		}
	}
	{
		BinaryReader_t2158806251 * L_61 = __this->get_reader_0();
		NullCheck(L_61);
		uint32_t L_62 = VirtFuncInvoker0< uint32_t >::Invoke(25 /* System.UInt32 System.IO.BinaryReader::ReadUInt32() */, L_61);
		uint32_t L_63 = L_62;
		Il2CppObject * L_64 = Box(UInt32_t985925326_il2cpp_TypeInfo_var, &L_63);
		return L_64;
	}

IL_016b:
	{
		Type_t * L_65 = ___type;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_66 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(UInt64_t985925421_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_65) == ((Il2CppObject*)(Type_t *)L_66))))
		{
			goto IL_018c;
		}
	}
	{
		BinaryReader_t2158806251 * L_67 = __this->get_reader_0();
		NullCheck(L_67);
		uint64_t L_68 = VirtFuncInvoker0< uint64_t >::Invoke(26 /* System.UInt64 System.IO.BinaryReader::ReadUInt64() */, L_67);
		uint64_t L_69 = L_68;
		Il2CppObject * L_70 = Box(UInt64_t985925421_il2cpp_TypeInfo_var, &L_69);
		return L_70;
	}

IL_018c:
	{
		Type_t * L_71 = ___type;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_72 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Decimal_t1688557254_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_71) == ((Il2CppObject*)(Type_t *)L_72))))
		{
			goto IL_01ad;
		}
	}
	{
		BinaryReader_t2158806251 * L_73 = __this->get_reader_0();
		NullCheck(L_73);
		Decimal_t1688557254  L_74 = VirtFuncInvoker0< Decimal_t1688557254  >::Invoke(16 /* System.Decimal System.IO.BinaryReader::ReadDecimal() */, L_73);
		Decimal_t1688557254  L_75 = L_74;
		Il2CppObject * L_76 = Box(Decimal_t1688557254_il2cpp_TypeInfo_var, &L_75);
		return L_76;
	}

IL_01ad:
	{
		Type_t * L_77 = ___type;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_78 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(DateTime_t339033936_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_77) == ((Il2CppObject*)(Type_t *)L_78))))
		{
			goto IL_01d3;
		}
	}
	{
		BinaryReader_t2158806251 * L_79 = __this->get_reader_0();
		NullCheck(L_79);
		int64_t L_80 = VirtFuncInvoker0< int64_t >::Invoke(20 /* System.Int64 System.IO.BinaryReader::ReadInt64() */, L_79);
		DateTime_t339033936  L_81;
		memset(&L_81, 0, sizeof(L_81));
		DateTime__ctor_m4059138700(&L_81, L_80, /*hidden argument*/NULL);
		DateTime_t339033936  L_82 = L_81;
		Il2CppObject * L_83 = Box(DateTime_t339033936_il2cpp_TypeInfo_var, &L_82);
		return L_83;
	}

IL_01d3:
	{
		Type_t * L_84 = ___type;
		Il2CppObject * L_85 = ResourceReader_ReadNonPredefinedValue_m3316818289(__this, L_84, /*hidden argument*/NULL);
		return L_85;
	}
}
// System.Object System.Resources.ResourceReader::ReadNonPredefinedValue(System.Type)
extern TypeInfo* IFormatter_t2688329594_il2cpp_TypeInfo_var;
extern TypeInfo* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2997114813;
extern const uint32_t ResourceReader_ReadNonPredefinedValue_m3316818289_MetadataUsageId;
extern "C"  Il2CppObject * ResourceReader_ReadNonPredefinedValue_m3316818289 (ResourceReader_t4097835539 * __this, Type_t * ___exp_type, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ResourceReader_ReadNonPredefinedValue_m3316818289_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		Il2CppObject * L_0 = __this->get_formatter_2();
		BinaryReader_t2158806251 * L_1 = __this->get_reader_0();
		NullCheck(L_1);
		Stream_t219029575 * L_2 = VirtFuncInvoker0< Stream_t219029575 * >::Invoke(5 /* System.IO.Stream System.IO.BinaryReader::get_BaseStream() */, L_1);
		NullCheck(L_0);
		Il2CppObject * L_3 = InterfaceFuncInvoker1< Il2CppObject *, Stream_t219029575 * >::Invoke(0 /* System.Object System.Runtime.Serialization.IFormatter::Deserialize(System.IO.Stream) */, IFormatter_t2688329594_il2cpp_TypeInfo_var, L_0, L_2);
		V_0 = L_3;
		Il2CppObject * L_4 = V_0;
		NullCheck(L_4);
		Type_t * L_5 = Object_GetType_m2022236990(L_4, /*hidden argument*/NULL);
		Type_t * L_6 = ___exp_type;
		if ((((Il2CppObject*)(Type_t *)L_5) == ((Il2CppObject*)(Type_t *)L_6)))
		{
			goto IL_002e;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_7 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_7, _stringLiteral2997114813, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_7);
	}

IL_002e:
	{
		Il2CppObject * L_8 = V_0;
		return L_8;
	}
}
// System.Void System.Resources.ResourceReader::LoadResourceValues(System.Resources.ResourceReader/ResourceCacheItem[])
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t ResourceReader_LoadResourceValues_m1190122720_MetadataUsageId;
extern "C"  void ResourceReader_LoadResourceValues_m1190122720 (ResourceReader_t4097835539 * __this, ResourceCacheItemU5BU5D_t2722755166* ___store, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ResourceReader_LoadResourceValues_m1190122720_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ResourceInfo_t4074584572  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Il2CppObject * V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	int32_t V_3 = 0;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = __this->get_readerLock_1();
		V_2 = L_0;
		Il2CppObject * L_1 = V_2;
		Monitor_Enter_m476686225(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			V_3 = 0;
			goto IL_00c1;
		}

IL_0014:
		{
			ResourceInfoU5BU5D_t4060746389* L_2 = __this->get_infos_7();
			int32_t L_3 = V_3;
			NullCheck(L_2);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
			V_0 = (*(ResourceInfo_t4074584572 *)((L_2)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_3))));
			int32_t L_4 = (&V_0)->get_TypeIndex_2();
			if ((!(((uint32_t)L_4) == ((uint32_t)(-1)))))
			{
				goto IL_0051;
			}
		}

IL_0033:
		{
			ResourceCacheItemU5BU5D_t2722755166* L_5 = ___store;
			int32_t L_6 = V_3;
			NullCheck(L_5);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
			String_t* L_7 = (&V_0)->get_ResourceName_1();
			ResourceCacheItem_t3699857703  L_8;
			memset(&L_8, 0, sizeof(L_8));
			ResourceCacheItem__ctor_m3336742541(&L_8, L_7, NULL, /*hidden argument*/NULL);
			(*(ResourceCacheItem_t3699857703 *)((L_5)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_6)))) = L_8;
			goto IL_00bd;
		}

IL_0051:
		{
			BinaryReader_t2158806251 * L_9 = __this->get_reader_0();
			NullCheck(L_9);
			Stream_t219029575 * L_10 = VirtFuncInvoker0< Stream_t219029575 * >::Invoke(5 /* System.IO.Stream System.IO.BinaryReader::get_BaseStream() */, L_9);
			int64_t L_11 = (&V_0)->get_ValuePosition_0();
			NullCheck(L_10);
			VirtFuncInvoker2< int64_t, int64_t, int32_t >::Invoke(20 /* System.Int64 System.IO.Stream::Seek(System.Int64,System.IO.SeekOrigin) */, L_10, L_11, 0);
			int32_t L_12 = __this->get_resource_ver_10();
			if ((!(((uint32_t)L_12) == ((uint32_t)2))))
			{
				goto IL_0089;
			}
		}

IL_0076:
		{
			int32_t L_13 = (&V_0)->get_TypeIndex_2();
			Il2CppObject * L_14 = ResourceReader_ReadValueVer2_m3068653353(__this, L_13, /*hidden argument*/NULL);
			V_1 = L_14;
			goto IL_00a4;
		}

IL_0089:
		{
			StringU5BU5D_t2956870243* L_15 = __this->get_typeNames_5();
			int32_t L_16 = (&V_0)->get_TypeIndex_2();
			NullCheck(L_15);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_15, L_16);
			int32_t L_17 = L_16;
			IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
			Type_t * L_18 = il2cpp_codegen_get_type((methodPointerType)&Type_GetType_m3430407454, ((L_15)->GetAt(static_cast<il2cpp_array_size_t>(L_17))), (bool)1, "mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089");
			Il2CppObject * L_19 = ResourceReader_ReadValueVer1_m60842242(__this, L_18, /*hidden argument*/NULL);
			V_1 = L_19;
		}

IL_00a4:
		{
			ResourceCacheItemU5BU5D_t2722755166* L_20 = ___store;
			int32_t L_21 = V_3;
			NullCheck(L_20);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_20, L_21);
			String_t* L_22 = (&V_0)->get_ResourceName_1();
			Il2CppObject * L_23 = V_1;
			ResourceCacheItem_t3699857703  L_24;
			memset(&L_24, 0, sizeof(L_24));
			ResourceCacheItem__ctor_m3336742541(&L_24, L_22, L_23, /*hidden argument*/NULL);
			(*(ResourceCacheItem_t3699857703 *)((L_20)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_21)))) = L_24;
		}

IL_00bd:
		{
			int32_t L_25 = V_3;
			V_3 = ((int32_t)((int32_t)L_25+(int32_t)1));
		}

IL_00c1:
		{
			int32_t L_26 = V_3;
			int32_t L_27 = __this->get_resourceCount_3();
			if ((((int32_t)L_26) < ((int32_t)L_27)))
			{
				goto IL_0014;
			}
		}

IL_00cd:
		{
			IL2CPP_LEAVE(0xD9, FINALLY_00d2);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_00d2;
	}

FINALLY_00d2:
	{ // begin finally (depth: 1)
		Il2CppObject * L_28 = V_2;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, L_28, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(210)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(210)
	{
		IL2CPP_JUMP_TBL(0xD9, IL_00d9)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_00d9:
	{
		return;
	}
}
// System.Void System.Resources.ResourceReader::Close()
extern "C"  void ResourceReader_Close_m551956177 (ResourceReader_t4097835539 * __this, const MethodInfo* method)
{
	{
		ResourceReader_Dispose_m1411491503(__this, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IDictionaryEnumerator System.Resources.ResourceReader::GetEnumerator()
extern TypeInfo* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern TypeInfo* ResourceEnumerator_t2097019538_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2794314811;
extern const uint32_t ResourceReader_GetEnumerator_m1820124275_MetadataUsageId;
extern "C"  Il2CppObject * ResourceReader_GetEnumerator_m1820124275 (ResourceReader_t4097835539 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ResourceReader_GetEnumerator_m1820124275_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		BinaryReader_t2158806251 * L_0 = __this->get_reader_0();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, _stringLiteral2794314811, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		ResourceEnumerator_t2097019538 * L_2 = (ResourceEnumerator_t2097019538 *)il2cpp_codegen_object_new(ResourceEnumerator_t2097019538_il2cpp_TypeInfo_var);
		ResourceEnumerator__ctor_m3994194575(L_2, __this, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void System.Resources.ResourceReader::Dispose(System.Boolean)
extern "C"  void ResourceReader_Dispose_m1411491503 (ResourceReader_t4097835539 * __this, bool ___disposing, const MethodInfo* method)
{
	{
		bool L_0 = ___disposing;
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		BinaryReader_t2158806251 * L_1 = __this->get_reader_0();
		if (!L_1)
		{
			goto IL_001c;
		}
	}
	{
		BinaryReader_t2158806251 * L_2 = __this->get_reader_0();
		NullCheck(L_2);
		VirtActionInvoker0::Invoke(6 /* System.Void System.IO.BinaryReader::Close() */, L_2);
	}

IL_001c:
	{
		__this->set_reader_0((BinaryReader_t2158806251 *)NULL);
		__this->set_hashes_6((Int32U5BU5D_t1809983122*)NULL);
		__this->set_infos_7((ResourceInfoU5BU5D_t4060746389*)NULL);
		__this->set_typeNames_5((StringU5BU5D_t2956870243*)NULL);
		__this->set_cache_11((ResourceCacheItemU5BU5D_t2722755166*)NULL);
		return;
	}
}
// System.Void System.Resources.ResourceReader/ResourceCacheItem::.ctor(System.String,System.Object)
extern "C"  void ResourceCacheItem__ctor_m3336742541 (ResourceCacheItem_t3699857703 * __this, String_t* ___name, Il2CppObject * ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___name;
		__this->set_ResourceName_0(L_0);
		Il2CppObject * L_1 = ___value;
		__this->set_ResourceValue_1(L_1);
		return;
	}
}
// Conversion methods for marshalling of: System.Resources.ResourceReader/ResourceCacheItem
extern "C" void ResourceCacheItem_t3699857703_marshal_com(const ResourceCacheItem_t3699857703& unmarshaled, ResourceCacheItem_t3699857703_marshaled_com& marshaled)
{
	marshaled.___ResourceName_0 = il2cpp_codegen_marshal_bstring(unmarshaled.get_ResourceName_0());
	if (!(unmarshaled.get_ResourceValue_1()))
	{
		marshaled.___ResourceValue_1 = NULL;
	}
	else if ((unmarshaled.get_ResourceValue_1())->klass->is_import) 
	{
		marshaled.___ResourceValue_1 = (Il2CppIUnknown*)il2cpp_codegen_com_query_interface((Il2CppRCW*)(unmarshaled.get_ResourceValue_1()), Il2CppIUnknown::IID);
	}
	else
	{
		il2cpp_codegen_com_raise_exception_if_failed(IL2CPP_E_NOTIMPL);
	}
}
extern "C" void ResourceCacheItem_t3699857703_marshal_com_back(const ResourceCacheItem_t3699857703_marshaled_com& marshaled, ResourceCacheItem_t3699857703& unmarshaled)
{
	unmarshaled.set_ResourceName_0(il2cpp_codegen_marshal_bstring_result(marshaled.___ResourceName_0));
	unmarshaled.set_ResourceValue_1(il2cpp_codegen_com_create_rcw(marshaled.___ResourceValue_1));
}
// Conversion method for clean up from marshalling of: System.Resources.ResourceReader/ResourceCacheItem
extern "C" void ResourceCacheItem_t3699857703_marshal_com_cleanup(ResourceCacheItem_t3699857703_marshaled_com& marshaled)
{
	il2cpp_codegen_marshal_free_bstring(marshaled.___ResourceName_0);
	marshaled.___ResourceName_0 = NULL;
	if (marshaled.___ResourceValue_1)
	{
		(marshaled.___ResourceValue_1)->Release();
		(marshaled.___ResourceValue_1) = NULL;
	}
}
// System.Void System.Resources.ResourceReader/ResourceEnumerator::.ctor(System.Resources.ResourceReader)
extern "C"  void ResourceEnumerator__ctor_m3994194575 (ResourceEnumerator_t2097019538 * __this, ResourceReader_t4097835539 * ___readerToEnumerate, const MethodInfo* method)
{
	{
		__this->set_index_1((-1));
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		ResourceReader_t4097835539 * L_0 = ___readerToEnumerate;
		__this->set_reader_0(L_0);
		ResourceEnumerator_FillCache_m1751484935(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.DictionaryEntry System.Resources.ResourceReader/ResourceEnumerator::get_Entry()
extern TypeInfo* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2794314811;
extern Il2CppCodeGenString* _stringLiteral589928175;
extern const uint32_t ResourceEnumerator_get_Entry_m715318570_MetadataUsageId;
extern "C"  DictionaryEntry_t130027246  ResourceEnumerator_get_Entry_m715318570 (ResourceEnumerator_t2097019538 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ResourceEnumerator_get_Entry_m715318570_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ResourceReader_t4097835539 * L_0 = __this->get_reader_0();
		NullCheck(L_0);
		BinaryReader_t2158806251 * L_1 = L_0->get_reader_0();
		if (L_1)
		{
			goto IL_001b;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_2 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_2, _stringLiteral2794314811, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_001b:
	{
		int32_t L_3 = __this->get_index_1();
		if ((((int32_t)L_3) >= ((int32_t)0)))
		{
			goto IL_0032;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_4 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_4, _stringLiteral589928175, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4);
	}

IL_0032:
	{
		Il2CppObject * L_5 = ResourceEnumerator_get_Key_m424542697(__this, /*hidden argument*/NULL);
		Il2CppObject * L_6 = ResourceEnumerator_get_Value_m1012075067(__this, /*hidden argument*/NULL);
		DictionaryEntry_t130027246  L_7;
		memset(&L_7, 0, sizeof(L_7));
		DictionaryEntry__ctor_m2600671860(&L_7, L_5, L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
// System.Object System.Resources.ResourceReader/ResourceEnumerator::get_Key()
extern TypeInfo* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2794314811;
extern Il2CppCodeGenString* _stringLiteral589928175;
extern const uint32_t ResourceEnumerator_get_Key_m424542697_MetadataUsageId;
extern "C"  Il2CppObject * ResourceEnumerator_get_Key_m424542697 (ResourceEnumerator_t2097019538 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ResourceEnumerator_get_Key_m424542697_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ResourceReader_t4097835539 * L_0 = __this->get_reader_0();
		NullCheck(L_0);
		BinaryReader_t2158806251 * L_1 = L_0->get_reader_0();
		if (L_1)
		{
			goto IL_001b;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_2 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_2, _stringLiteral2794314811, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_001b:
	{
		int32_t L_3 = __this->get_index_1();
		if ((((int32_t)L_3) >= ((int32_t)0)))
		{
			goto IL_0032;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_4 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_4, _stringLiteral589928175, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4);
	}

IL_0032:
	{
		ResourceReader_t4097835539 * L_5 = __this->get_reader_0();
		NullCheck(L_5);
		ResourceCacheItemU5BU5D_t2722755166* L_6 = L_5->get_cache_11();
		int32_t L_7 = __this->get_index_1();
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, L_7);
		String_t* L_8 = ((L_6)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_7)))->get_ResourceName_0();
		return L_8;
	}
}
// System.Object System.Resources.ResourceReader/ResourceEnumerator::get_Value()
extern TypeInfo* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2794314811;
extern Il2CppCodeGenString* _stringLiteral589928175;
extern const uint32_t ResourceEnumerator_get_Value_m1012075067_MetadataUsageId;
extern "C"  Il2CppObject * ResourceEnumerator_get_Value_m1012075067 (ResourceEnumerator_t2097019538 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ResourceEnumerator_get_Value_m1012075067_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ResourceReader_t4097835539 * L_0 = __this->get_reader_0();
		NullCheck(L_0);
		BinaryReader_t2158806251 * L_1 = L_0->get_reader_0();
		if (L_1)
		{
			goto IL_001b;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_2 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_2, _stringLiteral2794314811, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_001b:
	{
		int32_t L_3 = __this->get_index_1();
		if ((((int32_t)L_3) >= ((int32_t)0)))
		{
			goto IL_0032;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_4 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_4, _stringLiteral589928175, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4);
	}

IL_0032:
	{
		ResourceReader_t4097835539 * L_5 = __this->get_reader_0();
		NullCheck(L_5);
		ResourceCacheItemU5BU5D_t2722755166* L_6 = L_5->get_cache_11();
		int32_t L_7 = __this->get_index_1();
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, L_7);
		Il2CppObject * L_8 = ((L_6)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_7)))->get_ResourceValue_1();
		return L_8;
	}
}
// System.Object System.Resources.ResourceReader/ResourceEnumerator::get_Current()
extern TypeInfo* DictionaryEntry_t130027246_il2cpp_TypeInfo_var;
extern const uint32_t ResourceEnumerator_get_Current_m3366221315_MetadataUsageId;
extern "C"  Il2CppObject * ResourceEnumerator_get_Current_m3366221315 (ResourceEnumerator_t2097019538 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ResourceEnumerator_get_Current_m3366221315_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		DictionaryEntry_t130027246  L_0 = ResourceEnumerator_get_Entry_m715318570(__this, /*hidden argument*/NULL);
		DictionaryEntry_t130027246  L_1 = L_0;
		Il2CppObject * L_2 = Box(DictionaryEntry_t130027246_il2cpp_TypeInfo_var, &L_1);
		return L_2;
	}
}
// System.Boolean System.Resources.ResourceReader/ResourceEnumerator::MoveNext()
extern TypeInfo* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2794314811;
extern const uint32_t ResourceEnumerator_MoveNext_m3916307456_MetadataUsageId;
extern "C"  bool ResourceEnumerator_MoveNext_m3916307456 (ResourceEnumerator_t2097019538 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ResourceEnumerator_MoveNext_m3916307456_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		ResourceReader_t4097835539 * L_0 = __this->get_reader_0();
		NullCheck(L_0);
		BinaryReader_t2158806251 * L_1 = L_0->get_reader_0();
		if (L_1)
		{
			goto IL_001b;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_2 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_2, _stringLiteral2794314811, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_001b:
	{
		bool L_3 = __this->get_finished_2();
		if (!L_3)
		{
			goto IL_0028;
		}
	}
	{
		return (bool)0;
	}

IL_0028:
	{
		int32_t L_4 = __this->get_index_1();
		int32_t L_5 = ((int32_t)((int32_t)L_4+(int32_t)1));
		V_0 = L_5;
		__this->set_index_1(L_5);
		int32_t L_6 = V_0;
		ResourceReader_t4097835539 * L_7 = __this->get_reader_0();
		NullCheck(L_7);
		int32_t L_8 = L_7->get_resourceCount_3();
		if ((((int32_t)L_6) >= ((int32_t)L_8)))
		{
			goto IL_004b;
		}
	}
	{
		return (bool)1;
	}

IL_004b:
	{
		__this->set_finished_2((bool)1);
		return (bool)0;
	}
}
// System.Void System.Resources.ResourceReader/ResourceEnumerator::Reset()
extern TypeInfo* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2794314811;
extern const uint32_t ResourceEnumerator_Reset_m4126890775_MetadataUsageId;
extern "C"  void ResourceEnumerator_Reset_m4126890775 (ResourceEnumerator_t2097019538 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ResourceEnumerator_Reset_m4126890775_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ResourceReader_t4097835539 * L_0 = __this->get_reader_0();
		NullCheck(L_0);
		BinaryReader_t2158806251 * L_1 = L_0->get_reader_0();
		if (L_1)
		{
			goto IL_001b;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_2 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_2, _stringLiteral2794314811, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_001b:
	{
		__this->set_index_1((-1));
		__this->set_finished_2((bool)0);
		return;
	}
}
// System.Void System.Resources.ResourceReader/ResourceEnumerator::FillCache()
extern TypeInfo* ResourceCacheItemU5BU5D_t2722755166_il2cpp_TypeInfo_var;
extern const uint32_t ResourceEnumerator_FillCache_m1751484935_MetadataUsageId;
extern "C"  void ResourceEnumerator_FillCache_m1751484935 (ResourceEnumerator_t2097019538 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ResourceEnumerator_FillCache_m1751484935_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	ResourceCacheItemU5BU5D_t2722755166* V_1 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		ResourceReader_t4097835539 * L_0 = __this->get_reader_0();
		NullCheck(L_0);
		ResourceCacheItemU5BU5D_t2722755166* L_1 = L_0->get_cache_11();
		if (!L_1)
		{
			goto IL_0011;
		}
	}
	{
		return;
	}

IL_0011:
	{
		ResourceReader_t4097835539 * L_2 = __this->get_reader_0();
		NullCheck(L_2);
		Il2CppObject * L_3 = L_2->get_cache_lock_12();
		V_0 = L_3;
		Il2CppObject * L_4 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
	}

IL_0023:
	try
	{ // begin try (depth: 1)
		{
			ResourceReader_t4097835539 * L_5 = __this->get_reader_0();
			NullCheck(L_5);
			ResourceCacheItemU5BU5D_t2722755166* L_6 = L_5->get_cache_11();
			if (!L_6)
			{
				goto IL_0038;
			}
		}

IL_0033:
		{
			IL2CPP_LEAVE(0x6D, FINALLY_0066);
		}

IL_0038:
		{
			ResourceReader_t4097835539 * L_7 = __this->get_reader_0();
			NullCheck(L_7);
			int32_t L_8 = L_7->get_resourceCount_3();
			V_1 = ((ResourceCacheItemU5BU5D_t2722755166*)SZArrayNew(ResourceCacheItemU5BU5D_t2722755166_il2cpp_TypeInfo_var, (uint32_t)L_8));
			ResourceReader_t4097835539 * L_9 = __this->get_reader_0();
			ResourceCacheItemU5BU5D_t2722755166* L_10 = V_1;
			NullCheck(L_9);
			ResourceReader_LoadResourceValues_m1190122720(L_9, L_10, /*hidden argument*/NULL);
			ResourceReader_t4097835539 * L_11 = __this->get_reader_0();
			ResourceCacheItemU5BU5D_t2722755166* L_12 = V_1;
			NullCheck(L_11);
			L_11->set_cache_11(L_12);
			IL2CPP_LEAVE(0x6D, FINALLY_0066);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0066;
	}

FINALLY_0066:
	{ // begin finally (depth: 1)
		Il2CppObject * L_13 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(102)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(102)
	{
		IL2CPP_JUMP_TBL(0x6D, IL_006d)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_006d:
	{
		return;
	}
}
// System.Void System.Resources.ResourceReader/ResourceInfo::.ctor(System.String,System.Int64,System.Int32)
extern "C"  void ResourceInfo__ctor_m4205157633 (ResourceInfo_t4074584572 * __this, String_t* ___resourceName, int64_t ___valuePosition, int32_t ___type_index, const MethodInfo* method)
{
	{
		int64_t L_0 = ___valuePosition;
		__this->set_ValuePosition_0(L_0);
		String_t* L_1 = ___resourceName;
		__this->set_ResourceName_1(L_1);
		int32_t L_2 = ___type_index;
		__this->set_TypeIndex_2(L_2);
		return;
	}
}
// Conversion methods for marshalling of: System.Resources.ResourceReader/ResourceInfo
extern "C" void ResourceInfo_t4074584572_marshal_pinvoke(const ResourceInfo_t4074584572& unmarshaled, ResourceInfo_t4074584572_marshaled_pinvoke& marshaled)
{
	marshaled.___ValuePosition_0 = unmarshaled.get_ValuePosition_0();
	marshaled.___ResourceName_1 = il2cpp_codegen_marshal_string(unmarshaled.get_ResourceName_1());
	marshaled.___TypeIndex_2 = unmarshaled.get_TypeIndex_2();
}
extern "C" void ResourceInfo_t4074584572_marshal_pinvoke_back(const ResourceInfo_t4074584572_marshaled_pinvoke& marshaled, ResourceInfo_t4074584572& unmarshaled)
{
	int64_t unmarshaled_ValuePosition_temp = 0;
	unmarshaled_ValuePosition_temp = marshaled.___ValuePosition_0;
	unmarshaled.set_ValuePosition_0(unmarshaled_ValuePosition_temp);
	unmarshaled.set_ResourceName_1(il2cpp_codegen_marshal_string_result(marshaled.___ResourceName_1));
	int32_t unmarshaled_TypeIndex_temp = 0;
	unmarshaled_TypeIndex_temp = marshaled.___TypeIndex_2;
	unmarshaled.set_TypeIndex_2(unmarshaled_TypeIndex_temp);
}
// Conversion method for clean up from marshalling of: System.Resources.ResourceReader/ResourceInfo
extern "C" void ResourceInfo_t4074584572_marshal_pinvoke_cleanup(ResourceInfo_t4074584572_marshaled_pinvoke& marshaled)
{
	il2cpp_codegen_marshal_free(marshaled.___ResourceName_1);
	marshaled.___ResourceName_1 = NULL;
}
// Conversion methods for marshalling of: System.Resources.ResourceReader/ResourceInfo
extern "C" void ResourceInfo_t4074584572_marshal_com(const ResourceInfo_t4074584572& unmarshaled, ResourceInfo_t4074584572_marshaled_com& marshaled)
{
	marshaled.___ValuePosition_0 = unmarshaled.get_ValuePosition_0();
	marshaled.___ResourceName_1 = il2cpp_codegen_marshal_bstring(unmarshaled.get_ResourceName_1());
	marshaled.___TypeIndex_2 = unmarshaled.get_TypeIndex_2();
}
extern "C" void ResourceInfo_t4074584572_marshal_com_back(const ResourceInfo_t4074584572_marshaled_com& marshaled, ResourceInfo_t4074584572& unmarshaled)
{
	int64_t unmarshaled_ValuePosition_temp = 0;
	unmarshaled_ValuePosition_temp = marshaled.___ValuePosition_0;
	unmarshaled.set_ValuePosition_0(unmarshaled_ValuePosition_temp);
	unmarshaled.set_ResourceName_1(il2cpp_codegen_marshal_bstring_result(marshaled.___ResourceName_1));
	int32_t unmarshaled_TypeIndex_temp = 0;
	unmarshaled_TypeIndex_temp = marshaled.___TypeIndex_2;
	unmarshaled.set_TypeIndex_2(unmarshaled_TypeIndex_temp);
}
// Conversion method for clean up from marshalling of: System.Resources.ResourceReader/ResourceInfo
extern "C" void ResourceInfo_t4074584572_marshal_com_cleanup(ResourceInfo_t4074584572_marshaled_com& marshaled)
{
	il2cpp_codegen_marshal_free_bstring(marshaled.___ResourceName_1);
	marshaled.___ResourceName_1 = NULL;
}
// System.Void System.Resources.ResourceSet::.ctor()
extern TypeInfo* Hashtable_t3875263730_il2cpp_TypeInfo_var;
extern const uint32_t ResourceSet__ctor_m2155833766_MetadataUsageId;
extern "C"  void ResourceSet__ctor_m2155833766 (ResourceSet_t3790468310 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ResourceSet__ctor_m2155833766_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		Hashtable_t3875263730 * L_0 = (Hashtable_t3875263730 *)il2cpp_codegen_object_new(Hashtable_t3875263730_il2cpp_TypeInfo_var);
		Hashtable__ctor_m1514037738(L_0, /*hidden argument*/NULL);
		__this->set_Table_1(L_0);
		__this->set_resources_read_2((bool)1);
		return;
	}
}
// System.Void System.Resources.ResourceSet::.ctor(System.IO.Stream)
extern TypeInfo* Hashtable_t3875263730_il2cpp_TypeInfo_var;
extern TypeInfo* ResourceReader_t4097835539_il2cpp_TypeInfo_var;
extern const uint32_t ResourceSet__ctor_m1435739197_MetadataUsageId;
extern "C"  void ResourceSet__ctor_m1435739197 (ResourceSet_t3790468310 * __this, Stream_t219029575 * ___stream, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ResourceSet__ctor_m1435739197_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		Hashtable_t3875263730 * L_0 = (Hashtable_t3875263730 *)il2cpp_codegen_object_new(Hashtable_t3875263730_il2cpp_TypeInfo_var);
		Hashtable__ctor_m1514037738(L_0, /*hidden argument*/NULL);
		__this->set_Table_1(L_0);
		Stream_t219029575 * L_1 = ___stream;
		ResourceReader_t4097835539 * L_2 = (ResourceReader_t4097835539 *)il2cpp_codegen_object_new(ResourceReader_t4097835539_il2cpp_TypeInfo_var);
		ResourceReader__ctor_m4021287506(L_2, L_1, /*hidden argument*/NULL);
		__this->set_Reader_0(L_2);
		return;
	}
}
// System.Void System.Resources.ResourceSet::.ctor(System.IO.UnmanagedMemoryStream)
extern TypeInfo* Hashtable_t3875263730_il2cpp_TypeInfo_var;
extern TypeInfo* ResourceReader_t4097835539_il2cpp_TypeInfo_var;
extern const uint32_t ResourceSet__ctor_m1446669582_MetadataUsageId;
extern "C"  void ResourceSet__ctor_m1446669582 (ResourceSet_t3790468310 * __this, UnmanagedMemoryStream_t4280280686 * ___stream, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ResourceSet__ctor_m1446669582_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		Hashtable_t3875263730 * L_0 = (Hashtable_t3875263730 *)il2cpp_codegen_object_new(Hashtable_t3875263730_il2cpp_TypeInfo_var);
		Hashtable__ctor_m1514037738(L_0, /*hidden argument*/NULL);
		__this->set_Table_1(L_0);
		UnmanagedMemoryStream_t4280280686 * L_1 = ___stream;
		ResourceReader_t4097835539 * L_2 = (ResourceReader_t4097835539 *)il2cpp_codegen_object_new(ResourceReader_t4097835539_il2cpp_TypeInfo_var);
		ResourceReader__ctor_m4021287506(L_2, L_1, /*hidden argument*/NULL);
		__this->set_Reader_0(L_2);
		return;
	}
}
// System.Void System.Resources.ResourceSet::.ctor(System.String)
extern TypeInfo* Hashtable_t3875263730_il2cpp_TypeInfo_var;
extern TypeInfo* ResourceReader_t4097835539_il2cpp_TypeInfo_var;
extern const uint32_t ResourceSet__ctor_m3385168668_MetadataUsageId;
extern "C"  void ResourceSet__ctor_m3385168668 (ResourceSet_t3790468310 * __this, String_t* ___fileName, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ResourceSet__ctor_m3385168668_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		Hashtable_t3875263730 * L_0 = (Hashtable_t3875263730 *)il2cpp_codegen_object_new(Hashtable_t3875263730_il2cpp_TypeInfo_var);
		Hashtable__ctor_m1514037738(L_0, /*hidden argument*/NULL);
		__this->set_Table_1(L_0);
		String_t* L_1 = ___fileName;
		ResourceReader_t4097835539 * L_2 = (ResourceReader_t4097835539 *)il2cpp_codegen_object_new(ResourceReader_t4097835539_il2cpp_TypeInfo_var);
		ResourceReader__ctor_m3906862375(L_2, L_1, /*hidden argument*/NULL);
		__this->set_Reader_0(L_2);
		return;
	}
}
// System.Collections.IEnumerator System.Resources.ResourceSet::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ResourceSet_System_Collections_IEnumerable_GetEnumerator_m2370128345 (ResourceSet_t3790468310 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = VirtFuncInvoker0< Il2CppObject * >::Invoke(7 /* System.Collections.IDictionaryEnumerator System.Resources.ResourceSet::GetEnumerator() */, __this);
		return L_0;
	}
}
// System.Void System.Resources.ResourceSet::Dispose()
extern "C"  void ResourceSet_Dispose_m1480295523 (ResourceSet_t3790468310 * __this, const MethodInfo* method)
{
	{
		VirtActionInvoker1< bool >::Invoke(6 /* System.Void System.Resources.ResourceSet::Dispose(System.Boolean) */, __this, (bool)1);
		GC_SuppressFinalize_m1160635446(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Resources.ResourceSet::Dispose(System.Boolean)
extern TypeInfo* IResourceReader_t2479967900_il2cpp_TypeInfo_var;
extern const uint32_t ResourceSet_Dispose_m3120910490_MetadataUsageId;
extern "C"  void ResourceSet_Dispose_m3120910490 (ResourceSet_t3790468310 * __this, bool ___disposing, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ResourceSet_Dispose_m3120910490_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = ___disposing;
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		Il2CppObject * L_1 = __this->get_Reader_0();
		if (!L_1)
		{
			goto IL_001c;
		}
	}
	{
		Il2CppObject * L_2 = __this->get_Reader_0();
		NullCheck(L_2);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.Resources.IResourceReader::Close() */, IResourceReader_t2479967900_il2cpp_TypeInfo_var, L_2);
	}

IL_001c:
	{
		__this->set_Reader_0((Il2CppObject *)NULL);
		__this->set_Table_1((Hashtable_t3875263730 *)NULL);
		__this->set_disposed_3((bool)1);
		return;
	}
}
// System.Collections.IDictionaryEnumerator System.Resources.ResourceSet::GetEnumerator()
extern TypeInfo* ObjectDisposedException_t973246880_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral627417496;
extern const uint32_t ResourceSet_GetEnumerator_m1116109982_MetadataUsageId;
extern "C"  Il2CppObject * ResourceSet_GetEnumerator_m1116109982 (ResourceSet_t3790468310 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ResourceSet_GetEnumerator_m1116109982_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = __this->get_disposed_3();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		ObjectDisposedException_t973246880 * L_1 = (ObjectDisposedException_t973246880 *)il2cpp_codegen_object_new(ObjectDisposedException_t973246880_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_1, _stringLiteral627417496, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		VirtActionInvoker0::Invoke(10 /* System.Void System.Resources.ResourceSet::ReadResources() */, __this);
		Hashtable_t3875263730 * L_2 = __this->get_Table_1();
		NullCheck(L_2);
		Il2CppObject * L_3 = VirtFuncInvoker0< Il2CppObject * >::Invoke(32 /* System.Collections.IDictionaryEnumerator System.Collections.Hashtable::GetEnumerator() */, L_2);
		return L_3;
	}
}
// System.Object System.Resources.ResourceSet::GetObjectInternal(System.String,System.Boolean)
extern TypeInfo* ArgumentNullException_t3214793280_il2cpp_TypeInfo_var;
extern TypeInfo* ObjectDisposedException_t973246880_il2cpp_TypeInfo_var;
extern TypeInfo* IEnumerator_t287207039_il2cpp_TypeInfo_var;
extern TypeInfo* DictionaryEntry_t130027246_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* CultureInfo_t3603717042_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3373707;
extern Il2CppCodeGenString* _stringLiteral627417496;
extern const uint32_t ResourceSet_GetObjectInternal_m2208724156_MetadataUsageId;
extern "C"  Il2CppObject * ResourceSet_GetObjectInternal_m2208724156 (ResourceSet_t3790468310 * __this, String_t* ___name, bool ___ignoreCase, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ResourceSet_GetObjectInternal_m2208724156_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	DictionaryEntry_t130027246  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Il2CppObject * V_2 = NULL;
	String_t* V_3 = NULL;
	Il2CppObject * V_4 = NULL;
	Il2CppObject * V_5 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		String_t* L_0 = ___name;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t3214793280 * L_1 = (ArgumentNullException_t3214793280 *)il2cpp_codegen_object_new(ArgumentNullException_t3214793280_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_1, _stringLiteral3373707, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		bool L_2 = __this->get_disposed_3();
		if (!L_2)
		{
			goto IL_0027;
		}
	}
	{
		ObjectDisposedException_t973246880 * L_3 = (ObjectDisposedException_t973246880 *)il2cpp_codegen_object_new(ObjectDisposedException_t973246880_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_3, _stringLiteral627417496, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0027:
	{
		VirtActionInvoker0::Invoke(10 /* System.Void System.Resources.ResourceSet::ReadResources() */, __this);
		Hashtable_t3875263730 * L_4 = __this->get_Table_1();
		String_t* L_5 = ___name;
		NullCheck(L_4);
		Il2CppObject * L_6 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(26 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_4, L_5);
		V_0 = L_6;
		Il2CppObject * L_7 = V_0;
		if (!L_7)
		{
			goto IL_0042;
		}
	}
	{
		Il2CppObject * L_8 = V_0;
		return L_8;
	}

IL_0042:
	{
		bool L_9 = ___ignoreCase;
		if (!L_9)
		{
			goto IL_00b7;
		}
	}
	{
		Hashtable_t3875263730 * L_10 = __this->get_Table_1();
		NullCheck(L_10);
		Il2CppObject * L_11 = VirtFuncInvoker0< Il2CppObject * >::Invoke(32 /* System.Collections.IDictionaryEnumerator System.Collections.Hashtable::GetEnumerator() */, L_10);
		V_2 = L_11;
	}

IL_0054:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0092;
		}

IL_0059:
		{
			Il2CppObject * L_12 = V_2;
			NullCheck(L_12);
			Il2CppObject * L_13 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, L_12);
			V_1 = ((*(DictionaryEntry_t130027246 *)((DictionaryEntry_t130027246 *)UnBox (L_13, DictionaryEntry_t130027246_il2cpp_TypeInfo_var))));
			Il2CppObject * L_14 = DictionaryEntry_get_Key_m3516209325((&V_1), /*hidden argument*/NULL);
			V_3 = ((String_t*)CastclassSealed(L_14, String_t_il2cpp_TypeInfo_var));
			String_t* L_15 = V_3;
			String_t* L_16 = ___name;
			IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t3603717042_il2cpp_TypeInfo_var);
			CultureInfo_t3603717042 * L_17 = CultureInfo_get_InvariantCulture_m764001524(NULL /*static, unused*/, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			int32_t L_18 = String_Compare_m279494420(NULL /*static, unused*/, L_15, L_16, (bool)1, L_17, /*hidden argument*/NULL);
			if (L_18)
			{
				goto IL_0092;
			}
		}

IL_0084:
		{
			Il2CppObject * L_19 = DictionaryEntry_get_Value_m4281303039((&V_1), /*hidden argument*/NULL);
			V_4 = L_19;
			IL2CPP_LEAVE(0xB9, FINALLY_00a2);
		}

IL_0092:
		{
			Il2CppObject * L_20 = V_2;
			NullCheck(L_20);
			bool L_21 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, L_20);
			if (L_21)
			{
				goto IL_0059;
			}
		}

IL_009d:
		{
			IL2CPP_LEAVE(0xB7, FINALLY_00a2);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_00a2;
	}

FINALLY_00a2:
	{ // begin finally (depth: 1)
		{
			Il2CppObject * L_22 = V_2;
			V_5 = ((Il2CppObject *)IsInst(L_22, IDisposable_t1628921374_il2cpp_TypeInfo_var));
			Il2CppObject * L_23 = V_5;
			if (L_23)
			{
				goto IL_00af;
			}
		}

IL_00ae:
		{
			IL2CPP_END_FINALLY(162)
		}

IL_00af:
		{
			Il2CppObject * L_24 = V_5;
			NullCheck(L_24);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, L_24);
			IL2CPP_END_FINALLY(162)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(162)
	{
		IL2CPP_JUMP_TBL(0xB9, IL_00b9)
		IL2CPP_JUMP_TBL(0xB7, IL_00b7)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_00b7:
	{
		return NULL;
	}

IL_00b9:
	{
		Il2CppObject * L_25 = V_4;
		return L_25;
	}
}
// System.Object System.Resources.ResourceSet::GetObject(System.String)
extern "C"  Il2CppObject * ResourceSet_GetObject_m543692318 (ResourceSet_t3790468310 * __this, String_t* ___name, const MethodInfo* method)
{
	{
		String_t* L_0 = ___name;
		Il2CppObject * L_1 = ResourceSet_GetObjectInternal_m2208724156(__this, L_0, (bool)0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Object System.Resources.ResourceSet::GetObject(System.String,System.Boolean)
extern "C"  Il2CppObject * ResourceSet_GetObject_m100760991 (ResourceSet_t3790468310 * __this, String_t* ___name, bool ___ignoreCase, const MethodInfo* method)
{
	{
		String_t* L_0 = ___name;
		bool L_1 = ___ignoreCase;
		Il2CppObject * L_2 = ResourceSet_GetObjectInternal_m2208724156(__this, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void System.Resources.ResourceSet::ReadResources()
extern TypeInfo* ObjectDisposedException_t973246880_il2cpp_TypeInfo_var;
extern TypeInfo* IResourceReader_t2479967900_il2cpp_TypeInfo_var;
extern TypeInfo* IEnumerator_t287207039_il2cpp_TypeInfo_var;
extern TypeInfo* IDictionaryEnumerator_t1541724277_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral627417496;
extern const uint32_t ResourceSet_ReadResources_m2589571379_MetadataUsageId;
extern "C"  void ResourceSet_ReadResources_m2589571379 (ResourceSet_t3790468310 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ResourceSet_ReadResources_m2589571379_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Hashtable_t3875263730 * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		bool L_0 = __this->get_resources_read_2();
		if (!L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		Il2CppObject * L_1 = __this->get_Reader_0();
		if (L_1)
		{
			goto IL_0022;
		}
	}
	{
		ObjectDisposedException_t973246880 * L_2 = (ObjectDisposedException_t973246880 *)il2cpp_codegen_object_new(ObjectDisposedException_t973246880_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_2, _stringLiteral627417496, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0022:
	{
		Hashtable_t3875263730 * L_3 = __this->get_Table_1();
		V_0 = L_3;
		Hashtable_t3875263730 * L_4 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
	}

IL_002f:
	try
	{ // begin try (depth: 1)
		{
			bool L_5 = __this->get_resources_read_2();
			if (!L_5)
			{
				goto IL_003f;
			}
		}

IL_003a:
		{
			IL2CPP_LEAVE(0x8B, FINALLY_0084);
		}

IL_003f:
		{
			Il2CppObject * L_6 = __this->get_Reader_0();
			NullCheck(L_6);
			Il2CppObject * L_7 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(1 /* System.Collections.IDictionaryEnumerator System.Resources.IResourceReader::GetEnumerator() */, IResourceReader_t2479967900_il2cpp_TypeInfo_var, L_6);
			V_1 = L_7;
			Il2CppObject * L_8 = V_1;
			NullCheck(L_8);
			InterfaceActionInvoker0::Invoke(2 /* System.Void System.Collections.IEnumerator::Reset() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, L_8);
			goto IL_006d;
		}

IL_0056:
		{
			Hashtable_t3875263730 * L_9 = __this->get_Table_1();
			Il2CppObject * L_10 = V_1;
			NullCheck(L_10);
			Il2CppObject * L_11 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(1 /* System.Object System.Collections.IDictionaryEnumerator::get_Key() */, IDictionaryEnumerator_t1541724277_il2cpp_TypeInfo_var, L_10);
			Il2CppObject * L_12 = V_1;
			NullCheck(L_12);
			Il2CppObject * L_13 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(2 /* System.Object System.Collections.IDictionaryEnumerator::get_Value() */, IDictionaryEnumerator_t1541724277_il2cpp_TypeInfo_var, L_12);
			NullCheck(L_9);
			VirtActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(29 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_9, L_11, L_13);
		}

IL_006d:
		{
			Il2CppObject * L_14 = V_1;
			NullCheck(L_14);
			bool L_15 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, L_14);
			if (L_15)
			{
				goto IL_0056;
			}
		}

IL_0078:
		{
			__this->set_resources_read_2((bool)1);
			IL2CPP_LEAVE(0x8B, FINALLY_0084);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0084;
	}

FINALLY_0084:
	{ // begin finally (depth: 1)
		Hashtable_t3875263730 * L_16 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, L_16, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(132)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(132)
	{
		IL2CPP_JUMP_TBL(0x8B, IL_008b)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_008b:
	{
		return;
	}
}
// System.Void System.Resources.RuntimeResourceSet::.ctor(System.IO.UnmanagedMemoryStream)
extern "C"  void RuntimeResourceSet__ctor_m1041659876 (RuntimeResourceSet_t2503739934 * __this, UnmanagedMemoryStream_t4280280686 * ___stream, const MethodInfo* method)
{
	{
		UnmanagedMemoryStream_t4280280686 * L_0 = ___stream;
		ResourceSet__ctor_m1446669582(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Resources.RuntimeResourceSet::.ctor(System.IO.Stream)
extern "C"  void RuntimeResourceSet__ctor_m3373926439 (RuntimeResourceSet_t2503739934 * __this, Stream_t219029575 * ___stream, const MethodInfo* method)
{
	{
		Stream_t219029575 * L_0 = ___stream;
		ResourceSet__ctor_m1435739197(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Resources.RuntimeResourceSet::.ctor(System.String)
extern "C"  void RuntimeResourceSet__ctor_m1737082738 (RuntimeResourceSet_t2503739934 * __this, String_t* ___fileName, const MethodInfo* method)
{
	{
		String_t* L_0 = ___fileName;
		ResourceSet__ctor_m3385168668(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Object System.Resources.RuntimeResourceSet::GetObject(System.String)
extern TypeInfo* ObjectDisposedException_t973246880_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral627417496;
extern const uint32_t RuntimeResourceSet_GetObject_m167723114_MetadataUsageId;
extern "C"  Il2CppObject * RuntimeResourceSet_GetObject_m167723114 (RuntimeResourceSet_t2503739934 * __this, String_t* ___name, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (RuntimeResourceSet_GetObject_m167723114_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = ((ResourceSet_t3790468310 *)__this)->get_Reader_0();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		ObjectDisposedException_t973246880 * L_1 = (ObjectDisposedException_t973246880 *)il2cpp_codegen_object_new(ObjectDisposedException_t973246880_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_1, _stringLiteral627417496, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		String_t* L_2 = ___name;
		Il2CppObject * L_3 = ResourceSet_GetObject_m543692318(__this, L_2, /*hidden argument*/NULL);
		Il2CppObject * L_4 = RuntimeResourceSet_CloneDisposableObjectIfPossible_m1308606215(__this, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Object System.Resources.RuntimeResourceSet::GetObject(System.String,System.Boolean)
extern TypeInfo* ObjectDisposedException_t973246880_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral627417496;
extern const uint32_t RuntimeResourceSet_GetObject_m3242127827_MetadataUsageId;
extern "C"  Il2CppObject * RuntimeResourceSet_GetObject_m3242127827 (RuntimeResourceSet_t2503739934 * __this, String_t* ___name, bool ___ignoreCase, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (RuntimeResourceSet_GetObject_m3242127827_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = ((ResourceSet_t3790468310 *)__this)->get_Reader_0();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		ObjectDisposedException_t973246880 * L_1 = (ObjectDisposedException_t973246880 *)il2cpp_codegen_object_new(ObjectDisposedException_t973246880_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_1, _stringLiteral627417496, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		String_t* L_2 = ___name;
		bool L_3 = ___ignoreCase;
		Il2CppObject * L_4 = ResourceSet_GetObject_m100760991(__this, L_2, L_3, /*hidden argument*/NULL);
		Il2CppObject * L_5 = RuntimeResourceSet_CloneDisposableObjectIfPossible_m1308606215(__this, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Object System.Resources.RuntimeResourceSet::CloneDisposableObjectIfPossible(System.Object)
extern TypeInfo* ICloneable_t2694744451_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t RuntimeResourceSet_CloneDisposableObjectIfPossible_m1308606215_MetadataUsageId;
extern "C"  Il2CppObject * RuntimeResourceSet_CloneDisposableObjectIfPossible_m1308606215 (RuntimeResourceSet_t2503739934 * __this, Il2CppObject * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (RuntimeResourceSet_CloneDisposableObjectIfPossible_m1308606215_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Il2CppObject * G_B4_0 = NULL;
	{
		Il2CppObject * L_0 = ___value;
		V_0 = ((Il2CppObject *)IsInst(L_0, ICloneable_t2694744451_il2cpp_TypeInfo_var));
		Il2CppObject * L_1 = V_0;
		if (!L_1)
		{
			goto IL_0023;
		}
	}
	{
		Il2CppObject * L_2 = ___value;
		if (!((Il2CppObject *)IsInst(L_2, IDisposable_t1628921374_il2cpp_TypeInfo_var)))
		{
			goto IL_0023;
		}
	}
	{
		Il2CppObject * L_3 = V_0;
		NullCheck(L_3);
		Il2CppObject * L_4 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Object System.ICloneable::Clone() */, ICloneable_t2694744451_il2cpp_TypeInfo_var, L_3);
		G_B4_0 = L_4;
		goto IL_0024;
	}

IL_0023:
	{
		Il2CppObject * L_5 = ___value;
		G_B4_0 = L_5;
	}

IL_0024:
	{
		return G_B4_0;
	}
}
// System.Void System.Resources.SatelliteContractVersionAttribute::.ctor(System.String)
extern TypeInfo* Version_t497901645_il2cpp_TypeInfo_var;
extern const uint32_t SatelliteContractVersionAttribute__ctor_m69110617_MetadataUsageId;
extern "C"  void SatelliteContractVersionAttribute__ctor_m69110617 (SatelliteContractVersionAttribute_t1109503379 * __this, String_t* ___version, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SatelliteContractVersionAttribute__ctor_m69110617_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Attribute__ctor_m2985353781(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___version;
		Version_t497901645 * L_1 = (Version_t497901645 *)il2cpp_codegen_object_new(Version_t497901645_il2cpp_TypeInfo_var);
		Version__ctor_m48000169(L_1, L_0, /*hidden argument*/NULL);
		__this->set_ver_0(L_1);
		return;
	}
}
// System.Void System.Runtime.CompilerServices.CompilationRelaxationsAttribute::.ctor(System.Runtime.CompilerServices.CompilationRelaxations)
extern "C"  void CompilationRelaxationsAttribute__ctor_m3469882264 (CompilationRelaxationsAttribute_t592669527 * __this, int32_t ___relaxations, const MethodInfo* method)
{
	{
		Attribute__ctor_m2985353781(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___relaxations;
		__this->set_relax_0(L_0);
		return;
	}
}
// System.Void System.Runtime.CompilerServices.CompilerGeneratedAttribute::.ctor()
extern "C"  void CompilerGeneratedAttribute__ctor_m2546676106 (CompilerGeneratedAttribute_t853953138 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m2985353781(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Runtime.CompilerServices.DecimalConstantAttribute::.ctor(System.Byte,System.Byte,System.UInt32,System.UInt32,System.UInt32)
extern TypeInfo* Convert_t1097883944_il2cpp_TypeInfo_var;
extern const uint32_t DecimalConstantAttribute__ctor_m844034077_MetadataUsageId;
extern "C"  void DecimalConstantAttribute__ctor_m844034077 (DecimalConstantAttribute_t1650182573 * __this, uint8_t ___scale, uint8_t ___sign, uint32_t ___hi, uint32_t ___mid, uint32_t ___low, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DecimalConstantAttribute__ctor_m844034077_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Attribute__ctor_m2985353781(__this, /*hidden argument*/NULL);
		uint8_t L_0 = ___scale;
		__this->set_scale_0(L_0);
		uint8_t L_1 = ___sign;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t1097883944_il2cpp_TypeInfo_var);
		bool L_2 = Convert_ToBoolean_m3519963972(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		__this->set_sign_1(L_2);
		uint32_t L_3 = ___hi;
		__this->set_hi_2(L_3);
		uint32_t L_4 = ___mid;
		__this->set_mid_3(L_4);
		uint32_t L_5 = ___low;
		__this->set_low_4(L_5);
		return;
	}
}
// System.Void System.Runtime.CompilerServices.DefaultDependencyAttribute::.ctor(System.Runtime.CompilerServices.LoadHint)
extern "C"  void DefaultDependencyAttribute__ctor_m1966503101 (DefaultDependencyAttribute_t2265285942 * __this, int32_t ___loadHintArgument, const MethodInfo* method)
{
	{
		Attribute__ctor_m2985353781(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___loadHintArgument;
		__this->set_hint_0(L_0);
		return;
	}
}
// System.Void System.Runtime.CompilerServices.InternalsVisibleToAttribute::.ctor(System.String)
extern "C"  void InternalsVisibleToAttribute__ctor_m1688588087 (InternalsVisibleToAttribute_t1035815915 * __this, String_t* ___assemblyName, const MethodInfo* method)
{
	{
		__this->set_all_visible_1((bool)1);
		Attribute__ctor_m2985353781(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___assemblyName;
		__this->set_assemblyName_0(L_0);
		return;
	}
}
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::.ctor()
extern "C"  void RuntimeCompatibilityAttribute__ctor_m3343718092 (RuntimeCompatibilityAttribute_t4174703210 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m2985353781(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::set_WrapNonExceptionThrows(System.Boolean)
extern "C"  void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m14743685 (RuntimeCompatibilityAttribute_t4174703210 * __this, bool ___value, const MethodInfo* method)
{
	{
		bool L_0 = ___value;
		__this->set_wrap_non_exception_throws_0(L_0);
		return;
	}
}
// System.Void System.Runtime.CompilerServices.RuntimeHelpers::InitializeArray(System.Array,System.IntPtr)
extern "C"  void RuntimeHelpers_InitializeArray_m3388114532 (Il2CppObject * __this /* static, unused */, Il2CppArray * ___array, IntPtr_t ___fldHandle, const MethodInfo* method)
{
	using namespace il2cpp::icalls;
	typedef void (*RuntimeHelpers_InitializeArray_m3388114532_ftn) (Il2CppArray *, IntPtr_t);
	 ((RuntimeHelpers_InitializeArray_m3388114532_ftn)mscorlib::System::Runtime::CompilerServices::RuntimeHelpers::InitializeArray) (___array, ___fldHandle);
}
// System.Void System.Runtime.CompilerServices.RuntimeHelpers::InitializeArray(System.Array,System.RuntimeFieldHandle)
extern TypeInfo* IntPtr_t_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentNullException_t3214793280_il2cpp_TypeInfo_var;
extern const uint32_t RuntimeHelpers_InitializeArray_m2058365049_MetadataUsageId;
extern "C"  void RuntimeHelpers_InitializeArray_m2058365049 (Il2CppObject * __this /* static, unused */, Il2CppArray * ___array, RuntimeFieldHandle_t3184214143  ___fldHandle, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (RuntimeHelpers_InitializeArray_m2058365049_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppArray * L_0 = ___array;
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		IntPtr_t L_1 = RuntimeFieldHandle_get_Value_m2124751302((&___fldHandle), /*hidden argument*/NULL);
		IntPtr_t L_2 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
		bool L_3 = IntPtr_op_Equality_m72843924(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0022;
		}
	}

IL_001c:
	{
		ArgumentNullException_t3214793280 * L_4 = (ArgumentNullException_t3214793280 *)il2cpp_codegen_object_new(ArgumentNullException_t3214793280_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m2162372070(L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4);
	}

IL_0022:
	{
		Il2CppArray * L_5 = ___array;
		IntPtr_t L_6 = RuntimeFieldHandle_get_Value_m2124751302((&___fldHandle), /*hidden argument*/NULL);
		RuntimeHelpers_InitializeArray_m3388114532(NULL /*static, unused*/, L_5, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 System.Runtime.CompilerServices.RuntimeHelpers::get_OffsetToStringData()
extern "C"  int32_t RuntimeHelpers_get_OffsetToStringData_m2708084937 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	using namespace il2cpp::icalls;
	typedef int32_t (*RuntimeHelpers_get_OffsetToStringData_m2708084937_ftn) ();
	return  ((RuntimeHelpers_get_OffsetToStringData_m2708084937_ftn)mscorlib::System::Runtime::CompilerServices::RuntimeHelpers::get_OffsetToStringData) ();
}
// System.Void System.Runtime.CompilerServices.StringFreezingAttribute::.ctor()
extern "C"  void StringFreezingAttribute__ctor_m476014489 (StringFreezingAttribute_t398281917 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m2985353781(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Runtime.ConstrainedExecution.CriticalFinalizerObject::.ctor()
extern "C"  void CriticalFinalizerObject__ctor_m358100503 (CriticalFinalizerObject_t3609670849 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Runtime.ConstrainedExecution.CriticalFinalizerObject::Finalize()
extern "C"  void CriticalFinalizerObject_Finalize_m3870795499 (CriticalFinalizerObject_t3609670849 * __this, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		IL2CPP_LEAVE(0xC, FINALLY_0005);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0005;
	}

FINALLY_0005:
	{ // begin finally (depth: 1)
		Object_Finalize_m3027285644(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(5)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(5)
	{
		IL2CPP_JUMP_TBL(0xC, IL_000c)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_000c:
	{
		return;
	}
}
// System.Void System.Runtime.ConstrainedExecution.ReliabilityContractAttribute::.ctor(System.Runtime.ConstrainedExecution.Consistency,System.Runtime.ConstrainedExecution.Cer)
extern "C"  void ReliabilityContractAttribute__ctor_m1813433017 (ReliabilityContractAttribute_t3872327357 * __this, int32_t ___consistencyGuarantee, int32_t ___cer, const MethodInfo* method)
{
	{
		Attribute__ctor_m2985353781(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___consistencyGuarantee;
		__this->set_consistency_0(L_0);
		int32_t L_1 = ___cer;
		__this->set_cer_1(L_1);
		return;
	}
}
// System.Void System.Runtime.InteropServices.ClassInterfaceAttribute::.ctor(System.Runtime.InteropServices.ClassInterfaceType)
extern "C"  void ClassInterfaceAttribute__ctor_m3543181584 (ClassInterfaceAttribute_t261660349 * __this, int32_t ___classInterfaceType, const MethodInfo* method)
{
	{
		Attribute__ctor_m2985353781(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___classInterfaceType;
		__this->set_ciType_0(L_0);
		return;
	}
}
// System.Void System.Runtime.InteropServices.ComCompatibleVersionAttribute::.ctor(System.Int32,System.Int32,System.Int32,System.Int32)
extern "C"  void ComCompatibleVersionAttribute__ctor_m187454405 (ComCompatibleVersionAttribute_t3476486075 * __this, int32_t ___major, int32_t ___minor, int32_t ___build, int32_t ___revision, const MethodInfo* method)
{
	{
		Attribute__ctor_m2985353781(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___major;
		__this->set_major_0(L_0);
		int32_t L_1 = ___minor;
		__this->set_minor_1(L_1);
		int32_t L_2 = ___build;
		__this->set_build_2(L_2);
		int32_t L_3 = ___revision;
		__this->set_revision_3(L_3);
		return;
	}
}
// System.Void System.Runtime.InteropServices.ComDefaultInterfaceAttribute::.ctor(System.Type)
extern "C"  void ComDefaultInterfaceAttribute__ctor_m3894140624 (ComDefaultInterfaceAttribute_t346532101 * __this, Type_t * ___defaultInterface, const MethodInfo* method)
{
	{
		Attribute__ctor_m2985353781(__this, /*hidden argument*/NULL);
		Type_t * L_0 = ___defaultInterface;
		__this->set__type_0(L_0);
		return;
	}
}
// System.Void System.Runtime.InteropServices.ComImportAttribute::.ctor()
extern "C"  void ComImportAttribute__ctor_m1130850294 (ComImportAttribute_t3229016760 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m2985353781(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Runtime.InteropServices.ComVisibleAttribute::.ctor(System.Boolean)
extern "C"  void ComVisibleAttribute__ctor_m1005514726 (ComVisibleAttribute_t1192118189 * __this, bool ___visibility, const MethodInfo* method)
{
	{
		Attribute__ctor_m2985353781(__this, /*hidden argument*/NULL);
		bool L_0 = ___visibility;
		__this->set_Visible_0(L_0);
		return;
	}
}
// System.Void System.Runtime.InteropServices.DispIdAttribute::.ctor(System.Int32)
extern "C"  void DispIdAttribute__ctor_m2923147436 (DispIdAttribute_t3066611329 * __this, int32_t ___dispId, const MethodInfo* method)
{
	{
		Attribute__ctor_m2985353781(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___dispId;
		__this->set_id_0(L_0);
		return;
	}
}
// System.Void System.Runtime.InteropServices.DllImportAttribute::.ctor(System.String)
extern "C"  void DllImportAttribute__ctor_m1952564553 (DllImportAttribute_t2977516789 * __this, String_t* ___dllName, const MethodInfo* method)
{
	{
		Attribute__ctor_m2985353781(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___dllName;
		__this->set_Dll_2(L_0);
		return;
	}
}
// System.String System.Runtime.InteropServices.DllImportAttribute::get_Value()
extern "C"  String_t* DllImportAttribute_get_Value_m819189346 (DllImportAttribute_t2977516789 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_Dll_2();
		return L_0;
	}
}
// System.Void System.Runtime.InteropServices.ExternalException::.ctor()
extern Il2CppCodeGenString* _stringLiteral569709146;
extern const uint32_t ExternalException__ctor_m3484548022_MetadataUsageId;
extern "C"  void ExternalException__ctor_m3484548022 (ExternalException_t1945928326 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ExternalException__ctor_m3484548022_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = Locale_GetText_m2389348044(NULL /*static, unused*/, _stringLiteral569709146, /*hidden argument*/NULL);
		SystemException__ctor_m3697314481(__this, L_0, /*hidden argument*/NULL);
		Exception_set_HResult_m3566571225(__this, ((int32_t)-2147467259), /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Runtime.InteropServices.ExternalException::.ctor(System.String)
extern "C"  void ExternalException__ctor_m3601652492 (ExternalException_t1945928326 * __this, String_t* ___message, const MethodInfo* method)
{
	{
		String_t* L_0 = ___message;
		SystemException__ctor_m3697314481(__this, L_0, /*hidden argument*/NULL);
		Exception_set_HResult_m3566571225(__this, ((int32_t)-2147467259), /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Runtime.InteropServices.ExternalException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void ExternalException__ctor_m1975451639 (ExternalException_t1945928326 * __this, SerializationInfo_t2995724695 * ___info, StreamingContext_t986364934  ___context, const MethodInfo* method)
{
	{
		SerializationInfo_t2995724695 * L_0 = ___info;
		StreamingContext_t986364934  L_1 = ___context;
		SystemException__ctor_m2083527090(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Runtime.InteropServices.FieldOffsetAttribute::.ctor(System.Int32)
extern "C"  void FieldOffsetAttribute__ctor_m3654465902 (FieldOffsetAttribute_t3640292593 * __this, int32_t ___offset, const MethodInfo* method)
{
	{
		Attribute__ctor_m2985353781(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___offset;
		__this->set_val_0(L_0);
		return;
	}
}
// System.Void System.Runtime.InteropServices.GCHandle::.ctor(System.IntPtr)
extern "C"  void GCHandle__ctor_m389774252 (GCHandle_t2146430982 * __this, IntPtr_t ___h, const MethodInfo* method)
{
	{
		IntPtr_t L_0 = ___h;
		int32_t L_1 = IntPtr_op_Explicit_m1500672818(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		__this->set_handle_0(L_1);
		return;
	}
}
// System.Void System.Runtime.InteropServices.GCHandle::.ctor(System.Object)
extern "C"  void GCHandle__ctor_m1067653356 (GCHandle_t2146430982 * __this, Il2CppObject * ___obj, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___obj;
		GCHandle__ctor_m396637490(__this, L_0, 2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Runtime.InteropServices.GCHandle::.ctor(System.Object,System.Runtime.InteropServices.GCHandleType)
extern "C"  void GCHandle__ctor_m396637490 (GCHandle_t2146430982 * __this, Il2CppObject * ___value, int32_t ___type, const MethodInfo* method)
{
	{
		int32_t L_0 = ___type;
		if ((((int32_t)L_0) < ((int32_t)0)))
		{
			goto IL_000e;
		}
	}
	{
		int32_t L_1 = ___type;
		if ((((int32_t)L_1) <= ((int32_t)3)))
		{
			goto IL_0011;
		}
	}

IL_000e:
	{
		___type = 2;
	}

IL_0011:
	{
		Il2CppObject * L_2 = ___value;
		int32_t L_3 = ___type;
		int32_t L_4 = GCHandle_GetTargetHandle_m414463034(NULL /*static, unused*/, L_2, 0, L_3, /*hidden argument*/NULL);
		__this->set_handle_0(L_4);
		return;
	}
}
// System.Boolean System.Runtime.InteropServices.GCHandle::get_IsAllocated()
extern "C"  bool GCHandle_get_IsAllocated_m1991616348 (GCHandle_t2146430982 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_handle_0();
		return (bool)((((int32_t)((((int32_t)L_0) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Object System.Runtime.InteropServices.GCHandle::get_Target()
extern TypeInfo* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral680831516;
extern const uint32_t GCHandle_get_Target_m1356841761_MetadataUsageId;
extern "C"  Il2CppObject * GCHandle_get_Target_m1356841761 (GCHandle_t2146430982 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GCHandle_get_Target_m1356841761_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = GCHandle_get_IsAllocated_m1991616348(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_001b;
		}
	}
	{
		String_t* L_1 = Locale_GetText_m2389348044(NULL /*static, unused*/, _stringLiteral680831516, /*hidden argument*/NULL);
		InvalidOperationException_t2420574324 * L_2 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_2, L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_001b:
	{
		int32_t L_3 = __this->get_handle_0();
		Il2CppObject * L_4 = GCHandle_GetTarget_m385786707(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Runtime.InteropServices.GCHandle System.Runtime.InteropServices.GCHandle::Alloc(System.Object)
extern "C"  GCHandle_t2146430982  GCHandle_Alloc_m3356655570 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___value, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value;
		GCHandle_t2146430982  L_1;
		memset(&L_1, 0, sizeof(L_1));
		GCHandle__ctor_m1067653356(&L_1, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Runtime.InteropServices.GCHandle System.Runtime.InteropServices.GCHandle::Alloc(System.Object,System.Runtime.InteropServices.GCHandleType)
extern "C"  GCHandle_t2146430982  GCHandle_Alloc_m4018232088 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___value, int32_t ___type, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value;
		int32_t L_1 = ___type;
		GCHandle_t2146430982  L_2;
		memset(&L_2, 0, sizeof(L_2));
		GCHandle__ctor_m396637490(&L_2, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void System.Runtime.InteropServices.GCHandle::Free()
extern "C"  void GCHandle_Free_m2878302728 (GCHandle_t2146430982 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_handle_0();
		GCHandle_FreeHandle_m2607926881(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		__this->set_handle_0(0);
		return;
	}
}
// System.Boolean System.Runtime.InteropServices.GCHandle::CheckCurrentDomain(System.Int32)
extern "C"  bool GCHandle_CheckCurrentDomain_m287479968 (Il2CppObject * __this /* static, unused */, int32_t ___handle, const MethodInfo* method)
{
	using namespace il2cpp::icalls;
	typedef bool (*GCHandle_CheckCurrentDomain_m287479968_ftn) (int32_t);
	return  ((GCHandle_CheckCurrentDomain_m287479968_ftn)mscorlib::System::Runtime::InteropServices::GCHandle::CheckCurrentDomain) (___handle);
}
// System.Object System.Runtime.InteropServices.GCHandle::GetTarget(System.Int32)
extern "C"  Il2CppObject * GCHandle_GetTarget_m385786707 (Il2CppObject * __this /* static, unused */, int32_t ___handle, const MethodInfo* method)
{
	using namespace il2cpp::icalls;
	typedef Il2CppObject * (*GCHandle_GetTarget_m385786707_ftn) (int32_t);
	return  ((GCHandle_GetTarget_m385786707_ftn)mscorlib::System::Runtime::InteropServices::GCHandle::GetTarget) (___handle);
}
// System.Int32 System.Runtime.InteropServices.GCHandle::GetTargetHandle(System.Object,System.Int32,System.Runtime.InteropServices.GCHandleType)
extern "C"  int32_t GCHandle_GetTargetHandle_m414463034 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___obj, int32_t ___handle, int32_t ___type, const MethodInfo* method)
{
	using namespace il2cpp::icalls;
	typedef int32_t (*GCHandle_GetTargetHandle_m414463034_ftn) (Il2CppObject *, int32_t, int32_t);
	return  ((GCHandle_GetTargetHandle_m414463034_ftn)mscorlib::System::Runtime::InteropServices::GCHandle::GetTargetHandle) (___obj, ___handle, ___type);
}
// System.Void System.Runtime.InteropServices.GCHandle::FreeHandle(System.Int32)
extern "C"  void GCHandle_FreeHandle_m2607926881 (Il2CppObject * __this /* static, unused */, int32_t ___handle, const MethodInfo* method)
{
	using namespace il2cpp::icalls;
	typedef void (*GCHandle_FreeHandle_m2607926881_ftn) (int32_t);
	 ((GCHandle_FreeHandle_m2607926881_ftn)mscorlib::System::Runtime::InteropServices::GCHandle::FreeHandle) (___handle);
}
// System.Boolean System.Runtime.InteropServices.GCHandle::Equals(System.Object)
extern TypeInfo* GCHandle_t2146430982_il2cpp_TypeInfo_var;
extern const uint32_t GCHandle_Equals_m2737763611_MetadataUsageId;
extern "C"  bool GCHandle_Equals_m2737763611 (GCHandle_t2146430982 * __this, Il2CppObject * ___o, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GCHandle_Equals_m2737763611_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GCHandle_t2146430982  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Il2CppObject * L_0 = ___o;
		if (!L_0)
		{
			goto IL_0011;
		}
	}
	{
		Il2CppObject * L_1 = ___o;
		if (((Il2CppObject *)IsInstSealed(L_1, GCHandle_t2146430982_il2cpp_TypeInfo_var)))
		{
			goto IL_0013;
		}
	}

IL_0011:
	{
		return (bool)0;
	}

IL_0013:
	{
		int32_t L_2 = __this->get_handle_0();
		Il2CppObject * L_3 = ___o;
		V_0 = ((*(GCHandle_t2146430982 *)((GCHandle_t2146430982 *)UnBox (L_3, GCHandle_t2146430982_il2cpp_TypeInfo_var))));
		int32_t L_4 = (&V_0)->get_handle_0();
		return (bool)((((int32_t)L_2) == ((int32_t)L_4))? 1 : 0);
	}
}
// System.Int32 System.Runtime.InteropServices.GCHandle::GetHashCode()
extern "C"  int32_t GCHandle_GetHashCode_m3173355379 (GCHandle_t2146430982 * __this, const MethodInfo* method)
{
	{
		int32_t* L_0 = __this->get_address_of_handle_0();
		int32_t L_1 = Int32_GetHashCode_m3396943446(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Runtime.InteropServices.GCHandle System.Runtime.InteropServices.GCHandle::FromIntPtr(System.IntPtr)
extern "C"  GCHandle_t2146430982  GCHandle_FromIntPtr_m2343506966 (Il2CppObject * __this /* static, unused */, IntPtr_t ___value, const MethodInfo* method)
{
	{
		IntPtr_t L_0 = ___value;
		GCHandle_t2146430982  L_1 = GCHandle_op_Explicit_m1878324757(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.IntPtr System.Runtime.InteropServices.GCHandle::ToIntPtr(System.Runtime.InteropServices.GCHandle)
extern "C"  IntPtr_t GCHandle_ToIntPtr_m756840421 (Il2CppObject * __this /* static, unused */, GCHandle_t2146430982  ___value, const MethodInfo* method)
{
	{
		GCHandle_t2146430982  L_0 = ___value;
		IntPtr_t L_1 = GCHandle_op_Explicit_m2697694073(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.IntPtr System.Runtime.InteropServices.GCHandle::op_Explicit(System.Runtime.InteropServices.GCHandle)
extern "C"  IntPtr_t GCHandle_op_Explicit_m2697694073 (Il2CppObject * __this /* static, unused */, GCHandle_t2146430982  ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = (&___value)->get_handle_0();
		IntPtr_t L_1 = IntPtr_op_Explicit_m2174167756(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Runtime.InteropServices.GCHandle System.Runtime.InteropServices.GCHandle::op_Explicit(System.IntPtr)
extern TypeInfo* IntPtr_t_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentException_t124305799_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1006189907;
extern Il2CppCodeGenString* _stringLiteral1312931768;
extern const uint32_t GCHandle_op_Explicit_m1878324757_MetadataUsageId;
extern "C"  GCHandle_t2146430982  GCHandle_op_Explicit_m1878324757 (Il2CppObject * __this /* static, unused */, IntPtr_t ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GCHandle_op_Explicit_m1878324757_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = ___value;
		IntPtr_t L_1 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
		bool L_2 = IntPtr_op_Equality_m72843924(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_001b;
		}
	}
	{
		ArgumentException_t124305799 * L_3 = (ArgumentException_t124305799 *)il2cpp_codegen_object_new(ArgumentException_t124305799_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3544856547(L_3, _stringLiteral1006189907, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_001b:
	{
		IntPtr_t L_4 = ___value;
		int32_t L_5 = IntPtr_op_Explicit_m1500672818(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		bool L_6 = GCHandle_CheckCurrentDomain_m287479968(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		if (L_6)
		{
			goto IL_0036;
		}
	}
	{
		ArgumentException_t124305799 * L_7 = (ArgumentException_t124305799 *)il2cpp_codegen_object_new(ArgumentException_t124305799_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3544856547(L_7, _stringLiteral1312931768, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_7);
	}

IL_0036:
	{
		IntPtr_t L_8 = ___value;
		GCHandle_t2146430982  L_9;
		memset(&L_9, 0, sizeof(L_9));
		GCHandle__ctor_m389774252(&L_9, L_8, /*hidden argument*/NULL);
		return L_9;
	}
}
// Conversion methods for marshalling of: System.Runtime.InteropServices.GCHandle
extern "C" void GCHandle_t2146430982_marshal_pinvoke(const GCHandle_t2146430982& unmarshaled, GCHandle_t2146430982_marshaled_pinvoke& marshaled)
{
	marshaled.___handle_0 = unmarshaled.get_handle_0();
}
extern "C" void GCHandle_t2146430982_marshal_pinvoke_back(const GCHandle_t2146430982_marshaled_pinvoke& marshaled, GCHandle_t2146430982& unmarshaled)
{
	int32_t unmarshaled_handle_temp = 0;
	unmarshaled_handle_temp = marshaled.___handle_0;
	unmarshaled.set_handle_0(unmarshaled_handle_temp);
}
// Conversion method for clean up from marshalling of: System.Runtime.InteropServices.GCHandle
extern "C" void GCHandle_t2146430982_marshal_pinvoke_cleanup(GCHandle_t2146430982_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: System.Runtime.InteropServices.GCHandle
extern "C" void GCHandle_t2146430982_marshal_com(const GCHandle_t2146430982& unmarshaled, GCHandle_t2146430982_marshaled_com& marshaled)
{
	marshaled.___handle_0 = unmarshaled.get_handle_0();
}
extern "C" void GCHandle_t2146430982_marshal_com_back(const GCHandle_t2146430982_marshaled_com& marshaled, GCHandle_t2146430982& unmarshaled)
{
	int32_t unmarshaled_handle_temp = 0;
	unmarshaled_handle_temp = marshaled.___handle_0;
	unmarshaled.set_handle_0(unmarshaled_handle_temp);
}
// Conversion method for clean up from marshalling of: System.Runtime.InteropServices.GCHandle
extern "C" void GCHandle_t2146430982_marshal_com_cleanup(GCHandle_t2146430982_marshaled_com& marshaled)
{
}
// System.Void System.Runtime.InteropServices.GuidAttribute::.ctor(System.String)
extern "C"  void GuidAttribute__ctor_m174694299 (GuidAttribute_t4178726869 * __this, String_t* ___guid, const MethodInfo* method)
{
	{
		Attribute__ctor_m2985353781(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___guid;
		__this->set_guidValue_0(L_0);
		return;
	}
}
// System.Void System.Runtime.InteropServices.InAttribute::.ctor()
extern "C"  void InAttribute__ctor_m476828707 (InAttribute_t1020461241 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m2985353781(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Runtime.InteropServices.InterfaceTypeAttribute::.ctor(System.Runtime.InteropServices.ComInterfaceType)
extern "C"  void InterfaceTypeAttribute__ctor_m2093508533 (InterfaceTypeAttribute_t465097163 * __this, int32_t ___interfaceType, const MethodInfo* method)
{
	{
		Attribute__ctor_m2985353781(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___interfaceType;
		__this->set_intType_0(L_0);
		return;
	}
}
// System.Void System.Runtime.InteropServices.Marshal::.cctor()
extern TypeInfo* Marshal_t3977632096_il2cpp_TypeInfo_var;
extern const uint32_t Marshal__cctor_m156400721_MetadataUsageId;
extern "C"  void Marshal__cctor_m156400721 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Marshal__cctor_m156400721_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B3_0 = 0;
	{
		((Marshal_t3977632096_StaticFields*)Marshal_t3977632096_il2cpp_TypeInfo_var->static_fields)->set_SystemMaxDBCSCharSize_0(2);
		OperatingSystem_t602923589 * L_0 = Environment_get_OSVersion_m2506362029(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_1 = OperatingSystem_get_Platform_m949444901(L_0, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)2))))
		{
			goto IL_001c;
		}
	}
	{
		G_B3_0 = 2;
		goto IL_001d;
	}

IL_001c:
	{
		G_B3_0 = 1;
	}

IL_001d:
	{
		((Marshal_t3977632096_StaticFields*)Marshal_t3977632096_il2cpp_TypeInfo_var->static_fields)->set_SystemDefaultCharSize_1(G_B3_0);
		return;
	}
}
// System.Void System.Runtime.InteropServices.Marshal::copy_to_unmanaged(System.Array,System.Int32,System.IntPtr,System.Int32)
extern "C"  void Marshal_copy_to_unmanaged_m1909563134 (Il2CppObject * __this /* static, unused */, Il2CppArray * ___source, int32_t ___startIndex, IntPtr_t ___destination, int32_t ___length, const MethodInfo* method)
{
	using namespace il2cpp::icalls;
	typedef void (*Marshal_copy_to_unmanaged_m1909563134_ftn) (Il2CppArray *, int32_t, IntPtr_t, int32_t);
	 ((Marshal_copy_to_unmanaged_m1909563134_ftn)mscorlib::System::Runtime::InteropServices::Marshal::copy_to_unmanaged) (___source, ___startIndex, ___destination, ___length);
}
// System.Void System.Runtime.InteropServices.Marshal::copy_from_unmanaged(System.IntPtr,System.Int32,System.Array,System.Int32)
extern "C"  void Marshal_copy_from_unmanaged_m1711077603 (Il2CppObject * __this /* static, unused */, IntPtr_t ___source, int32_t ___startIndex, Il2CppArray * ___destination, int32_t ___length, const MethodInfo* method)
{
	using namespace il2cpp::icalls;
	typedef void (*Marshal_copy_from_unmanaged_m1711077603_ftn) (IntPtr_t, int32_t, Il2CppArray *, int32_t);
	 ((Marshal_copy_from_unmanaged_m1711077603_ftn)mscorlib::System::Runtime::InteropServices::Marshal::copy_from_unmanaged) (___source, ___startIndex, ___destination, ___length);
}
// System.Void System.Runtime.InteropServices.Marshal::Copy(System.Byte[],System.Int32,System.IntPtr,System.Int32)
extern TypeInfo* Marshal_t3977632096_il2cpp_TypeInfo_var;
extern const uint32_t Marshal_Copy_m2999277726_MetadataUsageId;
extern "C"  void Marshal_Copy_m2999277726 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t58506160* ___source, int32_t ___startIndex, IntPtr_t ___destination, int32_t ___length, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Marshal_Copy_m2999277726_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ByteU5BU5D_t58506160* L_0 = ___source;
		int32_t L_1 = ___startIndex;
		IntPtr_t L_2 = ___destination;
		int32_t L_3 = ___length;
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t3977632096_il2cpp_TypeInfo_var);
		Marshal_copy_to_unmanaged_m1909563134(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Runtime.InteropServices.Marshal::Copy(System.IntPtr,System.Byte[],System.Int32,System.Int32)
extern TypeInfo* Marshal_t3977632096_il2cpp_TypeInfo_var;
extern const uint32_t Marshal_Copy_m1690250234_MetadataUsageId;
extern "C"  void Marshal_Copy_m1690250234 (Il2CppObject * __this /* static, unused */, IntPtr_t ___source, ByteU5BU5D_t58506160* ___destination, int32_t ___startIndex, int32_t ___length, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Marshal_Copy_m1690250234_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = ___source;
		int32_t L_1 = ___startIndex;
		ByteU5BU5D_t58506160* L_2 = ___destination;
		int32_t L_3 = ___length;
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t3977632096_il2cpp_TypeInfo_var);
		Marshal_copy_from_unmanaged_m1711077603(NULL /*static, unused*/, L_0, L_1, (Il2CppArray *)(Il2CppArray *)L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Runtime.InteropServices.Marshal::Copy(System.IntPtr,System.Char[],System.Int32,System.Int32)
extern TypeInfo* Marshal_t3977632096_il2cpp_TypeInfo_var;
extern const uint32_t Marshal_Copy_m1390445676_MetadataUsageId;
extern "C"  void Marshal_Copy_m1390445676 (Il2CppObject * __this /* static, unused */, IntPtr_t ___source, CharU5BU5D_t3416858730* ___destination, int32_t ___startIndex, int32_t ___length, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Marshal_Copy_m1390445676_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = ___source;
		int32_t L_1 = ___startIndex;
		CharU5BU5D_t3416858730* L_2 = ___destination;
		int32_t L_3 = ___length;
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t3977632096_il2cpp_TypeInfo_var);
		Marshal_copy_from_unmanaged_m1711077603(NULL /*static, unused*/, L_0, L_1, (Il2CppArray *)(Il2CppArray *)L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 System.Runtime.InteropServices.Marshal::GetLastWin32Error()
extern "C"  int32_t Marshal_GetLastWin32Error_m701978199 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	using namespace il2cpp::icalls;
	typedef int32_t (*Marshal_GetLastWin32Error_m701978199_ftn) ();
	return  ((Marshal_GetLastWin32Error_m701978199_ftn)mscorlib::System::Runtime::InteropServices::Marshal::GetLastWin32Error) ();
}
// System.Byte System.Runtime.InteropServices.Marshal::ReadByte(System.IntPtr,System.Int32)
extern "C"  uint8_t Marshal_ReadByte_m3705434677 (Il2CppObject * __this /* static, unused */, IntPtr_t ___ptr, int32_t ___ofs, const MethodInfo* method)
{
	using namespace il2cpp::icalls;
	typedef uint8_t (*Marshal_ReadByte_m3705434677_ftn) (IntPtr_t, int32_t);
	return  ((Marshal_ReadByte_m3705434677_ftn)mscorlib::System::Runtime::InteropServices::Marshal::ReadByte) (___ptr, ___ofs);
}
// System.Void System.Runtime.InteropServices.Marshal::WriteByte(System.IntPtr,System.Int32,System.Byte)
extern "C"  void Marshal_WriteByte_m4239949353 (Il2CppObject * __this /* static, unused */, IntPtr_t ___ptr, int32_t ___ofs, uint8_t ___val, const MethodInfo* method)
{
	using namespace il2cpp::icalls;
	typedef void (*Marshal_WriteByte_m4239949353_ftn) (IntPtr_t, int32_t, uint8_t);
	 ((Marshal_WriteByte_m4239949353_ftn)mscorlib::System::Runtime::InteropServices::Marshal::WriteByte) (___ptr, ___ofs, ___val);
}
// System.Void System.Runtime.InteropServices.MarshalAsAttribute::.ctor(System.Runtime.InteropServices.UnmanagedType)
extern "C"  void MarshalAsAttribute__ctor_m682499176 (MarshalAsAttribute_t1016163854 * __this, int32_t ___unmanagedType, const MethodInfo* method)
{
	{
		Attribute__ctor_m2985353781(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___unmanagedType;
		__this->set_utype_0(L_0);
		return;
	}
}
// System.Void System.Runtime.InteropServices.MarshalDirectiveException::.ctor()
extern Il2CppCodeGenString* _stringLiteral2537066041;
extern const uint32_t MarshalDirectiveException__ctor_m1090917820_MetadataUsageId;
extern "C"  void MarshalDirectiveException__ctor_m1090917820 (MarshalDirectiveException_t1317716672 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MarshalDirectiveException__ctor_m1090917820_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = Locale_GetText_m2389348044(NULL /*static, unused*/, _stringLiteral2537066041, /*hidden argument*/NULL);
		SystemException__ctor_m3697314481(__this, L_0, /*hidden argument*/NULL);
		Exception_set_HResult_m3566571225(__this, ((int32_t)-2146233035), /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Runtime.InteropServices.MarshalDirectiveException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void MarshalDirectiveException__ctor_m2323399933 (MarshalDirectiveException_t1317716672 * __this, SerializationInfo_t2995724695 * ___info, StreamingContext_t986364934  ___context, const MethodInfo* method)
{
	{
		SerializationInfo_t2995724695 * L_0 = ___info;
		StreamingContext_t986364934  L_1 = ___context;
		SystemException__ctor_m2083527090(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Runtime.InteropServices.OptionalAttribute::.ctor()
extern "C"  void OptionalAttribute__ctor_m1203139678 (OptionalAttribute_t4067831006 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m2985353781(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
