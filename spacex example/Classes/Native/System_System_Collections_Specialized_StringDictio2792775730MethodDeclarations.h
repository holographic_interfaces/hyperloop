﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Specialized.StringDictionary
struct StringDictionary_t2792775730;
// System.String
struct String_t;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"

// System.Void System.Collections.Specialized.StringDictionary::.ctor()
extern "C"  void StringDictionary__ctor_m3641162214 (StringDictionary_t2792775730 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Specialized.StringDictionary::set_Item(System.String,System.String)
extern "C"  void StringDictionary_set_Item_m402599440 (StringDictionary_t2792775730 * __this, String_t* ___key, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Specialized.StringDictionary::Add(System.String,System.String)
extern "C"  void StringDictionary_Add_m2876298361 (StringDictionary_t2792775730 * __this, String_t* ___key, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator System.Collections.Specialized.StringDictionary::GetEnumerator()
extern "C"  Il2CppObject * StringDictionary_GetEnumerator_m3815340904 (StringDictionary_t2792775730 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
