﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Camer
struct Camer_t64873628;

#include "codegen/il2cpp-codegen.h"

// System.Void Camer::.ctor()
extern "C"  void Camer__ctor_m1916359759 (Camer_t64873628 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Camer::Start()
extern "C"  void Camer_Start_m863497551 (Camer_t64873628 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Camer::Update()
extern "C"  void Camer_Update_m1004472478 (Camer_t64873628 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
