﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class OrientTest : MonoBehaviour {
	
	// Update is called once per frame
	void Update () {
        RectTransform rectT = GetComponent<RectTransform>();
        Text output = GetComponentInChildren<Text>();
        output.text = Input.gyro.gravity.ToString();
    }
}
