﻿using UnityEngine;
using System.Collections;

public class attentionCrosshairsUI : MonoBehaviour
{
	public Texture2D crosshairTexture;
	public Rect position;
	public bool OriginalOn = true;
	public RaycastHit hit;

	// Use this for initialization
	void Start ()
	{
		SetScreenCenter ();
	}

	void SetScreenCenter()
	{
		position = new Rect ((Screen.width - crosshairTexture.width) / 2, (Screen.height - crosshairTexture.height) / 2, crosshairTexture.width, crosshairTexture.height);
	}

	void OnGUI()
	{
		if(OriginalOn == true)
		{
			GUI.DrawTexture(position, crosshairTexture);
		}
	}

	void Update()
	{
		SetScreenCenter();
	}
}