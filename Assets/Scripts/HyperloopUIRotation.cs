﻿using UnityEngine;
using System.Collections;

public class HyperloopUIRotation : MonoBehaviour {
	public GameObject target;
	public Camera cam;

    void Start()
    {
        StartCoroutine(LateOrient());
    }

    IEnumerator LateOrient()
    {
        yield return new WaitForEndOfFrame();
        OrientHUD();
    }

	// Update is called once per frame
	void Update () {
		if (Input.GetMouseButtonDown (0)) {
            OrientHUD();
            DataInput.Reset();
        }
	}

    void OrientHUD()
    {
        target.transform.position = cam.ScreenToWorldPoint(new Vector3(Screen.width / 2, Screen.height / 2, Camera.main.nearClipPlane + 56));
        target.transform.rotation = cam.transform.rotation;
    }
}
