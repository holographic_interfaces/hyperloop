﻿using UnityEngine;
using System.Collections;

public class FaceCamera : MonoBehaviour {

    float min = 15;
    float max = 90;

    Vector3 initialScale;

    void Start()
    {
        initialScale = transform.localScale;
    }

	// Update is called once per frame
	void Update () {
        transform.rotation = Camera.main.transform.rotation;

        float dot = Mathf.Abs(Vector3.Dot(Camera.main.transform.position - this.transform.position, this.transform.forward));
        if (dot < min || dot > max)
        {
            transform.localScale = Vector3.zero;
            return;
        }

        float scaleFactor = (dot - min) / (max - min);

        transform.localScale = initialScale * (scaleFactor * 2);


    }
}
