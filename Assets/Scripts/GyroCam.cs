﻿using UnityEngine;
using System.Collections;

// iPhone gyroscope-controlled camera demo v0.3 8/8/11
// Perry Hoberman <hoberman@bway.net>
// Directions: Attach this script to main camera.
// Note: Unity Remote does not currently support gyroscope. 
public class GyroCam : MonoBehaviour
{
	private bool gyroBool;
	private Gyroscope gyro;
	private Quaternion rotFix = Quaternion.identity;
    GameObject camParent;

	public void Start() {

		var originalParent = transform.parent; // check if this transform has a parent
		camParent = new GameObject ("camParent"); // make a new parent
		camParent.transform.position = transform.position; // move the new parent to this transform position
		transform.SetParent(camParent.transform); // make this transform a child of the new parent
		camParent.transform.SetParent(originalParent); // make the new parent a child of the original parent

		gyroBool = SystemInfo.supportsGyroscope;

		if (gyroBool) {

			gyro = Input.gyro;
			gyro.enabled = true;

            SetRotation();
            //Screen.sleepTimeout = 0;
        } else {
			print("NO GYRO");
		}
	}

    void SetRotation()
    {
        if (Screen.orientation == ScreenOrientation.LandscapeLeft)
        {
            camParent.transform.eulerAngles = new Vector3(90, 90, 0);
        }
        else if (Screen.orientation == ScreenOrientation.Portrait)
        {
            camParent.transform.eulerAngles = new Vector3(90, 0, 270);
        }


         rotFix = new Quaternion(0, 0, 1f, 0);
    }

	public void Update () {
		if (gyroBool) {
            SetRotation();
            Quaternion camRot = gyro.attitude * rotFix;
			transform.localRotation = camRot;
		}
	}
}
