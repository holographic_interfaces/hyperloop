﻿using UnityEngine;
using System.Collections;
using System.Net.Sockets;
using System;
using System.Net;
using System.Text;
using System.Collections.Generic;

public class UDPClient : MonoBehaviour {

	//Client uses as receive udp client
	public int listenPort = 9100;
	UdpClient udpClient;
    UdpClient udpSendClient;

    // Use this for initialization
    void Start () {
        IPEndPoint listenEP = new IPEndPoint(IPAddress.Any, listenPort);
        udpClient = new UdpClient(listenEP);

        try
		{
			udpClient.BeginReceive(new AsyncCallback(recv), null);
		}
		catch(Exception e)
		{
			Debug.Log(e.ToString());
		}
	}

	//CallBack
	private void recv(IAsyncResult res)
	{
		IPEndPoint RemoteIpEndPoint = new IPEndPoint(IPAddress.Any, listenPort);
		byte[] received = udpClient.EndReceive(res, ref RemoteIpEndPoint);

        switch (ByteArrayParser.GetPacketType(received)) {
            case ByteArrayParser.PacketType.AccelerometerFullData:
                DisplayTestPacketData(received);
                break;
            case ByteArrayParser.PacketType.BatteryTemperatureFullData:

                break;

        }
        
		udpClient.BeginReceive(new AsyncCallback(recv), null);
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.F10))
            SendTestPacket();
	}

    void DisplayTestPacketData(byte[] data)
    {
        Vector3 linearAccel;
        Vector2 pitchAndRoll;
        if (ByteArrayParser.GetAverageAccelerationData(out linearAccel, out pitchAndRoll, data))
        {
            Debug.Log("Acceleration: " + linearAccel.ToString());
            Debug.Log("Pitch & Roll: " + pitchAndRoll.ToString());
        }
        else {
            Debug.Log("Invalid data!");
        }
    }

    void SendTestPacket()
    {
        udpSendClient = new UdpClient();
      
        udpSendClient.Connect("localhost", 555);
        try
        {
            List<Byte> sendBytes = new List<byte>();
            sendBytes.AddRange(System.BitConverter.GetBytes((Int32)0));
            sendBytes.AddRange(System.BitConverter.GetBytes((Int16)0x1001));
            sendBytes.AddRange(System.BitConverter.GetBytes((Int16)34));

            //fault flags
            sendBytes.AddRange(System.BitConverter.GetBytes((Int32)0));

            //RAW X Y Z
            sendBytes.AddRange(System.BitConverter.GetBytes((Int16)0));
            sendBytes.AddRange(System.BitConverter.GetBytes((Int16)0));
            sendBytes.AddRange(System.BitConverter.GetBytes((Int16)0));

            //X, Y Z
            sendBytes.AddRange(System.BitConverter.GetBytes((float)1));
            sendBytes.AddRange(System.BitConverter.GetBytes((float)2));
            sendBytes.AddRange(System.BitConverter.GetBytes((float)3));

            //Pitch, Roll
            sendBytes.AddRange(System.BitConverter.GetBytes((float)11));
            sendBytes.AddRange(System.BitConverter.GetBytes((float)22));

            IPEndPoint listenEP = new IPEndPoint(IPAddress.Loopback, listenPort);
            Debug.Log("Sending packet");
            udpSendClient.BeginSend(sendBytes.ToArray(), sendBytes.Count, listenEP, new AsyncCallback(sent), null);
            
        }
        catch (Exception e)
        {
            Console.WriteLine(e.ToString());
        }
    }

    private void sent(IAsyncResult res)
    {
        int count = udpSendClient.EndSend(res);
        Debug.Log("Packet Sent (" + count + " bytes)");
        udpSendClient.Close();
    }
}
