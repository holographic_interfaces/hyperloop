﻿using UnityEngine;
using System.Collections;

public static class ByteArrayParser {

    public enum PacketType
    {
        AccelerometerFullData = 0x1001,
        BatteryTemperatureFullData = 0x321,

    }
    static int int16Size = 2;
    static int int32Size = 4;
    static int floatSize = 4;

    static int sequenceOffset = 0;
    static int sequenceLength = int32Size;
    static int packetTypeOffset = sequenceOffset + sequenceLength;
    static int packetTypeLength = int16Size;
    static int payloadLengthOffset = packetTypeOffset + packetTypeLength;
    static int payloadLengthLength = int16Size;
    static int payloadOffset = payloadLengthOffset + payloadLengthLength;

    public static PacketType GetPacketType(byte[] data)
    {
        int packetType = System.BitConverter.ToInt16(data, packetTypeOffset);
        return (PacketType)packetType;
    }


    static int accelXOffset = 10 + payloadOffset; //10 bytes of fault flags, xRaw, yRaw, zRaw
    static int accelYOffset = accelXOffset + floatSize;
    static int accelZOffset = accelYOffset + floatSize;
    static int pitchOffset = accelZOffset + floatSize;
    static int rollOffset = pitchOffset + floatSize;

    //Source for composing a packet: https://github.com/rLoopTeam/eng-software-pod/blob/development/FIRMWARE/PROJECT_CODE/LCCM655__RLOOP__FCU_CORE/ACCELEROMETERS/fcu__accel__ethernet.c
    public static bool GetAverageAccelerationData(out Vector3 linearAcceleration, out Vector2 pitchAndRoll, byte[] data)
    {
        //[4 Sequence|2 Packet Type|2 Payload Length|{4 Fault Flags|2 XRaw|2 YRaw|2 ZRaw|4 XAccel|4 YAccel|4 ZAccel|4 Pitch|4 Roll}|2 CRC]
        //
        linearAcceleration = new Vector3();
        pitchAndRoll = new Vector2();

        if (GetPacketType(data) != PacketType.AccelerometerFullData)
            return false;

        int payloadLength = System.BitConverter.ToInt16(data, payloadLengthOffset);
        int sensorCount = (payloadLength - 4) / (30); //https://github.com/rLoopTeam/eng-software-pod/blob/development/FIRMWARE/PROJECT_CODE/LCCM655__RLOOP__FCU_CORE/ACCELEROMETERS/fcu__accel__ethernet.c#L60

        //Average all the sensors data together
        for (int i = 0; i < sensorCount; i++)
        {
            float x = System.BitConverter.ToSingle(data, accelXOffset);
            float y = System.BitConverter.ToSingle(data, accelYOffset);
            float z = System.BitConverter.ToSingle(data, accelZOffset);
            float pitch = System.BitConverter.ToSingle(data, pitchOffset);
            float roll = System.BitConverter.ToSingle(data, rollOffset);

            linearAcceleration = new Vector3(x + linearAcceleration.x, y + linearAcceleration.y, z + linearAcceleration.z);
            pitchAndRoll = new Vector2(pitch + pitchAndRoll.y, roll + pitchAndRoll.x);
        }
        linearAcceleration /= sensorCount;
        pitchAndRoll /= sensorCount;

        return true;
    }


    static int tempSensorCountOffset = 0 + payloadOffset;
    static int sensorTempsStartOffset = tempSensorCountOffset + 4; //4 bytes of temp sensor count, spare?

    //Source for composing a packet: https://github.com/rLoopTeam/eng-software-pod/blob/development/FIRMWARE/PROJECT_CODE/LCCM653__RLOOP__POWER_CORE/BATTERY_TEMP/power_core__battery_temp__ethernet.c
    public static bool GetAverageTempData(out float temp, byte[] data)
    {
        //[4 Sequence|2 Packet Type|2 Payload Length|2 Sensor Count|2 Spare|{4 Temp}|2 CRC]
        temp = 0;

        if (GetPacketType(data) != PacketType.BatteryTemperatureFullData)
            return false;

        int sensorCount = System.BitConverter.ToInt16(data, tempSensorCountOffset);

        //Average all the sensors data together
        for (int i = 0; i < sensorCount; i++)
        {
            temp += System.BitConverter.ToSingle(data, sensorTempsStartOffset + (i * int16Size));
            
        }
        temp /= sensorCount;

        return true;
    }
}
