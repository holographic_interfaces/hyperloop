﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;


public class liveVideo : MonoBehaviour {

	public RawImage rawimage;
	public int selectedDevice;
	public RectTransform imageParent;

	WebCamTexture webCamTexture;
	WebCamDevice[] devices;
	float timer;

	// Image rotation
	Vector3 rotationVector = new Vector3(0f, 0f, 0f);

	// Image uvRect
	Rect defaultRect = new Rect(0f, 0f, 1f, 1f);
	Rect fixedRect = new Rect(0f, 1f, 1f, -1f);

	// Image Parent's scale
	Vector3 defaultScale = new Vector3(1f, 1f, 1f);
	Vector3 fixedScale = new Vector3(-1f, 1f, 1f);

	void Start() {
		if (Application.HasUserAuthorization (UserAuthorization.WebCam | UserAuthorization.Microphone)) {
			devices = WebCamTexture.devices;
			
            //Iterate selected devices until 
			for(;selectedDevice < devices.Length - 1; selectedDevice++)
            {
                if (InitializeSelectedDevice())
                    break;
            }

		} else {
			GetAuthorization ();
			Debug.Log ("no authorization in start");
		}
	}

    bool InitializeSelectedDevice()
    {
        if (webCamTexture != null)
            webCamTexture.Stop();

        if (Application.isEditor)
            webCamTexture = new WebCamTexture(devices[selectedDevice].name);// must be like this for using webcam on unity remote
        else {
            webCamTexture = new WebCamTexture();

            // Set camera filter modes for a smoother looking image
            webCamTexture.filterMode = FilterMode.Trilinear;
            webCamTexture.deviceName = devices[selectedDevice].name;
        }

        rawimage.texture = webCamTexture;
        rawimage.material.mainTexture = webCamTexture;
        if (webCamTexture != null)
        {
            webCamTexture.Play();
            transform.rotation = Quaternion.AngleAxis(webCamTexture.videoRotationAngle, Vector3.up);
            return true;
        }
        else
        {
            return false;
        }
    }

    IEnumerator GetAuthorization(){
//		Debug.Log (Application.RequestUserAuthorization (UserAuthorization.WebCam | UserAuthorization.Microphone));
		yield return Application.RequestUserAuthorization(UserAuthorization.WebCam | UserAuthorization.Microphone);
	}

	// Update is called once per frame
	void Update () {
		if (webCamTexture != null) {
			float extraRotation = 0f;//correcting issue on iOS when camera feed is virtically mirrored

			#if UNITY_EDITOR
				//print ("UNITY_EDITOR");
				extraRotation = 180f;
			#endif
			// Rotate image to show correct orientation 
			rotationVector.z = -webCamTexture.videoRotationAngle + extraRotation;
			rawimage.rectTransform.localEulerAngles = rotationVector;

			// Unflip if vertically flipped
			rawimage.uvRect = 
				webCamTexture.videoVerticallyMirrored ? fixedRect : defaultRect;

			// Mirror front-facing camera's image horizontally to look more natural
			imageParent.localScale = 
				devices[selectedDevice].isFrontFacing ? fixedScale : defaultScale;

			#if UNITY_EDITOR
				imageParent.localScale = fixedScale;
			#endif
		}
		// Increment the timer after pressing F5 once, holding mouse down, 
		// or holding finger on screen for longer than 1 second then releasing
		if (Input.GetMouseButtonDown (0)) {
			timer = Time.time;
		} else if (Input.GetMouseButtonUp (0)) {
			timer = Time.time - timer;

			if (timer > 1) {
				changeWebCamDevice ();
			}
		} 
		else if (Input.GetKeyDown (KeyCode.F5)) {
			changeWebCamDevice();
		}
	}

	private void changeWebCamDevice() {
		devices = WebCamTexture.devices;
		selectedDevice++;
		if (selectedDevice > devices.Length - 1)
			selectedDevice = 0;

		InitializeSelectedDevice();
	}
}


