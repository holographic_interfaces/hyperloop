﻿using UnityEngine;
using System.Collections;

public class Cam : MonoBehaviour {

	//variables privadas
	public static WebCamDevice[] devices = WebCamTexture.devices;
	private static float VerticalViewAngle;
	private float mi;

	//variables publicas



	void Awake () {
		//mi = hva ();
		Playcam ();
	}

	// Use this for initialization
	void Start () {

	}

	// Update is called once per frame
	void Update () {
	}

	public static Texture Startcamera(){
//		if (webCameraTexture.isPlaying) {
//			webCameraTexture.Stop ();
//		}

		//WebCamDevice cam = WebCamDevice();
		WebCamDevice cam = devices [0];
		WebCamTexture webCameraTexture  =    new WebCamTexture(cam.name);
		webCameraTexture.deviceName  = cam.name;
		webCameraTexture.Play();
		return webCameraTexture;

	}
	//gnereacion de variables
	public static float hva ()
	{

		AndroidJavaClass cameraClass = new AndroidJavaClass ("android.hardware.Camera");
		int camID = 0;
		AndroidJavaObject camera = cameraClass.CallStatic<AndroidJavaObject> ("open", camID);

		AndroidJavaObject cameraParameters = camera.Call<AndroidJavaObject> ("getParameters");
		VerticalViewAngle = cameraParameters.Call<float>("getFocalLength");
		//Debug.Log ("miguel                     "+ VerticalViewAngle);
		//Debug.Log ("fernando                     "+ VerticalViewAngle);
		return VerticalViewAngle;

	}

	public void  Playcam () {
		GUITexture BackgroundTexture = gameObject.AddComponent<GUITexture>();
		BackgroundTexture.pixelInset = new Rect(0,0,Screen.width,Screen.height);
		BackgroundTexture.texture = Startcamera ();
	}
}