﻿using UnityEngine;
using System.Collections;

public abstract class ComponentBase : MonoBehaviour {

    void Start()
    {
        StartCoroutine(LateStart());
    }

    IEnumerator LateStart()
    {
        yield return new WaitForEndOfFrame();
        Initialize();
        //Everyone listens for a reset
        DataInput.instance.onReset += Reset;
    }

    public abstract void Initialize();

    public abstract void Reset();
}
