﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine.UI;

[AddComponentMenu("HUD Components/LogPanel")]
public class LogPanel : ComponentBase
{
    public int maxLogItems = 12;
    List<GameObject> currentLogItems = new List<GameObject>();
    public Transform logPanel;

    public override void Initialize()
    {
        DataInput.instance.onLogMessageAdded += OnLogMessageAdded;
    }

    public override void Reset()
    {
        foreach (GameObject go in currentLogItems)
            Destroy(go);
        currentLogItems.Clear();
    }

    void OnLogMessageAdded(string logMessage) {

        AddLogItem(logMessage);

        if (currentLogItems.Count > maxLogItems)
        {
            GameObject.Destroy(currentLogItems[0]);
            currentLogItems.RemoveAt(0);
        }
    }

    public void AddLogItem(string logMessage)
    {
        GameObject newLogMessage = GameObject.Instantiate<GameObject>(Resources.Load("Prefabs/LogItem") as GameObject);
        RectTransform logMessageTransform = newLogMessage.GetComponent<RectTransform>();
        newLogMessage.GetComponentInChildren<Text>().text = DateTime.Now.ToShortTimeString() + " : " + logMessage;

        //Weird stuff (bug maybe) requires arranging the log message before parenting it.
        logMessageTransform.rotation = logPanel.rotation;
        logMessageTransform.position = logPanel.position;
        logMessageTransform.SetParent(logPanel);

        logMessageTransform.localScale = Vector3.one;
        currentLogItems.Add(newLogMessage);
    }
}
