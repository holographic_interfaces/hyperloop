﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

[AddComponentMenu("HUD Components/Battery")]
public class Battery : ComponentBase
{

    public Text batteryText;
    public Image batteryImage;

    public Text powerUsageText;


    void OnBatteryChange(float value)
    {
        value = (float)Math.Round(value, 1);
        batteryText.text = value.ToString() + "%";
        batteryImage.fillAmount = value;
    }

    void OnPowerUsageChange(float value)
    {
        value = (float)Math.Round(value);
        powerUsageText.text = value.ToString();
    }
    
    public override void Initialize()
    {
        DataInput.instance.onBatteryChange += OnBatteryChange;
        DataInput.instance.onPowerUsageChange += OnPowerUsageChange;
    }

    public override void Reset()
    {

    }
}

