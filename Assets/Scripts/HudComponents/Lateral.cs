﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

[AddComponentMenu("HUD Components/Lateral")]
public class Lateral : ComponentBase
{

    public Text rightLateralText;
    public Image rightLateralImage;

    public Text leftLateralText;
    public Image leftLateralImage;
    public string formatting = "0.00";

    void OnLateralChange(float value)
    {
        value = (float)Math.Round(value, 2);
        float lateralPercent = Mathf.Abs(value) / DataInput.instance.maximumLateralAcceleration;
        if (value > 0)
        {
            if (rightLateralText != null)
                rightLateralText.text = value.ToString(formatting);

            if (rightLateralImage != null)
                rightLateralImage.fillAmount = lateralPercent;

            if (leftLateralText != null)
                leftLateralText.text = 0.ToString(formatting);

            if (leftLateralImage != null)
                leftLateralImage.fillAmount = 0;
        }
        else {
            if (rightLateralText != null && leftLateralText == null)
                rightLateralText.text = value.ToString(formatting);
            else if (rightLateralText != null)
                rightLateralText.text = 0.ToString(formatting);

            if (rightLateralImage != null)
                rightLateralImage.fillAmount = 0;

            if (leftLateralText != null)
                leftLateralText.text = (value * -1).ToString(formatting);

            if (leftLateralImage != null)
                leftLateralImage.fillAmount = lateralPercent;
        }

    }

    public override void Initialize()
    {
        DataInput.instance.onLateralChange += OnLateralChange;
    }

    public override void Reset()
    {

    }

}
