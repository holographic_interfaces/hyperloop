﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

[AddComponentMenu("HUD Components/Speed")]
public class Speed : ComponentBase
{
    public Text speedText;
	public Image speedImage;
    public Image parentGauge;
	public RectTransform speedGauge;
    GameObject markers;
    public int increment = 50; // incrementation of units
    public float textFillTime = 1f;
    public float gaugeFillTime = .5f;
    public float arcLength = 270;// how many degrees the arc should be
    public float numbersToHighlight = 200; // highlight every # unit
    public bool onlyShowSignificantNumbers = true;
    public float minDisplaySpeed = 0;
    public float maxDisplaySpeed = 1000;
    float maxFillPercent;

    void OnSpeedChange(float value) {
        
        float milesPerHour = value;
        float speedPercent = (milesPerHour / maxDisplaySpeed)* maxFillPercent;

        speedText.text = ((int)milesPerHour).ToString();
        speedImage.fillAmount = speedPercent;
    }

    public override void Initialize()
    {
        maxFillPercent = arcLength / 360f;
        StartCoroutine(CreateUnitText());
        StartCoroutine(FillParentGauge());
        DataInput.instance.onSpeedChange += OnSpeedChange;
    }

    public override void Reset()
    {
        StopAllCoroutines();
        GameObject.Destroy(markers);
        StartCoroutine(CreateUnitText());
        StartCoroutine(FillParentGauge());
    }

	/// <summary>
	/// Creates the circle.
	/// </summary>
	/// <returns>The circle.</returns>
	/// <param name="center">Center.</param>
	/// <param name="radius">Radius.</param>
	/// <param name="angle">Angle.</param>
	public Vector3 CreateCircle(Vector3 center, float radius, float angle) { 

		// create angle between 0 to 360 degrees 
		Vector3 pos; 
		pos.x = center.x + radius * Mathf.Sin(angle * Mathf.Deg2Rad); 
		pos.y = center.y + radius * Mathf.Cos(angle * Mathf.Deg2Rad); 
		pos.z = center.z;

		return pos; 
	}

    IEnumerator FillParentGauge()
    {
        float fillTimeStart = Time.time;
        parentGauge.fillAmount = 0;
        while (Time.time < fillTimeStart + gaugeFillTime)
        {
            yield return new WaitForEndOfFrame();
            parentGauge.fillAmount = (Time.time - fillTimeStart) / gaugeFillTime;
        }
    }

    /// <summary>
    /// Creates the unit text.
    /// </summary>
    IEnumerator CreateUnitText()
	{
        //Zero angle is at the top of the circle
        int objCount = (int)((maxDisplaySpeed - minDisplaySpeed) / increment);
        float angle = 180; // where to start angle at
		float dgreesPerMarker = arcLength / objCount; // number of degrees between each marker

		float rot = angle; // start the rotation of the first element to the initial angle
        Vector3 center = speedGauge.anchoredPosition;

        float timePerMarker = gaugeFillTime / objCount; //amount of time to spend creating each marker, for visual effect

        // create container object for units
        markers = new GameObject ("Gauge Markers");
		RectTransform markersRect = markers.AddComponent<RectTransform> ();
		markersRect.transform.SetParent (gameObject.transform);
		markersRect.transform.localPosition = new Vector3 (0, 0, 0);
		markersRect.transform.localScale = new Vector3 (1, 1, 0);
		markersRect.rotation = speedGauge.rotation;
		// loop through and create units for measure
		for (float i = angle, markerValue = 0; i < angle + arcLength + 1; i += dgreesPerMarker, markerValue += increment)
        {
            // create rectTransforms for unit text UI
            GameObject textObject = new GameObject (markerValue.ToString());
			textObject.transform.SetParent (markers.transform);
			RectTransform trans = textObject.AddComponent<RectTransform> ();
			Text text = textObject.AddComponent<Text> ();

            string markerText = markerValue.ToString();
            if (markerValue >= 1000) // reduce numbers greater than 1000 to simplified units (1000 becomes 1k)
                markerText = (markerValue / 1000f).ToString("0.0") + "k";

			if (markerValue == 0 || markerValue % numbersToHighlight == 0) {
				text.fontSize = 42;
				text.font = Resources.Load<Font> ("Fonts/Orbitron/orbitron-bold");
                text.text = markerText;
            } else {
				text.fontSize = 30;
				text.font = Resources.Load<Font> ("Fonts/Orbitron/orbitron-medium");
                if (onlyShowSignificantNumbers)
                {
                    text.text = "|";
                }
                else {
                    text.text = markerText;
                }

            }
            ContentSizeFitter sizeFitter = textObject.AddComponent<ContentSizeFitter>();
            sizeFitter.horizontalFit = ContentSizeFitter.FitMode.PreferredSize;
            sizeFitter.verticalFit = ContentSizeFitter.FitMode.PreferredSize;

            //get and set position of element around radius
            float radius = (speedGauge.rect.width / 2) + (text.fontSize / 2);


            Vector3 pos = CreateCircle(center, radius, i);
			trans.anchoredPosition = pos;

            // make the object face the center
            trans.localEulerAngles = new Vector3(0, 0, rot);
            trans.localScale = Vector3.one;

            rot -= dgreesPerMarker; // reverse the angle so we can point the bottom of the units at center

            yield return new WaitForSeconds(timePerMarker);
		}
	}
}
