﻿using UnityEngine;
using UnityEngine.UI;
using System;

[AddComponentMenu("HUD Components/RollPitchYaw")]
public class RollPitchYaw : ComponentBase
{

    public Text rollText;
    public Text pitchText;
    public Text yawText;

    void OnRollChange(float value)
    {
        rollText.text = value.ToString();
    }

    void OnPtichChange(float value)
    {
        pitchText.text = value.ToString();
    }

    void OnYawChange(float value)
    {
        yawText.text = value.ToString();
    }

    public override void Initialize()
    {
        DataInput.instance.onRollChange += OnRollChange;
        DataInput.instance.onPitchChange += OnPtichChange;
        DataInput.instance.onYawChange += OnYawChange;
    }

    public override void Reset()
    {

    }
}
