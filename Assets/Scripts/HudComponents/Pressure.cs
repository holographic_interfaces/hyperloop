﻿using UnityEngine;

[AddComponentMenu("HUD Components/Pressure")]
class Pressure : GraphGenerator
{
    void PressureLog(float value)
    {
        AddDataPoint(value);
    }

    public override void Initialize()
    {
        DataInput.instance.onPressureLog += PressureLog;
    }

    public override void Reset()
    {
        YData.Clear();
    }
}