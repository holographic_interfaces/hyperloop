﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

[AddComponentMenu("HUD Components/Acceleration")]
public class Acceleration : ComponentBase
{

    public Text positiveAccelerationText;
    public Image positiveAccelerationImage;

    public Text negativeAccelerationText;
    public Image negativeAccelerationImage;
    public string formatting = "0.00";

    void OnAccelerationChange(float value)
    {
        value = (float)Math.Round(value, 2);
        float accelerationPercent = Mathf.Abs(value) / DataInput.instance.maximumAcceleration;
        if (value > 0)
        {
            if(positiveAccelerationText != null)
                positiveAccelerationText.text = value.ToString(formatting);

            if(positiveAccelerationImage != null)
                positiveAccelerationImage.fillAmount = accelerationPercent;

            if (negativeAccelerationText != null)
                negativeAccelerationText.text = 0.ToString(formatting);

            if (negativeAccelerationImage != null)
                negativeAccelerationImage.fillAmount = 0;
        }
        else {
            if (positiveAccelerationText != null && negativeAccelerationText == null)
                positiveAccelerationText.text = value.ToString();
            else if (positiveAccelerationText != null)
                positiveAccelerationText.text = 0.ToString(formatting);

            if (positiveAccelerationImage != null)
                positiveAccelerationImage.fillAmount = 0;

            if (negativeAccelerationText != null)
                negativeAccelerationText.text = (value * -1).ToString(formatting);

            if (negativeAccelerationImage != null)
                negativeAccelerationImage.fillAmount = accelerationPercent;
        }
        
    }

    public override void Initialize()
    {
        DataInput.instance.onAccelerationChange += OnAccelerationChange;
    }

    public override void Reset()
    {
        
    }
}
