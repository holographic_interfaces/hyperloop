﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

[AddComponentMenu("HUD Components/Map")]
public class Map : ComponentBase
{
    public Text mapText;
 
    void OnMapValuesChange(string value)
    {
        mapText.text = value;
    }

    public override void Initialize()
    {
        DataInput.instance.onMapValuesChange += OnMapValuesChange;
    }

    public override void Reset()
    {

    }
}

