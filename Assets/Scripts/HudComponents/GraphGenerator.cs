﻿using UnityEngine;
using System.Collections;
using System.Linq;
using UnityEngine.UI;
using System.Collections.Generic;
using System;

[RequireComponent(typeof(LineRenderer))]
[RequireComponent(typeof(Image))]
public abstract class GraphGenerator : ComponentBase
{
    LineRenderer lr;
    public RectTransform parentRectTransform;
    int texWidth;
    int texHeight;
    int xPixelsBetweenPoints;
    public List<PointData> YData = new List<PointData>();
    public float min;
    public float max;
    public float lineWidth = .1f;
    public Color lineColor;
    public Color backgroundColor;
    public int secondsToShow = 5;
    public Text maxMarker;
    public RectTransform extents;

    public class PointData
    {
        public float time;
        public int value;

        public PointData(float time, int value)
        {
            this.time = time;
            this.value = value;
        }
    }

    protected void AddDataPoint(float value)
    {
        InitializeLineRenderer();
        float normalizedValue = (value - min) / (max - min);
        YData.Add(new PointData(Time.time, (int)(normalizedValue * texHeight)));

        float zeroTime = Time.time - secondsToShow - DataInput.tempAndPressureInterval;
        while (YData[0].time < zeroTime)
            YData.RemoveAt(0);
        UpdateTexture();

        if (extents != null)
        {
            extents.anchorMax = new Vector2(1, ((YData.Max(x => x.value) + 4) / (float)texHeight));
            extents.anchorMin = new Vector2(0, ((YData.Min(x => x.value) - 4) / (float)texHeight));
        }
    }

    void InitializeLineRenderer()
    {
        if (lr == null)
        {
            texWidth = (int)(parentRectTransform.rect.width);
            texHeight = (int)(parentRectTransform.rect.height);
            lr = GetComponent<LineRenderer>();
            xPixelsBetweenPoints = Mathf.Max((int)(texWidth / (secondsToShow / DataInput.tempAndPressureInterval)), 1);
            lr.material.color = lineColor;
            lr.SetColors(lineColor, lineColor);
            GetComponent<Image>().color = backgroundColor;
            lr.SetWidth(lineWidth, lineWidth);
            lr.useWorldSpace = false;

        }
    }

    public void UpdateTexture()
    {
        InitializeLineRenderer();
        if (maxMarker != null)
            maxMarker.text = "T+" + secondsToShow + " seconds";

        float zeroTime = Time.time - secondsToShow;
        float maxTime = Time.time;
        int index = 0;
        lr.SetVertexCount(YData.Count);
        for (int p = 0; p < YData.Count; p++)
        {
            int x1 = texWidth - (int)((YData[p].time - zeroTime) / (maxTime - zeroTime) * texWidth);
            if (x1 < 0)
                x1 = 0;

            lr.SetPosition(index++, new Vector3(x1 - texWidth/2, YData[p].value - texHeight/2, -.01f));
        }
    }
}
