﻿using UnityEngine;

[AddComponentMenu("HUD Components/Temp")]
class Temp : GraphGenerator
{

    void TempLog(float value)
    {
        AddDataPoint(value);
    }

    public override void Initialize()
    {
        DataInput.instance.onTempLog += TempLog;
    }

    public override void Reset()
    {
        YData.Clear();
    }
}
