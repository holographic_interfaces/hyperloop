﻿using UnityEngine;
using System.Collections;

public class InWorldCanvasScaler : MonoBehaviour
{
    public float pixelsPerWorldUnit = 100f;

    public void OnValidate()
    {
        float canvasWidth = GetComponent<RectTransform>().sizeDelta.x;
        float canvasHeight = GetComponent<RectTransform>().sizeDelta.y;

        float scale = Mathf.Min(pixelsPerWorldUnit / canvasWidth, pixelsPerWorldUnit / canvasHeight);


        this.transform.localScale = new Vector3(scale, scale, 1);
    }
}