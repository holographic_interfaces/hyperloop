﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;
using System.Collections.Generic;

public class DataInput : MonoBehaviour {
    public static DataInput instance;

    public delegate void UpdateFloatValue(float value);
    public delegate void UpdateStringValue(string value);
    public delegate void NoValue();

    public UpdateFloatValue onRollChange;
    public UpdateFloatValue onPitchChange;
    public UpdateFloatValue onYawChange;

    public UpdateFloatValue onTempLog;
    public UpdateFloatValue onPressureLog;

    public UpdateFloatValue onSpeedChange;
    public UpdateFloatValue onAccelerationChange;
    public UpdateFloatValue onLateralChange;
    public UpdateFloatValue onBatteryChange;
    public UpdateFloatValue onPowerUsageChange;
    public UpdateStringValue onMapValuesChange;
    public UpdateStringValue onLogMessageAdded;
    public NoValue onReset;

    public static float tempAndPressureInterval = .05f;
    public bool DEBUGMODE = false;

    [HideInInspector]
    public float metersPerSecondPerMPH = 0.44704f;

    [Range(-2, 2)]
    public float acceleration = .1f;
    float _acceleration;

    public float maxSpeed = 1000;
    
    public float maximumAcceleration = 2;
    public float maximumLateralAcceleration = .5f;

    [Range(0, 1000)]
    public float speed = 0;
    float _speed;

    [Range(0, 100)]
    public float batteryLevel = 100f;
    float _batteryLevel;

    [Range(0, 500)]
    public float power = 500f;
    float _power;

    float metersToGo = 1126540f;
    float _metersToGo;

    float secondsToGo = 60f;

    float timeScale = 10;


    public void Start()
    {
        instance = this;
        Reset();
        if(DEBUGMODE)
            Application.logMessageReceived += HandleLog;
    }

    void HandleLog(string logString, string stackTrace, LogType type)
    {
        onLogMessageAdded(logString);
    }

    public void Update()
    {
        if (_metersToGo > 0)
        {
            float accerlationVariance = (Mathf.PerlinNoise(Time.time / 100, 0) - .5f) / 1000;
            _acceleration += accerlationVariance;

            _speed += _acceleration * Time.deltaTime * timeScale;
            if (_speed >= maxSpeed)
            {
                _acceleration = 0;
                _speed = maxSpeed;
            }

            if (_speed < 0)
            {
                _acceleration = 0;
                _speed = 0;
            }

            _batteryLevel -= Mathf.Abs(_acceleration) * .001f;
            _power = Mathf.Abs(_acceleration) * 250;
            SetDistanceAndTime();
            SetSpeedValueMPH(_speed);
            
            SetAcceleration(_acceleration);
            SetBatteryValue(_batteryLevel);
            SetPowerUsage(_power);

            if(!DEBUGMODE)
                SimulateLogMessages();
        } else
        {
            SetSpeedValueMPH(0);
            SetAcceleration(0);
            SetPowerUsage(0);
            StopAllCoroutines();
        }
    }

    public static void Reset()
    {
        if (instance.onReset != null)
            instance.onReset();
        instance._Reset();
    }

    public void _Reset()
    {
        StopAllCoroutines();
        _acceleration = acceleration;
        _batteryLevel = batteryLevel;
        _metersToGo = metersToGo;
        _power = power;
        _speed = speed;
        StartCoroutine(TempAndPressure());
    }

    //Update Temp and Pressure separate from update loop
    IEnumerator TempAndPressure()
    {
        while (true)
        {
            float temp = (Mathf.PerlinNoise(Time.time / 5, 0) / 2) + .15f;
            float pressure = Mathf.PerlinNoise(0, Time.time / 10);

            if(onTempLog != null)
                onTempLog(temp);
            if(onPressureLog != null)
                onPressureLog(pressure);

            yield return new WaitForSeconds(tempAndPressureInterval);
        }
    }

    private List<string> logMessages = new List<string>()
    {
        "BTLE scanner Powered Off",
        "ker_open_file_for_direct_io(28)",
        "AppleCarmin::systemWakeCall - messageType = 0xE0000280",
        "en0: BSSID changed to 00:f7:6f:d1:e0:22",
        "vm_compressor_flush - starting",
        "vm_compressor_flush completed - took 11533 msecs",
        "hibernate_page_list_setall(preflight 1) start",
        "hibernate_page_listsetall time: 232 ms",
        "Pod#24 left Station 2",
        "Current speed of Pod#24 - 346 kmph",
        "WARNING -Temperature inside cabin #14 abnormal",
        "Initiating cooldown...",
        "Temperatures lowered to 23 Celsius"
    };

    float timeSinceLastMessage = 0f;
    float timeBetweenMessages = 1f;
    float randomVariance = 1f;
    int logPointer = 0;
    

    private void SimulateLogMessages()
    {
        timeSinceLastMessage += Time.deltaTime;
        if(timeSinceLastMessage > timeBetweenMessages + (UnityEngine.Random.Range(0,1) * randomVariance))
        {
            if (onLogMessageAdded != null)
                onLogMessageAdded(logMessages[logPointer++]);

            if (logPointer >= logMessages.Count)
                logPointer = 0;
            timeSinceLastMessage = 0;
        }
    }

    public void SetSpeedValueMPH(float value)
    {
        if (onSpeedChange != null)
            onSpeedChange(value);
    }

    public void SetAcceleration(float value)
    {
        if (onAccelerationChange != null)
            onAccelerationChange(value);

        float lateral = (Mathf.PerlinNoise(Time.time/10, 0) - .5f);
        if (onLateralChange != null)
            onLateralChange(lateral);
    }

    public void SetBatteryValue(float value)
    {
        if (onBatteryChange != null)
            onBatteryChange(value);
        //batteryValue.text = Mathf.Round(value).ToString() + "%";
        //batteryGauge.fillAmount = value / 100;
    }

    public void SetPowerUsage(float value)
    {
        if (onPowerUsageChange != null)
            onPowerUsageChange(value);

        //powerUsage.text = value.ToString();
    }

    public void SetDistanceAndTime()
    {
        _metersToGo -= _speed * Time.deltaTime * timeScale;
        secondsToGo = _metersToGo/_speed;
        if(onMapValuesChange != null)
            onMapValuesChange(Math.Round(secondsToGo / 60 / 60, 2) + " hr | " + Mathf.Round(_metersToGo / 1609.34f) + " mi");
    }

}
