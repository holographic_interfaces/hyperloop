using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
using SimpleJSON;
using UnityEngine.Networking;

public class GoogleMap : MonoBehaviour
{
	public enum MapType
	{
		RoadMap,
		Satellite,
		Terrain,
		Hybrid
	}
	public bool loadOnStart = true;
	public bool autoLocateCenter = true;
	public GoogleMapLocation centerLocation;
	public bool directionsMap = true;
	public GoogleMapLocation destinationLocation;
	public int zoom = 13;
	public MapType mapType;
	public string size = "512";
	public bool doubleResolution = false;
	public GoogleMapMarker[] markers;
	public GoogleMapPath[] paths;
	
	void Start() {
		Canvas.ForceUpdateCanvases ();
		if(loadOnStart) Refresh();	
	}
	
	public void Refresh() {
		if(autoLocateCenter && (markers.Length == 0 && paths.Length == 0)) {
			Debug.LogError("Auto Center will only work if paths or markers are used.");	
		}
		StartCoroutine(_Refresh());
	}

	private WWW getDirections()
	{
		var urlDirections = string.Empty;
		var qsDirections = string.Empty;

		urlDirections = "https://maps.googleapis.com/maps/api/directions/json";

		qsDirections += "key=AIzaSyCTVHonwmWvaQ8iU-Gy-OukT94xAePkvjc";
		if (centerLocation.address != "") {
			qsDirections += "&visible=" + WWW.UnEscapeURL (centerLocation.address);
			qsDirections += "&origin=" + WWW.UnEscapeURL (centerLocation.address);
		}
		else {
			qsDirections += "&visible=" + WWW.UnEscapeURL (string.Format ("{0},{1}", centerLocation.latitude, centerLocation.longitude));
			qsDirections += "&origin=" + WWW.UnEscapeURL (string.Format ("{0},{1}", centerLocation.latitude, centerLocation.longitude));
		}

		if (destinationLocation.address != "") {
			qsDirections += "&destination=" + WWW.UnEscapeURL (destinationLocation.address);
		} 
		else {
			qsDirections += "&destination=" + WWW.UnEscapeURL (string.Format ("{0},{1}", destinationLocation.latitude, destinationLocation.longitude));
		}

		var reqDirections = new WWW (urlDirections + "?" + qsDirections);

		return reqDirections;
	}

	private string getMap(string url, WWW reqDirections)
	{
		var qs = "";
		if (!autoLocateCenter) {
			if (!directionsMap) {
				if (centerLocation.address != "")
					qs += "center=" + WWW.UnEscapeURL (centerLocation.address);
				else {
					qs += "center=" + WWW.UnEscapeURL (string.Format ("{0},{1}", centerLocation.latitude, centerLocation.longitude));
				}
			} else {
				if (destinationLocation.address != "")
					qs += "center=" + WWW.UnEscapeURL (destinationLocation.address);
				else {
					qs += "center=" + WWW.UnEscapeURL (string.Format ("{0},{1}", destinationLocation.latitude, destinationLocation.longitude));
				}
			}

			if (!directionsMap) {
				qs += "&zoom=" + zoom.ToString ();
			}
		}

		string txt = reqDirections.text;
		var t = JSON.Parse(txt);
		var legs = t ["routes"][0] ["legs"][0];
		var steps = t ["routes"][0] ["legs"][0] ["steps"];

		paths = GetPaths (legs, steps);
		markers = GetMarkers (legs, steps);


		if (size.ToLower ().IndexOf ('x') != -1) {
			qs += "&size=" + WWW.UnEscapeURL (size);
		} else {
			qs += "&size=" + WWW.UnEscapeURL (string.Format ("{0}x{0}", size));
		}

		qs += "&scale=" + (doubleResolution ? "2" : "1");
		qs += "&maptype=" + mapType.ToString ().ToLower ();
		var usingSensor = false;
		#if UNITY_IPHONE
		usingSensor = Input.location.isEnabledByUser && Input.location.status == LocationServiceStatus.Running;
		#endif
		qs += "&sensor=" + (usingSensor ? "true" : "false");

		foreach (var i in markers) {
			qs += "&markers=" + string.Format ("size:{0}|color:{1}|label:{2}", i.size.ToString().ToLower (), i.color, i.label);
			foreach (var loc in i.locations) {
				if (loc.address != "" && loc.address != null)
					qs += "|" + WWW.UnEscapeURL (loc.address);
				else
					qs += "|" + WWW.UnEscapeURL (string.Format ("{0},{1}", loc.latitude, loc.longitude));
			}
		}

		foreach (var i in paths) {
			qs += "&path=" + string.Format ("weight:{0}|color:{1}", i.weight, i.color);
			if(i.fill) qs += "|fillcolor:" + i.fillColor;
			foreach (var loc in i.locations) {
				if (loc.address != "" && loc.address != null)
					qs += "|" + WWW.UnEscapeURL (loc.address);
				else
					qs += "|" + WWW.UnEscapeURL (string.Format ("{0},{1}", loc.latitude, loc.longitude));
			}
		}

		string req = url + "?" + qs;

		return req;
	}

	private GoogleMapPath[] GetPaths(JSONNode legs, JSONNode steps)
	{
		paths = new GoogleMapPath[1];
		paths [0] = new GoogleMapPath(){
			locations = new GoogleMapLocation[steps.Count],
			color = new GoogleMapColor()
		};
		paths [0].color = GoogleMapColor.red;

		for(var index = 0; index < steps.Count; index++)
		{
			paths [0].locations [index] = new GoogleMapLocation ();
			paths [0].locations[index].latitude = float.Parse(steps[index]["end_location"]["lat"].Value);
			paths [0].locations[index].longitude = float.Parse(steps[index]["end_location"]["lng"].Value);
		}

		paths [0].locations[steps.Count-1].latitude = float.Parse(legs["end_location"]["lat"].Value);
		paths [0].locations[steps.Count-1].longitude = float.Parse(legs["end_location"]["lng"].Value);


		return paths;
	}



	private GoogleMapMarker[] GetMarkers(JSONNode legs, JSONNode steps)
	{
		markers = new GoogleMapMarker[2];

		for (var index = 0; index < 2; index++) {
			var lab = "Start";
			if (index == 1) {
				lab = "End";
			}

			markers [index] = new GoogleMapMarker () {
				locations = new GoogleMapLocation[1],
				size = new GoogleMapMarker.GoogleMapMarkerSize (),
				color = new GoogleMapColor (),
				label = lab,

			};

			markers [index].color = GoogleMapColor.red;
			markers [index].size = GoogleMapMarker.GoogleMapMarkerSize.Mid;



			for (var locInt = 0; locInt < markers [index].locations.Length; locInt++) {
				markers [index].locations [locInt] = new GoogleMapLocation ();
			}
		}


		//starting marker
		markers [0].locations[0].latitude = float.Parse(legs ["start_location"]["lat"].Value);
		markers [0].locations[0].longitude = float.Parse(legs ["start_location"]["lng"].Value);

		//ending marker
		markers [1].locations[0].latitude = float.Parse(legs["end_location"]["lat"].Value);
		markers [1].locations[0].longitude = float.Parse(legs["end_location"]["lng"].Value);

		return markers;
	}
	
	IEnumerator _Refresh ()
	{
		string url = "https://maps.googleapis.com/maps/api/staticmap";
		WWW reqDirections = null;
		string req = null;

		if(directionsMap)
		{
			reqDirections = getDirections ();
			yield return reqDirections;
		}

		req = getMap (url, reqDirections);



		using (UnityWebRequest www = UnityWebRequestTexture.GetTexture (req)) {
			yield return www.Send();

			if(www.isNetworkError) {
				Debug.Log(www.error);
			}
			else {
				GetComponent<RawImage>().texture = DownloadHandlerTexture.GetContent(www);
			}
		}
			
		yield return req;
	}
}

public enum GoogleMapColor
{
	black,
	brown,
	green,
	purple,
	yellow,
	blue,
	gray,
	orange,
	red,
	white
}

[System.Serializable]
public class GoogleMapLocation
{
	public string address;
	public float latitude;
	public float longitude;
}

[System.Serializable]
public class GoogleMapMarker
{
	public enum GoogleMapMarkerSize
	{
		Tiny,
		Small,
		Mid
	}
	public GoogleMapMarkerSize size;
	public GoogleMapColor color;
	public string label;
	public GoogleMapLocation[] locations;
	
}

[System.Serializable]
public class GoogleMapPath
{
	public int weight = 5;
	public GoogleMapColor color;
	public bool fill = false;
	public GoogleMapColor fillColor;
	public GoogleMapLocation[] locations;	
}