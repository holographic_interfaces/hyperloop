﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

[RequireComponent(typeof(Text))]
public class FPS : MonoBehaviour {

    float avg;
    Text text;
	// Use this for initialization
	void Start () {
        this.text = GetComponent<Text>();
	}
	
	// Update is called once per frame
	void Update () {
        text.text = (Math.Round(1.0f / Time.smoothDeltaTime)).ToString();
    }
}
